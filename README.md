## The first steps

To create the symbolic link, you have to use the storage:link Artisan command:

`php artisan storage:link`


## VUE i18n

[Source](https://github.com/martinlindhe/laravel-vue-i18n-generator).

Command to generate the file: `php artisan vue-i18n:generate`

## IDE Helper

[Source](https://github.com/barryvdh/laravel-ide-helper)

Command to generate phpDoc for models: `php artisan ide-helper:models`

## Migrations and Seeds

Command: `php artisan migrate:fresh --seed`

