<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\User\Email;

use App\Models\User\Email;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\ApiTestTrait;
use Tests\TestCase;

class WeeklyNewsletterTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait, RefreshDatabase;

    protected string $mainPath = '/api/v1/authenticated/user/weekly_newsletter';


    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */

    /** @test */
    public function can_change_status_from_active_to_inactive()
    {
        $this->signIn();

        $email = Email::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'confirmed' => Email::STATUS_CONFIRMED,
                'get_delivery' => Email::STATUS_GET_DELIVERY
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => Email::STATUS_DO_NOT_GET_DELIVERY]
        );
        $this->response->assertStatus(200);

        $data = $this->getResponseData();

        $this->assertArrayHasKey('newStatus', $data);
        $this->assertEquals(Email::STATUS_DO_NOT_GET_DELIVERY, $data['newStatus']);
    }

    /** @test */
    public function can_change_status_from_inactive_to_active()
    {
        $this->signIn();

        $email = Email::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'confirmed' => Email::STATUS_CONFIRMED,
                'get_delivery' => Email::STATUS_DO_NOT_GET_DELIVERY
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => Email::STATUS_GET_DELIVERY]
        );
        $this->response->assertStatus(200);

        $data = $this->getResponseData();

        $this->assertArrayHasKey('newStatus', $data);
        $this->assertEquals(Email::STATUS_GET_DELIVERY, $data['newStatus']);
    }

    /** @test */
    public function can_not_change_to_the_same_status()
    {
        $this->signIn();

        $email = Email::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'confirmed' => Email::STATUS_CONFIRMED,
                'get_delivery' => Email::STATUS_DO_NOT_GET_DELIVERY
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => Email::STATUS_DO_NOT_GET_DELIVERY]
        );

        $this->response->assertStatus(400);

        $data = $this->getResponseData();

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('The same status', $data['message']);
    }

    /** @test */
    public function can_not_change_status_when_does_not_have_a_linked_email()
    {
        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => Email::STATUS_DO_NOT_GET_DELIVERY]
        );

        $this->response->assertStatus(400);

        $data = $this->getResponseData();

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User does not have a linked email', $data['message']);
    }


    /** @test */
    public function can_not_set_unavailable_status()
    {
        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => 'status']
        );
        $this->response->assertStatus(422);
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_set_new_email()
    {
        $this->response = $this->postJson(
            $this->mainPath,
            ['status' => Email::STATUS_GET_DELIVERY]
        );
        $this->assertResponseUnauthorized();
    }
}
