<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Models\Post;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\ApiTestTrait;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait;

    protected Post $post;

    protected string $message = 'simple text';

    protected string $mainPath;

    public function setUp(): void
    {
        parent::setUp();

        $this->post = Post::factory()->create();

        $this->mainPath = '/api/v1/authenticated/posts/' . $this->post->id . '/comments';
    }

    /**
     * All tests without 'an_unauthenticated_user_' work with authenticated user
     */

    /** @test */
    public function can_create_the_first_comment()
    {
        $this->signIn();

        $this->response = $this->postJson($this->mainPath, ['message' => $this->message]);
        $this->assertResponseCreated();

        $data = $this->getResponseData();

        $this->assertArrayHasKey('id', $data);
        $this->assertIsInt($data['id']);

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $data['id'],
                'user_id' => Auth::user()->id,
                'post_id' => $this->post->id,
                'message' => $this->message,
                'parent_id' => null,
                'status' => Post\Comment::STATUS_PUBLISHED,
            ]
        );
    }

    /** @test */
    public function can_create_a_reply_comment()
    {
        $this->signIn();

        // the first comment for the post
        $this->response = $this->postJson($this->mainPath, ['message' => $this->message]);
        $dataFirstComment = $this->getResponseData();

        $this->response = $this->postJson(
            $this->mainPath,
            ['message' => $this->message, 'parentId' => $dataFirstComment['id']]
        );
        $this->assertResponseCreated();

        $dataSecondComment = $this->getResponseData();

        $this->assertArrayHasKey('id', $dataSecondComment);
        $this->assertIsInt($dataSecondComment['id']);

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $dataSecondComment['id'],
                'user_id' => Auth::user()->id,
                'post_id' => $this->post->id,
                'message' => $this->message,
                'parent_id' => $dataFirstComment['id'],
                'status' => Post\Comment::STATUS_PUBLISHED,
            ]
        );
    }

    /** @test */
    public function can_not_create_comment_to_nonexistent_post()
    {
        $this->signIn();

        $this->response = $this->postJson('/api/v1/authenticated/posts/0/comments', ['message' => $this->message]);
        $this->response->assertStatus(404);
    }

    /** @test */
    public function can_not_create_a_reply_comment_to_nonexistent_post()
    {
        $this->signIn();

        $this->response = $this->postJson($this->mainPath, ['message' => $this->message]);
        $dataFirstComment = $this->getResponseData();

        $this->response = $this->postJson(
            $this->mainPath,
            ['message' => $this->message, 'parentId' => $dataFirstComment['id'] + 1]
        );
        $this->response->assertStatus(404);
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_add_comment()
    {
        $this->response = $this->postJson($this->mainPath, ['message' => $this->message]);
        $this->assertResponseUnauthorized();
    }
}
