<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\Post\Writing;

use App\Models\Post;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\ApiTestTrait;
use Tests\TestCase;

class PostStatusChangingTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait;

    protected string $mainPath = '/api/v1/authenticated/posts';


    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */

    /** @test */
    public function can_publish_own_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['author_id' => Auth::user()->id, 'status' => Post::STATUS_DRAFT]);

        $this->response = $this->patchJson($this->mainPath . '/' . $post->id . '/publish');
        $this->response->assertStatus(200);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('slug', $data);

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_POSTED]
        );
    }

    /** @test */
    public function can_unpublish_own_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['author_id' => Auth::user()->id, 'status' => Post::STATUS_POSTED]);

        $this->response = $this->patchJson($this->mainPath . '/' . $post->id . '/unpublish');
        $this->assertResponseWithoutBody();

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_DRAFT]
        );
    }

    /** @test */
    public function can_delete_own_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['author_id' => Auth::user()->id, 'status' => Post::STATUS_DELETED]);

        $this->response = $this->deleteJson($this->mainPath . '/' . $post->id);
        $this->assertResponseWithoutBody();

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_DELETED]
        );
    }

    /** @test */
    public function can_not_publish_someone_else_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['status' => Post::STATUS_DRAFT]);

        $this->response = $this->patchJson($this->mainPath . '/' . $post->id . '/publish');
        $this->response->assertStatus(403);

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_DRAFT]
        );
    }

    /** @test */
    public function can_not_unpublish_someone_else_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['status' => Post::STATUS_POSTED]);

        $this->response = $this->patchJson($this->mainPath . '/' . $post->id . '/unpublish');
        $this->response->assertStatus(403);

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_POSTED]
        );
    }

    /** @test */
    public function can_not_delete_someone_else_post()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['status' => Post::STATUS_DELETED]);

        $this->response = $this->deleteJson($this->mainPath . '/' . $post->id);
        $this->response->assertStatus(403);

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'status' => Post::STATUS_DELETED]
        );
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_publish_publish()
    {
        $this->response = $this->patchJson($this->mainPath . '/1/publish');
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_unpublish_post()
    {
        $this->response = $this->patchJson($this->mainPath . '/1/unpublish');
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_delete_post()
    {
        $this->response = $this->deleteJson($this->mainPath . '/1');
        $this->assertResponseUnauthorized();
    }
}
