<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\User\Email;

use App\Models\User;
use App\Models\User\Email;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\ApiTestTrait;
use Tests\TestCase;

class EmailTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait, RefreshDatabase;

    protected string $mainPath = '/api/v1/authenticated/user/email';


    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */

    /** @test */
    public function can_set_new_valid_email()
    {
        $this->signIn();
        $emailAddress = 'email@mail.com';

        $this->assertDatabaseMissing(
            'emails',
            [
                'email' => $emailAddress,
            ]
        );

        $this->assertDatabaseMissing(
            'email_confirmations',
            [
                'user_id' => Auth::user()->id,
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => $emailAddress]
        );
        $this->assertResponseWithoutBody();

        $this->assertDatabaseHas(
            'emails',
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_UNCONFIRMED,
            ]
        );

        $this->assertDatabaseHas(
            'email_confirmations',
            [
                'user_id' => Auth::user()->id,
            ]
        );
    }

    /** @test */
    public function can_set_valid_unconfirmed_email()
    {
        $this->signIn();
        $emailAddress = 'email@mail.com';

        /** @var Email $email */
        $email = Email::factory()->create(
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_UNCONFIRMED
            ]
        );
        $this->assertDatabaseHas(
            'emails',
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_UNCONFIRMED,
            ]
        );

        $this->assertDatabaseMissing(
            'email_confirmations',
            [
                'email_id' => $email->id,
                'user_id' => Auth::user()->id
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => $emailAddress]
        );
        $this->assertResponseWithoutBody();

        $this->assertDatabaseHas(
            'emails',
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_UNCONFIRMED,
            ]
        );
        $this->assertDatabaseHas(
            'email_confirmations',
            [
                'email_id' => $email->id,
                'user_id' => Auth::user()->id
            ]
        );
    }

    /** @test */
    public function can_set_valid_confirmed_email_without_linked_user()
    {
        $this->signIn();
        $emailAddress = 'email@mail.com';

        /** @var Email $email */
        $email = Email::factory()->create(
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_CONFIRMED,
            ]
        );
        $this->assertDatabaseHas(
            'emails',
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_CONFIRMED,
                'user_id' => null
            ]
        );

        $this->assertDatabaseMissing(
            'email_confirmations',
            [
                'email_id' => $email->id,
                'user_id' => Auth::user()->id
            ]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => $emailAddress]
        );
        $this->assertResponseWithoutBody();

        $this->assertDatabaseHas(
            'emails',
            [
                'email' => $emailAddress,
                'confirmed' => Email::STATUS_CONFIRMED,
                'user_id' => null
            ]
        );

        $this->assertDatabaseHas(
            'email_confirmations',
            [
                'email_id' => $email->id,
                'user_id' => Auth::user()->id
            ]
        );
    }

    /** @test */
    public function can_not_set_unavailable_email()
    {
        $this->signIn();
        $emailAddress = 'email@mail.com';

        $user = User::factory()->create();

        /** @var Email $email */
        $email = Email::factory()->create(['email' => $emailAddress, 'user_id' => $user->id]);
        $this->assertDatabaseHas(
            'emails',
            ['email' => $emailAddress, 'user_id' => $user->id, 'confirmed' => Email::STATUS_CONFIRMED]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => $emailAddress]
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Email is busy', $data['message']);
    }

    /** @test */
    public function can_not_set_invalid_email()
    {
        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => 'some_code']
        );
        $this->response->assertStatus(422);
    }

    /** @test */
    public function can_not_set_the_same_email()
    {
        $this->signIn();

        $emailAddress = 'email@mail.com';

        /** @var Email $email */
        $email = Email::factory()->create(['user_id' => Auth::user()->id, 'email' => $emailAddress]);
        $this->assertDatabaseHas(
            'emails',
            ['user_id' => Auth::user()->id, 'email' => $emailAddress, 'confirmed' => Email::STATUS_CONFIRMED]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => $emailAddress]
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Email is already linked', $data['message']);
    }

    /**
     * @test
     * @group futureFeature
     * TODO Make this test and functionality
     */
    public function can_not_set_email_more_than_three_times_per_day()
    {
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_set_new_email()
    {
        $this->response = $this->postJson(
            $this->mainPath,
            ['value' => 'some_email@some.com']
        );
        $this->assertResponseUnauthorized();
    }
}
