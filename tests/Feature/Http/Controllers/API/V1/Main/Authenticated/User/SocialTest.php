<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\User;

use App\Models\User\SocialAccount;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Socialite\Facades\Socialite;
use Mockery;
use Tests\ApiTestTrait;
use Tests\TestCase;

class SocialTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait;

    protected string $mainPath = '/api/v1/authenticated/user/socials';


    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */


    /*
     * FACEBOOK LINKING
     */

    /** @test */
    public function can_link_facebook_with_valid_code()
    {
        $providerId = 1234567890;
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->shouldReceive('getId')
            ->andReturn($providerId);

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with(SocialAccount::PROVIDER_NAME_FB)->andReturn($provider);

        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_FB, 'code' => 'some_code']
        );
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );
    }

    /** @test */
    public function can_not_link_facebook_with_invalid_code()
    {
        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_FB, 'code' => 'some_code']
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Error getting data from provider', $data['message']);
    }

    /** @test */
    public function can_not_link_facebook_second_time()
    {
        $providerId = 1234567890;
        $this->signIn();

        /** @var SocialAccount $socialAccount */
        $socialAccount = SocialAccount::factory()->create(
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );
        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_FB, 'code' => 'some_code']
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User is already linked with this provider', $data['message']);
    }

    /*
     * VKONTAKTE LINKING
     */

    /** @test */
    public function can_link_vkontakte_with_valid_code()
    {
        $providerId = 1234567890;
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->shouldReceive('getId')
            ->andReturn($providerId);

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('user')->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with(SocialAccount::PROVIDER_NAME_VK)->andReturn($provider);

        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_VK, 'code' => 'some_code']
        );
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_VK, 'provider_id' => $providerId]
        );
    }

    /** @test */
    public function can_not_link_vkontakte_with_invalid_code()
    {
        $this->signIn();

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_VK, 'code' => 'some_code']
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Error getting data from provider', $data['message']);
    }

    /** @test */
    public function can_not_link_vkontakte_second_time()
    {
        $providerId = 1234567890;
        $this->signIn();

        /** @var SocialAccount $socialAccount */
        $socialAccount = SocialAccount::factory()->create(
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_VK, 'provider_id' => $providerId]
        );
        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_VK, 'provider_id' => $providerId]
        );

        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_VK, 'code' => 'some_code']
        );
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User is already linked with this provider', $data['message']);
    }

    /*
     * UNLINKING
     */

    /** @test */
    public function can_remove_linked_provider()
    {
        $this->signIn();
        $providerId = 1234567890;

        /** @var SocialAccount $socialAccount */
        $socialAccount = SocialAccount::factory()->create(
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_VK, 'provider_id' => $providerId]
        );
        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_VK, 'provider_id' => $providerId]
        );

        /** @var SocialAccount $socialAccount */
        $socialAccount = SocialAccount::factory()->create(
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );
        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );

        $this->response = $this->deleteJson($this->mainPath, ['provider' => SocialAccount::PROVIDER_NAME_FB]);
        $this->assertResponseWithoutBody();

        $this->assertDatabaseMissing(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'deleted_at' => null]
        );
    }

    /** @test */
    public function can_not_remove_the_last_linked_provider()
    {
        $this->signIn();
        $providerId = 1234567890;

        /** @var SocialAccount $socialAccount */
        $socialAccount = SocialAccount::factory()->create(
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );
        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );

        $this->response = $this->deleteJson($this->mainPath, ['provider' => SocialAccount::PROVIDER_NAME_FB]);
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);

        $this->assertEquals('Can\'t unlink the last linked account', $data['message']);

        $this->assertDatabaseHas(
            'social_accounts',
            ['user_id' => Auth::user()->id, 'provider' => SocialAccount::PROVIDER_NAME_FB, 'provider_id' => $providerId]
        );
    }

    /** @test
     * @group only
     */
    public function can_not_remove_not_linked_provider()
    {
        $this->signIn();

        $this->response = $this->deleteJson($this->mainPath, ['provider' => SocialAccount::PROVIDER_NAME_FB]);
        $this->response->assertStatus(400);

        $data = json_decode($this->response->getContent(), true);

        $this->assertArrayHasKey('message', $data);

        $this->assertEquals('User is not linked with the provider', $data['message']);
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_link_anything()
    {
        $this->response = $this->postJson(
            $this->mainPath,
            ['provider' => SocialAccount::PROVIDER_NAME_FB, 'code' => 'some_code']
        );
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_unlink_anythin()
    {
        $this->response = $this->deleteJson($this->mainPath, ['provider' => SocialAccount::PROVIDER_NAME_FB]);
        $this->assertResponseUnauthorized();
    }
}
