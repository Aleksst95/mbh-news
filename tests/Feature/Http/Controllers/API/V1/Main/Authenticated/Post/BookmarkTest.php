<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Models\Post;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\ApiTestTrait;
use Tests\TestCase;

class BookmarkTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase, ApiTestTrait;

    protected string $mainPath = '/api/v1/authenticated/posts/bookmarks';

    /**
     * All tests without 'an_unauthenticated_user_' work with authenticated user
     */

    // TODO check the status of posts in the bookmark list

    /** @test */
    public function can_see_bookmarks()
    {
        $this->signIn();

        $this->response = $this->getJson($this->mainPath);
        $this->response->assertStatus(200);

        $response = json_decode($this->response->getContent());
        $this->assertCount(0, $response);
    }

    /** @test */
    public function can_add_published_article_to_bookmarks()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $this->response = $this->postJson($this->mainPath . '/' . $post->id);
        $this->assertResponseCreated();

        $this->assertDatabaseHas('bookmarks', ['user_id' => Auth::user()->id, 'post_id' => $post->id]);
    }

    /** @test */
    public function can_not_add_not_published_article_to_bookmarks()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['status' => Post::STATUS_DRAFT]);

        $this->response = $this->postJson($this->mainPath . '/' . $post->id);
        $this->response->assertStatus(422);
    }

    /** @test */
    public function can_delete_post_from_bookmarks()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $bookmark = Post\Bookmark::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id
            ]
        );

        $this->assertDatabaseHas('bookmarks', ['user_id' => Auth::user()->id, 'post_id' => $post->id]);


        $this->response = $this->deleteJson($this->mainPath . '/' . $post->id);
        $this->assertResponseWithoutBody();

        $this->assertDatabaseMissing('bookmarks', ['user_id' => Auth::user()->id, 'post_id' => $post->id]);
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_see_bookmarks()
    {
        $this->response = $this->getJson($this->mainPath);
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_add_bookmarks()
    {
        $this->response = $this->postJson($this->mainPath . '/1');
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_delete_bookmarks()
    {
        $this->response = $this->deleteJson($this->mainPath . '/1');
        $this->assertResponseUnauthorized();
    }
}
