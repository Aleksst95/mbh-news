<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\Post\Writing;

use App\Models\Image;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Tests\ApiTestTrait;
use Tests\TestCase;

class FileTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait;

    protected string $pathUploadFromDevice = '/api/v1/authenticated/posts/writing/file_from_device';
    protected string $pathUploadByUrl = '/api/v1/authenticated/posts/writing/file_by_url';


    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */

    /** @test */
    public function can_upload_a_file_from_device()
    {
        $this->signIn();

        Storage::fake('public');

        $this->response = $this->postJson(
            $this->pathUploadFromDevice,
            [
                'file' => $this->getFakeImage()
            ]
        );
        $this->assertResponseCreated();

        $data = $this->getResponseData();
        $this->assertIsArray($data);
        $this->assertArrayHasKey('url', $data);

        $this->response = $this->get($data['url']);
        $this->response->assertStatus(200);
    }

    /** @test */
    public function can_not_upload_a_file_from_device_with_wrong_extension()
    {
        $this->signIn();

        Storage::fake('public');

        $this->response = $this->postJson(
            $this->pathUploadFromDevice,
            [
                'file' => $this->getFakeFileWithWrongExtension()
            ]
        );
        $this->response->assertStatus(422);

        $data = $this->getResponseData();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('errors', $data);

        $this->assertIsArray($data['errors']);
        $this->assertArrayHasKey('file', $data['errors']);

        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']['file']);
        $this->assertCount(2, $data['errors']['file'][0]);

        $this->assertArrayHasKey('type', $data['errors']['file'][0]);
        $this->assertEquals('mimes', $data['errors']['file'][0]['type']);

        $this->assertArrayHasKey('value', $data['errors']['file'][0]);
        $this->assertEquals(implode(', ', Image::AVAILABLE_EXTENSIONS), $data['errors']['file'][0]['value']);
    }

    /** @test */
    public function can_not_upload_too_large_file_from_device()
    {
        $this->signIn();

        Storage::fake('public');

        $this->response = $this->postJson(
            $this->pathUploadFromDevice,
            [
                'file' => $this->getFakeTooLargeImage()
            ]
        );
        $this->response->assertStatus(422);

        $data = $this->getResponseData();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('errors', $data);

        $this->assertIsArray($data['errors']);
        $this->assertArrayHasKey('file', $data['errors']);

        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']['file']);
        $this->assertEquals('MAX_FILE_SIZE=' . Image::MAX_FILE_SIZE, $data['errors']['file'][0]);
    }

    /** @test */
    public function can_upload_a_file_by_url()
    {
        $this->signIn();

        Storage::fake('public');

        Http::fake(
            [
                'somedomain.com/*' => Http::response(
                    $this->getFakeImage()->get(),
                    200,
                    [
                        'Content-Type' => [
                            0 => 'image/jpeg'
                        ],
                        'Content-Length' => [
                            0 => '48821'
                        ]
                    ]
                )
            ]
        );

        $this->response = $this->postJson(
            $this->pathUploadByUrl,
            [
                'url' => 'https://somedomain.com/image.jpg'
            ]
        );
        $this->assertResponseCreated();

        $data = $this->getResponseData();
        $this->assertIsArray($data);
        $this->assertArrayHasKey('url', $data);

        $this->response = $this->get($data['url']);
        $this->response->assertStatus(200);
    }

    /** @test */
    public function can_not_upload_a_file_by_url_because_download_error()
    {
        $this->signIn();

        Storage::fake('public');

        Http::fake(
            [
                'somedomain.com/*' => Http::response(
                    $this->getFakeImage()->get(),
                    404,
                    [
                        'Content-Type' => [
                            0 => 'image/jpeg'
                        ],
                        'Content-Length' => [
                            0 => '48821'
                        ]
                    ]
                )
            ]
        );

        $this->response = $this->postJson(
            $this->pathUploadByUrl,
            [
                'url' => 'https://somedomain.com/image.jpg'
            ]
        );

        $this->response->assertStatus(422);

        $data = $this->getResponseData();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('errors', $data);

        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']);
        $this->assertCount(2, $data['errors'][0]);

        $this->assertArrayHasKey('type', $data['errors'][0]);
        $this->assertEquals('status', $data['errors'][0]['type']);

        $this->assertArrayHasKey('value', $data['errors'][0]);
        $this->assertEquals('', $data['errors'][0]['value']);
    }

    /** @test */
    public function can_not_upload_a_file_by_url_because_wrong_mime_type()
    {
        $this->signIn();

        Storage::fake('public');

        Http::fake(
            [
                'somedomain.com/*' => Http::response(
                    $this->getFakeImage()->get(),
                    200,
                    [
                        'Content-Type' => [
                            0 => 'plain/text'
                        ],
                        'Content-Length' => [
                            0 => '48821'
                        ]
                    ]
                )
            ]
        );

        $this->response = $this->postJson(
            $this->pathUploadByUrl,
            [
                'url' => 'https://somedomain.com/image.jpg'
            ]
        );

        $this->response->assertStatus(422);

        $data = $this->getResponseData();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('errors', $data);

        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']);
        $this->assertCount(2, $data['errors'][0]);

        $this->assertArrayHasKey('type', $data['errors'][0]);
        $this->assertEquals('mimes', $data['errors'][0]['type']);

        $this->assertArrayHasKey('value', $data['errors'][0]);
        $this->assertEquals(implode(', ', Image::AVAILABLE_EXTENSIONS), $data['errors'][0]['value']);
    }

    /** @test */
    public function can_not_upload_a_file_by_url_because_of_too_large_file()
    {
        $this->signIn();

        Storage::fake('public');

        Http::fake(
            [
                'somedomain.com/*' => Http::response(
                    $this->getFakeImage()->get(),
                    200,
                    [
                        'Content-Type' => [
                            0 => 'image/jpeg'
                        ],
                        'Content-Length' => [
                            0 => Image::MAX_FILE_SIZE * 1024 + 10
                        ]
                    ]
                )
            ]
        );

        $this->response = $this->postJson(
            $this->pathUploadByUrl,
            [
                'url' => 'https://somedomain.com/image.jpg'
            ]
        );

        $this->response->assertStatus(422);

        $data = $this->getResponseData();

        $this->assertIsArray($data);
        $this->assertArrayHasKey('errors', $data);

        $this->assertIsArray($data['errors']);
        $this->assertCount(1, $data['errors']);
        $this->assertCount(2, $data['errors'][0]);

        $this->assertArrayHasKey('type', $data['errors'][0]);
        $this->assertEquals('size', $data['errors'][0]['type']);

        $this->assertArrayHasKey('value', $data['errors'][0]);
        $this->assertEquals(Image::MAX_FILE_SIZE, $data['errors'][0]['value']);
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_upload_a_file_from_device()
    {
        $this->response = $this->postJson(
            $this->pathUploadFromDevice,
            ['file' => $this->getFakeImage()]
        );
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_pload_file_by_url()
    {
        $this->response = $this->postJson(
            $this->pathUploadByUrl,
            ['file' => $this->getFakeImage()]
        );
        $this->assertResponseUnauthorized();
    }

    private function getFakeImage(): File
    {
        return UploadedFile::fake()->image('random.jpg');
    }

    private function getFakeFileWithWrongExtension(): File
    {
        return UploadedFile::fake()->create('random.txt', 10, 'text/plain');
    }

    private function getFakeTooLargeImage(): File
    {
        return UploadedFile::fake()->create('random.jpg', 5000, 'image/jpeg');
    }
}
