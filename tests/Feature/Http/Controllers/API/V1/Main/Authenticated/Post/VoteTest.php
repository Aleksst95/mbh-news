<?php

namespace Tests\Feature\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Models\Post;
use App\Models\Post\Vote;
use Auth;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\ApiTestTrait;
use Tests\TestCase;

class VoteTest extends TestCase
{
    use DatabaseMigrations, ApiTestTrait;

    protected string $mainPath = '/api/v1/authenticated/posts/votes';

    /**
     * All tests without 'an_unauthenticated_user_' work with an authenticated user
     */

    /** @test */
    public function can_add_like_to_published_article()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_POSITIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_POSITIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 1]
        );
    }

    /** @test */
    public function can_add_dislike_to_published_article()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_NEGATIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_NEGATIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 1]
        );
    }

    /** @test */
    public function can_change_dislike_to_like()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_NEGATIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_NEGATIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 1]
        );


        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_POSITIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_POSITIVE]
        );
        $this->assertDatabaseMissing(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_NEGATIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 1]
        );
        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 0]
        );
    }

    /** @test */
    public function can_change_like_to_dislike()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create();

        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_POSITIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_POSITIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 1]
        );


        $this->response = $this->postJson($this->mainPath . '/' . $post->id, ['type' => Vote::TYPE_NEGATIVE]);
        $this->assertResponseCreated();

        $this->assertDatabaseHas(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_NEGATIVE]
        );
        $this->assertDatabaseMissing(
            'post_votes',
            ['user_id' => Auth::user()->id, 'post_id' => $post->id, 'type' => Vote::TYPE_POSITIVE]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 0]
        );
        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 1]
        );
    }

    /** @test */
    public function can_not_add_vote_to_not_published_article()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['status' => Post::STATUS_DRAFT]);

        $this->response = $this->postJson($this->mainPath . '/' . $post->id);
        $this->response->assertStatus(422);
    }

    /** @test */
    public function can_delete_like()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['count_positive_votes' => 1]);

        $vote = Post\Vote::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id,
                'type' => Vote::TYPE_POSITIVE
            ]
        );

        $this->assertDatabaseHas(
            'post_votes',
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id,
                'type' => Vote::TYPE_POSITIVE
            ]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 1]
        );


        $this->response = $this->deleteJson($this->mainPath . '/' . $post->id);
        $this->assertResponseWithoutBody();

        $this->assertDatabaseMissing(
            'post_votes',
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id
            ]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_positive_votes' => 0]
        );
    }

    /** @test */
    public function can_delete_dislike()
    {
        $this->signIn();

        /** @var Post $post */
        $post = Post::factory()->create(['count_negative_votes' => 1]);

        $vote = Post\Vote::factory()->create(
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id,
                'type' => Vote::TYPE_NEGATIVE
            ]
        );

        $this->assertDatabaseHas(
            'post_votes',
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id,
                'type' => Vote::TYPE_NEGATIVE
            ]
        );

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 1]
        );


        $this->response = $this->deleteJson($this->mainPath . '/' . $post->id);
        $this->assertResponseWithoutBody();

        $this->assertDatabaseMissing('post_votes', ['user_id' => Auth::user()->id, 'post_id' => $post->id]);

        $this->assertDatabaseHas(
            'posts',
            ['id' => $post->id, 'count_negative_votes' => 0]
        );
    }

    /*
     * Tests to check the response to an unauthenticated user
     */

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_add_vote()
    {
        $this->response = $this->postJson($this->mainPath . '/1');
        $this->assertResponseUnauthorized();
    }

    /**
     * @test
     * @group accessDeniedToUnauthenticatedUser
     */
    public function an_unauthenticated_user_can_not_delete_vote()
    {
        $this->response = $this->deleteJson($this->mainPath . '/1');
        $this->assertResponseUnauthorized();
    }
}
