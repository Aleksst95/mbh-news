<?php

namespace Tests\Feature\Servicies\Post\Comment;

use App\Exceptions\NotFoundException;
use App\Models\Post;
use App\Models\User;
use App\Services;
use Database\Factories\Post\CommentFactory;
use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use DatabaseMigrations;

    private Services\Post\CommentService $service;
    private User $user;
    private Post $post;
    private string $message = 'simple text';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Services\Post\CommentService::class);
        $this->user = User::factory()->create();
        $this->post = Post::factory()->create();
        CommentFactory::resetNodeId();
    }


    /** @test */
    public function add_the_first_comment()
    {
        $comment = $this->service->addCommentWithoutParent($this->user->id, $this->post->id, $this->message);

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $comment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => null,
                'message' => $this->message,
                'node_id' => 1,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 2,
                'status' => Post\Comment::STATUS_PUBLISHED,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 1,
            ]
        );
    }

    /** @test */
    public function add_a_new_comment_which_is_not_the_first()
    {
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id
            ]
        );
        $this->post->increment('count_comments');

        $newComment = $this->service->addCommentWithoutParent($this->user->id, $this->post->id, $this->message);

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => null,
                'message' => $this->message,
                'node_id' => 2,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 2,
                'status' => Post\Comment::STATUS_PUBLISHED,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 2,
            ]
        );
    }

    /** @test */
    public function add_a_reply_comment()
    {
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id
            ]
        );

        $this->post->increment('count_comments');

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            $this->message,
            $firstComment->id
        );

        // Check the new comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'message' => $this->message,
                'node_id' => 1,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 3,
            ]
        );

        // Check the old comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $firstComment->id,
                'post_id' => $this->post->id,
                'node_id' => 1,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 4,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 2,
            ]
        );
    }

    /** @test */
    public function add_a_third_level_reply_comment()
    {
        $nodeId = 1;
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'right_key' => 4,
                'node_id' => $nodeId
            ]
        );
        $this->post->increment('count_comments');

        $secondComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'left_key' => 2,
                'right_key' => 3,
                'node_id' => $nodeId,
                'level' => 2,
            ]
        );
        $this->post->increment('count_comments');

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            $this->message,
            $secondComment->id
        );

        // Check the new comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $secondComment->id,
                'message' => $this->message,
                'node_id' => $nodeId,
                'level' => 3,
                'left_key' => 3,
                'right_key' => 4,
            ]
        );

        // Check the first comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $firstComment->id,
                'post_id' => $this->post->id,
                'node_id' => $nodeId,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 6,
            ]
        );

        // Check the second comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $secondComment->id,
                'post_id' => $this->post->id,
                'node_id' => $nodeId,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 5,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 3,
            ]
        );
    }

    /** @test */
    public function add_a_second_level_reply_comment_when_the_third_level_reply_comment_already_exists()
    {
        $nodeId = 1;
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'right_key' => 6,
                'node_id' => $nodeId
            ]
        );
        $this->post->increment('count_comments');

        $secondComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'left_key' => 2,
                'right_key' => 5,
                'node_id' => $nodeId,
                'level' => 2,
            ]
        );
        $this->post->increment('count_comments');

        $thirdComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'parent_id' => $secondComment->id,
                'left_key' => 3,
                'right_key' => 4,
                'node_id' => $nodeId,
                'level' => 3,
            ]
        );
        $this->post->increment('count_comments');

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            $this->message,
            $firstComment->id
        );

        // Check the new comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'message' => $this->message,
                'node_id' => $nodeId,
                'level' => 2,
                'left_key' => 6,
                'right_key' => 7,
            ]
        );

        // Check the first comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $firstComment->id,
                'post_id' => $this->post->id,
                'node_id' => $nodeId,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 8,
            ]
        );

        // Check the second comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $secondComment->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'node_id' => $nodeId,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 5,
            ]
        );

        // Check the third comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $thirdComment->id,
                'post_id' => $this->post->id,
                'parent_id' => $secondComment->id,
                'node_id' => $nodeId,
                'level' => 3,
                'left_key' => 3,
                'right_key' => 4,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 4,
            ]
        );
    }

    /**
     * Checks the left and right keys of the comments.
     * A user tries to add a reply comment to the first created
     * reply comment but the second created reply comment has the same level like previous
     *
     * @test
     */
    public function add_a_third_level_reply_comment_when_two_second_level_reply_comment_already_exist()
    {
        $nodeId = 1;
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'right_key' => 6,
                'node_id' => $nodeId
            ]
        );
        $this->post->increment('count_comments');

        $secondComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'left_key' => 2,
                'right_key' => 3,
                'node_id' => $nodeId,
                'level' => 2,
            ]
        );
        $this->post->increment('count_comments');

        $thirdComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'left_key' => 4,
                'right_key' => 5,
                'node_id' => $nodeId,
                'level' => 2,
            ]
        );
        $this->post->increment('count_comments');

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            $this->message,
            $secondComment->id
        );

        // Check the new comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $secondComment->id,
                'message' => $this->message,
                'node_id' => $nodeId,
                'level' => 3,
                'left_key' => 3,
                'right_key' => 4,
            ]
        );

        // Check the first comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $firstComment->id,
                'post_id' => $this->post->id,
                'node_id' => $nodeId,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 8,
            ]
        );

        // Check the second comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $secondComment->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'node_id' => $nodeId,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 5,
            ]
        );

        // Check the third comment
        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $thirdComment->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'node_id' => $nodeId,
                'level' => 2,
                'left_key' => 6,
                'right_key' => 7,
            ]
        );

        // Check incrementing
        $this->assertDatabaseHas(
            'posts',
            [
                'id' => $this->post->id,
                'count_comments' => 4,
            ]
        );
    }

    /** @test */
    public function trim_comment_message_without_parent()
    {
        $comment = $this->service->addCommentWithoutParent(
            $this->user->id,
            $this->post->id,
            ' ' . $this->message . ' '
        );

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $comment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => null,
                'message' => $this->message,
                'node_id' => 1,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 2,
                'status' => Post\Comment::STATUS_PUBLISHED,
            ]
        );
    }

    /** @test */
    public function trim_comment_message_with_parent()
    {
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id
            ]
        );

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            ' ' . $this->message . ' ',
            $firstComment->id
        );

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'message' => $this->message,
                'node_id' => 1,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 3,
            ]
        );
    }

    /** @test */
    public function strip_tags_from_the_comment_message_without_parent()
    {
        $newComment = $this->service->addCommentWithoutParent(
            $this->user->id,
            $this->post->id,
            $this->message . ' <script>s</script><p>p</p><br />'
        );

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => null,
                'message' => $this->message . ' sp',
                'node_id' => 1,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 2,
            ]
        );
    }

    /** @test */
    public function strip_tags_from_the_reply_comment_message()
    {
        $firstComment = Post\Comment::factory()->create(
            [
                'post_id' => $this->post->id
            ]
        );

        $newComment = $this->service->addCommentWithParent(
            $this->user->id,
            $this->post->id,
            $this->message . ' <script>s</script><p>p</p><br />',
            $firstComment->id
        );

        $this->assertDatabaseHas(
            'post_comments',
            [
                'id' => $newComment->id,
                'user_id' => $this->user->id,
                'post_id' => $this->post->id,
                'parent_id' => $firstComment->id,
                'message' => $this->message . ' sp',
                'node_id' => 1,
                'level' => 2,
                'left_key' => 2,
                'right_key' => 3,
            ]
        );
    }

    /** @test */
    public function exception_for_a_comment_without_parent_and_with_only_spaces_in_the_message()
    {
        $exceptionIsHappened = false;
        try {
            $comment = $this->service->addCommentWithoutParent($this->user->id, $this->post->id, '   &nbsp;');
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Message is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function exception_for_a_reply_comment_with_only_spaces_in_the_message()
    {
        $exceptionIsHappened = false;
        try {
            $firstComment = Post\Comment::factory()->create(
                [
                    'post_id' => $this->post->id
                ]
            );

            $newComment = $this->service->addCommentWithParent(
                $this->user->id,
                $this->post->id,
                '   &nbsp;',
                $firstComment->id
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Message is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function exception_for_nonexistent_post_when_a_user_try_to_add_a_comment_without_parent()
    {
        $exceptionIsHappened = false;
        try {
            $comment = $this->service->addCommentWithoutParent($this->user->id, 0, $this->message);
        } catch (NotFoundException $e) {
            $exceptionIsHappened = true;
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function exception_for_nonexistent_post_when_a_user_try_to_add_a_reply_comment()
    {
        $exceptionIsHappened = false;
        try {
            $comment = $this->service->addCommentWithParent($this->user->id, $this->post->id, $this->message, 0);
        } catch (NotFoundException $e) {
            $exceptionIsHappened = true;
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

}
