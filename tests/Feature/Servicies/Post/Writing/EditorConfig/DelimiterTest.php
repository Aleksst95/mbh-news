<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Tests\TestCase;

class DelimiterTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'delimiter',
                'data' => []
            ]
        ]
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }

    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            json_encode($this->data),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertEmpty($blocks[0]['data']);
    }


    /** @test */
    public function wrong_data()
    {
        $data = $this->data;
        $data['blocks'][0]['data'] = ['marvel' => 'is better than DC'];

        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                json_encode($data),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Found extra param `marvel`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }
}
