<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Tests\TestCase;

class ListTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'list',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string Text of the first item of the list
     *
     *  This line contains all available tags except the 'br' tag
     */
    private string $textFirst = 'text, <a href="#href">link</a>, <i>i</i>, <b>b</b><br><mark>m</mark>';

    /**
     * @var string Text of the second item of the list
     */
    private string $textSecond = 'just text';

    /**
     * @var string
     */
    private string $style = 'ordered';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(2, $blocks[0]['data']);

        $this->assertArrayHasKey('style', $blocks[0]['data']);
        $this->assertArrayHasKey('items', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['style']);
        $this->assertIsArray($blocks[0]['data']['items']);

        // from editorJs we get <br>, but from HTMLPurifier we get <br /> instead
        $this->assertEquals(str_replace('<br>', '<br />', $this->textFirst), $blocks[0]['data']['items'][0]);
    }

    /** @test */
    public function without_items()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutItems(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `items`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function empty_text_in_item_of_single_items_array()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithCustomTextInItemOfSingleItemsArray(''),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('List is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function text_with_spaces_only_in_item_of_single_items_array()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithCustomTextInItemOfSingleItemsArray('  '),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('List is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function empty_text_in_first_item_of_multiple_items_array()
    {
        $this->textFirst = '';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks[0]['data']['items']);
    }

    /** @test */
    public function text_with_spaces_only_in_first_item_of_multiple_items_array()
    {
        $this->textFirst = '  ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks[0]['data']['items']);
    }

    /** @test */
    public function text_with_br_tags_only_in_item_of_single_items_array()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithCustomTextInItemOfSingleItemsArray('<br><br>'),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('List is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function text_with_br_tags_only_in_first_item_of_multiple_items_array()
    {
        $this->textFirst = '<br> <br>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks[0]['data']['items']);
    }

    /** @test */
    public function text_with_empty_tags_only_in_item_of_single_items_array()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithCustomTextInItemOfSingleItemsArray('<i> </i>'),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('List is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function text_with_br_empty_only_in_first_item_of_multiple_items_array()
    {
        $this->textFirst = '<i> <br> </i>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks[0]['data']['items']);
    }

    /** @test */
    public function text_with_spaces_only_in_all_items_of_multiple_items_array()
    {
        $this->textFirst = '  ';
        $this->textSecond = '  ';

        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('List is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_item()
    {
        $this->textFirst = 'Text with<script>wrong tag</script> <span>i</span>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with i', $blocks[0]['data']['items'][0]);
    }

    /** @test */
    public function trim_spaces_from_text()
    {
        $text = 'text';
        $this->textFirst = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['items'][0]);

        // spaces as '&nbsp;'
        $this->textFirst = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['items'][0]);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_text()
    {
        $text = 'text';
        $this->textFirst = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['items'][0]);

        // spaces as '&nbsp;'
        $this->textFirst = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['items'][0]);
    }

    /** @test */
    public function without_style()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutStyle(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `style`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_style()
    {
        $exceptionIsHappened = false;
        $this->style = 'Spider Man';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals(
                'Option \'style\' with value `Spider Man` has invalid value. Check canBeOnly param.',
                $e->getMessage()
            );
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    private function getDataWithoutItems(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['items']);

        return json_encode($data);
    }

    private function getDataWithoutStyle(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['style']);

        return json_encode($data);
    }

    private function getDataWithCustomTextInItemOfSingleItemsArray(string $text): string
    {
        $data = json_decode($this->getData(), true);

        $data['blocks'][0]['data']['items'] = [$text];

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'style' => $this->style,
            'items' => [$this->textFirst, $this->textSecond],
        ];

        return json_encode($data);
    }
}
