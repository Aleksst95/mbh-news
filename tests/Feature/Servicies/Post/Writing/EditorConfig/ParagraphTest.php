<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Tests\TestCase;

class ParagraphTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'paragraph',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $text = 'text with <a href="#href">a</a>, <b>b</b>, <i>i</i>, <mark>m</mark>';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }

// TODO add tests from ListTest.php
    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(1, $blocks[0]['data']);

        $this->assertArrayHasKey('text', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['text']);

        $this->assertEquals($this->text, $blocks[0]['data']['text']);
    }

    /** @test */
    public function empty_text()
    {
        $exceptionIsHappened = false;
        $this->text = '';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Paragraph is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_text()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutText(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `text`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function text_with_spaces_only()
    {
        $exceptionIsHappened = false;
        $this->text = ' ';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Paragraph is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_text()
    {
        $this->text = 'Text with<script>wrong tag</script>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with', $blocks[0]['data']['text']);
    }

    /** @test */
    public function trim_spaces_from_text()
    {
        $text = 'text';
        $this->text = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->text = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_text()
    {
        $text = 'text';
        $this->text = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->text = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);
    }

    private function getDataWithoutText(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['text']);

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'text' => $this->text
        ];

        return json_encode($data);
    }
}
