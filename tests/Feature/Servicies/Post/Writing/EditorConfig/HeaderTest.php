<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use EditorJS\EditorJSException;
use Exception;
use Illuminate\Support\Str;
use Tests\TestCase;

class HeaderTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'header',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $text = 'text with';

    /**
     * @var string This line contains all available characters
     */
    private string $anchor = '_anchor-1';

    private int $level = 2;

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(3, $blocks[0]['data']);

        $this->assertArrayHasKey('text', $blocks[0]['data']);
        $this->assertArrayHasKey('level', $blocks[0]['data']);
        $this->assertArrayHasKey('anchor', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['text']);
        $this->assertIsString($blocks[0]['data']['anchor']);
        $this->assertIsInt($blocks[0]['data']['level']);

        $this->assertEquals($this->text, $blocks[0]['data']['text']);
    }

    /** @test */
    public function empty_text()
    {
        $exceptionIsHappened = false;
        $this->text = '';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Header is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function text_with_spaces_only()
    {
        $exceptionIsHappened = false;
        $this->text = ' ';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Header is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_text()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutText(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `text`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_text()
    {
        $this->text = 'Text with<script>wrong tag</script> <a href="#href">link</a> <span>span</span>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with link span', $blocks[0]['data']['text']);
    }

    /** @test */
    public function wrong_text_length_in_text()
    {
        $exceptionIsHappened = false;
        $this->text = Str::random($this->config['tools']['header']['text']['length'] + 1);

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Header text is too long', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function trim_spaces_from_text()
    {
        $text = $this->text;
        $this->text = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->text = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_text()
    {
        $text = $this->text;
        $this->text = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->text = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);
    }

    /** @test */
    public function without_level()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutLevel(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `level`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_level()
    {
        $exceptionIsHappened = false;
        $this->level = 5;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals(
                'Option \'level\' with value `5` has invalid value. Check canBeOnly param.',
                $e->getMessage()
            );
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_anchor()
    {
        $exceptionIsHappened = false;
        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutAnchor(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `anchor`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_text_in_anchor()
    {
        $exceptionIsHappened = false;

        // In config the anchor length is more than 40
        $this->anchor = 'wrong, <tags>tags</tags>. "Q",- !@#$авы';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Invalid characters in anchor text', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_text_length_in_anchor()
    {
        $exceptionIsHappened = false;
        $this->anchor = Str::random($this->config['tools']['header']['anchor']['length'] + 1);

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Anchor text is too long', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    private function getDataWithoutText(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['text']);

        return json_encode($data);
    }

    private function getDataWithoutLevel(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['level']);

        return json_encode($data);
    }

    private function getDataWithoutAnchor(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['anchor']);

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'text' => $this->text,
            'level' => $this->level,
            'anchor' => $this->anchor
        ];

        return json_encode($data);
    }
}
