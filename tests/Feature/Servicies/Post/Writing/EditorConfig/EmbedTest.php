<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Illuminate\Support\Str;
use Tests\TestCase;

class EmbedTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'embed',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string Name of embedded service
     */
    private string $serviceName = 'instagram';

    /**
     * @var string Source of embedded service
     */
    private string $source = '';

    /**
     * @var string Embedded service
     */
    private string $embed = '';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text caption';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(6, $blocks[0]['data']);

        $this->assertArrayHasKey('caption', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['caption']);

        $this->assertEquals($this->textCaption, $blocks[0]['data']['caption']);
    }

    /** @test */
    public function without_service()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutService(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `service`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_source()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutSource(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `source`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_embed()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutEmbed(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `embed`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_width()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutWidth(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `width`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_height()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutHeight(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `height`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_caption()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutCaption(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `caption`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_caption()
    {
        $this->textCaption = 'Text with<script>wrong tag</script>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with', $blocks[0]['data']['caption']);
    }

    /** @test */
    public function wrong_caption_length()
    {
        $exceptionIsHappened = false;
        $this->textCaption = Str::random($this->config['tools']['embed']['caption']['length'] + 1);

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Embed\'s caption is too long', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function trim_spaces_from_caption()
    {
        $text = 'text';
        $this->textCaption = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_caption()
    {
        $text = 'text';
        $this->textCaption = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);
    }

    private function getDataWithoutService(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['service']);

        return json_encode($data);
    }

    private function getDataWithoutSource(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['source']);

        return json_encode($data);
    }

    private function getDataWithoutEmbed(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['embed']);

        return json_encode($data);
    }

    private function getDataWithoutWidth(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['width']);

        return json_encode($data);
    }

    private function getDataWithoutHeight(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['height']);

        return json_encode($data);
    }

    private function getDataWithoutCaption(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['caption']);

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'service' => $this->serviceName,
            'caption' => $this->textCaption,
            'source' => $this->source,
            'embed' => $this->embed,
            'width' => 500,
            'height' => 500,
        ];

        return json_encode($data);
    }
}
