<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Illuminate\Support\Str;
use Tests\TestCase;

class ImageTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'image',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string URL of an image file
     */
    private string $url = 'https://mbhn.ru';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text caption with <a href="href">a link</a>';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }

// TODO add tests from ListTest.php
    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(5, $blocks[0]['data']);

        $this->assertArrayHasKey('caption', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['caption']);
        $this->assertIsBool($blocks[0]['data']['stretched']);
        $this->assertIsBool($blocks[0]['data']['withBackground']);
        $this->assertIsBool($blocks[0]['data']['withBorder']);

        $this->assertEquals($this->textCaption, $blocks[0]['data']['caption']);
    }

    /** @test */
    public function without_file()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutFile(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `file`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_file_url()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutFileUrl(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `url`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function invalid_url_without_shema()
    {
        $exceptionIsHappened = false;
        $this->url = 'mbhn.ru';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Invalid image url', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function invalid_whole_url()
    {
        $exceptionIsHappened = false;
        $this->url = 'I\'m not the MBH News, Man!';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Invalid image url', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_stretched()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutStretched(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `stretched`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_with_background()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutWithBackground(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `withBackground`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_with_border()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutWithBorder(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `withBorder`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_caption()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutCaption(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `caption`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_caption()
    {
        $this->textCaption = 'Text with<script>wrong tag</script>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with', $blocks[0]['data']['caption']);
    }

    /** @test */
    public function wrong_caption_length()
    {
        $exceptionIsHappened = false;
        $this->textCaption = Str::random($this->config['tools']['embed']['caption']['length'] + 1);

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Image\'s caption is too long', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function trim_spaces_from_caption()
    {
        $text = 'text';
        $this->textCaption = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_caption()
    {
        $text = 'text';
        $this->textCaption = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);
    }

    private function getDataWithoutFile(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['file']);

        return json_encode($data);
    }

    private function getDataWithoutFileUrl(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['file']['url']);

        return json_encode($data);
    }

    private function getDataWithoutStretched(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['stretched']);

        return json_encode($data);
    }

    private function getDataWithoutWithBackground(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['withBackground']);

        return json_encode($data);
    }

    private function getDataWithoutWithBorder(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['withBorder']);

        return json_encode($data);
    }

    private function getDataWithoutCaption(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['caption']);

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'file' => [
                'url' => $this->url
            ],
            'caption' => $this->textCaption,
            'withBorder' => false,
            'withBackground' => false,
            'stretched' => true,
        ];

        return json_encode($data);
    }
}
