<?php

namespace Tests\Feature\Servicies\Post\Writing\EditorConfig;

use App\Services\Post;
use Exception;
use Illuminate\Support\Str;
use Tests\TestCase;

class QuoteTest extends TestCase
{
    private Post\EditorService $service;

    /**
     * @var array EditorJS config
     */
    private array $config = [];

    /**
     * @var array Base data
     */
    private array $data = [
        'time' => 1599007084658,
        'version' => '2.18.0',
        'blocks' => [
            [
                'type' => 'quote',
                'data' => []
            ]
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $textQuote = 'text with <a href="#href">a link</a><br>';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text with <a href="#href">a link</a>';

    private string $alignment = 'left';

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\EditorService::class);

        $this->config = config('editorjs');
    }
// TODO add tests from ListTest.php

    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertArrayHasKey('data', $blocks[0]);

        $this->assertCount(3, $blocks[0]['data']);

        $this->assertArrayHasKey('text', $blocks[0]['data']);
        $this->assertArrayHasKey('caption', $blocks[0]['data']);
        $this->assertArrayHasKey('alignment', $blocks[0]['data']);

        $this->assertIsString($blocks[0]['data']['text']);
        $this->assertIsString($blocks[0]['data']['caption']);
        $this->assertIsString($blocks[0]['data']['alignment']);

        // from editorJs we get <br>, but from HTMLPurifier we get <br /> instead
        $this->assertEquals(str_replace('<br>', '<br />', $this->textQuote), $blocks[0]['data']['text']);
        $this->assertEquals($this->textCaption, $blocks[0]['data']['caption']);
        $this->assertEquals($this->alignment, $blocks[0]['data']['alignment']);
    }

    /** @test */
    public function empty_quote()
    {
        $exceptionIsHappened = false;
        $this->textQuote = '';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Quote is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_quote()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutQuote(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `text`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function quote_with_spaces_only()
    {
        $exceptionIsHappened = false;
        $this->textQuote = ' ';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Quote is empty', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function wrong_tags_in_quote()
    {
        $this->textQuote = 'Text with<script>wrong tag</script>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with', $blocks[0]['data']['text']);
    }

    /** @test */
    public function trim_spaces_from_quote()
    {
        $text = 'text';
        $this->textQuote = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->textQuote = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['text']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_quote()
    {
        $text = 'text';
        $this->textQuote = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);

        // spaces as '&nbsp;'
        $this->textQuote = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['text']);
    }

    /** @test */
    public function without_caption()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutCaption(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `caption`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function caption_with_spaces_only()
    {
        $this->textCaption = ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('', $blocks[0]['data']['caption']);
    }

    /** @test */
    public function wrong_tags_in_caption()
    {
        $this->textCaption = 'Text with<script>wrong tag</script>';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals('Text with', $blocks[0]['data']['caption']);
    }

    /** @test */
    public function wrong_caption_length()
    {
        $exceptionIsHappened = false;
        $this->textCaption = Str::random($this->config['tools']['quote']['caption']['length'] + 1);

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Quote\'s caption is too long', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function trim_spaces_from_caption()
    {
        $text = $this->textCaption;
        $this->textCaption = ' ' . $text . ' ';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp;' . $text . '&nbsp;';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text, $blocks[0]['data']['caption']);
    }

    /** @test */
    public function remove_double_and_more_spaces_from_caption()
    {
        $text = $this->textCaption;
        $this->textCaption = '  ' . $text . '   text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);

        // spaces as '&nbsp;'
        $this->textCaption = '&nbsp; &nbsp;' . $text . '&nbsp; &nbsp; &nbsp; text';

        $blocks = $this->service->sanitizeData(
            $this->getData(),
            $this->config
        );

        $this->assertCount(1, $blocks);

        $this->assertEquals($text . ' text', $blocks[0]['data']['caption']);
    }

    /** @test */
    public function empty_alignment()
    {
        $exceptionIsHappened = false;
        $this->alignment = '';

        try {
            $blocks = $this->service->sanitizeData(
                $this->getData(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals(
                'Option \'alignment\' with value `` has invalid value. Check canBeOnly param.',
                $e->getMessage()
            );
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    /** @test */
    public function without_alignment()
    {
        $exceptionIsHappened = false;

        try {
            $blocks = $this->service->sanitizeData(
                $this->getDataWithoutAlignment(),
                $this->config
            );
        } catch (Exception $e) {
            $exceptionIsHappened = true;
            $this->assertEquals('Not found required param `alignment`', $e->getMessage());
        }

        $this->assertEquals(true, $exceptionIsHappened);
    }

    private function getDataWithoutQuote(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['text']);

        return json_encode($data);
    }

    private function getDataWithoutCaption(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['caption']);

        return json_encode($data);
    }

    private function getDataWithoutAlignment(): string
    {
        $data = json_decode($this->getData(), true);

        unset($data['blocks'][0]['data']['alignment']);

        return json_encode($data);
    }

    private function getData(): string
    {
        $data = $this->data;

        $data['blocks'][0]['data'] = [
            'text' => $this->textQuote,
            'caption' => $this->textCaption,
            'alignment' => $this->alignment,
        ];

        return json_encode($data);
    }
}
