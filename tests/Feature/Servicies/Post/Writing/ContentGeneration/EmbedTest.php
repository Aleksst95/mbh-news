<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class EmbedTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'embed',
            'data' => []
        ]
    ];


    /**
     * @var string Name of embedded service
     */
    private string $serviceName = 'instagram';

    /**
     * @var string Source of embedded service
     */
    private string $source = 'https://www.instagram.com/p/CE9e7Tqopau/';

    /**
     * @var string Embedded service
     */
    private string $embed = 'https://www.instagram.com/p/CE9e7Tqopau/embed';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text caption';


    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $height = $this->getServiceIframeHeight();
        $this->assertEquals(
            '<figure class="layout-center-column"><iframe class="w-full shadow-xl post-figure__iframe-with-border" height="'
            . $height . '" src="' . $this->embed . '" frameborder="0" allowfullscreen></iframe>'
            . '<figcaption class="post-figure__caption">' . $this->textCaption . '</figcaption></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_embed()
    {
        $this->embed = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function with_empty_caption()
    {
        $this->textCaption = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $height = $this->getServiceIframeHeight();
        $this->assertEquals(
            '<figure class="layout-center-column"><iframe class="w-full shadow-xl post-figure__iframe-with-border" height="'
            . $height . '" src="' . $this->embed . '" frameborder="0" allowfullscreen></iframe></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_link_from_current_domain()
    {
        $this->textCaption = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $textCaptionResult = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank">'
            . 'link from current domain</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $height = $this->getServiceIframeHeight();
        $this->assertEquals(
            '<figure class="layout-center-column"><iframe class="w-full shadow-xl post-figure__iframe-with-border" height="'
            . $height . '" src="' . $this->embed . '" frameborder="0" allowfullscreen></iframe>'
            . '<figcaption class="post-figure__caption">' . $textCaptionResult . '</figcaption></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function full_width_service()
    {
        // Embed link is not important at tests, so we check just the generated result
        $this->serviceName = 'youtube';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $height = $this->getServiceIframeHeight();
        $this->assertEquals(
            '<figure><iframe class="w-full shadow-xl" height="' . $height . '" src="' . $this->embed . '"'
            . ' frameborder="0" allowfullscreen></iframe>'
            . '<figcaption class="post-figure__caption layout-center-column">' . $this->textCaption . '</figcaption></figure>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'service' => $this->serviceName,
            'caption' => $this->textCaption,
            'source' => $this->source,
            'embed' => $this->embed,
            'width' => 500,
            'height' => 500,
        ];

        return $data;
    }

    private function getServiceIframeHeight(): int
    {
        $height = 320;

        $service = $this->serviceName;
        switch ($service) {
            case 'youtube':
            case 'vimeo':
                $height = 420;
                break;
            case 'vine':
            case 'coub':
                $height = 320;
                break;
            case 'twitter':
                $height = 600;
                break;
            case 'instagram':
                $height = 505;
                break;
            case 'yandex-music-track':
                $height = 100;
                break;
            case 'gfycat':
                $height = 436;
                break;
            case 'imgur':
                $height = 500;
                break;
            case 'twitch-video':
                $height = 366;
                break;
        }

        return $height;
    }
}
