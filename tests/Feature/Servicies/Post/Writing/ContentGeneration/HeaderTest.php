<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class HeaderTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'header',
            'data' => []
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $text = 'text';

    /**
     * @var string This line contains all available characters
     */
    private string $anchor = 'anchor';

    private int $level = 2;

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<h' . ($this->level + 1) . ' class="layout-center-column header--'
            . ($this->level + 1) . '" id="anchor-' . $this->anchor . '">'
            . $this->text . '</h' . ($this->level + 1) . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_text()
    {
        $this->text = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function with_empty_anchor()
    {
        $this->anchor = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<h' . ($this->level + 1) . ' class="layout-center-column header--' . ($this->level + 1) . '">'
            . $this->text . '</h' . ($this->level + 1) . '>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'text' => $this->text,
            'level' => $this->level,
            'anchor' => $this->anchor
        ];

        return $data;
    }
}
