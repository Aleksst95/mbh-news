<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class QuoteTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'quote',
            'data' => []
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $textQuote = 'text with <a href="https://tesla.com">a</a>, <b>b</b>, <i>i</i>, <mark>m</mark>';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text with <a href="https://tesla.com">a link</a>';


    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $this->textQuote . '</p>'
            . '<cite class="post-quote__caption">' . $this->textCaption . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_text()
    {
        $this->textQuote = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function text_in_quote_with_seo_link_and_user_is_usual()
    {
        $this->textQuote = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $this->textQuote . '</p>'
            . '<cite class="post-quote__caption">' . $this->textCaption . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_in_quote_with_link_from_current_domain()
    {
        $this->textQuote = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $textQuoteResult = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank">'
            . 'link from current domain</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $textQuoteResult . '</p>'
            . '<cite class="post-quote__caption">' . $this->textCaption . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_in_quote_with_seo_links_and_user_is_service_member()
    {
        $this->textQuote = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a> and <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>one more</mark></a> and <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<i>one usual</i></a>';

        $textQuoteResult = 'text with <a href="https://tesla.com" target="_blank">'
            . 'seo link</a> and <a href="https://tesla.com" target="_blank">'
            . 'one more</a> and <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<i>one usual</i></a>';


        $blocks = $this->service->generate($this->getData(), true);

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $textQuoteResult . '</p>'
            . '<cite class="post-quote__caption">' . $this->textCaption . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_in_quote_with_anchor_link()
    {
        $this->textQuote = 'text with <a href="#anchor" target="_blank" rel="nofollow noreferrer noopener">'
            . 'anchor link</a>';

        $textQuoteResult = 'text with <a href="#anchor-anchor">anchor link</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $textQuoteResult . '</p>'
            . '<cite class="post-quote__caption">' . $this->textCaption . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_caption()
    {
        $this->textCaption = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $this->textQuote . '</p></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_in_caption_with_link_from_current_domain()
    {
        $this->textCaption = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $textCaptionResult = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank">'
            . 'link from current domain</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $this->textQuote . '</p>'
            . '<cite class="post-quote__caption">' . $textCaptionResult . '</cite></blockquote>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_in_caption_with_anchor_link()
    {
        $this->textCaption = 'text with <a href="#anchor" target="_blank" rel="nofollow noreferrer noopener">'
            . 'anchor link</a>';

        $textCaptionResult = 'text with <a href="#anchor-anchor">anchor link</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<blockquote class="layout-center-column post-quote"><p>' . $this->textQuote . '</p>'
            . '<cite class="post-quote__caption">' . $textCaptionResult . '</cite></blockquote>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'text' => $this->textQuote,
            'caption' => $this->textCaption
        ];

        return $data;
    }
}
