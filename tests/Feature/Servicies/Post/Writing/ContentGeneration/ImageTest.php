<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class ImageTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'image',
            'data' => []
        ]
    ];

    /**
     * @var string URL of an image file
     */
    private string $url = 'https://mbhn.ru';

    /**
     * @var string This line contains all available tags
     */
    private string $textCaption = 'text with <a href="https://tesla.com">a link</a>';

    /**
     * @var bool This line contains all available tags
     */
    private bool $stretched = false;


    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<figure class="layout-center-column"><img class="w-full" src="' . $this->url . '" alt="">'
            . '<figcaption class="post-figure__caption">' . $this->textCaption . '</figcaption></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_url()
    {
        $this->url = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function with_empty_caption()
    {
        $this->textCaption = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<figure class="layout-center-column"><img class="w-full" src="' . $this->url . '" alt=""></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_link_from_current_domain()
    {
        $this->textCaption = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $textCaptionResult = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank">'
            . 'link from current domain</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<figure class="layout-center-column"><img class="w-full" src="' . $this->url . '" alt="">'
            . '<figcaption class="post-figure__caption">' . $textCaptionResult . '</figcaption></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_anchor_link()
    {
        $this->textCaption = 'text with <a href="#anchor" target="_blank" rel="nofollow noreferrer noopener">'
            . 'anchor link</a>';

        $textCaptionResult = 'text with <a href="#anchor-anchor">anchor link</a>';


        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<figure class="layout-center-column"><img class="w-full" src="' . $this->url . '" alt="">'
            . '<figcaption class="post-figure__caption">' . $textCaptionResult . '</figcaption></figure>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_stretched()
    {
        $this->stretched = true;

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<figure class="mb-4"><img class="w-full" src="' . $this->url . '" alt="">'
            . '<figcaption class="post-figure__caption layout-center-column">' . $this->textCaption
            . '</figcaption></figure>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'file' => [
                'url' => $this->url,
            ],
            'caption' => $this->textCaption,
            'withBorder' => false,
            'withBackground' => false,
            'stretched' => $this->stretched,
        ];

        return $data;
    }
}
