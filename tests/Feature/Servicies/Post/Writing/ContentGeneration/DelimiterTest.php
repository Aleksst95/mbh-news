<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class DelimiterTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'delimiter',
            'data' => []
        ]
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->data);

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column text-center text-4xl">* * *</div>',
            $blocks[0]
        );
    }

}
