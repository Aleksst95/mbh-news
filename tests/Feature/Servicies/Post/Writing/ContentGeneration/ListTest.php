<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class ListTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'list',
            'data' => []
        ]
    ];

    /**
     * @var string Text of the first item of the list
     *
     *  This line contains all available tags except the 'br' tag
     */
    private string $textFirst = 'text with <a href="https://tesla.com">a</a>, <b>b</b>, <i>i</i>, <mark>m</mark>';

    /**
     * @var string Text of the second item of the list
     */
    private string $textSecond = 'just text';
    /**
     * @var string
     */
    private string $style = 'ordered';


    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();

        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>' . $this->textFirst . '</li>'
            . '<li>' . $this->textSecond . '</li>'
            . '</' . $tag . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_items()
    {
        $data = $this->getData();
        $data[0]['data']['items'] = [];

        $blocks = $this->service->generate($data);

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function text_with_seo_link_in_first_item_and_user_is_usual()
    {
        $this->textFirst = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();
        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>' . $this->textFirst . '</li>'
            . '<li>' . $this->textSecond . '</li>'
            . '</' . $tag . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_seo_links_in_first_item_and_user_is_service_member()
    {
        $this->textFirst = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a> and '
            . '<a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener"><b>one usual</b></a> and '
            . '<a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener"><mark>seo one</mark></a>';

        $textFirstResult = 'text with <a href="https://tesla.com" target="_blank">seo link</a> and '
            . '<a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener"><b>one usual</b></a> and '
            . '<a href="https://tesla.com" target="_blank">seo one</a>';

        $blocks = $this->service->generate($this->getData(), true);

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();
        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>' . $textFirstResult . '</li><li>' . $this->textSecond . '</li></' . $tag . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_link_from_current_domain_in_first_item()
    {
        $this->textFirst = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $textFirstResult = 'text with <a href="' . config('app.url') . '/some-link" target="_blank">'
            . 'link from current domain</a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();
        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>' . $textFirstResult . '</li><li>' . $this->textSecond . '</li></' . $tag . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_anchor_link_in_first_item()
    {
        $this->textFirst = 'text with <a href="#anchor" target="_blank" rel="nofollow noreferrer noopener">'
            . 'anchor link</a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();
        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>text with <a href="#anchor-anchor">anchor link</a></li>'
            . '<li>' . $this->textSecond . '</li>'
            . '</' . $tag . '>',
            $blocks[0]
        );
    }

    /** @test */
    public function ordered_style()
    {
        $this->style = 'ordered';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $tag = $this->getCurrentTag();
        $this->assertEquals(
            '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">'
            . '<li>' . $this->textFirst . '</li>'
            . '<li>' . $this->textSecond . '</li>'
            . '</' . $tag . '>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'style' => $this->style,
            'items' => [$this->textFirst, $this->textSecond],
        ];

        return $data;
    }

    private function getCurrentTag(): string
    {
        if ($this->style === 'ordered') {
            return 'ol';
        }

        return 'ul';
    }
}
