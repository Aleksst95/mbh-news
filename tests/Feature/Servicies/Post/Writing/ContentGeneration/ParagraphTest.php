<?php

namespace Tests\Feature\Servicies\Post\Writing\ContentGeneration;

use App\Services\Post;
use Tests\TestCase;

class ParagraphTest extends TestCase
{
    private Post\ContentGenerationService $service;

    /**
     * @var array Base data
     */
    private array $data = [
        [
            'type' => 'paragraph',
            'data' => []
        ]
    ];

    /**
     * @var string This line contains all available tags
     */
    private string $text = 'text with <a href="https://tesla.com">a</a>, <b>b</b>, <i>i</i>, <mark>m</mark>';


    public function setUp(): void
    {
        parent::setUp();

        $this->service = resolve(Post\ContentGenerationService::class);
    }


    /** @test */
    public function correct_data()
    {
        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column">' . $this->text . '</div>',
            $blocks[0]
        );
    }

    /** @test */
    public function with_empty_text()
    {
        $this->text = '';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(0, $blocks);
    }

    /** @test */
    public function text_with_seo_link_and_user_is_usual()
    {
        $this->text = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column">' . $this->text . '</div>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_link_from_current_domain()
    {
        $this->text = 'text with <a href="' . config('app.url')
            . '/some-link" target="_blank" rel="nofollow noreferrer noopener">'
            . 'link from current domain</a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column">text with <a href="' . config('app.url')
            . '/some-link" target="_blank">link from current domain</a></div>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_seo_links_and_user_is_service_member()
    {
        $this->text = 'text with <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>seo link</mark></a> and <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<mark>one more</mark></a> and <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<i>one usual</i></a>';

        $blocks = $this->service->generate($this->getData(), true);

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column">text with <a href="https://tesla.com" target="_blank">seo link</a> and'
            . ' <a href="https://tesla.com" target="_blank">one more</a> and'
            . ' <a href="https://tesla.com" target="_blank" rel="nofollow noreferrer noopener">'
            . '<i>one usual</i></a></div>',
            $blocks[0]
        );
    }

    /** @test */
    public function text_with_anchor_link()
    {
        $this->text = 'text with <a href="#anchor" target="_blank" rel="nofollow noreferrer noopener">'
            . 'anchor link</a>';

        $blocks = $this->service->generate($this->getData());

        $this->assertCount(1, $blocks);

        $this->assertEquals(
            '<div class="layout-center-column">text with <a href="#anchor-anchor">anchor link</a></div>',
            $blocks[0]
        );
    }

    private function getData(): array
    {
        $data = $this->data;

        $data[0]['data'] = [
            'text' => $this->text
        ];

        return $data;
    }
}
