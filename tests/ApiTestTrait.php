<?php

namespace Tests;

trait ApiTestTrait
{
    protected $response;

    public function assertResponseUnauthorized()
    {
        $this->response->assertStatus(401);
        $this->response->assertJson(['message' => 'Unauthenticated.']);
    }

    public function assertResponseWithoutBody()
    {
        $this->response->assertStatus(204);
    }

    public function assertResponseCreated()
    {
        $this->response->assertStatus(201);
    }

    public function getResponseData(): array
    {
        return json_decode($this->response->getContent(), true);
    }
}
