const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');
require('laravel-mix-purgecss');

mix.js('resources/js/app.js', 'public/js').
sass('resources/sass/app.scss', 'public/css').
copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts').
options({
    processCssUrls: false,
    postCss: [tailwindcss('./tailwind.config.js')],
}).
purgeCss().
browserSync('http://127.0.0.1:8000/');

// For tests
mix.webpackConfig({node: {fs: 'empty', module: 'empty'}});
