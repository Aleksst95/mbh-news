var change = false;

var editor = new EditorJS({
    /**
     * Id of Element that should contain the Editor
     */
    holderId: 'editor',

    /**
     * Enable autofocus
     */
    autofocus: true,

    /**
     * Available Tools list.
     * Pass Tool's class or Settings object for each Tool you want to use
     */
    tools: {
        header: Header,
        image: {
            class: ImageTool,
            inlineToolbar: true,
            config: {
                endpoints: {
                    byFile: '/writing/upload_img_new', // Your backend file uploader endpoint
                    byUrl: '/writing/upload_img_by_url' // Your endpoint that provides uploading by Url
                }
            }
        },
        embed: {
            class: Embed,
            config: {
                services: {
                    youtube: true,
                    vimeo: true,
                    coub: true,
                    vine: true
                }
            }
        },
        list: {
            class: List,
            inlineToolbar: true
        },
        quote: {
            class: Quote,
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+O',
            config: {
                captionPlaceholder: 'Имя Автора'
            }
        },
        delimiter: Delimiter

    },

    data : {
        time: 1552744582955,
        blocks: [
            {
                type: "paragraph",
                data: {
                    text: "https://cdn.pixabay.com/photo/2017/09/01/21/53/blue-2705642_1280.jpg"
                }
            }
        ],
        version: "2.11.10"
    },
    /**
     * onReady callback
     */
    onReady: () => {console.log('Editor.js is ready to work!')},

    /**
     * onChange callback
     */
    onChange: () => {
        change = true;

        console.log('Now I know that Editor\'s content changed!')
    }
});



//Автосохранение каждые 10 секунд, если были изменения
var saving = setInterval(function() {
    if (change) {
        editor.save().then((outputData) => {
            mbhn.post.save_new( true, JSON.stringify(outputData, null, 4) );
            //console.log('Article data: ', outputData);
        }).catch((error) => {
            console.log('Saving failed: ', error)
        });
        change = false;
    }
}, 10000);

$(document).ready(function() {
    /* find all iframes with ids starting with "tweet_" */
    $("iframe[id^='tweet_']").load(function() {
        this.contentWindow.postMessage({ element: this.id, query: "height" },
            "https://twitframe.com");
    });
});

/* listen for the return message once the tweet has been loaded */
$(window).bind("message", function(e) {
    var oe = e.originalEvent;
    if (oe.origin != "https://twitframe.com")
        return;

    if (oe.data.height && oe.data.element.match(/^tweet_/))
        $("#" + oe.data.element).css("height", parseInt(oe.data.height) + "px");
});