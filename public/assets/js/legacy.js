// User methods Class.

var user = function ( window ){


    function saveSettings ( $this ) {
        var form         = document.forms['js-user-form'],
            data         = mbhn.ajax.serialize(form, true),
            submitURL    = form.getAttribute('action');

        mbhn.simpleAjax.call({
            type: 'post',
            url: submitURL,
            data: data,
            success: function (response) {
                if (response.success){
                    mbhn.alerts.success(response.success);
                } else if (response.error) {
                    mbhn.alerts.error(response.error);
                }
                else {
                    mbhn.alerts.error('Упс. Что-то пошло не так.');
                }
            }
        });
    }

    function unlinkSocialAccount ( link, type ) {
        var url = '';
        switch (type) {
            case 1 : url = '/auth/unlinkvk';
                break;
            case 2 : url = '/auth/unlinkfb';
                break;
            case 3 : url = '/auth/unlinktw';
                break;
            case 4 : url = '/auth/unlinkgg';
                break;

        }
        mbhn.simpleAjax.call({
            type: 'get',
            url: url,
            success: function (response) {
                if (response.success){
                    mbhn.alerts.success(response.success);

                    link.onclick = null;

                    link.classList.add("ajaxfree");
                    link.style.opacity = 1;

                    switch (type) {
                        case 1 :
                            link.innerHTML = "<span>ВКонтакте</span>";
                            link.href = "/auth/linkvk";
                            break;
                        case 2 :
                            link.innerHTML = "<span>Facebook</span>";
                            link.href = "/auth/linkfb";
                            break;
                        case 3 :
                            link.innerHTML = "<span>Twitter</span>";
                            link.href = "/auth/linktw";
                            break;
                        case 4 :
                            link.innerHTML = "<span>Google</span>";
                            link.href = "/auth/linkgg";
                            break;
                    }
                } else if (response.error) {
                    mbhn.alerts.error(response.error);
                }
            },
            error: function () {
                mbhn.alerts.error('Упс. Что-то пошло не так.');
            }
        });
    }

    function showEmailSubscribe() {
        email = $("#email_subscribe");

        mbhn.simpleAjax.call({
            type: 'get',
            url: '/emails/validate_email',
            data: {email: email.val()},
            success: function (response) {
                if (response.result == 'success'){
                    mbhn.popup.show({url: '/emails/popup_captcha'});
                }
                else {
                    mbhn.alerts.error('Неправильный email-адрес');
                }
            },
            error: function() {
                mbhn.alerts.error('Неправильный email-адрес');
            }
        });
    }

    function  subscribeEmailDelivery() {
        var form         = document.forms['emailSubscribe'],
            data         = mbhn.ajax.serialize(form, true),
            submitURL    = form.getAttribute('action'),
            email        = $("#email_subscribe").val();

        if (data['g-recaptcha-response'] == undefined){
            mbhn.alerts.error('Пройдите капчу и подтвердите, что вы не робот.');
            return;
        }
        else if (data['g-recaptcha-response'].length == 0) {
            mbhn.alerts.error('Пройдите капчу и подтвердите, что вы не робот.');
            return;
        }
        console.log(email);

        mbhn.simpleAjax.call({
            type: 'post',
            url:  submitURL,
            data: {email: email, token: data['g-recaptcha-response']},
            success: function (response) {
                if (response.success){
                    mbhn.alerts.success(response.success);
                }
                else if (response.error){
                    mbhn.alerts.error(response.error);
                }
                else {
                    mbhn.alerts.error('Упс. Что-то пошло не так.');
                }
            },
            error: function() {
                mbhn.alerts.error('Упс. Что-то пошло не так.');
            }
        });
    }

    function getEmailDelivery () {
        var input = $("#get_email_delivery");

        if (input.prop('disabled')) {
            return;
        }

        var is_checked = input.attr("checked") == "checked",
            get_delivery = is_checked ? 0 : 1;

        mbhn.simpleAjax.call({
            type: 'get',
            url: '/user/get_delivery/' + get_delivery,
            success: function (response) {
                if (response.success){
                    mbhn.alerts.success(response.success);
                } else if (response.error) {
                    mbhn.alerts.error(response.error);
                    if (is_checked) {
                        input.attr("checked", "checked");
                        input.parent(".checkbox").addClass('checked');
                    }
                    else {
                        input.attr("checked", false);
                        input.parent(".checkbox").removeClass('checked');
                    }
                }
            },
            error: function () {
                mbhn.alerts.error('Упс. Что-то пошло не так.');
                if (is_checked) {
                    input.attr("checked", "checked");
                    input.parents(".checkbox").addClass('checked');
                }
                else {
                    input.removeAttr("checked");
                    input.parents('.checkbox').removeClass('checked');
                }
            }
        });
    }

    function getReplyForm () {

        if ( window.static_nodes.replyForm ){
            return window.static_nodes.replyForm;
        }

        var $form = $( $.parseHTML($('.replyFormWrapper').html() )).filter('*');

        if (!$form) {
            mbhn.core.log('Reply form not found','Comments','error'); return;
        } else {
            window.static_nodes.replyForm = $form;
            mbhn.core.log('Node reply form was updated' , 'Static nodes', 'info');
        }

        return $form;
    }

    function commentsReply ( $this , target , id_parent ){

        var $form = getReplyForm();

        $('#cReplyForm, .reply_form').remove();

        var action       = '/comments/' + target,
            $parentBlock = $( '#comment' + id_parent ),
            form         = $form.attr('id','cReplyForm').attr('action', action),
            textarea     = form.find('textarea');

        textarea.css('height', 'auto');


            setTimeout(function () {
                textarea
                    .unbind('input')
                    .bind("input", user.resizeTextarea)
                    .bind("keydown", mbhn.pages.submitByCtrlEnter);
            }, 300);



        form.find('#parent').val( id_parent );

        form.addClass('reply_form');

        $parentBlock.find('.comment_wrap:first').after( form );
        mbhn.scroller.to(form.get(0), 150);
    }

    function counter_toggle ( obj, count ){
        obj.css({'display': 'inline-block', 'overflow' : 'hidden', 'vertical-align' : 'bottom'});
        var start_value = obj.html();
        obj.html('');
        var start_value_wrap = $('<div class="counter_switcher" />').html(start_value).appendTo(obj);
        start_value_wrap.animate({"top" :"20"}, 150, 'linear', function(){
            start_value_wrap.css({"opacity": 0, "top": "-20px"});
            setTimeout(function(){
                start_value_wrap.html(count);
                start_value_wrap.css({"opacity": 1}).animate({"top": 0}, 150, 'linear', function(){
                    obj.html(count);
                });
            }, 50);
        });
    }

    function handleLimitedInput ( obj ){

        var value         = obj.val(),
            originalLimit = obj.data('originalLimit'),
            wrapperClassName = obj.data('parentClass') || '.input_text',
            prevValue;

        if ( value.length > originalLimit ) {

            prevValue = obj.attr("value");

            if (!prevValue){
                return;
            }

            obj.attr( "value", prevValue.slice(0, originalLimit));
            obj.parent(wrapperClassName).attr("data-limit", 0);

        } else {

            obj.parent(wrapperClassName).attr("data-limit", originalLimit - value.length);

        }

    }


    /**
     * Limited input styling function
     * Not provides input blocking
     */
    function getInputLimits ( obj ) {

        if ( obj.length > 0 ){

            obj.each(function(){

                var $this = $(this),
                    wrapperClassName = $this.data('parentClass') || '.input_text';

                $this.data('originalLimit' , $this.parent(wrapperClassName).data('limit') );

                handleLimitedInput( $this );

                $this.bind('input',function(e){
                    handleLimitedInput( $this );
                });

            });
        }
    }

    /** use's event.data.$input and event.data.$lengthHelper */
    function checkFieldLength ( event ){

        var $input = $(event.target),
            $lengthHelper = event.data.$lengthHelper;

        setTimeout(function() {

            var value         = $input.val(),
                minWordsCount = parseInt( $input.data('minWords'), 10 ),
                maxWordsCount = parseInt( $input.data('maxWords'), 10 ),
                sequence      = (maxWordsCount - minWordsCount) / 3,
                betterCount   = minWordsCount + sequence,
                bestCount     = minWordsCount + sequence * 2,
                wordsCount    = value.wordsCount(),
                helperText    = '',
                helperClass   = '';

            $input.data('limitReached', false);

            if ( wordsCount < minWordsCount ) {
                helperText  = $lengthHelper.data('min');
                helperClass = 'wrong';
            } else if ( wordsCount > maxWordsCount ) {

                helperText = $lengthHelper.data('max');
                helperClass = 'wrong max';

                $input.data('limitReached', 1);

            } else if ( wordsCount >= minWordsCount && wordsCount <= betterCount){
                helperText = $lengthHelper.data('less');
                helperClass = 'less';
            } else if ( wordsCount > betterCount && wordsCount <= bestCount){
                helperText = $lengthHelper.data('optimum');
                helperClass = 'good';
            } else {
                helperText  = $lengthHelper.data('more');
                helperClass = 'more';
            }

            $lengthHelper.removeClass('wrong max less good more').addClass(helperClass).children('.desc').text(helperText);

        }, 100);

    }

    function placeScrollUpBlock(){

        var window_width = parseInt(window.innerWidth, 10),
            center_side_width = parseInt($('#head_center').width(), 10),
            $scrollUp = $('.scroll_up'),
            scrollBlockWidth = (window_width - center_side_width) / 2 * 0.56;

        $scrollUp.css( 'width' , scrollBlockWidth > 30 ? scrollBlockWidth + 'px' : 30 + 'px' );

    }

    function vote ( $button, target, vote ) {

        if (window.our_variables.likesLokerTimeout) {
            $button.removeClass('hide wobble').addClass('wobble');
            $('#likesLoker').removeClass('wobble').addClass('wobble');
            setTimeout(function() { $button.removeClass('wobble'); $('#likesLoker').removeClass('wobble'); }, 300);
            return;
        }

        var x = $button.position().left + 8,
            y = $button.position().top;

        var icon_blinked = vote > 0 ? 'icon-up-big' : 'icon-down-big',
            blinked = $('<i class="' + icon_blinked + ' blinkedLike">').css({
                'position' : 'absolute', 'font-size' : '10px', 'left': x + 'px', 'top' : y + 'px',
                'color' : (vote > 0 ? '#1ED22E' : '#FF5E5E')
            }).appendTo($button.offsetParent()).animate({
                'fontSize': '50px', 'opacity' : '0', 'top': y - 15  + 'px',  'left' : x - 30 + 'px'
            }, 250, function(){ $(this).remove(); });

        if ($button.data('clicked')) return;

        /** Immediately puts estimated new vote. It was updated after request */
        var $count   = $('#likes_count_' + target + '_' + vote),
            current_count = parseInt($count.text(), 10),
            previous_vote = parseInt($count.data('voted'), 10),
            new_count_estimated;

        /** Save original count to revert selfie likes */
        $count.data('origin' , $count.data('origin') || current_count );

        if (!isNaN(current_count)) {

            /** First vote */
            if (isNaN(previous_vote)) {

                new_count_estimated = vote > 0 ? current_count + 1 : current_count - 1;

                /** Vote removing */
            } else if(previous_vote == vote) {

                new_count_estimated = vote > 0 ? current_count - 1 : current_count + 1;

                /** Vote changed to opposite */
            } else {

                new_count_estimated =  previous_vote > 0 ? current_count - 2 : current_count + 2;

            }

            user.placeNewVote( $count, (new_count_estimated > 0 ? '+' : '') + new_count_estimated );
        }

        /**
         * Remember vote
         * Uses to detect vote removing
         */
        $count.data('voted', vote);

        mbhn.simpleAjax.call({
            url: '/likes/index',
            data: {
                type: vote,
                post_id: target
            },
            beforeSend: function(){
                $('#likes_plus_' + target + '_' + vote).data('clicked', false).removeClass('clicked');
                $('#likes_minus_' + target + '_' + vote).data('clicked', false).removeClass('clicked');
                $button.data('clicked', true);
                $button.addClass('clicked');
            },
            success: function(res) {
                if (!res || !res.result) return;

                if (res.result == 'success') {

                    var estimated_count_wrong = new_count_estimated != res.data.rating;

                    if (estimated_count_wrong) {
                        user.placeNewVote( $count, res.data.rating );
                    }

                    if ( res.data.removed ){
                        setTimeout(function(){
                            $button.removeClass('clicked');
                        }, 300);
                    }

                    setTimeout(function(){
                        $button.data('clicked', false);
                    }, 800);

                } else if ( res.result == 'selfie' ) {

                    $button.addClass('wobble');
                    setTimeout(function() {
                        $button.removeClass('clicked wobble');
                        $button.data('clicked', false);
                    }, 300);

                    $count.data('voted', null);

                    mbhn.alerts.warning('Selfie! Здорово, но не серьезно.');

                } else if ( res.result == 'user is too active' ){

                    mbhn.alerts.warning('Притормози. Слишком много оценок.');
                    $button.removeClass('clicked');

                    setTimeout(function(){
                        $button.data('clicked', false);
                    }, 800);

                } else if ( res.result == 'user is too active today' ){

                    mbhn.alerts.warning('Притормози. Слишком много оценок.');
                    $button.removeClass('clicked');

                    setTimeout(function(){
                        $button.data('clicked', false);
                    }, 800);

                } else if ( res.result == 'user is too lazy' ){

                    mbhn.alerts.warning('Похоже, вы тут недавно. Пользователи с низкой активностью не могут ставить отрицательные оценки. Пишите больше комментариев.');
                    $button.removeClass('clicked');

                    setTimeout(function(){
                        $button.data('clicked', false);
                    }, 800);

                } else if ( res.result == 'user rejected' ){

                    mbhn.alerts.error('Воу, полегче! Кажется, ваш аккаунт заблокирован. Подробную информацию смотрите в профиле.');
                    $button.removeClass('clicked');

                } else if ( res.result == 'error' ){

                    mbhn.alerts.error('Что-то пошло не так.');
                    $button.removeClass('clicked');

                    setTimeout(function(){
                        $button.data('clicked', false);
                    }, 800);

                }

                if ( res.result != 'success' ) {
                    user.placeNewVote( $count, $count.data('origin') );
                }

            }
        });
    }

    function placeNewVote ( $count, vote ){

        var $parent = $count.parent('.vote_block');

        user.counter_toggle($count, vote);

        if ( parseInt(vote, 10) < 0 ){
            setTimeout(function(){
                $count.removeClass('zero').addClass('bad');
                $parent.removeClass('zero good').addClass('bad');
            },150);
        } else if ( parseInt(vote, 10) !== 0 ){
            setTimeout(function(){
                $count.removeClass('zero bad');
                $parent.removeClass('zero bad').addClass('good');
            },150);
        } else if ( parseInt(vote, 10) === 0 ){
            setTimeout(function(){
                $count.addClass('zero').removeClass('bad');
                $parent.removeClass('good bad');
            },150);
        }

        return true;

    }

    function mobileLink (url, data){

        if ( window.innerWidth > 900 ) return;

        mbhn.ajax.segue(url, data);

    }

    function inputFilter( event , regexp ) {

        if (event.altKey || event.ctrlKey || event.metaKey || $.inArray(event.keyCode, [37,38,39,40,8]) != -1) return true;

        var input  = event.target,
            supprotsSelection = ['text','search','password','tel','url'],
            sStart, sEnd;

        if ( input.type && supprotsSelection.indexOf(input.type) != -1 ) {
            sStart = input.selectionStart;
            sEnd   = input.selectionEnd;
        }

        regexp = regexp ? new RegExp(regexp) : /[\D]+/g;

        input.value = input.value.replace(regexp ,'' );

        if ( sStart !== undefined ){
            input.setSelectionRange(sStart, sEnd);
        }
    }

    /** Auto-resizes textarea by content height */
    function resizeTextarea (event, textarea) {

        var textarea = textarea || event.target,
            taStyles = window.getComputedStyle(textarea, null),
            paddingTop    = parseInt(taStyles.paddingTop || 0, 10),
            paddingBottom = parseInt(taStyles.paddingBottom || 0, 10),
            newHeight = textarea.scrollHeight - paddingTop - paddingBottom,
            minHeight = textarea.dataset.minHeight || 50,
            maxHeight = 200,
            callback = textarea.dataset.resizeCallback;

        var resultHeight = newHeight > minHeight ? newHeight : minHeight;

        if (resultHeight > maxHeight) {
            return;
        }

        textarea.style.minHeight = resultHeight  + 'px';


        if (callback == 'hangleImTaResize') {
            window._im && window._im.handleChatBottomResize();
        }

    }

    function showHelper ( $this, type ) {

        $('.post_helper').addClass('hide');
        $('#' + type).removeClass('hide');

    }

    function printText ( $element, string , speed) {

        $element.text('');

        var appendLetter = function ($element, letter, i) {
            setTimeout(function() {
                $element.text($element.text() + letter);
            }, (speed || 50) * i);
        };

        for (var i = 0; i < string.length ; i++) {
            appendLetter($element, string[i], i);
        }

    }

    // Public methods & properties. __________________________________________________________________

    function USER(){

        this.saveSettings               = saveSettings;
        this.unlinkSocialAccount        = unlinkSocialAccount;
        this.showEmailSubscribe         = showEmailSubscribe;
        this.subscribeEmailDelivery     = subscribeEmailDelivery;
        this.getEmailDelivery           = getEmailDelivery;
        this.commentsReply              = commentsReply;
        this.getInputLimits             = getInputLimits;
        this.placeScrollUpBlock         = placeScrollUpBlock;
        this.counter_toggle             = counter_toggle;
        this.inputFilter                = inputFilter;
        this.vote                       = vote;
        this.placeNewVote               = placeNewVote;
        this.mobileLink                 = mobileLink;
        this.showHelper                 = showHelper;
        this.printText                  = printText;
        this.checkFieldLength           = checkFieldLength;
        this.resizeTextarea             = resizeTextarea;

        this.isHistoryAPI = 'history'       in window &&
            'pushState'     in window.history &&
            'replaceState'  in window.history;

    }

    return new USER();

}( window );

//ajax requests class
var fsAjax = (function(fsAjax, $){
    fsAjax.pageEvents = function( firstLoad ){

        $('#body').css('min-height', window.innerHeight + 'px');


        window.static_nodes.$scrollUp.css({'opacity': 0, 'visibility' : 'hidden'});

        $('textarea.redactor').each(function(){
            callback.redactor($(this));
        });

        user.getInputLimits($(".limited input, .limited textarea"));


        $("#templateAllowed").each(function(){
            $(this).css('height', $(this).get(0).scrollHeight);
        });

        fsAjax.coolSelects();
        fsAjax.coolCheckboxes();


        $('.injector').each(function(){
            fsAjax.injectBlock($(this));
        });


        /** Enable gallery for post page */
        if ($('.b_body').length) {
            mbhn.gallery.getImages($('.b_body'));
        }

        if ($('#yandex_rtb_R-A-347548-1').length) {
            setTimeout(function() {
                if (document.querySelector('#yandex_rtb_R-A-347548-1').offsetHeight) {
                    mbhn.Fixy.init({
                        el: '#sticky',
                        stick_bottom: '.grid-col--right',
                        add_holder: 0,
                        skipMobile: true
                    });
                }
                else {
                    mbhn.Fixy.init({
                        el: '#second_sticky',
                        stick_bottom: '.grid-col--right',
                        add_holder: 0,
                        skipMobile: true
                    });
                }
            }, 1000);

        }

        $(".autosubmit").bind("change", function(){
            $(this).parents("form").find('input[type=submit]').click();
        });

        $(".limited input, .limited textarea").bind("keydown", function(e){

            var value = $(this).val(),
                wrapperClassName = $(this).data('parentClass') || '.input_text',
                max_length = parseInt( $(this).parent(wrapperClassName).attr("maxlength") , 10 );

            $(this).parent(wrapperClassName).attr("data-limit", max_length - value.length);

            switch (e.keyCode){
                case 13: case 8: case 9: case 46: case 37: case 38: case 39: case 40: return true;
            }

            var limit = value.length ? max_length - value.length : max_length;
            return limit > 0;

        });

        $(".autoResizing").filter(function(){return $(this).data('noBr')}).bind("keydown", function(e){
            if ( e.keyCode == 13 ) return false;
        });
        $(".autoResizing").bind("input", user.resizeTextarea);



        $(".aFileUpload").bind("click", mbhn.transport.buttonCallback);

        /**
         * Shows live-animated blocks by scroll
         */
        mbhn.content.showLiveUpdates();

    };

    fsAjax.injectBlock = function( $wrapper , $new_post_data ){

        var url          = $wrapper.data('injection'),
            loader_class = $wrapper.data('loaderClass') || 'page_loader' ,
            scroll_id    = $wrapper.data('scroll'),
            post_data    = $wrapper.data('postData'),
            callback     = $wrapper.data('callback');

        if ( !url ){
            mbhn.core.log('Failed: no url defined','Injector', 'error');
            return;
        }

        var send = $new_post_data || $.extend( post_data , {injectBlockId: window.our_variables.inject_block_id} );

        mbhn.simpleAjax.call({
            type : 'post',
            async : true,
            data : send,
            url  : url,
            beforeSend : function(){
                window.our_variables.inject_block_id++;
                if (loader_class != 'none') $wrapper.addClass(loader_class);
                $wrapper.empty();
            },
            success : function( response ){

                $wrapper.removeClass(loader_class);

                if ( response.result == 'success' && response.content ){

                    $wrapper.html(response.content);

                    if ( scroll_id ){
                        // setTimeout(function(){scroller.to( $('#comment' + scroll_id) , false )}, 200);
                        setTimeout(function(){
                            mbhn.scroller.to( $('#comment' + scroll_id) );
                        }, 200);
                        $('#comment' + scroll_id).find('.message:first').addClass('highlight_comment');
                    }

                    if ( response.addCommentForm ){
                        var $form = $( $.parseHTML( response.addCommentForm )).filter('*');
                        if ($form) {
                            window.static_nodes.replyForm = $form;
                            mbhn.core.log('Node reply form was updated' , 'Static nodes', 'info');
                        }
                    }

                    if ( callback  ){
                        eval( callback + '();' );
                    }

                }
            }
        });

    };

    fsAjax.injectorTabs = function ( $injector , $newPostData ) {

        var data = $injector.data('postData');

        $injector.data('loaderClass' , 'injector_loader');

        fsAjax.injectBlock( $injector , $.extend( data , $newPostData ) );

    };

    fsAjax.coolSelects = function(){
        var selects = $(".select");
        if( selects.find("select") !== 0 ){
            jQuery.each(selects,function(){
                var hasGroups = ($(this).find("optgroup").length >= 1),
                    text;

                if (hasGroups){
                    text = $(this).children("select").children("optgroup").children("option:selected").html();
                } else {
                    text = $(this).children("select").children("option:selected").html();
                }
                $(this).prepend('<span>'+text+'</span>');
                $(this).children("select").change(function(){
                    var nval = $(this).val();
                    var nval_text = $(this).find("option[value='"+nval+"']").html();
                    $(this).parent(".select").children("span").html(nval_text);
                });
            });
        }
    };

    fsAjax.coolCheckboxes = function(){

        var checkboxes     = $(".checkbox");

        if ( checkboxes.length > 0 ){

            checkboxes.each(function(){

                var checkbox     = $(this),
                    chbx_input   = $(this).find('input[type="checkbox"]'),
                    input_name   = chbx_input.prop('name'),
                    is_fake      = checkbox.data('fake'),
                    is_alone     = checkbox.data('alone'),
                    callback     = checkbox.data('callback'),
                    is_disabled  = chbx_input.prop('disabled'),
                    is_email  = chbx_input.data('email')


                checkbox.bind('click', function(e){

                    if (!is_disabled) {
                        var is_checked = $(this).hasClass("checked");

                        /** Can clearing other checkboxes with same input-name */
                        if (is_alone) {

                            $('input[name="' + input_name + '"]').each(function () {
                                $(this).prop('checked', false);
                                $(this).removeAttr('checked');
                                $(this).parents('.checkbox').removeClass('checked');
                            });

                            chbx_input.attr("checked", "checked");
                            chbx_input.prop("checked", true);
                            $(this).addClass('checked');

                        } else {

                            if (!is_checked) {

                                $(this).find('input[type="checkbox"]').attr("checked", "checked");
                                $(this).addClass('checked');

                            } else {

                                $(this).find('input[type="checkbox"]').removeAttr("checked");
                                $(this).removeClass('checked');

                            }
                        }
                    }
                    else {
                        if (is_email) {
                            mbhn.alerts.error("Для начала нужно указать email и подтвердить его.");
                        }
                    }

                    if ( callback ) { eval(callback) };

                    if ( ! is_fake ){
                        e.stopPropagation();
                        e.stopImmediatePropagation();
                        e.preventDefault();
                    }

                });


            });


        }

    };

    return fsAjax;
})({}, jQuery);

// directajax callback functions
var directajax = (function(directajax,$){

    directajax.submitComment = function ( data , form ){

        form = $( form );

        if (data.message == 'user_missed') {

            var parentBlock = data.parent ? $('.comments_block') : $('.replyFormWrapper');

            if (!parentBlock.data('formPasted')) {
                var auth_form = $( $.parseHTML(data.content) ).filter("*").insertAfter(form);
                parentBlock.data('formPasted', true);
            }

            return;
        }

        directajax.insertNewComment( data.new_comment, data.parent_id , true );

        if (data.first_comment_land) {
            var firstCommentLanding = $( $.parseHTML(data.first_comment_land) ).filter("*").addClass('hide').insertBefore($('.replyFormWrapper'));
            setTimeout(function() {
                firstCommentLanding.toggleClass('hide bounceIn');
                if (data.parent_id !== 0) {
                    mbhn.scroller.to( firstCommentLanding.get(0), 150);
                }
            }, 300);
        }

        form.find('textarea').val('');

        if ( form.hasClass('reply_form') ){
            form.remove();
        }

        $('#noOneComment').remove();

    };

    directajax.insertNewComment = function( commentHtml , id_parent , isSelf){

        var $new_comment          = $( $.parseHTML(commentHtml) ).filter("*"),
            parentComment         = id_parent ? $('#comment' + id_parent) : false,
            parentBlock           = id_parent ? (parentComment.hasClass('children') ? parentComment.parent('.body') : parentComment.children('.body')) : $('.comments_block'),
            newCommentsAlertBlock = parentBlock.next('.new_comments_alert'),
            newCommentsCount      = parseInt(newCommentsAlertBlock.data('count'), 10);

        if ( id_parent ) $new_comment.addClass('children');
        if ( !isSelf   ) $new_comment.addClass('hide');

        if ( parentBlock.data('sort') == 1 ){
            parentBlock.prepend( $new_comment );
        } else {
            parentBlock.append( $new_comment );
        }

        if (!isSelf) {
            newCommentsAlertBlock.data('count', ++newCommentsCount );
            newCommentsAlertBlock.html(
                newCommentsCount.num_decline('новый комментарий', 'новых комментария', 'новых комментариев')
            );
            newCommentsAlertBlock.removeClass('hide');
        }

    };


    return directajax;

})({},jQuery,window);

var callback =  function (callback, window, $) {

    callback.page_scroll = function (event) {

        /** Catches window-scroll stop */
        window.our_variables.scroll_stop_timer && clearTimeout(window.our_variables.scroll_stop_timer);
        window.our_variables.scroll_stop_timer = setTimeout(function() {
            callback.scroll_finished();
        }, 100);

        if ( !window.our_variables.scroll_handler_blocked ){

            window.static_nodes.$scrollUp.removeClass('up');
            window.static_nodes.$scrollUp.data('is_scrolled', true);

            if ( window.scrollY > 200 ){
                window.static_nodes.$scrollUp.css({'opacity': 1, 'visibility' : 'visible'});
            } else if ( !window.static_nodes.$scrollUp.hasClass('up') ) {
                window.static_nodes.$scrollUp.css({'opacity': 0, 'visibility' : 'hidden'});
            }

        }

    };

    callback.scroll_to_top = function (event , is_animate ) {

        is_animate = false;

        var $scrollUp   = window.static_nodes.$scrollUp;
        is_scrolled = $scrollUp.data('is_scrolled'),
            prevY       = is_scrolled ? window.scrollY : $scrollUp.data('prevY'),
            newY        = is_scrolled ? 0 : prevY;

        if ( newY === 0 ){
            $scrollUp.addClass('up');
            prevY = window.scrollY;
        } else {
            $scrollUp.removeClass('up');
            prevY = 0;
        }

        if ( is_animate ){
            $('html').animate({ scrollTop : newY  }, 300, function () {
                setTimeout(function() {
                    $scrollUp.data('is_scrolled', false).data('prevY', prevY );
                    // $scrollUp.addClass('up');
                }, 200);
            });
        } else {
            window.our_variables.scroll_handler_blocked = true;
            $(window).scrollTop( newY );
            setTimeout(function() {
                $scrollUp.data('is_scrolled', false).data('prevY', prevY );
                window.our_variables.scroll_handler_blocked = false;
            }, 100);
        }
    };

    callback.body_resize = function () {
        user.placeScrollUpBlock();
    };

    /** Methods which need to fire after window scroll */
    callback.scroll_finished = function(){

    };

    callback.globalKeyDown = function( e ){

        if (e.keyCode == 80 && (e.ctrlKey || e.metaKey) ){
            window.scrollTo(0,0);
            $('body').toggleClass('admin_aside_opened');
            window.scrollTo(0,0);
            e.preventDefault();
        }

        if (mbhn.gallery.initialized) {
            switch (e.keyCode){
                case 39: mbhn.gallery.next(); break; // arr right
                case 37: mbhn.gallery.prev(); break; // arr left
                case 27: mbhn.gallery.close(); break; // escape
            }
        }

        if (mbhn.popup.initialized && e.keyCode == 27) mbhn.popup.close();
        if (window.our_variables.commentEditMode && e.keyCode == 27) user.cancelEditComments();
    };

    callback.redactor = function ( $textarea , extSettings ){

        var replaced_textarea;

        /**
         * Workaround double-initialization bug with History navigation
         */
        if ( $textarea.attr('data-initialized') ){

            replaced_textarea = $textarea.clone();
            replaced_textarea.css('display',"");

            $textarea.parents('.redactor-box').replaceWith(replaced_textarea);
        }

        var textarea = $textarea,
            type     = textarea.data('type'),
            notFixed = !!textarea.data('notFixed'),
            settings = {
                lang           : 'ru',
                toolbarFixed   : !notFixed,
                minHeight      : 300,
                linkNofollow : true,
                codemirror : true,
                removeEmpty: ['p', 'h1', 'h2', 'h3', 'blockquote', 'br','div', 'ul', 'ol', 'li','a', 'code', 'strong', 'em', 'pre'],
                allowedAttr: [
                    ['img', ['src', 'style', 'class']],
                    ['span', ['class']],
                    ['blockquote', ['class']],
                    ['a', ['href', 'target', 'rel'] ],
                    ['ol', ['start', 'type', 'reversed'] ],
                    ['iframe', ['src', 'style', 'allowfullscreen', 'frameborder', 'width', 'height'] ]
                ],
                replaceTags: [
                    ['i', 'em'],
                    ['b', 'strong'],
                    ['big', 'strong'],
                    ['strike', 'del']
                ],
                cleanStyleOnEnter: true,
                cleanOnPaste: true,
                convertLinksImages: true,
                plugins        : ['video'],
                imageUpload    : '/writing/upload_img_for_old_editor',
                imageUploadErrorCallback : function(response){
                    mbhn.alerts.error(response.error);
                },
                focusCallback : function(e){
                    var focusCallback = this.$element.data('focusCallback');
                    if (focusCallback) eval(focusCallback);
                }

            };

        switch (type){

            case 'new_post' :
                settings.allowedTags   = ['p', 'h2', 'h3', 'blockquote', 'img', 'ul', 'li', 'ol', 'a', 'iframe', 'span', 'strong' , 'em', 'code', 'pre'];
                settings.buttons       = ['formatting', '|','unorderedlist', 'orderedlist', 'image', 'link', 'html'];
                settings.formatting    = ['p', 'h2', 'h3', 'blockquote'];
                settings.formattingAdd = [{
                    tag   : 'pre',
                    title : 'Блок с кодом'
                }, {
                    tag   : 'strong',
                    title : 'Жирность'
                }, {
                    tag   : 'em',
                    title : 'Курсив'
                }];
                break;

            case 'job_text' :
                settings.buttons = ['custom-header','unorderedlist', 'orderedlist', 'link'];
                settings.plugins = ['customHeader'];
                settings.allowedTags = ['p', 'h2', 'h3', 'ul', 'li', 'ol', 'a', 'span'];
                settings.minHeight = 300;
                break

            default:
                settings.allowedTags = ['p', 'h2', 'h3', 'img', 'ul', 'li', 'ol', 'a', 'iframe', 'span', 'strong' , 'em'];
                settings.buttons     = ['formatting', '|', 'unorderedlist', 'orderedlist', 'image', 'link', 'html'];
                settings.formatting    = ['p', 'h2', 'h3'];
        }



        settings = $.extend(settings, extSettings);

        var tabindex = textarea.data('tabIndex');
        if (tabindex) settings.tabindex = tabindex;

        if (!replaced_textarea) {
            textarea.redactor(settings);
            $textarea.attr('data-initialized', 1);

        } else {
            replaced_textarea.redactor(settings);
            replaced_textarea.attr('data-initialized', 1);

        }

        // init codemirror after redactor's call
        var codeMirror = CodeMirror.fromTextArea(textarea.get(0), {
            lineNumbers: true,
            mode: "text/html",
            matchBrackets: true
        });

        codeMirror.on('change', function(){
            codeMirror.save();
        });

    };

    return callback;

} ({}, window, jQuery);


/**
 * @deprecated
 */
window.our_variables = {
    scroll_handler_blocked : false,
    scroll_stop_timer      : null,
    image_processing_turn  : 1,
    selected_item          : {},

    blur_blocked           : false,
    blog_draft_id          : 0,
    scrollToComment        : 0,
    Pocketload             : false,
    JCROPload              : false,
    inject_block_id        : 0,
    updateFeedInterval     : false,
    likesLokerTimeout      : false,
    state                  : {
        url     : location.pathname,
        infListPortion : {} // Object with { original-url : saved-url, original-url-2 : saved-url-2 }
    },
    gaPageReadingTimeout : false // Send 'page' isHere event after 15 sec to decrease bounce-rate

};

window.static_nodes = {};
window.notifiesPolling = null;


mbhn.docReady(function(){

    window.static_nodes = {
        $window         : $(window),
        $document       : $(document),
        $main_content   : $('.main'),
        replyForm       : false,
        $scrollUp       : $('.scroll_up'),
        $transport_form : $("#transport_form")
    };


    // Bindings.

    window.static_nodes.$document.on( "scroll" , callback.page_scroll );
    window.static_nodes.$document.on( "click" , '.scroll_up' , callback.scroll_to_top );
    window.static_nodes.$window.on( "resize" , callback.body_resize );
    window.static_nodes.$window.on( "keydown" , callback.globalKeyDown );


    /** Old page initiator */
    fsAjax.pageEvents(true);

    user.placeScrollUpBlock();
});

/** Object.assign polyfill **/
if (typeof Object.assign != 'function') {
    (function () {
        Object.assign = function (target) {
            'use strict';
            if (target === undefined || target === null) {
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var output = Object(target);
            for (var index = 1; index < arguments.length; index++) {
                var source = arguments[index];
                if (source !== undefined && source !== null) {
                    for (var nextKey in source) {
                        if (source.hasOwnProperty(nextKey)) {
                            output[nextKey] = source[nextKey];
                        }
                    }
                }
            }
            return output;
        };
    })();
}