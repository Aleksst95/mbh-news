var mbhn =
    /******/ (function(modules) { // webpackBootstrap
    /******/ 	// The module cache
    /******/ 	var installedModules = {};

    /******/ 	// The require function
    /******/ 	function __webpack_require__(moduleId) {

        /******/ 		// Check if module is in cache
        /******/ 		if(installedModules[moduleId])
        /******/ 			return installedModules[moduleId].exports;

        /******/ 		// Create a new module (and put it into the cache)
        /******/ 		var module = installedModules[moduleId] = {
            /******/ 			exports: {},
            /******/ 			id: moduleId,
            /******/ 			loaded: false
            /******/ 		};

        /******/ 		// Execute the module function
        /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

        /******/ 		// Flag the module as loaded
        /******/ 		module.loaded = true;

        /******/ 		// Return the exports of the module
        /******/ 		return module.exports;
        /******/ 	}


    /******/ 	// expose the modules object (__webpack_modules__)
    /******/ 	__webpack_require__.m = modules;

    /******/ 	// expose the module cache
    /******/ 	__webpack_require__.c = installedModules;

    /******/ 	// __webpack_public_path__
    /******/ 	__webpack_require__.p = "";

    /******/ 	// Load entry module and return exports
    /******/ 	return __webpack_require__(0);
    /******/ })
/************************************************************************/
/******/ ([
    /* 0 */
    /***/ function(module, exports, __webpack_require__) {

        /**
         * Mbhn client
         */
        var mbhn = (function(mbhn){

            'use strict';

            /**
             * Static nodes cache
             */
            mbhn.nodes = {
                content : null
            };

            mbhn.init = function () {

                /**
                 * Fixy header, dont destroy with ajax-segues
                 */
                mbhn.Fixy.init({
                    el: '#mbhn-site-header',
                    add_holder: 1,
                    persistWithSegues : true,
                    skipMobile : true
                });

                /**
                 * Activate AJAX layout
                 */
                mbhn.ajax.init();


                /**
                 * Add global callbacks
                 */
                window.addEventListener('scroll', mbhn.globals.scroll);
                window.addEventListener('resize', mbhn.globals.resize);


            };

            return mbhn;

        })({});

        /**
         * Document ready handler
         */
        mbhn.docReady = function(f){
            /in/.test(document.readyState) ? setTimeout(mbhn.docReady, 9, f) : f();
        };

        /**
         * Apply polyfills
         */
        __webpack_require__(1);

        /**
         * Load modules
         */
        mbhn.core = __webpack_require__(2);
        mbhn.detect = __webpack_require__(3);
        mbhn.counter = __webpack_require__(4);
        mbhn.content = __webpack_require__(5);
        mbhn.globals = __webpack_require__(6);
        mbhn.supports = __webpack_require__(7);
        mbhn.load = __webpack_require__(8);
        mbhn.ajax = __webpack_require__(9);
        mbhn.pages = __webpack_require__(10);
        mbhn.post = __webpack_require__(11);
        mbhn.errors = __webpack_require__(12);
        mbhn.simpleAjax = __webpack_require__(13);
        mbhn.Fixy = __webpack_require__(14);
        mbhn.GarbageCollector = __webpack_require__(15);
        mbhn.share = __webpack_require__(16);
        mbhn.gallery = __webpack_require__(17);
        mbhn.alerts = __webpack_require__(18);
        mbhn.ImageProcessing = __webpack_require__(19);
        mbhn.scroller = __webpack_require__(20);
        mbhn.transport = __webpack_require__(21);
        mbhn.popup = __webpack_require__(22);

        module.exports = mbhn;

        mbhn.docReady(function(){

            mbhn.init();

        });

        /***/ },
    /* 1 */
    /***/ function(module, exports) {

        Number.prototype.div = function(by){
            return (this - this % by) / by;
        };

        Date.prototype.format = function (timestamp) {

            var hours = this.getHours(),
                mins  = this.getMinutes(),
                now   = new Date();

            hours = hours < 10 ? '0' + hours : hours;
            mins  = mins  < 10 ? '0' + mins  : mins;

            var localeMonth = function ( month ) {

                if ( isNaN(month) ) return false;

                var lang = navigator.language == 'ru' ? 0 : 1,
                    map  = {
                        ru : ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт‚','Ноя','Дек'],
                        en : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                    };

                return lang ? map.en[month] : map.ru[month];

            };

            if ( now.getDate() == this.getDate() ) {
                return hours + ':' + mins;
            } else {
                return this.getDate() + ' ' + localeMonth(this.getMonth()) + ' ' + this.getFullYear() + ' ' + hours + ':' + mins;
            }
        };

        Number.prototype.num_decline = function(nominative, genitive_singular, genitive_plural){

            iNumber = this % 100;

            if ( iNumber >= 11 && iNumber <= 19 ) {
                return this + ' ' + genitive_plural;
            } else {
                switch (iNumber % 10){
                    case (1): return this + ' ' + nominative;
                    case (2): case (3): case (4): return this + ' ' + genitive_singular;
                    case (5): case (6): case (7): case (8): case (9): case (0): return this + ' ' + genitive_plural;
                }
            }
        };

        /**
         * Number.prototype.format(n, x, s, c)
         *
         * @param integer n: length of decimal
         * @param integer x: length of whole part
         * @param mixed   s: sections delimiter
         * @param mixed   c: decimal delimiter
         */
        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };

        String.prototype.wordsCount = function(){
            var str = this;
            //str = str.replace(/[^\s\wР°-СЏРђ-РЇ]/g, "");
            str = str.replace(/(^\s*)|(\s*$)/gi,""); //exclude  start and end white-space
            str = str.replace(/[ ]{2,}/gi," "); //2 or more space to 1
            str = str.replace(/\n /,"\n"); // exclude newline with a start spacing
            return str.split(' ').length;
        };

        /***/ },
    /* 2 */
    /***/ function(module, exports) {

        /**
         * Some significant methods
         * Core
         */
        var core = {

            /** Logging method */
            log : function( str, prefix, type, arg ){

                var static_length = 32;

                if ( prefix ){
                    prefix = prefix.length < static_length ? prefix : prefix.substr( 0, static_length - 2 );

                    while ( prefix.length < static_length - 1 ){
                        prefix += ' ';
                    }

                    prefix += ':';
                    str = prefix + str;
                }

                type = type || 'log';

                try { if ( 'console' in window && window.console[ type ] ){
                    if ( arg ) console[ type ]( str , arg );
                    else console[ type ]( str );
                }}catch(e){}
            },

            /**
             * @return {object} dom element real offset
             */
            getOffset : function ( elem ){

                var docElem, win, rect, doc;

                if ( !elem ) {
                    return;
                }

                /**
                 * Support: IE <=11 only
                 * Running getBoundingClientRect on a
                 * disconnected node in IE throws an error
                 */
                if ( !elem.getClientRects().length ) {
                    return { top: 0, left: 0 };
                }

                rect = elem.getBoundingClientRect();

                /** Make sure element is not hidden (display: none) */
                if ( rect.width || rect.height ) {
                    doc = elem.ownerDocument;
                    win = window;
                    docElem = doc.documentElement;

                    return {
                        top: rect.top + win.pageYOffset - docElem.clientTop,
                        left: rect.left + win.pageXOffset - docElem.clientLeft
                    };
                }

                /** Return zeros for disconnected and hidden elements (gh-2310) */
                return rect;
            },

            /**
             * Checks if element visible on screen at the moment
             * @param {Element} - HTML NodeElement
             */
            isElementOnScreen : function ( el ){

                var elPositon    = mbhn.core.getOffset(el).top,
                    screenBottom = window.scrollY + window.innerHeight;

                return screenBottom > elPositon;

            },

            /**
             * Returns computed css styles for element
             * @param {Element} el
             */
            css : function( el ){

                return window.getComputedStyle(el);

            },

            /**
             * Helper for inserting one element after another
             */
            insertAfter : function (target, element) {
                target.parentNode.insertBefore(element, target.nextSibling);
            },

            /**
             * Replaces node with
             * @param {Element} nodeToReplace
             * @param {Element} replaceWith
             */
            replace : function (nodeToReplace, replaceWith) {
                return nodeToReplace.parentNode.replaceChild(replaceWith, nodeToReplace);
            },

            /**
             * Helper for insert one element before another
             */
            insertBefore : function (target, element) {
                target.parentNode.insertBefore(element, target);
            },


            /**
             * Returns unique named string for element
             * Looks like TAGNAME#id@className:45
             * @param {Element} el
             */
            composeElementIdetity : function(el){

                var tagName   = el.tagName,
                    id        = el.id || '',
                    className = el.className.replace(' ', '+'),
                    random    = Math.floor(Math.random() * 1000) + 10;

                return tagName + (id ? '#' + id : '') + '@' + className + ':' + random;

            },

            /**
             * Returns random {int} between numbers
             */
            random : function(min, max) {

                return Math.floor(Math.random() * (max - min + 1)) + min;

            },

            /**
             * Element.matches workaround
             */
            selectorMatches : function (el, selector) {
                var p = Element.prototype;
                var f = p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || function(s) {
                        return [].indexOf.call(document.querySelectorAll(s), this) !== -1;
                    };
                return f.call(el, selector);
            },

            /**
             * Attach event to Element in parent
             * @param {Element} parentNode    - Element that holds event
             * @param {string} targetSelector - selector to filter target
             * @param {string} eventName      - name of event
             * @param {function} callback     - callback function
             */
            delegateEvent : function (parentNode, targetSelector, eventName, callback ) {

                parentNode.addEventListener(eventName, function(event) {

                    var el = event.target, matched;

                    while ( el && !matched && mbhn.core.isDomNode(el) ){

                        matched = mbhn.core.selectorMatches( el, targetSelector );

                        if ( !matched ) el = el.parentElement;
                    }

                    if (matched) {

                        callback.call( event.target, event, el );

                    }

                }, true);

            },


            /**
             * Readable DOM-node types map
             */
            nodeTypes : {
                TAG     : 1,
                TEXT    : 3,
                COMMENT : 8,
                DOCUMENT_FRAGMENT : 11
            },

            /**
             * Readable keys map
             */
            keys : { BACKSPACE: 8, TAB: 9, ENTER: 13, SHIFT: 16, CTRL: 17, ALT: 18, ESC: 27, SPACE: 32, LEFT: 37, UP: 38, DOWN: 40, RIGHT: 39, DELETE: 46, META: 91 },

            /**
             * @protected
             * Check object for DOM node
             */
            isDomNode : function (el) {
                return el && typeof el === 'object' && el.nodeType && el.nodeType == this.nodeTypes.TAG;
            },

            /**
             * Parses string to nodeList
             * Removes empty text nodes
             * @param {string} inputString
             * @return {array} of nodes
             *
             * Does not supports <tr> and <td> on firts level of inputString
             */

            parseHTML : function (inputString) {

                var contentHolder,
                    childs,
                    parsedNodes = [];

                contentHolder = document.createElement('div');
                contentHolder.innerHTML = inputString.trim();

                childs = contentHolder.childNodes;


                /**
                 * Iterate childNodes and remove empty Text Nodes on first-level
                 */
                for (var i = 0, node; !!(node = childs[i]); i++) {

                    if (node.nodeType == mbhn.core.nodeTypes.TEXT && !node.textContent.trim()) {
                        continue;
                    }

                    parsedNodes.push(node);

                }

                return parsedNodes;

            },

            /**
             * Checks passed object for emptiness
             * @require ES5 - Object.keys
             * @param {object}
             */
            isEmpty : function( obj ) {

                return Object.keys(obj).length === 0;

            },

            /**
             * Check for Element visibility
             * @param {Element} el
             */
            isVisible : function(el) {
                return el.offsetParent !== null;
            },

            setCookie : function (name, value, expires, path, domain){
                var str = name + '='+value;
                if (expires) str += '; expires=' + expires.toGMTString();
                if (path)    str += '; path=' + path;
                if (domain)  str += '; domain=' + domain;
                document.cookie = str;
            },

            getCookie : function(name) {
                var dc = document.cookie;

                var prefix = name + "=";
                var begin = dc.indexOf("; " + prefix);
                if (begin == -1) {
                    begin = dc.indexOf(prefix);
                    if (begin !== 0) return null;
                } else
                    begin += 2;

                var end = document.cookie.indexOf(";", begin);
                if (end == -1) end = dc.length;

                return unescape(dc.substring(begin + prefix.length, end));
            }

        };

        module.exports = core;



        /***/ },
    /* 3 */
    /***/ function(module, exports) {

        /**
         * Browser and platform detection module
         */
        module.exports = (function () {

            // Detect the browser out of list.
            function isOthers_(arr){
                for(var i in arr){
                    if(arr[i]){
                        return false;
                    }
                }
                return true;
            }

            // Event support detect method.
            function isEventSupported( eventName ) {
                if(!eventName) return false;
                var el = document.createElement('div');
                if(!el) return false;
                eventName = 'on' + eventName;
                var isSupported = (eventName in el);
                if (!isSupported) {
                    el.setAttribute(eventName, 'return;');
                    isSupported = typeof el[eventName] == 'function';
                }
                el = null;
                return isSupported;
            }

            var ua = navigator.userAgent.toLowerCase();

            var platform = {
                    'WIN'       : /win/i.test(ua) && !(/windows phone/i.test(ua)),
                    'MAC'       : /macintosh/i.test(ua),
                    'LINUX'     : /linux/i.test(ua),

                    'IPHONE'    : /iphone/i.test(ua),
                    'IPAD'      : /ipad/i.test(ua),
                    'IPOD'      : /ipod/i.test(ua),
                    'ANDROID'   : /android/i.test(ua),
                    'PIKE'      : /pike/i.test(ua),
                    'SYMBIAN'   : /symbian/i.test(ua),
                    'WINPHONE'  : /windows phone/i.test(ua)
                },
                browser = {
                    'IE'        : /msie/i.test(ua) && !(/iemobile/i.test(ua)) && /MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(navigator.userAgent) && parseInt(/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(navigator.userAgent)[1] , 10 ),
                    'OPERA'     : /opera/i.test(ua) && window.opera && !(/opera mobi|opera mini/i.test(ua)),
                    'SAFARI'    : /webkit|safari|khtml/i.test(ua) && !(/chrome|mobile safari/i.test(ua)),
                    'FIREFOX'   : /firefox/i.test(ua),
                    'CHROME'    : /chrome/i.test(ua),
                    'WEBKIT'    : /webkit/i.test(ua),
                    'YANDEX'    : /yabrowser/i.test(ua),
                    'YANDEX_BOT': /yandexbot/i.test(ua),

                    'OPERA_MOBILE'   : /opera mobi/i.test(ua) && window.opera,
                    'OPERA_MINI'     : /opera mini/i.test(ua) && window.opera,
                    'OVI'            : /nokiabrowser/i.test(ua),
                    'UC'             : /ucbrowser|ucweb/i.test(ua),
                    'ANDROID'        : /android/i.test(ua) && /mobile safari/i.test(ua),
                    'SAFARI_MOBILE'  : /webkit|safari|khtml/i.test(ua) && !(/chrome/i.test(ua)) && !(/crios/i.test(ua)),
                    'FIREFOX_MOBILE' : /mobile/i.test(ua) && /firefox/i.test(ua),
                    'CHROME_MOBILE'  : /webkit|safari|khtml/i.test(ua) && (!(/chrome/i.test(ua)) && /crios/i.test(ua) || /chrome/i.test(ua) && /mobile safari/i.test(ua)),
                    'IE_MOBILE'      : /iemobile/i.test(ua),
                    'BLACKBERRY'     : /blackberry/i.test(ua)
                };

            platform.OTHERS = isOthers_(platform);
            browser.OTHERS = isOthers_(browser);

            var isMobile = function() {
                return window.innerWidth < 980;
            };

            return {

                platform : platform,
                browser : browser,
                isMobile : isMobile,
                isEventSupported : isEventSupported,

            };

        })();

        /***/ },
    /* 4 */
    /***/ function(module, exports) {

        module.exports = {

            /**
             * @param {Element} params.el - counter holder element
             * @param {int} params.start - start value of counter
             * @param {int} params.finish - finish value of counter
             */
            animate: function( params ){

                if (!params.el || !params.finish) {
                    mbhn.core.log('Missed required params', 'counter');
                    return;
                }

                var TIME = 1000;

                var start = params.start || 0,
                    step  = 1,
                    gain  = params.finish - start,
                    iteration_time = TIME / gain;

                while (step <= params.finish){
                    this.changeCounterByTimer(params.el, step, iteration_time * step);
                    step = start++;
                }

            },

            changeCounterByTimer : function(el, val, timer){

                setTimeout(function(){

                    el.textContent = val ? val : '';

                }, timer);

            },

        };

        /***/ },
    /* 5 */
    /***/ function(module, exports) {

        /**
         * @module Content
         * Operations with Elements
         */
        module.exports = {

            /**
             * Animatedly shows live-updated blocks by scroll
             */
            showLiveUpdates : function () {

                var items = document.getElementsByName('js-live-update'),
                    item,
                    delay,
                    lifetime,
                    prepare = function ( el ) {
                        el.classList.remove('hide');
                        el.removeAttribute('name');
                    },
                    show    = function ( el ) { el.classList.add('shown'); },
                    hide    = function ( el ) { el.classList.remove('shown'); },
                    finish  = function ( el ) { el.remove(); },
                    animate = function ( el , delay, lifetime ) {

                        prepare(el);

                        setTimeout(function() {

                            show(el);

                            setTimeout(function() {

                                hide(el);

                                setTimeout(function() {

                                    finish(el);

                                }, 500);

                            }, lifetime);

                        }, 500 + delay );

                    };

                for (var i = items.length - 1; i >= 0; i--) {

                    item = items[i];

                    if (mbhn.core.isElementOnScreen(item.parentNode)) {

                        delay    = mbhn.core.random(10, 2500);
                        lifetime = item.dataset.lifetime || 2000;
                        animate(item, delay, parseInt(lifetime, 10));

                    }
                }

            },

            /**
             * Toggles classname on passed blocks
             * @param {string} selector
             * @param {string} toggled classname
             * @param {string} scrollTo - selector of element to scroll after toggling
             */
            toggle : function ( which , marker , scrollTo ) {

                var elements = document.querySelectorAll( which ),
                    scrollToElement = scrollTo ? document.querySelector(scrollTo) : null,
                    toggled;

                for (var i = elements.length - 1; i >= 0; i--) {

                    elements[i].classList.toggle( marker );

                }

                if (scrollToElement && scrollToElement.classList.contains( marker ) ) {
                    mbhn.scroller.to( scrollToElement , 60 , true );
                }

            },


            /**
             * Show next hidden elements
             */
            showNextItems : function (elementsSelector, PORTION, portionCallback , finishCallback ){

                var items = document.querySelectorAll(elementsSelector),
                    itemsHidden = [],
                    loadMoreButton = this;

                for (var i = 0, item; !!(item = items[i]); i++) {

                    if ( item.classList.contains('hide') ){
                        itemsHidden.push(item);
                    }

                }

                itemsHidden.map(function(_item, index ){

                    if ( index >= PORTION ) {

                        return;
                    }

                    _item.classList.remove('hide');


                });

                /**
                 * No more items. Its last portion
                 */
                if (itemsHidden.length <= PORTION && typeof finishCallback == 'function') {

                    loadMoreButton.remove();

                    finishCallback.call(this);

                }

                if (typeof portionCallback == 'function') {
                    portionCallback.call(this);
                }

            },

            /**
             * Shows content hided by 'hide' classname if passed cookie WAS NOT SET
             * @param {string} hidden element id
             * @param {string} cookie name
             * @param {functin} callback fired after showing
             */
            showByCookie : function ( elementId , cookieName, callback ) {

                var elementToShow = document.getElementById(elementId),
                    cookie        = mbhn.core.getCookie(cookieName);

                if (elementToShow && !cookie) {

                    elementToShow.classList.remove('hide');

                    if (typeof callback == 'function') {
                        callback.call(this);
                    }

                }

            },

            /**
             * Onlick handler for 'close block' buttons
             * Hides passed element and sets cookie
             * @param {object} params are:
             *   toHide - ID element need to be hided
             *   cookie - cookie name
             *   cookieLifetime - clising cookie lifetime. Default: 30 days
             *   callback - function fired after element hiding
             */
            hideWithCookie : function function_name(params) {

                var elementToHide  = document.getElementById(params.toHide),
                    cookieLifetime = params.cookieLifetime || 60*60*24 * 30*1000; // defalut 30 days

                if (!elementToHide) {
                    mbhn.core.log('nothing to hide with ID: %o', 'hideByCookie', 'log', params.toHide);
                    return;
                }

                elementToHide.classList.add('hide');

                if (window.mbhn && window.mbhn.core.setCookie){
                    mbhn.core.setCookie(params.cookie, 1, new Date((new Date()).getTime() + cookieLifetime), '/');
                }

                if (typeof params.callback == 'function') {
                    params.callback.call(this);
                }

            },

        };

        /***/ },
    /* 6 */
    /***/ function(module, exports) {

        /**
         * Global callbacks and stuff
         */
        module.exports = {

            /** Timer to detect scroll finish */
            scrollFinishTimeout : null,

            /** Detect window resize */
            resizeFinishTimeout : null,

            /**
             * Window scroll handler
             *
             * @uses    scrollFinishTimeout
             * @fires   mbhn.globals.scrollEnd
             */
            scroll : function () {

                if ( this.scrollFinishTimeout ) clearTimeout(this.scrollFinishTimeout);

                scrollFinishTimeout = setTimeout(function() {

                    mbhn.globals.scrollEnd();

                }, 100);

            },

            /**
             * Fires when window scroll ends
             * @fires mbhn.content.showLiveUpdates
             */
            scrollEnd : function () {

                mbhn.content.showLiveUpdates();
                mbhn.ImageProcessing.loadVisible();

            },

            /**
             * Window scroll handler
             *
             * @uses    scrollFinishTimeout
             * @fires   mbhn.globals.scrollEnd
             */
            resize : function () {

                if ( this.resizeFinishTimeout ) clearTimeout(this.resizeFinishTimeout);

                resizeFinishTimeout = setTimeout(function() {

                    mbhn.globals.resizeEnd();

                }, 300);

            },

            /**
             * Fires when window scroll ends
             * @fires mbhn.content.showLiveUpdates
             */
            resizeEnd : function () {

                /**
                 * Fixy header, dont destroy with ajax-segues
                 */
                mbhn.Fixy.reinit({
                    el: '#mbhn-site-header',
                    add_holder: 1,
                    persistWithSegues : true,
                    skipMobile : true
                });

            },
        };

        /***/ },
    /* 7 */
    /***/ function(module, exports) {

        /**
         * Detect supporting methods
         */
        module.exports = {

            /**
             * Check for native Promise support
             */
            promise : function () {
                return typeof Promise !== "undefined" && Promise.toString().indexOf("[native code]") !== -1;
            },

            /**
             * HTML <template> tags.
             * Not supproted by IE , Opera Mini
             */
            templates: function() {
                return 'content' in document.createElement('template');
            },

            /**
             * Support for localStorage and sessionStorage
             */
            storage : function () {
                return typeof(Storage) !== "undefined";
            }

        };

        /***/ },
    /* 8 */
    /***/ function(module, exports) {

        module.exports = function(){

            var version_name   = 'data-version',
                prefix  = {
                    script  : 'mbhn_script_',
                    css     : 'mbhn_css_'
                };

            function createSCRIPT(){

                var tag = document.createElement( 'script' );

                !! this.loadType && tag.setAttribute( this.loadType, true );

                tag.setAttribute( 'type', 'text/javascript' );
                tag.setAttribute( 'charset',  this.charset || 'windows-1251'); // Safari doesn't work without it correctly

                return tag;
            }

            function createCSS(){

                var tag = document.createElement( 'link' );

                tag.setAttribute( 'type', 'text/css' );
                tag.setAttribute( 'rel', 'stylesheet');

                return tag;
            }

            function appendToHead( tag, type ){

                var firstElementTag = document.getElementsByTagName( type );
                if ( firstElementTag.length ) {
                    firstElementTag[0].parentNode.insertBefore( tag, firstElementTag[0] );
                } else { document.head.appendChild( tag ); }
            }

            function setSOURCE( settings ){

                this.id = settings.id;
                this.onload = settings.callback;
                this.setAttribute( version_name,    settings.version );
                var name;
                switch( this.nodeName ){
                    case 'SCRIPT':
                        this.setAttribute( 'async', !!settings.async );
                        if (settings.defer) this.setAttribute( 'defer' , !!settings.defer );
                        if (settings.loadCallback) this.addEventListener('load' , settings.loadCallback, false);
                        if (settings.crossorigin) {
                            this.setAttribute('crossorigin', settings.crossorigin);
                        }
                        name = 'src';
                        break;
                    case 'LINK':
                        name = 'href';
                        break;
                }
                this.setAttribute( name, settings.url );
            }

            function createNode( old, settings ){

                !! old && old.parentNode.removeChild( old );

                var tag;

                switch( this.valueOf() ){
                    case 'script':
                        tag = createSCRIPT.call( settings );
                        setSOURCE.call( tag, settings );
                        //appendToHead( tag, 'script' );
                        break;
                    case 'css':
                        tag = createCSS.call( settings );
                        setSOURCE.call( tag, settings );
                        //appendToHead( tag, 'link' );
                        break;
                }
            }

            // Settings are:
            // {
            //      'url'       :   url,
            //      'async'     :   true/false,
            //      'callback'  :   function(){...}
            //      'instance'  :   'instance of the loadings class'
            //      'id'        :   script tag id
            //      'defer'     :   if need defer attr
            //      'version'   :   static script version
            // }
            function getElement( name, settings ){

                if (!settings.instance) mbhn.core.log('Incorrect instance passed: ' + settings.url, 'DYNAMIC LOAD', 'warn');

                settings.id         = prefix[ name ] + settings.instance;
                settings.version    = ! settings.version ? 0 : settings.version;

                var elem = document.getElementById( settings.id ),
                    version;

                if( ! elem ){
                    createNode.call( name, elem, settings );
                    return;
                } else {
                    /** Class already loaded */
                    mbhn.core.log('Class %o already loaded', 'LOAD', 'log', settings.instance);
                    if ( typeof settings.loadCallback === 'function') {
                        settings.loadCallback.call();
                    }
                }

                version = elem.getAttribute( version_name );

                if( ! version || version != settings.version ){
                    createNode.call( name, elem, settings );
                    return;
                } //load

                setTimeout(function() {
                    typeof settings.callback === 'function' && settings.callback();
                }, 400);
            }

            function LOAD () {}

            LOAD.prototype.getScript = function( settings ){
                getElement( 'script', settings );
            };

            LOAD.prototype.getCss = function( settings ){
                getElement( 'css', settings );
            };

            return new LOAD();
        }();

        /***/ },
    /* 9 */
    /***/ function(module, exports) {

        /**
         * @module Ajax layout
         */
        module.exports = (function( window ){

            'use strict';

            /**
             * Public methods & properties
             */
            function _ajax () {

                /** Module status */
                this.initialized = false;

                /** Uses to prevent double submitting */
                this.blocked = false;

                this.state = {

                    url     : location.pathname,
                    content : '',

                    /**
                     * Object stores load-more buttons urls
                     * Supports several appenders on same page
                     * {
	            *   initial-url : saved-url,
	            *   ...,
	            * }
                     */
                    loadMorePortion : {}

                };

                /**
                 * Static nodes cache
                 */
                this.nodes = {
                    content : null,
                    header  : null,
                    loader  : null,
                    metaImage : null,
                    linkImage : null,
                    metaKeywords    : null,
                    metaDescription : null,
                    linkCanonical   : null,
                    linksToHighlight : null
                };

            }

            /**
             * @protected
             * Preparations method
             */
            _ajax.prototype.init = function () {

                /**
                 * Cache static nodes
                 */
                mbhn.ajax.nodes.content   = document.getElementById('mbhn-content');
                mbhn.ajax.nodes.loader    = document.getElementById("mbhn-process-loader");
                mbhn.ajax.nodes.header    = document.getElementById('mbhn-site-header');
                mbhn.ajax.nodes.metaImage = document.getElementById('mbhn-meta-image');
                mbhn.ajax.nodes.linkImage = document.getElementById('mbhn-link-image');
                mbhn.ajax.nodes.metaKeywords    = document.getElementById('mbhn-meta-keywords');
                mbhn.ajax.nodes.metaDescription = document.getElementById('mbhn-meta-description');
                mbhn.ajax.nodes.linkCanonical   = document.getElementById('mbhn-link-canonical');
                mbhn.ajax.nodes.linksToHighlight = document.getElementsByName('js-link-highlightable');

                updateLocalState_({
                    url               : location.pathname + location.search,
                    content           : mbhn.ajax.nodes.content.innerHTML,
                    title             : document.title,
                    sequential_number : history && history.state && history.state.sequential_number || 1
                });

                updateHistoryRecord_();

                mbhn.core.delegateEvent(document, 'a',    'click',  linkClicked_ );
                mbhn.core.delegateEvent(document, 'form', 'submit', formSubmitted_ );

                window.addEventListener('popstate', popstate_ );

                mbhn.ajax.initialized = true;

                /** New page initiator */
                mbhn.pages.init(mbhn.ajax.nodes.content);

            };

            /**
             * @protected
             * Performs page segue
             * @fires private getPage_
             */
            _ajax.prototype.segue = function ( url , data , evt ){

                try {

                    evt = evt || event;

                    /**
                     * Allow opening page in new tab
                     */
                    if ( evt && !data ){

                        if (evt.ctrlKey || evt.metaKey){

                            window.open( url , '_blank' );

                            return false;

                        }

                    }

                } catch (e){}

                getPage_( url , data );

            };



            /**
             * @protected
             * Form submitting dispatcher
             *
             * @uses to handle delegated event from #document to form
             * @param {string|Element} form - form [name]
             * @fires formSubmitted_
             */
            _ajax.prototype.submit = function (form) {

                var event = new Event('submit');

                if ( typeof form == 'string' ) {
                    form = document.forms[form];
                }

                console.assert( form , 'Wrong form passed %o' , form );

                formSubmitted_( event , form );

            };

            /**
             * @protected
             * Loads additional content by click on 'load-more' button
             * @param {object} params = {
	    *   url        : '', // URL of current page
	    *   data       : {},  // json data to send with request
	    *   appendMode : 'before|after'
	    *  }
             */
            _ajax.prototype.loadMore = function ( params ) {

                try {

                    if (event) {

                        event.stopPropagation();
                        event.stopImmediatePropagation();
                        event.preventDefault();

                    }

                } catch (e){}

                var button     = this,
                    initialUrl = button.dataset.url || params.url,
                    page       = 1,
                    data       = params.data || {},
                    appendMode = params.appendMode,
                    blocksWrapper  = params.blocksWrapper ? document.querySelector(params.blocksWrapper) : null,
                    currentPageURL = '',
                    nextPageURL    = '';


                var callbackFunction = button.dataset.callbackFunction || params.callbackFunction,
                    callbackData     = button.dataset.callbackData || params.callbackData;

                /**
                 * Use stored URL if clicked after 'popstate'
                 */
                if ( mbhn.ajax.loadMorePortion && mbhn.ajax.loadMorePortion[initialUrl] ){
                    currentPageURL = mbhn.ajax.loadMorePortion[initialUrl];
                } else {
                    currentPageURL = initialUrl;
                }

                nextPageURL = getNextPageURL_(currentPageURL);

                mbhn.simpleAjax.call({

                    type : data && !mbhn.core.isEmpty(data) ? 'POST' : 'GET',
                    url  : nextPageURL + (!nextPageURL.includes('?') ? '?loader=1' : '&loader=1'),
                    data : data || null,

                    beforeSend : function () {

                        /** Save URL for next requests */
                        button.dataset.url = nextPageURL;

                        button.classList.add('loading');

                    },

                    success : function (response) {

                        button.classList.remove('loading');

                        if ( !response  || !response.content ) {
                            return;
                        }

                        var content  = response.content.trim(),
                            fragment = document.createDocumentFragment(),
                            blocks;

                        if ( content && content != 'empty' ){

                            blocks = mbhn.core.parseHTML( content );

                            /** Iterate with elements */
                            for (var i = 0, node; !!(node = blocks[i]); i++) {

                                fragment.appendChild(node);

                            }

                            mbhn.ImageProcessing.cut( fragment );

                            /**
                             * Save url in state to continue loading after 'popstate' event
                             */
                            mbhn.ajax.state.loadMorePortion[initialUrl] = nextPageURL;

                            if (appendMode == 'before') {

                                /**
                                 * 'before' mode: content will inserted before present items
                                 * Button places on the top of them, so we need to insert fragment after button
                                 */

                                mbhn.core.insertAfter( button, fragment );

                            } else {

                                console.assert(blocksWrapper, 'Pleace pass В«blocksWrapperВ» into a click hanlder');
                                blocksWrapper.appendChild( fragment );
                            }

                            mbhn.ImageProcessing.start();

                            if (typeof callbackFunction == 'function') {
                                window[callbackFunction](callbackData);
                            }

                            /** Removes 'load-more' button if there is no more elements*/
                            if (response.hasMore === false) button.remove();


                        } else {

                            mbhn.core.log('Empty portion. Blocking' , 'Load more callback');
                            button.remove();

                        }

                    }
                });

            };

            /**
             * @protected
             * Updates History record with passed URL
             * @fires updateHistoryRecord_
             */
            _ajax.prototype.updateCurrentState = function ( URL ) {

                updateHistoryRecord_( URL );

            };


            /**
             * @protected
             * Serializes form to query string
             * @param {Element} form to serialize
             * @param {Bool} returnObject
             * @license https://code.google.com/archive/p/form-serialize/
             */
            _ajax.prototype.serialize = function(form, returnObject) {

                if (!form || form.nodeName !== "FORM") {
                    return;
                }

                var i, j, q = [], o = {};

                for (i = form.elements.length - 1; i >= 0; i = i - 1) {

                    if (form.elements[i].name === "") {
                        continue;
                    }

                    switch (form.elements[i].nodeName) {
                        case 'INPUT':
                            switch (form.elements[i].type) {
                                case 'text':
                                case 'email':
                                case 'search':
                                case 'hidden':
                                case 'password':
                                case 'button':
                                case 'reset':
                                case 'submit':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    o[form.elements[i].name] = form.elements[i].value;
                                    break;
                                case 'checkbox':
                                case 'radio':
                                    if (form.elements[i].checked) {
                                        q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                        o[form.elements[i].name] = form.elements[i].value;
                                    }
                                    break;
                                case 'file':
                                    break;
                            }
                            break;
                        case 'TEXTAREA':
                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                            o[form.elements[i].name] = form.elements[i].value;
                            break;
                        case 'SELECT':
                            switch (form.elements[i].type) {
                                case 'select-one':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    o[form.elements[i].name] = form.elements[i].value;
                                    break;
                                case 'select-multiple':
                                    for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
                                        o[form.elements[i].name] = [];
                                        if (form.elements[i].options[j].selected) {
                                            q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
                                            o[form.elements[i].name].push(form.elements[i].options[j].value);
                                        }
                                    }
                                    break;
                            }
                            break;
                        case 'BUTTON':
                            switch (form.elements[i].type) {
                                case 'reset':
                                case 'submit':
                                case 'button':
                                    q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
                                    o[form.elements[i].name] = form.elements[i].value;
                                    break;
                            }
                            break;
                    }
                }

                return returnObject ? o : q.join("&");
            };


            /**
             * @private
             * Increments page number in URL string
             * @param {string} URL of current page
             */
            function getNextPageURL_ ( URL ) {

                if ( URL == '/' ){
                    return '/2';
                }

                var urlParts = URL.match( /\w+/gi ),
                    lastPart = urlParts[urlParts.length - 1],
                    urlWithoutPage,
                    page;

                /**
                 * If URL endings with number
                 */
                if ( parseFloat(lastPart) ){

                    page = lastPart;
                    urlWithoutPage = URL.substring(0, URL.length - page.length);

                } else {

                    page =  1;
                    urlWithoutPage = URL;

                }

                page++;

                if (URL.indexOf('&p=')) {
                    return urlWithoutPage + page;
                }

                return urlWithoutPage + (urlWithoutPage.substr(-1) != '/' ? '/' : '') + page;

            }

            /**
             * @private
             * <A> click handler
             */
            function linkClicked_(event, link) {

                var url = link.getAttribute('href');
                url = url ? url.toLowerCase() : '';

                /**
                 * Skip incorrect links
                 */
                if (!url || (url == '#') || /javascript:/.test(url) ) return;

                var isExternal = link.target == "_blank" || /(?:ht|f)tp/.test( url.substr(0,4) ) || /\/\//.test( url.substr(0,2) ),
                    isMailto   = /mailto/.test( url.substr(0,6) ),
                    isFileLink = false,
                    hostRx     = new RegExp('^(?:ht|f)tp:\/\/' + location.host),
                    isMouseWheelClicked = event.which && ( event.which == 2 || event.button == 4 ),
                    fileExtension;

                /**
                 * Allow opening page in new tab
                 */
                if (event.ctrlKey || event.metaKey || isMouseWheelClicked) return;

                /**
                 * Checking for internal links with protocol
                 */
                if ( isExternal && hostRx.test(url) ){

                    isExternal = false;

                }

                /**
                 * Check for file link
                 */
                fileExtension = url.match(/\.(\w+)($|#|\?)/);
                isFileLink    = fileExtension && /(png|jpg|gif|txt|doc|docx|mp3|torrent|rar|zip)/.test(fileExtension[1]);


                if ( isExternal || isFileLink || isMailto ){

                    link.target = "_blank";
                    /**
                     * Workaround window.opener vulnerability
                     * @see https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/
                     */
                    link.setAttribute('rel', 'noopener noreferrer nofollow');
                    return;

                }

                /**
                 * Skip links marked with 'ajaxfree' or 'directLink'
                 */
                if (link.classList.contains("ajaxfree") || link.classList.contains('directLink')){
                    return;
                }

                /**
                 * 'Directajax' method support
                 */
                if ( link.classList.contains("directajax") ){

                    event.stopPropagation();
                    event.stopImmediatePropagation();
                    event.preventDefault();

                    var directajaxHandler = link.dataset.callback;

                    console.assert( directajaxHandler , 'Directajax handler was not passed with data-callback' );
                    console.assert( directajax[directajaxHandler] , 'Wrong handler passed with data-callback');

                    directajax[directajaxHandler](link);

                    return false;

                }

                var postData = link.dataset.postData,
                    postDataParsed;

                if (postData) {
                    try{
                        postDataParsed = JSON.parse(postData);
                    } catch (error){
                        mbhn.core.log('Incorrect post-data value. Error while parsing %o', 'linkClicked', 'warn', error);
                    }
                }

                if ( !mbhn.ajax.blocked ){

                    mbhn.ajax.state.content = mbhn.ajax.nodes.content.innerHTML;
                    mbhn.ajax.state.offset  = window.scrollY;

                    updateHistoryRecord_();

                    mbhn.ajax.segue( url , postDataParsed || postData || {} );

                    $('#header_search').each(function() {
                        this.value = '';
                    });

                }

                mbhn.ajax.blocked = true;
                setTimeout(function(){ mbhn.ajax.blocked = false; }, 500);

                event.preventDefault();

            }

            /**
             * @protected
             * Forms submitting handler
             */
            function formSubmitted_ ( event, form ) {

                var isFileUploading = form.enctype == 'multipart/form-data',
                    inputs          = form.elements,
                    submitBlocked   = false,
                    firstEmptyRequiredField;

                /**
                 * Check required fields
                 */
                for ( var i = 0, input, value; !!(input = inputs[i]); i++ ) {

                    value = input.value.trim();

                    /**
                     * Remove 'error' classname from empty inputs if they are filled
                     */
                    if ( input.classList.contains('error') && value ){
                        input.classList.remove('error');
                    }

                    /**
                     * Add 'error' classname for empty required fields
                     * And blocks submit
                     */
                    if ( input.required && !value ){

                        input.classList.add('error');
                        submitBlocked = true;

                        if ( !firstEmptyRequiredField ) {
                            firstEmptyRequiredField = input;
                        }
                    }
                }

                /**
                 * Dont submit form with errors
                 */
                if ( submitBlocked ) {

                    mbhn.scroller.to( firstEmptyRequiredField, 50 );
                    event.preventDefault();
                    return false;
                }

                /**
                 * Prevent double sending
                 */
                if ( form.classList.contains("blocked") || mbhn.ajax.blocked ) {

                    event.preventDefault();
                    return false;

                }

                /**
                 * Skip forms marked with 'ajaxfree' class and 'multipart/form-data'
                 */
                if ( form.classList.contains("ajaxfree") || isFileUploading ){

                    return true;

                }

                event.preventDefault();

                /**
                 * Block ajax to prevent double sending
                 */
                mbhn.ajax.blocked = true;

                setTimeout(function(){
                    mbhn.ajax.blocked = false;
                }, 1500);


                var url  = form.getAttribute('action') || location.pathname + location.search,
                    data = mbhn.ajax.serialize( form ),
                    submitButton = form.querySelector('input[type="submit"]');

                /**
                 * 'Directajax' method support
                 */
                if ( form.classList.contains("directajax") ){

                    var directajaxHandler = form.dataset.callback;

                    console.assert( directajax[directajaxHandler] , 'Wrong handler passed with data-callback');

                    mbhn.simpleAjax.call({
                        url: url,
                        type: 'post',
                        data: data,
                        beforeSend: function(){

                            form.classList.add('blocked');
                            submitButton && submitButton.classList.add('loading');

                        },
                        success: function(response){

                            form.classList.remove('blocked');
                            submitButton && submitButton.classList.remove('loading');

                            var isSucceeded = response.result == 'ok' || response.success;

                            if ( isSucceeded && directajax[directajaxHandler] ){

                                directajax[directajaxHandler]( response.data , form, response );

                            }
                        }
                    });

                    event.preventDefault();
                    return;

                }

                mbhn.ajax.segue( url, data );

                event.preventDefault();

            }

            /**
             * @private
             * History Popstate callback
             * @fires mbhn.ajax.updateLocalState
             * @fires mbhn.ajax.renderPage
             * @fires mbhn.GarbageCollector.kill
             */
            function popstate_(event) {

                var savedState = event.state;

                if (!savedState) return;

                mbhn.GarbageCollector.kill();

                if (!savedState.content) {

                    mbhn.ajax.segue( location.pathname + location.search );

                } else {

                    updateLocalState_( savedState );
                    renderPage_();

                }
            }


            /**
             * @private
             * Save current page state in history
             */
            function updateHistoryRecord_( url ) {

                mbhn.ajax.state.url = url || mbhn.ajax.state.url || document.URL;

                /** Workaround with firefox state limit of 640kB. We save it without content and it will be requested from the server */
                if ( mbhn.ajax.state.content.length > 320000 ) delete mbhn.ajax.state.content;

                window.history.replaceState(
                    mbhn.ajax.state || {},
                    document.title,
                    mbhn.ajax.state.url
                );

                // mbhn.core.log( 'updateHistoryRecord_ new state %o', 'ajax' ,'log', mbhn.ajax.state );

            }

            /**
             * @private
             * Collects data from server and push a new state.
             * @param {object} state - new state
             * @param {string} url - new page url
             * @param {string} title - new page title
             */
            function addHistoryRecord_ ( state, url, title ) {

                if (state === undefined || url === undefined) return;

                state.sequential_number = ++state.sequential_number;
                title = title ? title : document.title;

                /** Workaround with firefox state limit. We save it without content and it will be requested from the server */
                if ( state.content && state.content.length > 320000 ) delete state.content;

                window.history.pushState(
                    state || {},
                    title,
                    url
                );
            }


            /**
             * @private
             * Updates window state object
             * @param {object} state data
             */
            function updateLocalState_ ( data ) {

                if (data === undefined) return {};

                var state = mbhn.ajax.state;

                state = state || {};

                state.content           = data.content.trim();
                state.url               = data.url;
                state.title             = data.title || document.title;
                state.header            = data.header || '';
                state.seo_image         = data.seo_image || '';
                state.keywords          = data.keywords || '';
                state.description       = data.description || '';
                state.canonical         = data.canonical || '';
                state.top_line          = data.top_line || '';
                state.meta              = data.meta || {};
                state.styles            = data.styles || [];
                state.clientStats       = data.clientStats || {};
                state.infListPortion    = data.infListPortion || {};


                state.offset            = data.offset || 0;
                state.sequential_number = data.sequential_number || state.sequential_number;

                return state;
            }

            /**
             * @private
             * Sends request for passed URL
             * @param {string} url
             * @param {object} data to send
             * @todo remove closures
             */
            function getPage_ ( url , data ){

                var loader = mbhn.ajax.nodes.loader,
                    previousPageURL = location.pathname;

                mbhn.simpleAjax.call({
                    url  : url,
                    type : data && !mbhn.core.isEmpty(data) ? 'POST' : 'GET',
                    data : data || {},
                    dataType: 'json',
                    beforeSend: function(){

                        if ( loader ) {
                            loader.classList.add('active');
                        }

                        mbhn.GarbageCollector.kill();

                        console.time('Fetching');

                    },
                    error: function(jqXHR, textStatus, errorThrown){

                        mbhn.core.log("Ajax error", 'Ajax', 'error');

                        if ( loader ) {
                            loader.classList.add('finished');
                            setTimeout(function(){ loader.classList.remove('finished', 'active'); }, 800);
                        }

                        mbhn.alerts.error('Error loading page');

                    },
                    success: function(response){

                        console.timeEnd('Fetching');

                        console.time('Handle response time');

                        if ( loader ) {

                            loader.classList.add('finished');
                            setTimeout(function(){ loader.classList.remove('finished', 'active'); }, 550);

                        }

                        mbhn.ajax.blocked = false;

                        if ( response.prepend || response.append || response.before || response.after ){

                            var el;

                            if (response.prepend) el = $($.parseHTML(response.content, document, true)).filter("*").prependTo( response.prepend );
                            if (response.append)  el = $($.parseHTML(response.content, document, true)).filter("*").appendTo( response.append );
                            if (response.before)  el = $($.parseHTML(response.content, document, true)).filter("*").insertBefore( response.before );
                            if (response.after)   el = $($.parseHTML(response.content, document, true)).filter("*").insertAfter( response.after );

                        } else {

                            var state = updateLocalState_( response );

                            renderPage_();

                            /**
                             * Sometimes state.content may be empty (if it > 320k, 4ex),
                             * so AddHistoryRecord should trigger after page is rendered
                             */
                            addHistoryRecord_( state , state.url || url, state.title );
                        }

                        console.timeEnd('Handle response time');

                    }
                });

            }

            /**
             * @private
             * Page initialization event
             *
             * @fires private evaluateScripts_
             * @fires private processing.replaceSrc
             * @fires private processing.catchImages
             * @fires private updateMetaTags_
             * @fires private appendStylesheets_
             */
            function renderPage_ (){

                console.time('page-render');

                var state = mbhn.ajax.state,
                    contentNodes  = mbhn.core.parseHTML(state.content),
                    html,
                    header,
                    scripts;

                /**
                 * Workaround case where in state.content we have several nodes:
                 * AdFox branding, AdFox shifter and site content (#wrapper)
                 */
                for (var i = contentNodes.length - 1; i >= 0; i--) {
                    if (contentNodes[i].id == 'wrapper'){
                        html = contentNodes[i];
                    }
                }

                console.assert( html, 'Empty content in state %o', state.content);

                /**
                 * Actions before HTML append
                 */
                mbhn.ImageProcessing.cut( html );

                scripts = html.getElementsByTagName('SCRIPT');

                if ( state.styles ) {
                    appendStylesheets_(state.styles);
                }


                console.time('append');

                mbhn.ajax.nodes.content.innerHTML = '';
                mbhn.ajax.nodes.content.appendChild(html);

                if ( window.uid && window.uid < 3 ) {
                    console.assert( !scripts.length , "Avoid to use inline scripts:" , scripts);
                }

                if (scripts) {
                    evaluateScripts_(scripts);
                }

                console.timeEnd('append');

                mbhn.ImageProcessing.start();

                /**
                 * Scroll page after some delay
                 */
                setTimeout(scrollPage_.bind(null, state.offset), 0);

                document.title = state.title;

                if ( state.header ) {

                    header = mbhn.core.parseHTML(state.header)[0];

                    console.assert( header , 'Wrong header passed', header);

                    mbhn.core.replace(mbhn.ajax.nodes.header, header);

                }

                updateMetaTags_();

                if ( !mbhn.core.isEmpty(state.clientStats) ) {
                    mbhn.stats.hit(JSON.stringify(state.clientStats));
                }

                switchCurrentTab_();

                /** Empty old image gallery */
                mbhn.gallery.sources = [];
                mbhn.gallery.position = 0;

                /** Old page initiator */
                fsAjax.pageEvents( false );

                /** New page initiator */
                mbhn.pages.init(html);

                console.timeEnd('page-render');

            }

            /**
             * @private
             * Scrolls page to offset or to the top
             * @param {int} offset
             */
            function scrollPage_ (offset) {

                window.scrollTo( 0 , offset && !isNaN(offset) ? offset : 0 );

            }

            /**
             * @private
             * Calls passed scripts on page and saves theirs position in DOM
             * @param {nodeList}
             */
            function evaluateScripts_ ( scripts ) {

                [].forEach.call( scripts , function(script) {

                    var newScript = document.createElement('SCRIPT');

                    newScript.innerHTML = script.innerHTML;

                    if (script.src) {
                        newScript.src = script.src;
                    }

                    mbhn.core.replace(script, newScript);

                });

            }

            /**
             * @private
             * Updates meta tags with current state
             */
            function updateMetaTags_ () {

                var state = mbhn.ajax.state,
                    metaTags,
                    tagToAppend,
                    field;

                if (state.seo_image) {

                    mbhn.ajax.nodes.metaImage.content = state.seo_image;
                    mbhn.ajax.nodes.linkImage.href    = state.seo_image;

                }

                if (state.keywords)    mbhn.ajax.nodes.metaKeywords.content = state.keywords;
                if (state.description) mbhn.ajax.nodes.metaKeywords.content = state.description;
                if (state.canonical && mbhn.ajax.nodes.linkCanonical){
                    mbhn.ajax.nodes.linkCanonical.href = state.canonical;
                }

                metaTags = document.head.getElementsByTagName('META');

                [].forEach.call(metaTags, function( tag ) {

                    if ( tag.dataset.ajaxMetaTag ) tag.remove();

                });

                if (mbhn.core.isEmpty(state.meta)) {
                    return;
                }

                ['name', 'property'].forEach(function( value ) {

                    if ( ! mbhn.core.isEmpty(state.meta[value]) ) {

                        for (field in state.meta[value]){
                            tagToAppend = document.createElement('META');

                            tagToAppend.name    = field;
                            tagToAppend.content = state.meta[value][field];

                            tagToAppend.dataset.ajaxMetaTag = true;

                            document.head.appendChild(tagToAppend);
                        }
                    }
                });
            }

            /**
             * @private
             * Highlights active header-tab by page url
             */
            function switchCurrentTab_ () {

                /**
                 * Remove first '/' and all after second '/'
                 * '/jobs' -> 'jobs'
                 * '/user/2' -> 'user'
                 */
                var url_parts = mbhn.ajax.state.url.split('/'),
                    page       = url_parts[1],
                    links     = mbhn.ajax.nodes.linksToHighlight;

                if ( page == 'blogs' ) {
                    page = url_parts[2];
                }

                if ( page == 'startups-popular' || page == 'startups-new' ) {
                    page = 'startups';
                }



                if (!page) {
                    page = 'general';
                }

                [].forEach.call(links, function(link) {

                    link.classList.remove('active');

                    if ( page && link.href.includes(page) && page != 'startup' ){
                        link.classList.add('active');
                    }

                });

            }

            /**
             * @private
             * Append new stylesheets to <head>
             * @uses native js, Array.map
             */
            function appendStylesheets_ (stylesheets){

                var presentSheets  = document.getElementsByTagName('LINK');

                stylesheets.map(function(style){

                    var isAlreadyAdded = false,
                        newStyle;

                    /** Check if style already appended */
                    for (var i = presentSheets.length - 1; i >= 0; i--) {
                        if (presentSheets[i].href.includes(style)) {
                            isAlreadyAdded = true;
                        }
                    }

                    if (isAlreadyAdded) {
                        return false;
                    }

                    newStyle = document.createElement('LINK');

                    newStyle.href = style;
                    newStyle.type = 'text/css';
                    newStyle.rel  = 'stylesheet';
                    newStyle.dataset.ajaxStylesheet = true;

                    document.head.appendChild(newStyle);

                });
            }

            return new _ajax();

        })( window );

        /***/ },
    /* 10 */
    /***/ function(module, exports) {

        /**
         * Pages methods
         */
        module.exports = {

            /**
             * Mark current visited page
             * 'articlePage', 'indexPage', 'job' and other
             */
            currentPage : null,

            /**
             * All page initiator
             * Fire events and other
             * @param {Element} page
             */
            init : function( page ){

                /**
                 * Find and fire inline handlers
                 */
                this.initPageHandlers(page);

                /**
                 * Overall pages method
                 */
                this.global(page);


            },

            /**
             * Clear page prorties
             */
            clear : function( ){

                mbhn.pages.currentPage = null;

            },

            /**
             * Overall pages method
             */
            global: function( page ) {

                /**
                 * From submititng by CTRL+ENTER in textarea or input
                 */
                var fastSubmitElements = page.querySelectorAll('.js-fast-submit');
                if ( fastSubmitElements.length ) {
                    for (var i = fastSubmitElements.length - 1; i >= 0; i--) {
                        fastSubmitElements[i].addEventListener('keydown', this.submitByCtrlEnter);
                    }
                }

            },

            /**
             * Fires inline page handlers stored in <div data-js-handler="__method__"></div>
             */
            initPageHandlers : function( page ){

                var pageHandlers = page.querySelectorAll('[data-js-handler]');

                for (var i = 0, handler, args; i < pageHandlers.length; i++) {

                    handler = pageHandlers[i].dataset.jsHandler;
                    args    = pageHandlers[i].dataset.jsArguments;

                    if (this[handler]) {
                        this[handler].call( pageHandlers[i] , args );
                    }

                }

            },

            /**
             * Index page methods
             */
            indexPage : function( args ){

                /**
                 * Save current page name
                 */
                mbhn.pages.currentPage = 'articlesList';

                try {

                    args = JSON.parse(args);

                } catch (e){

                    mbhn.core.log('Invalid arguments: %o', 'indexPage', 'warn', e);
                    args = {};

                }

                mbhn.core.log('index page methods');


            },

            /**
             * Article page methods
             */
            articlePage : function(){

                /**
                 * Save current page name
                 */
                mbhn.pages.currentPage = 'articlePage';
            },

            /**
             * Sets elements stickiness
             * @fires mbhn.Fixy.init
             * @param {object} args - argument from data-js-arguments
             */
            initFixy : function( args ){

                var fixyParams = JSON.parse(args);

                mbhn.Fixy.init( fixyParams );

            },


            /**
             * Keydown handler to submit form by CMD+ENTER key
             * @fires mbhn.ajax.submit
             */
            submitByCtrlEnter: function(event){

                var ctrlPressed = event.metaKey || event.ctrlKey;

                if ( event.keyCode != mbhn.core.keys.ENTER || !ctrlPressed ) {

                    return;

                }

                if ( event.target && event.target.form ) {

                    mbhn.ajax.submit(event.target.form);
                }

            }

        };

        /***/ },
    /* 11 */
    /***/ function(module, exports) {

        /**
         * Post form client
         */
        module.exports = (function(){

            var _post = function(){};

            /**
             * Post saver
             * @param {Element} button   submit post or saving draft
             * @param {Bool} is_draft    pass true to save draft
             */
            _post.prototype.save = function( button , is_draft ){

                if ( button.dataset.clicked ){
                    mbhn.core.log('Double click', 'post.save', 'warn');
                    return;
                }

                var form         = document.forms['js-post-form'],
                    postIdInput  = document.getElementById('js-post-id'),
                    data         = mbhn.ajax.serialize(form, true),
                    required     = form.dataset.required,
                    submitURL    = form.getAttribute('action'),
                    fieldMissed  = false;

                if ( required ) {

                    required = JSON.parse(required);

                    required.map(function( name ){

                        var input = form.querySelector('[name="' + name + '"]'),
                            value = input.value;

                        if ( !value.trim() ) {

                            fieldMissed = true;
                            input.classList.add('missed');
                            input.focus();

                            if ( input.classList.contains('redactor') ) {
                                $( input ).redactor('focus.setStart');
                            }

                            mbhn.scroller.to( input , 30 );
                        }

                    });

                    /**
                     * Check for required fields filled
                     */
                    if ( fieldMissed ) {

                        mbhn.alerts.error('Заполните недостающие поля');

                        return;

                    }

                }

                if ( is_draft ) {

                    data.is_draft = 1;

                }

                button.dataset.clicked = true;

                mbhn.simpleAjax.call({

                    url  : submitURL,
                    data : data,
                    type : 'post',
                    beforeSend: function () {

                        button.classList.add('loading');

                    },
                    success : function( response ){

                        setTimeout(function() {
                            delete button.dataset.clicked;
                        }, 1000);

                        button.classList.remove('loading');

                        if ( response.success ){

                            var draftSavedMessage = document.getElementById('js-post-draft-saved'),
                                postLink = draftSavedMessage && draftSavedMessage.querySelector('a');

                            if ( response.inserted_id ){

                                postIdInput.value = response.inserted_id;

                                if ( postLink ) {
                                    postLink.setAttribute('href' , response.redirect);
                                }

                            }

                            if ( !is_draft ){

                                mbhn.ajax.segue( response.redirect || href );

                            }
                            else {
                                draftSavedMessage.innerHTML = response.message;

                                draftSavedMessage.classList.remove('hide');
                                draftSavedMessage.classList.add('bounceIn');

                                setTimeout(function() {
                                    draftSavedMessage.classList.remove('bounceIn');
                                }, 1000);

                            }
                        }
                        else {

                            button.classList.add('wobble');

                            setTimeout(function() {
                                button.classList.remove('wobble');
                            }, 1000);

                            if (response.error_type && response.error_type == 'REACHES_LIMIT'){
                                mbhn.alerts.error('Слишком частые публикации. Сохраните черновик и попробуйте позднее');
                            } else if (response.message) {
                                mbhn.alerts.error(response.message);
                            } else {
                                mbhn.alerts.error('Ошибка при сохранении записи. Попробуйте позднее');
                            }
                        }

                    }
                });
            };

            /**
             * Post saver
             * @param {Element} button   submit post or saving draft
             * @param {boolean} is_draft    pass true to save draft
             */
            _post.prototype.save_new = function( is_draft, outputData ){

                var form         = document.forms['js-post-form'],
                    postIdInput  = document.getElementById('js-post-id'),
                    data         = mbhn.ajax.serialize(form, true),
                    submitURL    = form.getAttribute('action');


                if ( is_draft ) {
                    data.is_draft = 1;
                }

                data.data_editor = outputData;

                mbhn.simpleAjax.call({

                    url  : submitURL,
                    data : data,
                    type : 'post',
                    success : function( response ){

                        if ( response.success ){

                            var draftSavedMessage = document.getElementById('js-post-draft-saved'),
                                postLink = draftSavedMessage && draftSavedMessage.querySelector('a');

                            if ( response.inserted_id ){

                                postIdInput.value = response.inserted_id;

                                if ( postLink ) {
                                    postLink.setAttribute('href' , response.redirect);
                                }

                            }

                            if ( !is_draft ){

                                mbhn.ajax.segue( response.redirect || href );

                            }
                            else {
                                draftSavedMessage.innerHTML = response.message;

                                draftSavedMessage.classList.remove('hide');
                                draftSavedMessage.classList.add('bounceIn');

                                setTimeout(function() {
                                    draftSavedMessage.classList.remove('bounceIn');
                                }, 1000);

                            }
                        }
                        else {

                            if (response.error_type && response.error_type == 'REACHES_LIMIT'){
                                mbhn.alerts.error('Слишком частые публикации. Сохраните черновик и попробуйте позднее');
                            } else if (response.message) {
                                mbhn.alerts.error(response.message);
                            } else {
                                mbhn.alerts.error('Ошибка при сохранении записи. Попробуйте позднее');
                            }
                        }

                    }
                });
            };

            /**
             * AJAX post deletion route
             * @param {Number} target    post target
             * @param {Number} id        post ID
             * @param {String} callback  function name in callbacks
             */
            _post.prototype.delete = function ( target , id, callback ) {

                var callbacks = {

                    underPost : function () {

                        var confirmation      = document.querySelectorAll('.confirm_deletion'),
                            postRemovingButton = document.getElementById('js-post-removing-button'),
                            recoverButton      = document.getElementById('js-post-recovering-button');

                        for (var i = confirmation.length - 1; i >= 0; i--) {

                            confirmation[i].classList.remove('bounceIn');
                            confirmation[i].classList.add('hide');
                        }

                        postRemovingButton.classList.add('hide');
                        recoverButton.classList.remove('hide');
                    },

                    /**
                     * Removing draft from list
                     */
                    inDraftList : function () {

                        var draftListItem = document.getElementById('draft_' + id);

                        if ( draftListItem ) {
                            draftListItem.remove();
                        }


                    }

                };

                mbhn.simpleAjax.call({
                    url  : '/writing/delete',
                    data : {target: target, id: id},
                    success : function ( response ) {

                        if ( response.success ) {
                            mbhn.alerts.success('Порядок, запись удалена.');
                        } else {
                            mbhn.alerts.error('Не получилось удалить запись.');
                        }

                        if ( callback && callbacks[callback] ) {
                            callbacks[callback].call(this);
                        }

                    }
                });

            };


            /**
             * AJAX post recovering route
             * @param {Number} target    post target
             * @param {Number} id        post ID
             */
            _post.prototype.recover = function ( target , id ) {

                mbhn.simpleAjax.call({
                    url  : '/writing/post_recover',
                    data : {target: target, id: id},
                    success : function ( response ) {

                        if ( response.success ) {
                            mbhn.alerts.success('Все нормально, запись восстановлена.');
                            mbhn.ajax.segue('/' + id);
                        } else {
                            mbhn.alerts.error('Не получилось восстановить запись.');
                        }
                    }
                });

            };

            /**
             * AJAX post publication route
             * @param {Number} target    post target
             * @param {Number} id        post ID
             */
            _post.prototype.publish = function ( target , id ) {

                mbhn.simpleAjax.call({
                    url  : '/writing/publish',
                    data : {target: target, id: id},
                    success : function ( response ) {

                        if ( response.success ){
                            mbhn.alerts.success('Запись опубликована. Отличная работа!');
                            mbhn.ajax.segue('/' + id);
                        } else {
                            mbhn.alerts.error('Публикация не удалась');
                        }

                    }
                });

            };

            /**
             * AJAX post recovering route
             * @param {Number} target    post target
             * @param {Number} id        post ID
             */
            _post.prototype.moderate = function ( target , id ) {

                mbhn.simpleAjax.call({
                    url  : '/writing/moderate',
                    data : {target: target, id: id},
                    success : function ( response ) {

                        if ( response.success ) {
                            mbhn.alerts.success('Все нормально. Запись отправлена на модерацию.');
                            mbhn.ajax.segue('/view/' + id);
                        } else {
                            mbhn.alerts.error('Не получилось отправить запись на модерацию.');
                        }

                    }
                });

            };

            /**
             * AJAX post rising method
             */
            _post.prototype.rise = function ( button, params ){

                console.log("button: %o", button);

                if (!params.postId) {
                    mbhn.core.log('Wrong params passed: %o', 'Post rise', 'warn', params);
                    return;
                }

                mbhn.simpleAjax.call({
                    url: '/ajax/postRise',
                    data: { postId: params.postId, rise: params.rise },
                    beforeSend: function(){

                        button.classList.add('bg-loader');

                    },
                    success: function(res){

                        if (res.success == '1') {

                            if (res.rised == '1') {
                                mbhn.alerts.success('Запись закреплена. Материал будет отображаться над остальными записями вашего блога');
                            } else {
                                mbhn.alerts.success('Запись откреплена. Теперь она окажется на своем месте');
                            }

                            mbhn.ajax.segue(document.location.pathname);

                        } else {
                            mbhn.alerts.error('Ошибка. Что-то не сработало и это очень странно.');
                        }

                        button.classList.remove('bg-loader');
                    }
                });

            };

            return new _post();

        })();

        /***/ },
    /* 12 */
    /***/ function(module, exports, __webpack_require__) {

        var _detect = __webpack_require__(3);

        module.exports = (function (errors) {

            /**
             * @protected
             * Window on error handler
             * @param {string} message of error.
             * @param {string } source - URL of the script where the error was raised
             * @param {number} lineno - Line number where error was raised
             * @param {number } colno - Column number for the line where the error occurred)
             * @param {Object} error -  Error Object
             */
            errors.catched = function (message, source, lineno, colno, error) {

                if (!message) {
                    mbhn.core.log('Error trace skipped: no message in arguments %o', arguments);
                    return;
                }

                var errData = {};

                errData.message   = message;
                errData.source    = source;
                errData.lineno    = lineno || 0;
                errData.colno     = colno || 0;
                errData.useragent = navigator.userAgent;

                errData.url = location.href;

                if (error && error.stack) {
                    errData.stack = error.stack;
                }

                errData.browser  = getBrowser_();
                errData.platform = getPlatform_();
                errData.isMobile = _detect.isMobile() ? 1 : 0;
                errData.windowWidth  = window.innerWidth;
                errData.windowHeight = window.innerHeight;


                mbhn.simpleAjax.call({ url : 'log', data: errData });

            };

            /**
             * @private
             * @uses user.browser object
             */
            function getBrowser_() {
                for ( var name in _detect.browser  ){
                    if (_detect.browser[name]) return name;
                }
            }
            /**
             * @private
             * @uses user.platform object
             */
            function getPlatform_() {
                for ( var name in _detect.platform  ){
                    if (_detect.platform[name]) return name;
                }
            }

            return errors;

        })({});

        /***/ },
    /* 13 */
    /***/ function(module, exports) {

        // simple ajax request // NOT for editing !!
        module.exports = (function(simpleAjax,$,gl){
            simpleAjax.call = function(sett){
                var _before   = sett.beforeSend || function(){},
                    _success  = sett.success    || function(){},
                    _complete = sett.complete   || function(){},
                    _error    = sett.error      || function(){},
                    _type     = sett.type       || 'post',
                    tp = {
                        type : _type,
                        beforeSend: function(jqXHR, settings){
                            _before.apply(gl,[jqXHR, settings]);
                        },
                        success: function(data, textStatus, jqXHR){
                            _success.apply(gl,[data, textStatus, jqXHR]);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            _error.apply(gl,[jqXHR, textStatus, errorThrown]);
                        },
                        complete: function(jqXHR, textStatus){
                            _complete.apply(gl,[jqXHR, textStatus]);
                        }
                    };
                return $.ajax($.extend(sett,tp));
            };
            return simpleAjax;
        })({},jQuery,window);

        /***/ },
    /* 14 */
    /***/ function(module, exports, __webpack_require__) {

        var _detect = __webpack_require__(3);

        /**
         * Sticky elements module
         * @uses native js
         */
        var Fixy = (function(Fixy){

            /**
             * Object storing active events on elements
             * @var {object} events { selector : handler , ... }
             */
            Fixy.events = {};

            /**
             * Initialization
             * Does not supports Internet Explorer
             * @param {object} settings
             *   settings.el {string|Element} - xpath selector to sticky element or Element instead
             *   settings.stick_bottom {string} - xpath selector to stick bottom element
             *   settings.add_holder {bool} should add elements placeholder to prevent jumping content under sticky element
             * @param {bool} settings.skipMobile
             */
            Fixy.init = function( settings ){

                if (_detect.browser.IE ) {
                    return;
                }

                var elementIdentity,
                    el;

                if ( settings.skipMobile && mbhn.detect.isMobile() ) {
                    return;
                }

                if (mbhn.core.isDomNode(settings.el)){
                    el = settings.el;
                    elementIdentity = mbhn.core.composeElementIdetity(el);
                } else {
                    el = settings.el ? document.querySelector(settings.el) : null;
                    elementIdentity = settings.el;
                }

                if (!el) {
                    // mbhn.core.log('Element not found by selector: %o', 'Fixy', 'warn', settings.el || 'undefined');
                    return;
                }

                if (mbhn.Fixy.events[elementIdentity]) {
                    mbhn.core.log('Already initialized on %o', 'Fixy', 'log', settings.el);
                    mbhn.Fixy.reinit(settings);
                    return;
                }

                var previouslyAddedHolders,
                    placeholder;

                /**
                 * Make placeholder if need
                 */
                if ( settings.add_holder ) {

                    /**
                     * Remove present placeholders saved in state
                     */
                    previouslyAddedHolders = document.getElementsByName('fixy-placeholder@' + elementIdentity);

                    for (var i = 0, holder; !!(holder = previouslyAddedHolders[i] ); i++) {
                        holder.remove();
                    }

                    placeholder = this.addPlaceholder(el, elementIdentity);

                }



                /**
                 * Make new anonimous function to save it in memory
                 * We can remove this handler on destroy
                 */
                var scrollHandler = function(){

                    Fixy.setClassAndPosition(elementIdentity);

                };

                window.addEventListener('scroll', scrollHandler, false);

                var block_style  = mbhn.core.css(el),
                    block_padding_left  = block_style.paddingLeft || 0,
                    block_padding_right = block_style.paddingRight || 0;

                Fixy.events[elementIdentity] = {
                    el              : el,
                    stick_bottom_el : document.querySelector(settings.stick_bottom),
                    event           : scrollHandler,
                    placeholder     : placeholder,
                    offset          : mbhn.core.getOffset(el),
                    height          : el.offsetHeight,
                    width           : el.offsetWidth - parseInt(block_padding_left, 10) - parseInt(block_padding_right, 10),
                    initialSettings : settings,
                    persistWithSegues : settings.persistWithSegues || false
                };

                /** Get correct position on initialization */
                this.setClassAndPosition(elementIdentity);

                // mbhn.core.log('initialized on %o', 'Fixy', 'log', settings.el);

            };

            /**
             * Scroll handler
             * Calculates element's positon and style
             * @param {string} el_selector - selector of element passed on init()
             */
            Fixy.setClassAndPosition = function (el_selector) {

                var cur = this.events[el_selector];

                if (!cur) {
                    mbhn.core.log('element wasn\'t found: %o', 'Fixy', 'log', el_selector);
                    return;
                }

                var stick_position_top       = false,
                    stick_postiton_botttom   = false,
                    stick_bottom_offset      = false,
                    FIXED_HEADER_HEIGHT = 50,
                    FIXED_TOP_MARGIN = 20,
                    pageYOffsetWithFixedHeader = window.pageYOffset + FIXED_HEADER_HEIGHT + FIXED_TOP_MARGIN;

                if (cur.stick_bottom_el) {

                    stick_bottom_offset = mbhn.core.getOffset(cur.stick_bottom_el).top;

                    if (stick_bottom_offset) {
                        stick_position_top = stick_bottom_offset + cur.stick_bottom_el.offsetHeight;
                    }

                    stick_postiton_botttom = stick_position_top - cur.height;

                }

                if ( cur.stick_bottom_el && pageYOffsetWithFixedHeader >= stick_postiton_botttom) {

                    cur.el.classList.remove('fixy_top');
                    cur.el.classList.add('fixy_stick_bottom');

                    cur.el.style.width = cur.width + 'px';

                    if (cur.placeholder) cur.placeholder.style.display = 'block';

                } else if ( pageYOffsetWithFixedHeader >= cur.offset.top ){

                    cur.el.classList.remove('fixy_stick_bottom');
                    cur.el.classList.add('fixy_top');

                    cur.el.style.width = cur.width + 'px';

                    if (cur.placeholder) cur.placeholder.style.display = 'block';

                } else {

                    cur.el.classList.remove('fixy_stick_bottom', 'fixy_top');
                    cur.el.style.width = 'auto';

                    if (cur.placeholder) cur.placeholder.style.display = 'none';

                }
            };

            /**
             * Adds palceholder to prevent page jumping when element goes fixed;
             * @param {Element} target - sticked element
             */
            Fixy.addPlaceholder = function(target, element){

                if (Fixy.events[element] && Fixy.events[element].placeholder) {
                    return Fixy.events[element].placeholder;
                }

                var placeholder = document.createElement('div');

                placeholder.style.height  = target.offsetHeight + 'px';
                placeholder.style.display = 'none';

                placeholder.setAttribute('name', 'fixy-placeholder@' + element);

                mbhn.core.insertAfter(target, placeholder);

                return placeholder;

            };

            Fixy.destroy = function (el) {

                var event = mbhn.Fixy.events[el];

                if (!event) {
                    mbhn.core.log('Fixy doesn\'t initalized on: %o', 'Fixy.destroy', 'log', el);
                    return;
                }

                event.el.classList.remove('fixy_top', 'fixy_stick_bottom');
                event.el.style.width = 'auto';

                if ( event.placeholder ) {

                    event.placeholder.remove();

                }

                window.removeEventListener('scroll', mbhn.Fixy.events[el].event);

                delete mbhn.Fixy.events[el];

                // mbhn.core.log('Destroyed from %o', 'Fixy', 'log', el);

            };

            Fixy.destroyAll = function(){

                if (!mbhn.Fixy.events) return;

                for (var el in mbhn.Fixy.events){

                    /**
                     * If block marked with persistWithSegues, dont destoy it with AJAX-segues
                     */
                    if ( mbhn.Fixy.events[el].persistWithSegues ){
                        continue;
                    }

                    Fixy.destroy(el);
                }

            };

            Fixy.reinit = function (settings) {

                mbhn.core.log('Reinitiaziation... %o', 'Fixy', 'log', settings.el);

                mbhn.Fixy.destroy(settings.el);
                mbhn.Fixy.init(settings);

            };

            /**
             * Reinitializes all sticky elements
             * Use when document height changed after Fixy was initialized
             */
            Fixy.reinitAll = function () {

                var events = mbhn.Fixy.events;

                if (!events) return;

                for (var item in events) {

                    Fixy.reinit(events[item].initialSettings);
                }

            };

            return Fixy;

        })({});

        module.exports = Fixy;

        /***/ },
    /* 15 */
    /***/ function(module, exports) {

        /**
         * Mbhn garbage collector
         */
        var GarbageCollector = (function(GarbageCollector){

            GarbageCollector.kill = function(){


                window.our_variables.inject_block_id  = 0;




                /**
                 * Clear image processing queue
                 */
                mbhn.ImageProcessing.clearQueue();

                /**
                 * Clear page properties
                 */
                mbhn.pages.clear();

                /**
                 * Desrtoy all fixed sticky elements
                 */
                mbhn.Fixy.destroyAll();
            };

            return GarbageCollector;

        })({});

        module.exports = GarbageCollector;

        /***/ },
    /* 16 */
    /***/ function(module, exports) {

        var share = (function( share ){

            var types = {
                vk : 1,
                fb : 2,
                tw : 3,
                gp : 4,
                tg : 5
            };

            share.vk = function(current_count, purl, ptitle, pimg, text, $this) {

                $this = $this || false;

                url  = 'https://vk.com/share.php?';
                url += 'url='          + encodeURIComponent(purl);
                url += '&title='       + encodeURIComponent(ptitle);
                url += '&description=' + encodeURIComponent(text);
                url += '&image='       + encodeURIComponent(pimg);
                url += '&noparse=true';
                share.popup(current_count, url, types.vk, $this, purl);
            };

            share.fb = function(current_count, purl, ptitle, pimg, text, $this) {

                $this = $this || false;

                url = 'https://www.facebook.com/dialog/share?';
                url += 'app_id=' + 333455297189145;
                url += '&display=popup';
                url += '&href=' + encodeURIComponent(purl);
                url += '&redirect_uri=' + encodeURIComponent(purl);

                share.popup(current_count, url, types.fb, $this, purl);

            };
            share.tw = function(current_count, purl, image, text , hashtags, $this) {

                $this = $this || false;

                url  = 'https://twitter.com/share?';
                url += 'text='      + encodeURIComponent(text);
                url += '&url='      + encodeURIComponent(purl);
                url += '&hashtags=' + hashtags;
                url += '&counturl=' + encodeURIComponent(purl);

                share.popup(current_count, url, types.tw, $this, purl);
            };
            share.gp = function(current_count, purl, $this) {

                $this = $this || false;

                url  = 'https://plus.google.com/share?';
                url += '&url='      + encodeURIComponent(purl);

                share.popup(current_count, url, types.gp, $this, purl);
            };

            share.tg = function(current_count, purl, image, text, $this) {

                $this = $this || false;

                url  = 'https://telegram.me/share/url?text=' + encodeURIComponent(text);
                url += '&url='      + encodeURIComponent(purl);

                share.popup(current_count, url, types.tg, $this, purl);

            };

            share.popup = function(current_count, url, social_type, $buttonObj, page_url) {

                window.open( url , '' , 'toolbar=0,status=0,width=626,height=436' );

                if ( !$buttonObj ) return;

                if ( !page_url ){
                    mbhn.core.log( 'page id (url) missed' , 'Share', 'info' );
                    return;
                } else {
                    mbhn.simpleAjax.call({
                        url  : '/social_share',
                        data : {
                            page_url: page_url,
                            type: social_type,
                        },
                        success: function (response) {
                            if ( response.counters ) {
                                share.updateCounters(response.counters);
                            }
                        }
                    });
                }
            };

            /**
             * Working with counters object
             * @param counters : {"vk": 0, "twitter": 0, "facebook": 0, "pocket": 0, "google": 0 }
             */
            share.updateCounters = function (counters){

                for (var service in counters){

                    var buttons  = $('.social_sharing').children('.' + service);

                    if (buttons.length) buttons.each(function(){

                        /**
                         * In Twitter there are <a> tag in .count
                         */
                        var $count = service != 'twitter' ? $(this).children('.count') : $(this).children('.count').children('a');

                        var prevVal = parseInt( $count.text(), 10),
                            newVal  = parseInt( counters[service] , 10 );

                        if ( prevVal != newVal && !isNaN(newVal)){
                            $count.html( newVal );
                        }

                    });
                }
            };

            return share;

        })({});

        module.exports = share;

        /***/ },
    /* 17 */
    /***/ function(module, exports) {

        var gallery = (function(gallery, $){

            gallery.initialized = false;

            gallery.sources = [];
            gallery.position = 0;

            /** To handle only scaled images set onlyBig to TRUE  */
            gallery.getImages = function ( $parent, onlyBig, clearSources ){

                $parent.each(function(){

                    var images  = $(this).find('img');

                    images.each(function(){

                        /** Skip image inside links */
                        if ( $(this).parent('a').length ) return;

                        var $parent       = $(this).parents('.img-block')
                        gallery.getImg($(this), onlyBig);

                    });

                });

            };

            gallery.getImg = function($img, onlyBig, preventPropagation){

                if ($img.data('galleryAdded')) return;

                $img.data('galleryAdded', true);

                gallery.sources.push($img.attr('src'));

                var img           = $img.get(0),
                    $parent       = $img.parents('.img-block'),
                    width         = img.offsetWidth,
                    height        = img.offsetHeight,
                    naturalWidth  = img.naturalWidth,
                    naturalHeight = img.naturalHeight,
                    wK = 1, hK = 1;

                wK = naturalWidth / width;
                hK = naturalHeight / height;
                if (!onlyBig || (wK > 1.2 || hK > 1.2)){

                    $parent.html($img);
                    $img.wrap('<div class="img_resizable" onclick="mbhn.gallery.init($(this));' + (preventPropagation ? 'event.preventDefault(); event.stopImmediatePropagation(); event.stopPropagation();' : '' ) + '"/>');

                }

            };

            gallery.init = function($this){

                var src = $this.children('img').attr('src');

                gallery.initialized = true;

                for (var i = 0; i < gallery.sources.length; i++) {
                    if (src == gallery.sources[i]){
                        gallery.position = i; break;
                    }
                }

                gallery.show();

            };

            gallery.show = function(){

                var viewerWrap = $('.photo_viewer_wrap'),
                    viewer     = $('.photo_viewer').empty(),
                    img        = $('<img  />').attr('src', gallery.sources[gallery.position]).appendTo(viewer).bind('load', function(){
                        gallery.setPosition($(this));
                        viewerWrap.removeClass('hide');
                    }).bind('click' , function(){
                        gallery.next();
                    }).bind('error' , function(){
                        mbhn.alerts.error('Image not loaded');
                        gallery.close();
                    });

            };

            gallery.next = function(){

                gallery.position++;

                if ( gallery.position >= gallery.sources.length ) gallery.position = 0;

                gallery.show();

            };

            gallery.prev = function(){

                gallery.position--;

                if ( gallery.position < 0 ) gallery.position = gallery.sources.length - 1;

                gallery.show();

            };

            gallery.setPosition = function(img){

                var viewer        = $('.photo_viewer'),
                    nav_left      = $('.photo_viewer_wrap .nav_l'),
                    windowMargins = user.isMobile ? 0 : 50,
                    maxWidth      = $(window).width()  - windowMargins * 2,
                    maxHeight     = $(window).height() - windowMargins * 2,
                    imgWidth      = img.get(0).naturalWidth,
                    imgHeight     = img.get(0).naturalHeight,
                    orientation   = imgWidth > imgHeight ? 'horizontal' : 'vertical',
                    k             = imgWidth / imgHeight,
                    width, height;

                if (orientation == 'horizontal') {

                    width  = imgWidth > maxWidth ? maxWidth : imgWidth;
                    height = width / k;


                } else {

                    height = imgHeight > maxHeight ? maxHeight : imgHeight;
                    width  = height * k;

                }

                viewer.css({
                    'width'      : width + 'px',
                    'height'     : height + 'px',
                    'marginTop'  : Math.round(-height / 2) + 'px',
                    'marginLeft' : Math.round(-width / 2) + 'px',
                });

                nav_left.css({
                    'width' : $(window).width() / 2 - width / 2 + 'px'
                });

            };

            gallery.close = function(){

                $('.photo_viewer_wrap').addClass('hide');
                // gallery.sources = [];
                gallery.position = 0;

                $('.photo_viewer').children('img').remove();

                gallery.initialized = false;

            };

            gallery.openImage = function () {

                var currentImage = $('.photo_viewer').children('img');

                if (currentImage.length) {
                    var src = currentImage.attr('src');
                    var tab = window.open(src,'_blank');
                    tab.focus();
                }

            };

            return gallery;

        })({}, $);

        module.exports = gallery;

        /***/ },
    /* 18 */
    /***/ function(module, exports) {

        var alerts = (function(alerts){

            /**
             * @var {Element} mainWrapper
             * Creates once
             */
            alerts.mainWrapper = null;

            /**
             * @var {Element} success alerts wrapper
             * Creates once
             */
            alerts.successAlertsWrapper = null;

            /**
             * @var {Element} warning alerts wrapper
             * Creates once
             */
            alerts.warningAlertsWrapper = null;

            /**
             * @var {Element} error alerts wrapper
             * Creates once
             */
            alerts.errorAlertsWrapper = null;

            /**
             * Shows client success alert
             * @param {string} text      Message text
             */
            alerts.success = function ( text ){

                createMainWrapper_();

                var wrapper = (alerts.successAlertsWrapper || createSuccessAlertsWrapper_()).cloneNode(true);
                wrapper.onclick = function() {
                    $(this).addClass("alert--swiped");
                };

                alerts.mainWrapper.appendChild(wrapper);

                var textBlock  = document.createElement('p');

                wrapper.innerHTML = '';

                textBlock.innerHTML = text;
                wrapper.appendChild(textBlock);

                wrapper.classList.remove('alert--hidden');
                wrapper.classList.add('alert--shown');

                setTimeout(function(){

                    wrapper.classList.remove('alert--shown');
                    wrapper.classList.add('alert--hidden');

                    wrapper.remove();

                }, 6000);

            };

            /**
             * Shows client warning alert
             * @param {string} text      Message text
             */
            alerts.warning = function ( text ){

                createMainWrapper_();

                var wrapper = (alerts.warningAlertsWrapper || createWarningAlertsWrapper_()).cloneNode();
                wrapper.onclick = function() {
                    $(this).addClass("alert--swiped");
                };

                alerts.mainWrapper.appendChild(wrapper);

                var textBlock  = document.createElement('p');

                wrapper.innerHTML = '';

                textBlock.innerHTML = text;
                wrapper.appendChild(textBlock);

                wrapper.classList.remove('alert--hidden');
                wrapper.classList.add('alert--shown');

                setTimeout(function(){

                    wrapper.classList.remove('alert--shown');
                    wrapper.classList.add('alert--hidden');

                    wrapper.remove();

                }, 6000);

            };

            /**
             * Shows client error alert
             * @param {string} text      Message text
             */
            alerts.error = function ( text ){

                createMainWrapper_();

                var wrapper = (alerts.errorAlertsWrapper || createErrorAlertsWrapper_()).cloneNode();
                wrapper.onclick = function() {
                    $(this).addClass("alert--swiped");
                };

                alerts.mainWrapper.appendChild(wrapper);

                var textBlock  = document.createElement('p');

                wrapper.innerHTML = '';

                textBlock.innerHTML = text;
                wrapper.appendChild(textBlock);

                wrapper.classList.remove('alert--hidden');
                wrapper.classList.add('alert--shown');

                setTimeout(function(){

                    wrapper.classList.remove('alert--shown');
                    wrapper.classList.add('alert--hidden');

                    wrapper.remove();

                }, 6000);

            };



            /**
             * Creates and remembers main wrapper block
             */
            var createMainWrapper_ = function () {

                if (alerts.mainWrapper) {
                    return;
                }

                var mainWrapper = document.createElement('DIV');

                mainWrapper.classList.add('alerts');

                document.body.appendChild(mainWrapper);

                /**
                 * Remember created block
                 */
                alerts.mainWrapper = mainWrapper;

                return mainWrapper;

            };

            /**
             * Creates and remembers success wrapper block
             */
            var createSuccessAlertsWrapper_ = function () {

                if (alerts.successAlertsWrapper) {
                    return;
                }

                var wrapper = document.createElement('DIV');
                wrapper.classList.add('alert');
                wrapper.classList.add('alert--success');
                wrapper.addEventListener('click', alerts.swipeAlert);

                document.body.appendChild(wrapper);

                /**
                 * Remember created block
                 */
                alerts.successAlertsWrapper = wrapper;

                return wrapper;

            };

            /**
             * Creates and remembers warning wrapper block
             */
            var createWarningAlertsWrapper_ = function () {

                if (alerts.warningAlertsWrapper) {
                    return;
                }

                var wrapper = document.createElement('DIV');
                wrapper.classList.add('alert');
                wrapper.classList.add('alert--warning');

                document.body.appendChild(wrapper);

                /**
                 * Remember created block
                 */
                alerts.warningAlertsWrapper = wrapper;

                return wrapper;

            };

            /**
             * Creates and remembers error wrapper block
             */
            var createErrorAlertsWrapper_ = function () {

                if (alerts.errorAlertsWrapper) {
                    return;
                }

                var wrapper = document.createElement('DIV');
                wrapper.classList.add('alert');
                wrapper.classList.add('alert--error');

                document.body.appendChild(wrapper);

                /**
                 * Remember created block
                 */
                alerts.errorAlertsWrapper = wrapper;

                return wrapper;

            };

            return alerts;

        })({});

        module.exports = alerts;


        /***/ },
    /* 19 */
    /***/ function(module, exports) {

        /**
         * Image processing module
         * Loads images after page loading complete
         * Working with visible images on screen
         */
        var ImageProcessing = (function(){

            var IMAGE_PROCESSING = function () {

                this.queue = [];

            };

            /**
             * @protected
             * Replaces 'src' attribute from images
             * Marks images with 'data-processing'
             * @param {Element|DocumentFragment} HTML - DOM Element with images
             */
            IMAGE_PROCESSING.prototype.cut = function ( HTML ) {

                var images, src;

                /**
                 * Supporting document fragments
                 */
                switch ( HTML.nodeType ){

                    case mbhn.core.nodeTypes.DOCUMENT_FRAGMENT :

                        images = HTML.querySelectorAll('img');
                        break;

                    default:

                        images = HTML.getElementsByTagName('img');

                }

                for (var i = 0, image; !!(image = images[i]); i++) {

                    if ( image.classList.contains('no-processing') ) continue;

                    src = image.src;

                    image.removeAttribute( 'src' );
                    image.dataset.src = src;
                    image.dataset.processing = true;

                    image.classList.add('image-preprocessed');

                }

            };

            /**
             * @protected
             * Finds images marked with 'data-processing' and append it to queue
             */
            IMAGE_PROCESSING.prototype.start = function () {

                var images = document.getElementsByTagName('IMG');

                for (var i = 0, image; image = images[i]; i++) {

                    if (!image.dataset.processing) continue;

                    this.queue.push({
                        image  : image,
                        offset : mbhn.core.getOffset(image).top
                    });

                }

                setTimeout(this.loadVisible.apply(this), 1000);

            };

            /**
             * @protected
             * Extract visible onscreen images from queue and loads them
             */
            IMAGE_PROCESSING.prototype.loadVisible = function () {

                var EXTRA_HEIGHT = 500,
                    windowBottom = window.scrollY + window.innerHeight + EXTRA_HEIGHT;

                this.queue.map(function(object, index, _queue){

                    /**
                     * Check if element visible on screen
                     */
                    if ( windowBottom > object.offset ) {

                        object.image.addEventListener('load', imageLoaded_ );

                        object.image.src = object.image.dataset.src;
                        object.image.removeAttribute('data-src');
                        object.image.removeAttribute('data-processing');

                        /** Delete element from queue, but save it in array for correct mapping next iterations */
                        delete _queue[index];
                    }

                });

            };

            /**
             * @protected
             * Deallocates processing queue
             */
            IMAGE_PROCESSING.prototype.clearQueue = function () {

                this.queue = [];

            };


            /**
             * @private
             * Image loading callback
             */
            function imageLoaded_() {

                this.classList.remove('image-preprocessed');

            }

            return new IMAGE_PROCESSING();

        })();

        module.exports = ImageProcessing;

        /***/ },
    /* 20 */
    /***/ function(module, exports) {

        /**
         * Page scroller module
         */
        module.exports = {

            /**
             * Performs scrolling to element
             * @param {Element} DOM element to scroll
             * @param {int} marginTop - scroll will be performed with this margin to element
             */
            to : function( element, marginTop, onlyUpward ){

                var pos = mbhn.core.getOffset( element );

                if (!pos.top) {
                    return;
                }

                if ( onlyUpward && pos.top > window.scrollY){
                    return;
                }

                window.scrollTo( 0 , pos.top  - (marginTop || 0) );

            }

        };

        /***/ },
    /* 21 */
    /***/ function(module, exports) {

        module.exports = (function(transport, $){

            transport.response = function( response ){

                if (response.callback) {

                    mbhn.core.log(response.callback, 'Transport');

                    eval( response.callback );

                }

                if ( response.result ){

                    if ( response.cover_img ) $('.cover_photo').css('backgroundImage', 'url(' + response.cover_img + ')');
                    if ( response.list_img ) $('#list_img_preview').css('backgroundImage', 'url(' + response.list_img + ')');
                    if ( response.index_img ) $('#index_img_preview').css('backgroundImage', 'url(' + response.index_img + ')');

                }
            };

            transport.fill = function( params ){

                if (!params.action) {
                    mbhn.core.log('Action missed', 'Transport', 'warn'); return;
                }

                var $transport_form  = window.static_nodes.$transport_form,
                    $transport_input = window.static_nodes.$transport_input;

                $hiddenAction = $('<input type="hidden" name="action" />').val(params.action).appendTo($transport_form);

                if ( params.inputName   ) $('<input type="hidden" name="name" />').val(params.inputName).appendTo($transport_form);
                if ( params.target_id   ) $('<input type="hidden" name="id" />').val( params.target_id).appendTo($transport_form);
                if ( params.string_id   ) $('<input type="hidden" name="str_id" />').val( params.string_id).appendTo($transport_form);
                if ( params.source_url  ) $('<input type="hidden" name="url" />').val(params.source_url).appendTo($transport_form);
                if ( params.is_multiple ) $transport_input.attr('multiple', 'multiple');

                $transport_form.data('action', params.action);

            };

            transport.buttonCallback = function(event){

                var name          = $(this).data('name'),
                    target_id     = $(this).data('id'),
                    action        = $(this).data('action'),
                    is_multiple   = !!$(this).data('multiple') || false;

                transport.fill({
                    inputName   : name,
                    action      : action,
                    target_id   : target_id,
                    is_multiple : is_multiple
                });

                window.static_nodes.$transport_input.click();

            };

            transport.saveFileByUrl = function( url , action ){

                transport.fill({
                    action : action,
                    source_url : url
                });

                transport.submitCallback();

            };

            transport.submitCallback = function (){
                var $form       = window.static_nodes.$transport_form,
                    $fileInputs = $form.find('input[type="file"]'),
                    action      = $form.data('action'),
                    source_url_input  = $form.find('input[name="url"]');

                // For saving by url
                if (source_url_input && source_url_input.val() ) {
                    window.static_nodes.$transport_form.submit();
                    if ( action == 'post_cover' ) callback.uploadPostCover.beforeSend();
                    return;
                }

                var files = transport.getFileObject( $(this).get(0) );

                for (var i = files.length - 1; i >= 0; i--) {

                    if ( transport.validateExtension(files[i]) && transport.validateMIME(files[i]) ){
                        if ( transport.validateSize( files[i] , 7.5 * 1024 * 1024) ) {

                            if (action != 'post_cover') {

                                window.static_nodes.$transport_form.submit();

                                $("#process_loader").addClass('active');

                            } else {

                                transport.checkImageSize({
                                    file: files[i],
                                    minSizes : {width: 570, height: 290},
                                    success: function(){
                                        callback.uploadPostCover.beforeSend();
                                        window.static_nodes.$transport_form.submit();
                                    },
                                    error: function(){
                                        mbhn.alerts.error('Минимальный размер изображения: 570x290 пикселей');
                                    }
                                });

                            }

                        } else {
                            mbhn.alerts.error('Максимальный размер файла - 7.5MB - ' + (files[i].size / (1000*1000)).toFixed(2) + 'MB');
                        }
                    } else {
                        mbhn.alerts.error('Мы пока не поддерживаем такое расширение: <b>' + files[i].name + '</b>');
                    }
                }

            };


            /**
             * @param {object} params are
             *   file - uploaded file
             *   minSizes - {width: 0, height: 0}
             *   success {function} - callback for success
             *   error {function} - error callback
             */
            transport.checkImageSize = function(params){

                var image = new Image(),
                    url   = window.URL;

                image.src = url.createObjectURL(params.file);

                image.onload = function(){
                    if (image.width >= params.minSizes.width && image.height >= params.minSizes.height){
                        params.success.call();
                    } else {
                        params.error.call();
                    }
                };

                image.onerror = function(){
                    params.error.call();
                };

            };

            transport.responseCallback = function( event ){

                if ( event.currentTarget.contentDocument.title == '413 Request Entity Too Large' ){

                    mbhn.alerts.error('File size reached limit');

                } else if ( event.currentTarget.contentDocument.body.firstChild && event.currentTarget.contentDocument.body.firstChild.attributes[0].value == 'kohana_error' ){

                    mbhn.alerts.error('Something went wrong');
                }

                $("#process_loader").removeClass('active');

                window.static_nodes.$transport_input.val('');
                window.static_nodes.$transport_form.find('input[type="hidden"]').remove();

            };


            transport.getFileObject = function ( fileInput ) {
                if ( !fileInput ) return false;
                return typeof ActiveXObject == "function" ? (new ActiveXObject("Scripting.FileSystemObject")).getFile(fileInput.value) : fileInput.files;
            };

            transport.validateMIME = function ( fileObj , accept ){

                accept = accept instanceof Array ? accept : ['image/jpeg','image/png'];

                for (var i = accept.length - 1; i >= 0; i--) {
                    if ( fileObj.type == accept[i] ) return true;
                }
                return false;

            };

            transport.validateExtension = function( fileObj , accept ){

                var ext = fileObj.name.match(/\.(\w+)($|#|\?)/);
                if (!ext) return false;

                ext = ext[1].toLowerCase();

                accept = accept instanceof Array ? accept : ['jpg','jpeg','png'];

                for (var i = accept.length - 1; i >= 0; i--) {
                    if ( ext == accept[i] ) return true;
                }

                return false;

            };

            transport.validateSize = function ( fileObj , max_size) {
                return fileObj.size < max_size;
            };

            return transport;

        })({}, $);



        /***/ },
    /* 22 */
    /***/ function(module, exports) {

        var popup = function ( popup ) {

            popup.initialized = false;

            popup.params = {};

            popup.show = function ( params ) {

                mbhn.simpleAjax.call({
                    type: 'get',
                    url : params.url,
                    success: function(response){
                        if (response.result == 'ok'){

                            params.title = response.title;
                            params.body = response.body;

                            popup.open(params);

                        } else {
                            mbhn.alerts.error('Error with popup 01');
                        }
                    },
                    error: function(){
                        mbhn.alerts.error('Error with popup 01');
                    }
                });

            };

            popup.open = function  (params) {

                this.params = params;
                $('#popupTitle').text(params.title);

                $('#popupBody').html(params.body);
                $('#popupWrapper').removeClass('hide');

                $('html').addClass('noscroll');

                fsAjax.coolSelects();
                fsAjax.coolCheckboxes();

                popup.initialized = true;

                popup.setPosition(params);

                /// For situations when image in popups loaded with delay
                setTimeout(function() { popup.setPosition(params); }, 200);

                if (params.openCallback && typeof params.openCallback == 'function'){
                    params.openCallback.call();
                }
            };

            popup.setPosition = function (params) {

                var $popup = $('#popup'),
                    $popupTitle = $('#popup > .title'),
                    $popupBody  = $('#popup > .body'),
                    maxHeight = null,
                    popupRealHeight = parseInt($popup.get(0).offsetHeight, 10);

                /** Except mobile version */
                if (window.innerWidth > 950) {
                    maxHeight = window.innerHeight - 2 * (window.innerHeight*0.14);
                }


                var popupHeight = maxHeight || popupRealHeight,
                    popupTitleHeight;

                if (params.overflow && maxHeight) {

                    popupTitleHeight = $popupTitle.get(0).offsetHeight;

                    $popup.css({height: popupHeight + 'px'});
                    $popupBody.css({height: popupHeight - popupTitleHeight + 'px'});
                    $popupBody.scrollTop(0);
                    $popup.addClass('fixed_head');

                } else {

                    $popup.css({height: 'auto'});
                    $popupBody.css({height: 'auto'});
                    $popup.removeClass('fixed_head');

                }

                var isHeightReachedWindow = !params.overflow && popupRealHeight >= window.innerHeight,
                    popupTop              = isHeightReachedWindow ? 0 : '50%',
                    popupMarginTop        = isHeightReachedWindow ? 0 : -1 * popupHeight / 2,
                    popupWidth            = params.width || 400,
                    popupMarginLeft       = -1 * popupWidth / 2;

                $popup.css({
                    top : popupTop,
                    marginTop  : popupMarginTop  + 'px',
                    marginLeft : popupMarginLeft + 'px',
                    width      : popupWidth + 'px'
                });
            };

            popup.close = function (e) {

                $('#popupBody').html('');
                $('#popupTitle').html('');
                $('#popupWrapper').addClass('hide');
                $('html').removeClass('noscroll');

                if (this.params.closeCallback && typeof this.params.closeCallback == 'function'){
                    this.params.closeCallback.call();
                }

                this.initialized = false;
                this.params = {};


            };

            return popup;

        }({});

        module.exports = popup;

        /***/ }
    /******/ ]);
