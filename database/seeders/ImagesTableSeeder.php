<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'images.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            $pathParts = explode('/', $item['path']);
            DB::table('images')->insert(
                [
                    'id' => $item['id'],
                    'user_id' => $item['user_id'],
                    'type' => $item['type'],
                    'path' => $item['path'],
                    'filename' => $pathParts[count($pathParts) - 1],
                    'optimized' => $item['optimized'] ?? 0,
                    'created_at' => $item['datetime'],
                ]
            );
        }
    }
}
