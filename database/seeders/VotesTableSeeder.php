<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VotesTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'votes.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('post_votes')->insert(
                [
                    'user_id' => $item['user_id'],
                    'post_id' => $item['post_id'] + 1,
                    'type' => $item['type'],
                    'created_at' => $item['date_publication']
                ]
            );
        }
    }
}
