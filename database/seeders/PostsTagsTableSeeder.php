<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'postsTags.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $postId => $tags) {
            foreach ($tags as $tagId) {
                DB::table('post_hashtags')->insert(
                    [
                        'post_id' => $postId + 1,
                        'hashtag_id' => $tagId,
                    ]
                );
            }
        }
    }
}
