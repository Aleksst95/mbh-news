<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'users.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('users')->insert([
                'id' => $item['id'],
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'avatar' => $item['avatar'],
                'bdate' => $item['bdate'],
                'sex' => $item['sex'],
                'role' => $item['role'],
                'created_at' => $item['date_register'],
            ]);
        }
    }
}
