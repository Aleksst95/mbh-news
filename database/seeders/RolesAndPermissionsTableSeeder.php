<?php
namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperAdmin = Role::create(['name' => 'Iron Man']);

        $user = User::find(1);
        if (!empty($user)) {
            $user->assignRole($roleSuperAdmin);
        }


        /*
         * Main roles
         */
        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleRedactor = Role::create(['name' => 'Redactor']);

        /*
         * Dashboard permission
         */
        $permission = Permission::create(['name' => 'dashboard all']);
        $permission->assignRole($roleAdmin);

        /*
         * Role permission
         */
        $permission = Permission::create(['name' => 'role view']);
        $permission = Permission::create(['name' => 'role edit']);
        $permission = Permission::create(['name' => 'role delete']);

        /*
         * User permission
         */
        $permission = Permission::create(['name' => 'user view']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'user edit']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'user delete']);
        $permission->assignRole($roleAdmin);

        /*
         * Service-member permission
         */
        $permission = Permission::create(['name' => 'service-member view']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'service-member edit']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'service-member delete']);
        $permission->assignRole($roleAdmin);

        /*
         * Post permission
         */
        $permission = Permission::create(['name' => 'post create']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);
        $permission = Permission::create(['name' => 'post view']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);
        $permission = Permission::create(['name' => 'post edit']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);
        $permission = Permission::create(['name' => 'post delete']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);

        /*
         * Comment permission
         */
        $permission = Permission::create(['name' => 'comment view']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);
        $permission = Permission::create(['name' => 'comment edit']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);
        $permission = Permission::create(['name' => 'comment delete']);
        $permission->assignRole($roleAdmin);
        $permission->assignRole($roleRedactor);

        /*
         * Statistic permission
         */
        $permission = Permission::create(['name' => 'statistic view']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'statistic edit']);
        $permission->assignRole($roleAdmin);

        /*
         * Jobs permission
         */
        $permission = Permission::create(['name' => 'job view']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'job edit']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'job delete']);
        $permission->assignRole($roleAdmin);

        /*
         * Complaint permission
         */
        $permission = Permission::create(['name' => 'complaint view']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'complaint edit']);
        $permission->assignRole($roleAdmin);
        $permission = Permission::create(['name' => 'complaint delete']);
        $permission->assignRole($roleAdmin);
    }
}
