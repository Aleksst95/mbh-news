<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'tags.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('hashtags')->insert([
                'name' => $item
            ]);
        }
    }
}
