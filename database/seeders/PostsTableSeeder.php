<?php
namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'posts.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('posts')->insert(
                [
                    'id' => $item['id'] + 1,
                    'author_id' => $item['author_id'],
                    'title' => $item['title'],
                    'status' => ($item['status'] == 0) ? Post::STATUS_DRAFT :
                        (($item['status'] == 1) ? Post::STATUS_POSTED :
                            (($item['status'] == 2) ? Post::STATUS_ON_CHECK : Post::STATUS_DELETED)
                        ),
                    'slug' => $item['link_name'],
                    'cover_img' => $item['cover_img'],
                    'type' => $item['type'],
                    'description' => $item['description'],

                    'meta_title' => $item['meta_title'],
                    'meta_description' => $item['meta_description'],

                    'content' => null,
                    'content_editor' => json_encode($item['content_editor']),

                    'count_view' => $item['count_view'],
                    'count_comments' => 0,
                    'count_positive_votes' => $item['positive_vote'],
                    'count_negative_votes' => $item['negative_vote'],

                    'posted_at' => $item['date_publication'],
                    'updated_at' => $item['last_edit'],
                    'created_at' => $item['last_edit'],
                ]
            );
        }
    }
}
