<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(EmailsTableSeeder::class);
         $this->call(ImagesTableSeeder::class);
         $this->call(SocialAccountsTableSeeder::class);
         $this->call(PostsTableSeeder::class);
         $this->call(VotesTableSeeder::class);
         $this->call(PostOldUrlsTableSeeder::class);
         $this->call(TagsTableSeeder::class);
         $this->call(PostsTagsTableSeeder::class);
         $this->call(ServiceMembersTableSeeder::class);
         $this->call(RolesAndPermissionsTableSeeder::class);
    }
}
