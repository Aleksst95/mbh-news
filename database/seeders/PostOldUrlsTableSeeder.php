<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostOldUrlsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'postOldUrls.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('post_old_urls')->insert([
                'post_id' => $item['post_id'] + 1,
                'old_slug' => $item['old_link_name'],
                'count_request' => $item['count_request'],
                'last_request' => $item['last_request']
            ]);
        }
    }
}
