<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'emails.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('emails')->insert([
                'id' => $item['id'],
                'email' => $item['email'],
                'user_id' => $item['user_id'],
                'confirmed' => $item['confirmed'],
                'get_delivery' => $item['get_delivery'],
                'unsubscribe_delivery_token' => $item['unsubscribe_delivery_token'],
            ]);
        }
    }
}
