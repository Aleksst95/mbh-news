<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path() . DIRECTORY_SEPARATOR . 'seeders' . DIRECTORY_SEPARATOR . 'socialAccounts.json';

        $items = json_decode(file_get_contents($file), true);

        foreach ($items as $item) {
            DB::table('social_accounts')->insert([
                'user_id' => $item['user_id'],
                'provider' => $item['provider'],
                'provider_id' => $item['provider_id']
            ]);
        }
    }
}
