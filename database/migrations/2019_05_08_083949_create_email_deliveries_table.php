<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->text('date_period')->comment('json');
            $table->text('data')->comment('json');
            $table->timestamp('datetime_send')->nullable();
            $table->tinyInteger('is_send')->unsigned();
            $table->text('full_email_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_deliveries');
    }
}