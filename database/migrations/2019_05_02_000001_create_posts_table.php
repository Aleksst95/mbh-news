<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('author_id')->unsigned();

            $table->string('title', 500);
            $table->string('slug', 500);
            $table->string('description', 700)->nullable();
            $table->string('cover_img', 300)->nullable();

            $table->longText('content')->nullable();
            $table->longText('content_editor')->nullable();

            $table->tinyInteger('status')->unsigned()->default(0);
            $table->tinyInteger('type')->unsigned()->default(0);

            $table->integer('count_view')->unsigned()->default(0);
            $table->integer('count_comments')->unsigned()->default(0);
            $table->integer('count_positive_votes')->unsigned()->default(0);
            $table->integer('count_negative_votes')->unsigned()->default(0);

            $table->string('meta_title', 500)->nullable();
            $table->string('meta_description', 700)->nullable();

            $table->timestamp('posted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}