<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumnConfirmationTokenFromTableEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('emails', 'confirmation_token')) {
            Schema::table(
                'emails',
                function (Blueprint $table) {
                    $table->dropColumn('confirmation_token');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('emails', 'confirmation_token')) {
            Schema::table(
                'emails',
                function (Blueprint $table) {
                    $table->string('confirmation_token', 100)->nullable();
                }
            );
        }
    }
}
