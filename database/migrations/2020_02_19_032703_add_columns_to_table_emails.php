<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTableEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'emails',
            function (Blueprint $table) {
                $table->timestamps();
                $table->softDeletes();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('emails', 'created_at')) {
            Schema::table(
                'emails',
                function (Blueprint $table) {
                    $table->dropColumn('created_at');
                }
            );
        }
        if (Schema::hasColumn('emails', 'updated_at')) {
            Schema::table(
                'emails',
                function (Blueprint $table) {
                    $table->dropColumn('updated_at');
                }
            );
        }
        if (Schema::hasColumn('emails', 'deleted_at')) {
            Schema::table(
                'emails',
                function (Blueprint $table) {
                    $table->dropColumn('deleted_at');
                }
            );
        }
    }
}
