<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticEmailDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic_email_deliveries', function (Blueprint $table) {
            $table->integer('email_delivery_id')->unsigned();
            $table->integer('email_id')->unsigned();
            $table->timestamp('datetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('event_id')->unsigned();
            $table->text('data')->comment('json')->nullable();

            $table->foreign('email_delivery_id')->references('id')->on('email_deliveries')->onDelete('cascade');
            $table->foreign('email_id')->references('id')->on('emails')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('statistic_events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistic_email_deliveries');
    }
}