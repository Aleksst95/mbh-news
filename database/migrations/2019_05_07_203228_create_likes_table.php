<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'likes',
            function (Blueprint $table) {
                $table->increments('id'); // Because Laravel does not support composite keys
                $table->integer('user_id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->tinyInteger('type')->default(1)->comment('dislike: -1, like: 1');

                $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');

                $table->unique(['user_id', 'post_id']);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
