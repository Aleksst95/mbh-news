<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostOldUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_old_urls', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->string('old_slug', 500);
            $table->integer('count_request')->unsigned()->default(0);
            $table->timestamp('last_request')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_old_urls');
    }
}