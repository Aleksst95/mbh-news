<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'post_comments',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->integer('parent_id')->unsigned()->nullable();
                $table->string('message', 2000);
                $table->integer('rating')->unsigned()->default(0);
                $table->smallInteger('left_key')->unsigned();
                $table->smallInteger('right_key')->unsigned();
                $table->smallInteger('level')->unsigned()->default(1);
                $table->smallInteger('node_id')->unsigned();
                $table->tinyInteger('status')->unsigned()->default(1);

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_comments');
    }
}
