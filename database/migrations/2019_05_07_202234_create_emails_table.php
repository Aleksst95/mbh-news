<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 300)->unique();
            $table->integer('user_id')->unsigned()->nullable();
            $table->tinyInteger('confirmed')->unsigned()->default(0);
            $table->tinyInteger('get_delivery')->unsigned()->default(0);
            $table->string('unsubscribe_delivery_token', 100)->nullable();
            $table->string('confirmation_token', 100)->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
