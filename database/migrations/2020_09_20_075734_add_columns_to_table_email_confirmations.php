<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTableEmailConfirmations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('email_confirmations', 'user_id')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('user_id')->unsigned()->nullable();

                    $table->timestamps();
                    $table->softDeletes();

                    $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('email_confirmations', 'id')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->dropColumn('id');
                }
            );
        }
        if (Schema::hasColumn('email_confirmations', 'user_id')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->dropForeign(['user_id']);
                    $table->dropColumn('user_id');
                }
            );
        }
        if (Schema::hasColumn('email_confirmations', 'created_at')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->dropColumn('created_at');
                }
            );
        }
        if (Schema::hasColumn('email_confirmations', 'updated_at')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->dropColumn('updated_at');
                }
            );
        }
        if (Schema::hasColumn('email_confirmations', 'deleted_at')) {
            Schema::table(
                'email_confirmations',
                function (Blueprint $table) {
                    $table->dropColumn('deleted_at');
                }
            );
        }
    }
}
