<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('images', 'hash')) {
            Schema::table(
                'images',
                function (Blueprint $table) {
                    $table->dropColumn('hash');
                }
            );
        }

        if (!Schema::hasColumn('images', 'filename')) {
            Schema::table(
                'images',
                function (Blueprint $table) {
                    $table->string('filename', 256);
                }
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('images', 'filename')) {
            Schema::table(
                'images',
                function (Blueprint $table) {
                    $table->dropColumn('filename');
                }
            );
        }

        if (!Schema::hasColumn('images', 'hash')) {
            Schema::table(
                'images',
                function (Blueprint $table) {
                    $table->string('hash', 512)->nullable();
                }
            );
        }
    }
}
