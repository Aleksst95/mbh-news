<?php

namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author_id' => User::factory()->create()->id,
            'title' => $this->faker->sentence,
            'meta_title' => $this->faker->sentence,
            'slug' => $this->faker->slug,
            'description' => $this->faker->sentence,
            'meta_description' => $this->faker->sentence,
            'status' => Post::STATUS_POSTED,
            'count_positive_votes' => 0,
            'count_negative_votes' => 0,
        ];
    }
}
