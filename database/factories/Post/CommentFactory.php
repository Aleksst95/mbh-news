<?php

namespace Database\Factories\Post;

use App\Models\Post;
use App\Models\Post\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    protected static int $nodeId = 1;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'post_id' => Post::factory()->create()->id,
            'message' => $this->faker->text(),
            'node_id' => self::$nodeId++,
            'level' => 1,
            'left_key' => 1,
            'right_key' => 2,
        ];
    }

    public static function resetNodeId()
    {
        self::$nodeId = 1;
    }
}
