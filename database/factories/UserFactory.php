<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fullNameParts = explode(' ', $this->faker->name);
        $firstName = $fullNameParts[0] ?? 'firstName';
        $lastName = $fullNameParts[1] ?? 'lastName';
        return [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'password' => Hash::make('123456')
        ];
    }
}
