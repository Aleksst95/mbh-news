<?php

namespace Database\Factories\User;

use App\Models\User;
use App\Models\User\SocialAccount;
use Illuminate\Database\Eloquent\Factories\Factory;

class SocialAccountFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SocialAccount::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'provider' => SocialAccount::PROVIDER_NAME_FB,
            'provider_id' => '1234567890'
        ];
    }
}

