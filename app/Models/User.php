<?php

namespace App\Models;

use App\Models\Post\Comment;
use App\Models\Post\Complaint;
use App\Models\Post\Vote;
use App\Models\User\Email;
use App\Models\User\SocialAccount;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @SWG\Definition (
 *  definition="User",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="first_name",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="last_name",
 *      type="string"
 *  )
 * )
 * @property int $id
 * @property string|null $password
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $avatar
 * @property string|null $bdate
 * @property int $sex
 * @property int $role
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property string|null $remember_token
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Collection|Email[] $allEmails
 * @property-read int|null $all_emails_count
 * @property-read Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read Collection|Complaint[] $complaints
 * @property-read int|null $complaints_count
 * @property-read Email|null $confirmedEmail
 * @property-read Collection|Image[] $images
 * @property-read int|null $images_count
 * @property-read string $full_name
 * @property-read string $sex_name
 * @property-read Collection|Vote[] $votes
 * @property-read int|null $votes_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|Post[] $posts
 * @property-read int|null $posts_count
 * @property-read Collection|Post[] $postsCheck
 * @property-read int|null $posts_check_count
 * @property-read Collection|Post[] $postsDraft
 * @property-read int|null $posts_draft_count
 * @property-read Collection|Post[] $postsPosted
 * @property-read int|null $posts_posted_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @property-read ServiceMember|null $serviceMember
 * @property-read Collection|SocialAccount[] $socials
 * @property-read int|null $socials_count
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User permission($permissions)
 * @method static Builder|User query()
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereBdate($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereSex($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 * @property-read Collection|Post[] $bookmarks
 * @property-read int|null $bookmarks_count
 */
class User extends Authenticatable
{
    use HasRoles, Notifiable, SoftDeletes, LogsActivity, HasApiTokens;
    use HasFactory;

    protected static $logName = 'user';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $logAttributes = ['*', 'roles.name'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public const ROLE_USUAL_USER = 0;
    public const ROLE_USUAL_AUTHOR = 10;
    public const ROLE_MODERATOR = 50;
    public const ROLE_ADMIN = 100;
    public const ROLE_IRON_MAN = 1000;

    public const SEX_UNKNOWN = 0;
    public const SEX_FEMALE = 1;
    public const SEX_MALE = 2;


    public function getFullNameAttribute(): string
    {
        return $this->attributes['last_name'] . ' ' . $this->attributes['first_name'];
    }

    public function getSexNameAttribute(): string
    {
        return self::getGenderNames()[$this->attributes['sex']] ?? '';
    }

    public function serviceMember()
    {
        return $this->hasOne(ServiceMember::class);
    }

    public function isServiceMember(): bool
    {
        return !is_null($this->serviceMember);
    }

    /*
     * Confirmed email.
     */
    public function confirmedEmail()
    {
        return $this->hasOne(Email::class)->where('confirmed', Email::STATUS_CONFIRMED);
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'author_id')->orderByPosted();
    }

    public function socials()
    {
        return $this->hasMany(SocialAccount::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class);
    }

    public function bookmarks()
    {
        return $this->belongsToMany(Post::class, 'bookmarks', 'user_id', 'post_id');
    }

    /**
     * Returns IDs of user roles
     *
     * @return array
     */
    public static function getRoles(): array
    {
        return [
            self::ROLE_USUAL_USER,
            self::ROLE_USUAL_AUTHOR,
            self::ROLE_MODERATOR,
            self::ROLE_ADMIN,
            self::ROLE_IRON_MAN,
        ];
    }

    /**
     * Returns names of user roles
     *
     * @return array
     */
    public static function getRoleNames(): array
    {
        return [
            self::ROLE_USUAL_USER => 'Usual',
            self::ROLE_USUAL_AUTHOR => 'Usual author', //2 images to a post and 4 images per day
            self::ROLE_MODERATOR => 'Moderator',
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_IRON_MAN => 'Main Admin'
        ];
    }

    /**
     * Returns IDs of user genders
     *
     * @return array
     */
    public static function getGenders(): array
    {
        return [
            self::SEX_UNKNOWN,
            self::SEX_FEMALE,
            self::SEX_MALE
        ];
    }

    /**
     * Returns names of user genders
     *
     * @return array
     */
    public static function getGenderNames(): array
    {
        return [
            self::SEX_UNKNOWN => 'Unknown',
            self::SEX_FEMALE => 'female',
            self::SEX_MALE => 'male'
        ];
    }

    /**
     * Get all user permissions in a flat array.
     *
     * @return array
     */
    public function getCanAttribute()
    {
        $permissions = [];
        foreach (Permission::all() as $permission) {
            if (Auth::user()->can($permission->name)) {
                $permissions[$permission->name] = true;
            }
        }
        return $permissions;
    }
}
