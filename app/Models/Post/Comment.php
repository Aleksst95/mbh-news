<?php

namespace App\Models\Post;

use App\Models\Post;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\Comment
 *
 * @property int $id
 * @property int $user_id
 * @property int $post_id
 * @property string $message
 * @property int $rating
 * @property int $left_key
 * @property int $right_key
 * @property int $level
 * @property int $node_id
 * @property int $parent_id
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Post $post
 * @property-read User $user
 * @method static Builder|Comment newModelQuery()
 * @method static Builder|Comment newQuery()
 * @method static Builder|Comment query()
 * @method static Builder|Comment whereCreatedAt($value)
 * @method static Builder|Comment whereDeletedAt($value)
 * @method static Builder|Comment whereId($value)
 * @method static Builder|Comment whereLeftKey($value)
 * @method static Builder|Comment whereLevel($value)
 * @method static Builder|Comment whereMessage($value)
 * @method static Builder|Comment whereNodeId($value)
 * @method static Builder|Comment whereParentId($value)
 * @method static Builder|Comment wherePostId($value)
 * @method static Builder|Comment whereRating($value)
 * @method static Builder|Comment whereRightKey($value)
 * @method static Builder|Comment whereUpdatedAt($value)
 * @method static Builder|Comment whereUserId($value)
 * @mixin Eloquent
 * @method static Builder|Comment whereStatus($value)
 */
class Comment extends Model
{
    use LogsActivity;
    use HasFactory;

    protected $table = 'post_comments';

    protected $guarded = [];

    public const STATUS_PUBLISHED = 1;
    public const STATUS_DELETED_BY_USER = 5;
    public const STATUS_BANNED = 10;

    protected $attributes = [
        'status' => self::STATUS_PUBLISHED
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
