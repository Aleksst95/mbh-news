<?php

namespace App\Models\Post;

use App\Models\Post;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\Complaint
 *
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Post $post
 * @property-read User $user
 * @method static Builder|Complaint newModelQuery()
 * @method static Builder|Complaint newQuery()
 * @method static Builder|Complaint query()
 * @mixin Eloquent
 */
class Complaint extends Model
{
    use LogsActivity;

    protected $table = 'complaint_posts';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
