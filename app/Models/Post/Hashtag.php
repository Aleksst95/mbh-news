<?php

namespace App\Models\Post;

use App\Models\Post;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\Hashtag
 *
 * @property int $id
 * @property string $name
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Collection|Post[] $posts
 * @property-read int|null $posts_count
 * @method static Builder|Hashtag newModelQuery()
 * @method static Builder|Hashtag newQuery()
 * @method static Builder|Hashtag query()
 * @method static Builder|Hashtag whereId($value)
 * @method static Builder|Hashtag whereName($value)
 * @mixin Eloquent
 */
class Hashtag extends Model
{
    use LogsActivity;

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_hashtag', 'hashtag_id', 'post_id');
    }
}
