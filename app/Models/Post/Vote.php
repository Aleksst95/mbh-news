<?php

namespace App\Models\Post;

use App\Models\Post;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\Vote
 *
 * @property int $user_id
 * @property int $post_id
 * @property int $type disVote: -1, Vote: 1
 * @property Carbon $created_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Post $post
 * @property-read User $user
 * @method static Builder|Vote newModelQuery()
 * @method static Builder|Vote newQuery()
 * @method static Builder|Vote query()
 * @method static Builder|Vote whereCreatedAt($value)
 * @method static Builder|Vote wherePostId($value)
 * @method static Builder|Vote whereType($value)
 * @method static Builder|Vote whereUserId($value)
 * @mixin Eloquent
 * @property int $id
 * @method static Builder|Vote whereId($value)
 */
class Vote extends Model
{
    use LogsActivity;
    use HasFactory;

    public const TYPE_POSITIVE = 1;
    public const TYPE_NEGATIVE = -1;

    protected $table = 'post_votes';

    protected $guarded = [];

    const UPDATED_AT = null;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
