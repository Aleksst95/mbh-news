<?php

namespace App\Models\Post;

use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\PostOldUrl
 *
 * @property int $post_id
 * @property string $old_slug
 * @property int $count_request
 * @property string $last_request
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Post $post
 * @method static Builder|PostOldUrl newModelQuery()
 * @method static Builder|PostOldUrl newQuery()
 * @method static Builder|PostOldUrl query()
 * @method static Builder|PostOldUrl whereCountRequest($value)
 * @method static Builder|PostOldUrl whereLastRequest($value)
 * @method static Builder|PostOldUrl whereOldSlug($value)
 * @method static Builder|PostOldUrl wherePostId($value)
 * @mixin \Eloquent
 */
class PostOldUrl extends Model
{
    use LogsActivity;

//    protected static $logName = 'post-old-url';
//    protected static $logOnlyDirty = true;
//    protected static $submitEmptyLogs = false;
//    protected static $recordEvents = ['update', 'deleted'];

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
