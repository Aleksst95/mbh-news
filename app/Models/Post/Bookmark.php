<?php

namespace App\Models\Post;

use App\Models\Post;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post\Bookmark
 *
 * @property int $user_id
 * @property int $post_id
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Post $post
 * @property-read User $user
 * @method static Builder|Bookmark newModelQuery()
 * @method static Builder|Bookmark newQuery()
 * @method static Builder|Bookmark query()
 * @method static Builder|Bookmark wherePostId($value)
 * @method static Builder|Bookmark whereUserId($value)
 * @mixin Eloquent
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Bookmark whereCreatedAt($value)
 * @method static Builder|Bookmark whereUpdatedAt($value)
 */
class Bookmark extends Model
{
    use LogsActivity;
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
