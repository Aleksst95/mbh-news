<?php

namespace App\Models;

use App\Models\Post\Comment;
use App\Models\Post\Complaint;
use App\Models\Post\Hashtag;
use App\Models\Post\Vote;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Post
 *
 * @SWG\Definition (
 *  definition="Post",
 *  @SWG\Property(
 *      property="id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="author_id",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="title",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="slug",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="description",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="cover_img",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="content",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="type",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="count_view",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="count_comments",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="count_positive_votes",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="count_negative_votes",
 *      type="integer"
 *  ),
 *  @SWG\Property(
 *      property="meta_title",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="meta_description",
 *      type="string"
 *  ),
 *  @SWG\Property(
 *      property="posted_at",
 *      type="string"
 *  )
 * )
 * @property int $id
 * @property int $author_id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property string|null $cover_img
 * @property string|null $content
 * @property string|null $content_editor
 * @property int $status
 * @property int $type
 * @property int $count_view
 * @property int $count_comments
 * @property int $count_positive_votes
 * @property int $count_negative_votes
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $posted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read Collection|Complaint[] $complaints
 * @property-read int|null $complaints_count
 * @property-read bool $is_draft
 * @property-read bool $is_posted
 * @property-read Collection|Hashtag[] $hashtags
 * @property-read int|null $hashtags_count
 * @property-read Collection|Vote[] $votes
 * @property-read int|null $votes_count
 * @property-read \App\Models\User $user
 * @method static Builder|Post checking()
 * @method static Builder|Post deleted()
 * @method static Builder|Post draft()
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static \Illuminate\Database\Query\Builder|Post onlyTrashed()
 * @method static Builder|Post orderByPosted($descSort = true)
 * @method static Builder|Post orderByUpdate($descSort = true)
 * @method static Builder|Post posted()
 * @method static Builder|Post query()
 * @method static Builder|Post whereAuthorId($value)
 * @method static Builder|Post whereContent($value)
 * @method static Builder|Post whereContentEditor($value)
 * @method static Builder|Post whereCountComments($value)
 * @method static Builder|Post whereCountNegativeVotes($value)
 * @method static Builder|Post whereCountPositiveVotes($value)
 * @method static Builder|Post whereCountView($value)
 * @method static Builder|Post whereCoverImg($value)
 * @method static Builder|Post whereCreatedAt($value)
 * @method static Builder|Post whereDeletedAt($value)
 * @method static Builder|Post whereDescription($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post whereMetaDescription($value)
 * @method static Builder|Post whereMetaTitle($value)
 * @method static Builder|Post wherePostedAt($value)
 * @method static Builder|Post whereSlug($value)
 * @method static Builder|Post whereStatus($value)
 * @method static Builder|Post whereTitle($value)
 * @method static Builder|Post whereType($value)
 * @method static Builder|Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Post withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Post withoutTrashed()
 * @mixin Eloquent
 */
class Post extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use HasFactory;

    /*
     * Parameters for log-activity package.
     */
//    protected static $logName = 'post';
//    protected static $logOnlyDirty = true;
//    protected static $submitEmptyLogs = false;
//    protected static $recordEvents = ['updated', 'deleted'];

    public const STATUS_DRAFT = 5;
    public const STATUS_POSTED = 10;
    public const STATUS_ON_CHECK = 15;
    public const STATUS_DELETED = 20;

    public const TYPE_POST_NORMAL = 0;
    public const TYPE_POST_AFFILIATE = 1;
    public const TYPE_POST_ADVERTISING = 2;

    protected $fillable = [
        'author_id',
        'title',
        'slug',
        'description',
        'cover_img',
        'content',
        'content_editor',
        'status',
        'count_view',
        'count_comments',
        'count_positive_votes',
        'count_negative_votes',
        'meta_title',
        'meta_description',
        'posted_at',
    ];

    protected $attributes = [
        'status' => self::STATUS_DRAFT
    ];


    public function getIsDraftAttribute(): bool
    {
        return $this->attributes['status'] === self::STATUS_DRAFT;
    }

    public function getIsPostedAttribute(): bool
    {
        return $this->attributes['status'] === self::STATUS_POSTED;
    }

    /**
     * Returns posts is ordered by the field `updated_at`
     *
     * @param Builder $query
     * @param bool $descSort If it's TRUE - uses DESC sort, else ASC sort
     *
     * @return Builder
     */
    public function scopeOrderByUpdate(Builder $query, bool $descSort = true)
    {
        return $query->orderBy('updated_at', $descSort ? 'DESC' : 'ASC');
    }

    /**
     * Returns posts is ordered by the field `posted_at`
     *
     * @param Builder $query
     * @param bool $descSort If it's TRUE - uses DESC sort, else ASC sort
     *
     * @return Builder
     */
    public function scopeOrderByPosted(Builder $query, bool $descSort = true)
    {
        return $query->orderBy('posted_at', $descSort ? 'DESC' : 'ASC');
    }

    /**
     * Returns all active posts
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopePosted(Builder $query)
    {
        return $query->where('status', self::STATUS_POSTED);
    }

    /**
     * Returns all draft posts
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDraft(Builder $query)
    {
        return $query->where('status', self::STATUS_DRAFT);
    }

    /**
     * Returns all posts that are checked
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeChecking(Builder $query)
    {
        return $query->where('status', self::STATUS_ON_CHECK);
    }

    /**
     * Returns all posts that have been deleted
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeDeleted(Builder $query)
    {
        return $query->where('status', self::STATUS_DELETED);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class);
    }

    public function hashtags()
    {
        return $this->belongsToMany(Hashtag::class, 'post_hashtags', 'post_id', 'hashtag_id');
    }

    /**
     * Returns IDs of post statuses
     *
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_DRAFT,
            self::STATUS_POSTED,
            self::STATUS_ON_CHECK,
            self::STATUS_DELETED,
        ];
    }

    /**
     * Returns names of post statuses
     *
     * @return array
     */
    public static function getStatusNames(): array
    {
        return [
            self::STATUS_DRAFT => 'Draft',
            self::STATUS_POSTED => 'Published',
            self::STATUS_ON_CHECK => 'On check',
            self::STATUS_DELETED => 'Deleted',
        ];
    }

    /**
     * Returns the status name by status index.
     *
     * @param int $key Status index
     *
     * @return string
     */
    public static function getStatusName(int $key): string
    {
        return self::getStatusNames()[$key] ?? '';
    }

    /**
     * Returns IDs of post types
     *
     * @return array
     */
    public static function getTypes(): array
    {
        return [
            self::TYPE_POST_NORMAL,
            self::TYPE_POST_AFFILIATE,
            self::TYPE_POST_ADVERTISING,
        ];
    }

    /**
     * Returns names of post statuses
     *
     * @return array
     */
    public static function getTypeNames(): array
    {
        return [
            self::TYPE_POST_NORMAL => 'Usual',
            self::TYPE_POST_AFFILIATE => 'Affiliate',
            self::TYPE_POST_ADVERTISING => 'Advertising',
        ];
    }
}
