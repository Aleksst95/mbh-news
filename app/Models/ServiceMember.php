<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\ServiceMember
 *
 * @property int $id
 * @property int $user_id
 * @property string $password
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read User $user
 * @method static Builder|ServiceMember newModelQuery()
 * @method static Builder|ServiceMember newQuery()
 * @method static Builder|ServiceMember query()
 * @method static Builder|ServiceMember whereCreatedAt($value)
 * @method static Builder|ServiceMember whereId($value)
 * @method static Builder|ServiceMember wherePassword($value)
 * @method static Builder|ServiceMember whereUpdatedAt($value)
 * @method static Builder|ServiceMember whereUserId($value)
 * @mixin \Eloquent
 */
class ServiceMember extends Model
{
    use LogsActivity;

    protected static $logName = 'service-member';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public const ROLE_ID_IRON_MAN = 1;

    public function tapActivity(Activity $activity, string $event)
    {
        /** @var Collection $properties */
        if ($properties = $activity->properties) {
            if ($properties->has('attributes')) {
                $attributes = $properties->get('attributes');
                if (isset($attributes['password'])) {
                    $attributes['password'] = '<secret>';
                }
                $properties->put('attributes', $attributes);
            }
            if ($properties->has('old')) {
                $old = $properties->get('old');
                if (isset($old['password'])) {
                    $old['password'] = '<secret>';
                }
                $properties->put('old', $old);
            }
            $activity->properties = $properties;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
