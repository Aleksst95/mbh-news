<?php

namespace App\Models;

use App\Models\User\Email;
use App\Models\User\EmailDelivery;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\StatisticEmailDelivery
 *
 * @property int $email_delivery_id
 * @property int $email_id
 * @property string $datetime
 * @property int $event_id
 * @property string|null $data json
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Email $email
 * @property-read EmailDelivery $emailDelivery
 * @property-read StatisticEvent $event
 * @method static Builder|StatisticEmailDelivery newModelQuery()
 * @method static Builder|StatisticEmailDelivery newQuery()
 * @method static Builder|StatisticEmailDelivery query()
 * @method static Builder|StatisticEmailDelivery whereData($value)
 * @method static Builder|StatisticEmailDelivery whereDatetime($value)
 * @method static Builder|StatisticEmailDelivery whereEmailDeliveryId($value)
 * @method static Builder|StatisticEmailDelivery whereEmailId($value)
 * @method static Builder|StatisticEmailDelivery whereEventId($value)
 * @mixin Eloquent
 */
class StatisticEmailDelivery extends Model
{
    use LogsActivity;

    public function email()
    {
        return $this->belongsTo(Email::class);
    }

    public function event()
    {
        return $this->belongsTo(StatisticEvent::class);
    }

    public function emailDelivery()
    {
        return $this->belongsTo(EmailDelivery::class);
    }
}
