<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\StatisticEvent
 *
 * @property int $id
 * @property string $name
 * @property string $name_rus
 * @property-read Collection|StatisticEmailConfirmation[] $statisticEmailConfirmations
 * @property-read int|null $statistic_email_confirmations_count
 * @property-read Collection|StatisticEmailDelivery[]  $statisticEmailDeliveries
 * @property-read int|null $statistic_email_deliveries_count
 * @method static Builder|StatisticEvent newModelQuery()
 * @method static Builder|StatisticEvent newQuery()
 * @method static Builder|StatisticEvent query()
 * @method static Builder|StatisticEvent whereId($value)
 * @method static Builder|StatisticEvent whereName($value)
 * @method static Builder|StatisticEvent whereNameRus($value)
 * @mixin Eloquent
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 */
class StatisticEvent extends Model
{
    use LogsActivity;

    public function statisticEmailConfirmations()
    {
        return $this->hasMany(StatisticEmailConfirmation::class);
    }

    public function statisticEmailDeliveries()
    {
        return $this->hasMany(StatisticEmailDelivery::class);
    }
}
