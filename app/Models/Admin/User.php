<?php

namespace App\Models\Admin;

use App\Models\User as UserMain;
use App\Models\User\Email;

class User extends UserMain
{
    protected $guarded = [];

    /*
     * All emails which the user tried to connect.
     */
    public function allEmails()
    {
        return $this->hasMany(Email::class);
    }
}
