<?php

namespace App\Models\Admin;

use App\Models\ServiceMember as ServiceMemberMain;

class ServiceMember extends ServiceMemberMain
{
    protected $guarded = [];
}
