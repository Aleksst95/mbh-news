<?php

namespace App\Models\User;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\User\SocialAccount
 *
 * @property int $id
 * @property int $user_id
 * @property string $provider_id
 * @property string $provider
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read User $user
 * @method static Builder|SocialAccount newModelQuery()
 * @method static Builder|SocialAccount newQuery()
 * @method static \Illuminate\Database\Query\Builder|SocialAccount onlyTrashed()
 * @method static Builder|SocialAccount query()
 * @method static Builder|SocialAccount whereCreatedAt($value)
 * @method static Builder|SocialAccount whereDeletedAt($value)
 * @method static Builder|SocialAccount whereId($value)
 * @method static Builder|SocialAccount whereProvider($value)
 * @method static Builder|SocialAccount whereProviderId($value)
 * @method static Builder|SocialAccount whereUpdatedAt($value)
 * @method static Builder|SocialAccount whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|SocialAccount withTrashed()
 * @method static \Illuminate\Database\Query\Builder|SocialAccount withoutTrashed()
 * @mixin Eloquent
 */
class SocialAccount extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use HasFactory;

    public const PROVIDER_NAME_FB = 'facebook';
    public const PROVIDER_NAME_VK = 'vkontakte';
    public const PROVIDER_NAME_TW = 'twitter';
    public const PROVIDER_NAME_GOOGLE = 'google';
    public const PROVIDER_NAME_YX = 'yandex';

    public const AVAILABLE_PROVIDERS = [
        self::PROVIDER_NAME_FB,
        self::PROVIDER_NAME_VK
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'provider_id',
        'provider',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
