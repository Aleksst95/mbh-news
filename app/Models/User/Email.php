<?php

namespace App\Models\User;

use App\Models\StatisticEmailConfirmation;
use App\Models\StatisticEmailDelivery;
use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\User\Email
 *
 * @property int $id
 * @property string $email
 * @property int|null $user_id
 * @property int $confirmed
 * @property int $get_delivery
 * @property string|null $unsubscribe_delivery_token
 * @property string|null $confirmation_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Collection|EmailConfirmation[] $emailConfirmations
 * @property-read int|null $email_confirmations_count
 * @property-read string $delivery_status
 * @property-read Collection|StatisticEmailConfirmation[] $statisticEmailConfirmations
 * @property-read int|null $statistic_email_confirmations_count
 * @property-read Collection|StatisticEmailDelivery[] $statisticEmailDeliveries
 * @property-read int|null $statistic_email_deliveries_count
 * @property-read User|null $user
 * @method static Builder|Email confirmed()
 * @method static Builder|Email newModelQuery()
 * @method static Builder|Email newQuery()
 * @method static \Illuminate\Database\Query\Builder|Email onlyTrashed()
 * @method static Builder|Email query()
 * @method static Builder|Email whereConfirmationToken($value)
 * @method static Builder|Email whereConfirmed($value)
 * @method static Builder|Email whereCreatedAt($value)
 * @method static Builder|Email whereDeletedAt($value)
 * @method static Builder|Email whereEmail($value)
 * @method static Builder|Email whereGetDelivery($value)
 * @method static Builder|Email whereId($value)
 * @method static Builder|Email whereUnsubscribeDeliveryToken($value)
 * @method static Builder|Email whereUpdatedAt($value)
 * @method static Builder|Email whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Email withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Email withoutTrashed()
 * @mixin Eloquent
 */
class Email extends Model
{
    use SoftDeletes;
    use LogsActivity;
    use HasFactory;

    public const STATUS_CONFIRMED = 1;
    public const STATUS_UNCONFIRMED = 0;

    public const STATUS_GET_DELIVERY = 1;
    public const STATUS_DO_NOT_GET_DELIVERY = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function getDeliveryStatusAttribute(): string
    {
        return $this->attributes['get_delivery'] == self::STATUS_GET_DELIVERY;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function emailConfirmations()
    {
        return $this->hasMany(EmailConfirmation::class);
    }

    public function statisticEmailConfirmations()
    {
        return $this->hasMany(StatisticEmailConfirmation::class);
    }

    public function statisticEmailDeliveries()
    {
        return $this->hasMany(StatisticEmailDelivery::class);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeConfirmed(Builder $query)
    {
        return $query->where('confirmed', self::STATUS_CONFIRMED);
    }
}
