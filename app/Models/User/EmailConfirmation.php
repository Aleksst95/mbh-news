<?php

namespace App\Models\User;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\User\EmailConfirmation
 *
 * @property int $email_id
 * @property string $token
 * @property string $expired
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Email $email
 * @method static Builder|EmailConfirmation newModelQuery()
 * @method static Builder|EmailConfirmation newQuery()
 * @method static Builder|EmailConfirmation query()
 * @method static Builder|EmailConfirmation whereEmailId($value)
 * @method static Builder|EmailConfirmation whereExpired($value)
 * @method static Builder|EmailConfirmation whereToken($value)
 * @mixin Eloquent
 * @property int $id
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|EmailConfirmation whereCreatedAt($value)
 * @method static Builder|EmailConfirmation whereDeletedAt($value)
 * @method static Builder|EmailConfirmation whereId($value)
 * @method static Builder|EmailConfirmation whereUpdatedAt($value)
 * @method static Builder|EmailConfirmation whereUserId($value)
 */
class EmailConfirmation extends Model
{
    use LogsActivity;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function email()
    {
        return $this->belongsTo(Email::class);
    }
}
