<?php

namespace App\Models\User;

use App\Models\StatisticEmailDelivery;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\User\EmailDelivery
 *
 * @property int $id
 * @property string $date_period json
 * @property string $data json
 * @property string|null $datetime_send
 * @property int $is_send
 * @property string|null $full_email_text
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Collection|StatisticEmailDelivery[] $statisticEmailDeliveries
 * @property-read int|null $statistic_email_deliveries_count
 * @method static Builder|EmailDelivery newModelQuery()
 * @method static Builder|EmailDelivery newQuery()
 * @method static Builder|EmailDelivery query()
 * @method static Builder|EmailDelivery whereData($value)
 * @method static Builder|EmailDelivery whereDatePeriod($value)
 * @method static Builder|EmailDelivery whereDatetimeSend($value)
 * @method static Builder|EmailDelivery whereFullEmailText($value)
 * @method static Builder|EmailDelivery whereId($value)
 * @method static Builder|EmailDelivery whereIsSend($value)
 * @mixin Eloquent
 */
class EmailDelivery extends Model
{
    use LogsActivity;

    public function statisticEmailDeliveries()
    {
        return $this->hasMany(StatisticEmailDelivery::class);
    }
}
