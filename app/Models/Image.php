<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property string $path
 * @property int $optimized
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $filename
 * @property-read User $user
 * @method static Builder|Image newModelQuery()
 * @method static Builder|Image newQuery()
 * @method static Builder|Image query()
 * @method static Builder|Image whereCreatedAt($value)
 * @method static Builder|Image whereFilename($value)
 * @method static Builder|Image whereId($value)
 * @method static Builder|Image whereOptimized($value)
 * @method static Builder|Image wherePath($value)
 * @method static Builder|Image whereType($value)
 * @method static Builder|Image whereUpdatedAt($value)
 * @method static Builder|Image whereUserId($value)
 * @mixin Eloquent
 */
class Image extends Model
{
//    use LogsActivity;

    protected static $logName = 'image';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['update', 'deleted'];

    protected $guarded = [];

    public const TYPE_POST = 0;
    public const TYPE_AVATAR = 1;

    public const AVAILABLE_EXTENSIONS = [
        'jpeg',
        'png',
        'bmp',
    ];

    public const MAX_FILE_SIZE = 4096;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
