<?php

namespace App\Models;

use App\Models\User\Email;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * App\Models\StatisticEmailConfirmation
 *
 * @property int $email_id
 * @property string $datetime
 * @property int $event_id
 * @property string|null $data json
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read Email $email
 * @property-read StatisticEvent $event
 * @method static Builder|StatisticEmailConfirmation newModelQuery()
 * @method static Builder|StatisticEmailConfirmation newQuery()
 * @method static Builder|StatisticEmailConfirmation query()
 * @method static Builder|StatisticEmailConfirmation whereData($value)
 * @method static Builder|StatisticEmailConfirmation whereDatetime($value)
 * @method static Builder|StatisticEmailConfirmation whereEmailId($value)
 * @method static Builder|StatisticEmailConfirmation whereEventId($value)
 * @mixin Eloquent
 */
class StatisticEmailConfirmation extends Model
{
    use LogsActivity;

    public function email()
    {
        return $this->belongsTo(Email::class);
    }

    public function event()
    {
        return $this->belongsTo(StatisticEvent::class);
    }
}
