<?php

namespace App\Services;

use App\Models\User\Email;
use App\Models\User\EmailConfirmation;
use App\Repositories\EmailRepository;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class EmailService
{
    /**
     * @var EmailRepository
     */
    private EmailRepository $emailRepository;

    public function __construct(EmailRepository $emailRepository)
    {
        $this->emailRepository = $emailRepository;
    }

    /**
     * Adds the confirmed email to the user or returns null.
     * In particular, the method is used after social registration.
     *
     * @param int $userId
     * @param string $email
     *
     * @return mixed
     */
    public function addConfirmedEmailToUser(int $userId, string $email)
    {
        $dbEmail = $this->emailRepository->getByEmail($email);

        if (empty($dbEmail)) {
            return $this->createNewEmail($email, Email::STATUS_CONFIRMED, $userId);
        }

        if ($dbEmail->confirmed == Email::STATUS_CONFIRMED) {
            if ($dbEmail->user_id == $userId) {
                return $dbEmail;
            }

            return null;
        }

        /**
         * If the email is old and is not linked to any user
         */
        $this->bindEmailToUserAndSetAsConfirmed($userId, $email);

        return $dbEmail;
    }

    /**
     * Sets the status of the email as confirmed.
     *
     * @param string $email
     *
     * @return bool
     */
    public function confirmEmail(string $email): bool
    {
        return Email::where('email', $email)->update(['confirmed' => Email::STATUS_CONFIRMED]);
    }

    /**
     * Binds the email to the user and sets the status as confirmed.
     *
     * @param int $userId
     * @param string $email
     *
     * @return bool
     */
    public function bindEmailToUserAndSetAsConfirmed(int $userId, string $email): bool
    {
        return Email::where('email', $email)->update(
            [
                'user_id' => $userId,
                'confirmed' => Email::STATUS_CONFIRMED
            ]
        );
    }

    /**
     * Creates a new email confirmation token for the user and sends them a confirmation email.
     *
     * @param int $userId
     * @param string $email
     *
     * @return bool
     *
     * @throws Exception
     */
    public function setNewEmailToUser(int $userId, string $email): bool
    {
        // TODO refactor this method. Rename the function and/or replace the part with getting an email from DB
        $dbEmail = $this->emailRepository->getByEmail($email);

        if (!empty($dbEmail)) {
            if ($dbEmail->confirmed && $dbEmail->user_id === $userId) {
                throw new Exception('Email is already linked');
            }

            if ($dbEmail->confirmed && $dbEmail->user_id !== null) {
                throw new Exception('Email is busy');
            }
        } else {
            $dbEmail = $this->createNewEmail($email);
        }

        $emailConfirmation = $this->createEmailConfirmationForUser($userId, $dbEmail->id);

        if ($this->setJobToSendEmailConfirmation($emailConfirmation)) {
            return true;
        }

        return false;
    }

    public function switchGettingWeeklyNewsLetter(int $userId, int $status): bool
    {
        $email = $this->emailRepository->getConfirmedEmailByUserId($userId);

        if (empty($email)) {
            throw new Exception('User does not have a linked email');
        }

        if ($email->get_delivery === $status) {
            throw new Exception('The same status');
        }

        return $email->update(['get_delivery' => $status]);
    }

    /**
     * @param string $email
     * @param int $status
     * @param int|null $userId
     *
     * @return Email
     */
    private function createNewEmail(string $email, int $status = Email::STATUS_UNCONFIRMED, ?int $userId = null): Email
    {
        return Email::create(
            [
                'email' => $email,
                'confirmed' => $status,
                'user_id' => $userId,
            ]
        );
    }

    /**
     * @param int $userId
     * @param int $emailId
     *
     * @return EmailConfirmation
     */
    private function createEmailConfirmationForUser(int $userId, int $emailId): EmailConfirmation
    {
        return EmailConfirmation::create(
            [
                'email_id' => $emailId,
                'user_id' => $userId,
                'token' => Str::random(),
                'expired' => Carbon::now()->addDays(1)->format('Y-m-d H:i:s')
            ]
        );
    }

    /**
     * @param EmailConfirmation $emailConfirmation
     *
     * @return bool
     */
    private function setJobToSendEmailConfirmation(EmailConfirmation $emailConfirmation): bool
    {
        // TODO make the method
        return true;
    }
}
