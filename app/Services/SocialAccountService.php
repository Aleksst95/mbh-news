<?php

namespace App\Services;

use App\Models\User\SocialAccount;
use App\Models\User;
use App\Repositories\SocialAccountRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Laravel\Socialite\Facades\Socialite;
use SocialiteProviders\Manager\OAuth2\User as OAuth2User;
use Illuminate\Contracts\Container\BindingResolutionException;

class SocialAccountService
{
    /**
     * @var SocialAccountRepository
     */
    private SocialAccountRepository $socialAccountRepository;

    public function __construct(SocialAccountRepository $socialAccountRepository)
    {
        $this->socialAccountRepository = $socialAccountRepository;
    }

    /**
     * Returns the user after authorization (registration) with Vkontakte
     *
     * @param OAuth2User $providerUser
     *
     * @return User
     *
     * @throws BindingResolutionException
     */
    public function getUserAfterVkontakteAuth($providerUser): User
    {
        if (empty($providerUser->id)) {
            throw new Exception('Wrong data for VK auth.');
        }

        $item = $this->socialAccountRepository->getUserByProviderAndProviderId(
            SocialAccount::PROVIDER_NAME_VK,
            $providerUser->id
        );

        if (empty($item)) {
            $item = $this->createNewUserAfterVkonakteAuth($providerUser);
        }

        return $item;
    }

    /**
     * Returns the user after authorization (registration) with Facebook
     *
     * @param OAuth2User $providerUser
     *
     * @return User
     * @throws Exception
     */
    public function getUserAfterFacebookAuth($providerUser): User
    {
        if (empty($providerUser->id)) {
            throw new Exception('Wrong data for FB auth.');
        }

        $item = $this->socialAccountRepository->getUserByProviderAndProviderId(
            SocialAccount::PROVIDER_NAME_FB,
            $providerUser->id
        );

        if (empty($item)) {
            $item = $this->createNewUserAfterFacebookAuth($providerUser);
        }

        return $item;
    }

    /**
     * @param OAuth2User $providerUser
     *
     * @return User
     * @throws BindingResolutionException
     */
    public function createNewUserAfterVkonakteAuth($providerUser): User
    {
        $dataUser = [
            'first_name' => $providerUser->user['first_name'] ?? '',
            'last_name' => $providerUser->user['last_name'] ?? '',
            'avatar' => $providerUser->user['photo'] ?? '',
        ];

        /** @var UserService $userService */
        $userService = app()->make(UserService::class);
        $user = $userService->createNewUser($dataUser);

        if (empty($user)) {
            throw new Exception('Error of creating new User.');
        }

        $email = $providerUser->accessTokenResponseBody['email'] ?? '';

        if (!empty($email)) {
            /** @var EmailService $emailService */
            $emailService = app()->make(EmailService::class);

            $emailService->addConfirmedEmailToUser($user->id, $email);
        }

        $this->addSocialAccountToUser($user->id, SocialAccount::PROVIDER_NAME_VK, $providerUser->id);

        return $user;
    }

    /**
     * @param OAuth2User $providerUser
     *
     * @return User
     * @throws BindingResolutionException
     */
    public function createNewUserAfterFacebookAuth($providerUser): User
    {
        $dataUser = [
            'avatar' => $providerUser->avatar ?? '',
        ];

        $fullName = $providerUser->name;

        if (!empty($fullName) && count($partsFullName = explode(' ', $fullName) == 2)) {
            $dataUser['first_name'] = $partsFullName[0];
            $dataUser['last_name'] = $partsFullName[1];
        }

        /** @var UserService $userService */
        $userService = app()->make(UserService::class);
        $user = $userService->createNewUser($dataUser);

        if (empty($user)) {
            throw new Exception('Error of creating new User.');
        }

        $email = $providerUser->confirmedEmail ?? '';

        if (!empty($email)) {
            /** @var EmailService $emailService */
            $emailService = app()->make(EmailService::class);

            $emailService->addConfirmedEmailToUser($user->id, $email);
        }

        $this->addSocialAccountToUser($user->id, SocialAccount::PROVIDER_NAME_FB, $providerUser->id);

        return $user;
    }

    /**
     * @param int $userId
     * @param string $provider
     * @param string $providerId
     *
     * @return SocialAccount|Model
     */
    public function addSocialAccountToUser(int $userId, string $provider, string $providerId)
    {
        return SocialAccount::create(
            [
                'user_id' => $userId,
                'provider' => $provider,
                'provider_id' => $providerId
            ]
        );
    }

    /**
     * Links a new provider to the user if the user is not already linked with the provider.
     *
     * @param int $userId
     * @param string $provider
     *
     * @return bool
     *
     * @throws Exception
     */
    public function linkNewProviderToUser(int $userId, string $provider): bool
    {
        $socialAccount = $this->socialAccountRepository->getProviderByUserIdAndProviderName($userId, $provider);

        if (!empty($socialAccount)) {
            throw new Exception('User is already linked with this provider');
        }

        $userProvider = Socialite::driver($provider)->user();

        return $this->addSocialAccountToUser($userId, $provider, $userProvider->getId()) ? true : false;
    }

    /**
     * Unlinks the provider from the user if the user is linked with the provider and it is not the last linked
     * provider.
     *
     * @param int $userId
     * @param string $provider
     *
     * @return bool
     *
     * @throws Exception
     */
    public function unlinkProviderFromUser(int $userId, string $provider): bool
    {
        $socialAccount = $this->socialAccountRepository->getProviderByUserIdAndProviderName($userId, $provider);

        if (empty($socialAccount)) {
            throw new Exception('User is not linked with the provider');
        }

        $socialAccounts = $this->socialAccountRepository->getProvidersByUserId($userId);

        if ($socialAccounts->count() < 2) {
            throw new Exception('Can\'t unlink the last linked account');
        }

        return $socialAccount->delete() ? true : false;
    }
}
