<?php

namespace App\Services\Admin;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


/*
 * DESCRIPTION:
 *
 * The group name is the first part of the permission name (before the first space).
 * For example, the permission name is 'user edit', then the group name is 'user'.
 *
 */

class RoleAndPermissionService
{
    /**
     * Returns permissions grouped by name.
     *
     * @return array
     */
    public function getPermissionGroups(): array
    {
        $permissionGroups = [];
        $permissions = Permission::orderBy('name')->get();

        foreach ($permissions as $permission) {
            $permissionGroupName = explode(' ', $permission->name)[0];

            if (empty($permissionGroups[$permissionGroupName])) {
                $permissionGroups[$permissionGroupName] = [];
            }

            $permissionGroups[$permissionGroupName][] = $permission;
        }

        return $permissionGroups;
    }

    /**
     * Returns role permissions with group name.
     *
     * @param  Role  $role
     *
     * @return Collection
     */
    public function getRolePermissionWithGroups(Role $role): Collection
    {
        $permissions = $role->permissions;

        foreach ($permissions as &$permission) {
            $permissionGroupName = explode(' ', $permission->name)[0];

            $permission->groupName = $permissionGroupName;
        }

        return $permissions;
    }

    /**
     * Returns role permissions with group name.
     *
     * @return Collection
     */
    public function getAllRolesWithoutMainAdmin(): Collection
    {
        return Role::where('name', '!=', 'Iron Man')->get();
    }

    /**
     * Creates a new role and returns ID the role.
     *
     * @param  string  $name  New role name
     * @param  array  $permissionIds  Permission IDs for a new role
     *
     * @return int New role ID
     */
    public function createRole(string $name, array $permissionIds): int
    {
        $role = Role::create(['name' => $name]);

        $this->addPermissionsToRole($role->id, $permissionIds);

        return $role->id;
    }

    /**
     * Adds permissions to the role.
     *
     * @param  int  $roleId  Role ID
     * @param  array  $permissionIds Permission IDs for the role
     */
    public function addPermissionsToRole(int $roleId, array $permissionIds)
    {
        $role = Role::findById($roleId);

        foreach ($permissionIds as $permissionId) {
            $permission = Permission::findById($permissionId);

            $role->givePermissionTo($permission);
        }
    }

    /**
     * Adds roles to the user.
     *
     * @param  int $userId  User ID
     * @param  array  $roleIds Role IDs for the user
     */
    public function addRolesToUser(int $userId, array $roleIds)
    {
        $user = User::find($userId);

        foreach ($roleIds as $roleId) {
            $role = Role::findById($roleId);

            $user->assignRole($role);
        }
    }

    /**
     * Updates a name of the role.
     *
     * @param  int  $roleId  Role ID
     * @param  string  $name  New role name
     *
     * @return bool Operation status
     */
    public function updateRoleName(int $roleId, string $name): bool
    {
        $role = Role::findById($roleId);

        $role->name = $name;

        return $role->save();
    }

    /**
     * Removes all permissions that were assigned to the role.
     *
     * @param  int  $roleId  Role ID
     */
    public function deleteAllPermissionFromRole(int $roleId)
    {
        $role = Role::findById($roleId);

        foreach ($role->permissions as $permission) {
            $role->revokePermissionTo($permission);
        }
    }

    /**
     * Deletes a role and returns operation status.
     *
     * @param  int  $roleId  Role ID
     *
     * @return bool Delete status
     */
    public function deleteRole(int $roleId): bool
    {
        $role = Role::findById($roleId);

        if (empty($role) || !$role->delete()) {
            return false;
        }

        return true;
    }

    /**
     * Deletes all user roles and returns operation status.
     *
     * @param  int  $userId  User ID
     */
    public function deleteAllRolesToUser(int $userId)
    {
        $user = User::find($userId);
        $roles = $user->roles;

        foreach ($roles as $role) {
            $user->removeRole($role);
        }
    }
}
