<?php

namespace App\Services\Admin;

use App\Models\Admin\ServiceMember;
use App\Models\User;
use App\Repositories\Admin\ServiceMemberRepository;
use Illuminate\Database\Eloquent\Collection;

class ServiceMemberService
{
    /**
     * @var ServiceMemberRepository
     */
    private ServiceMemberRepository $serviceMemberRepository;

    /**
     * ServiceMemberService constructor.
     *
     * @param ServiceMemberRepository $serviceMemberRepository
     */
    public function __construct(ServiceMemberRepository $serviceMemberRepository)
    {
        $this->serviceMemberRepository = $serviceMemberRepository;
    }

    /**
     * Returns all not service member users.
     *
     * @return Collection
     */
    public function getAllUserNotMembers(): Collection
    {
        return User::doesntHave('serviceMember')->get();
    }

    /**
     * Creates a new service member.
     *
     * @param int $userId User ID
     * @param string $password Password for the new user
     *
     * @return bool Delete status
     */
    public function createItem(int $userId, string $password): bool
    {
        $item = new ServiceMember();

        $item->user_id = $userId;
        $item->password = $password;

        return $item->save();
    }

    /**
     * Updated a service member.
     *
     * @param int $id Service Member ID
     * @param string|null $password Password for the user
     *
     * @return bool Update status
     */
    public function updateItem(int $id, ?string $password): bool
    {
        if (empty($password) || strlen($password) < 5) {
            return true;
        }

        $item = $this->serviceMemberRepository->getById($id);

        $item->password = $password;

        return $item->save();
    }

    /**
     * Deletes a service member.
     *
     * @param int $id Service Member ID
     *
     * @return bool Delete status
     * @throws \Exception
     */
    public function deleteItem(int $id): bool
    {
        $item = $this->serviceMemberRepository->getById($id);

        if (empty($item) || !$item->delete()) {
            return false;
        }

        return true;
    }
}
