<?php

namespace App\Services\Post;

use App\Models\Post\PostOldUrl;
use App\Repositories;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class OldUrlService
{
    private Repositories\Post\PostedRepository $postRepository;

    public function __construct(Repositories\Post\PostedRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Returns an old url for an article that previously had a different slug name.
     *
     * @param string $slug Field `old_slug` of the table `post_old_urls'
     *
     * @return string|null
     */
    public function getNewLinkForOldPostUrl(string $slug): ?string
    {
        $oldLink = PostOldUrl::query()
            ->with(
                [
                    'post' => function (BelongsTo $query) {
                        $query->select(
                            [
                                'id',
                                'slug'
                            ]
                        );
                    }
                ]
            )
            ->where('old_slug', $slug)
            ->first();

        if (empty($oldLink)) {
            return null;
        }

        return $oldLink->post->slug;
    }

    public function markUsageOfPostOldUrl(string $slug)
    {
        PostOldUrl::query()
            ->where('old_slug', $slug)
            ->update(
                [
                    'count_request' => DB::raw('count_request + 1'),
                    'last_request' => Carbon::now()->toDateTimeString()
                ]
            );
    }
}
