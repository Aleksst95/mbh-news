<?php

namespace App\Services\Post;

use App\Models\Post;
use DOMDocument;
use DOMElement;

class ContentGenerationService
{
    /**
     * Generates post content blocks from editor blocks
     *
     * @param array $blocks
     * @param bool $userHasPermissions Current user has permissions to change the text with special constructions.
     *
     * @return array
     */
    public function generate(array $blocks, bool $userHasPermissions = false): array
    {
        $contentBlocks = [];

        foreach ($blocks as $block) {
            if (empty($block['type'])
                || !isset($block['data'])
                || !is_array($block['data'])
                || !is_callable([$this, 'generate' . ucfirst($block['type'])])) {
                continue;
            }

            if (!empty(
            $blockContent = $this->{'generate' . ucfirst($block['type'])}($block['data'], $userHasPermissions)
            )) {
                $contentBlocks[] = $blockContent;
            }
        }

        return $contentBlocks;
    }

    /**
     * Generates header content from the editor data
     *
     * @param array $data
     *
     * @return string
     */
    private function generateHeader(array $data): string
    {
        if (empty($data['text'])) {
            return '';
        }

        /**
         * On the Frontend we have a level configuration: [1,2,3]. But for articles, we must increase the value by 1,
         * because the main heading (H1) is the title of the article.
         */
        $level = ($data['level'] ?? 1) + 1;

        $resultSting = '<h' . $level . ' class="layout-center-column header--' . $level . '"';

        if (!empty($data['anchor'])) {
            $resultSting .= ' id="anchor-' . $data['anchor'] . '"';
        }

        $resultSting .= '>' . $data['text'] . '</h' . $level . '>';

        return $resultSting;
    }

    /**
     * Generates paragraph content from the editor data
     *
     * @param array $data
     * @param bool $userHasPermissions {@see ContentGenerationService::generate(), parameter - $userHasPermissons}.
     *
     * @return string
     */
    private function generateParagraph(array $data, bool $userHasPermissions = false): string
    {
        if (empty($data['text'])) {
            return '';
        }

        $resultSting = '<div class="layout-center-column">';

        $resultSting .= $this->updateLinksInText($data['text'], $userHasPermissions);

        $resultSting .= '</div>';

        return $resultSting;
    }

    /**
     * Generates list content from the editor data
     *
     * @param array $data
     * @param bool $userHasPermissions {@see ContentGenerationService::generate(), parameter - $userHasPermissons}.
     *
     * @return string
     */
    private function generateList(array $data, bool $userHasPermissions = false): string
    {
        if (empty($data['items']) || !is_array($data['items'])) {
            return '';
        }

        $tag = (($data['style'] ?? '') === 'ordered') ? 'ol' : 'ul';

        $resultSting = '<' . $tag . ' class="layout-center-column post-list--' . $tag . '">';

        foreach ($data['items'] as $item) {
            $resultSting .= '<li>' . $this->updateLinksInText($item, $userHasPermissions) . '</li>';
        }

        $resultSting .= '</' . $tag . '>';

        return $resultSting;
    }

    /**
     * Generates quote content from the editor data
     *
     * @param array $data
     * @param bool $userHasPermissions {@see ContentGenerationService::generate(), parameter - $userHasPermissons}.
     *
     * @return string
     */
    private function generateQuote(array $data, bool $userHasPermissions = false): string
    {
        if (empty($data['text'])) {
            return '';
        }
        $resultSting = '<blockquote class="layout-center-column post-quote"><p>';
        $resultSting .= $this->updateLinksInText($data['text'], $userHasPermissions);
        $resultSting .= '</p>';

        if (!empty($data['caption'])) {
            $resultSting .= '<cite class="post-quote__caption">';
            $resultSting .= $this->updateLinksInText($data['caption'], $userHasPermissions);
            $resultSting .= '</cite>';
        }

        $resultSting .= '</blockquote>';

        return $resultSting;
    }

    /**
     * Generates image content from the editor data
     *
     * @param array $data
     *
     * @return string
     */
    private function generateImage(array $data): string
    {
        if (empty($data['file']['url'])) {
            return '';
        }

        $stretched = $data['stretched'] ?? false;

        $resultSting = '<figure class="' . (($stretched) ? 'mb-4' : 'layout-center-column') . '">'
            . '<img class="w-full" src="' . $data['file']['url'] . '" alt="">';

        if (!empty($data['caption'])) {
            $resultSting .= '<figcaption class="post-figure__caption' . (($stretched) ? ' layout-center-column' : '') . '">';
            $resultSting .= $this->updateLinksInText($data['caption']);
            $resultSting .= '</figcaption>';
        }

        $resultSting .= '</figure>';

        return $resultSting;
    }

    /**
     * Generates embed content from the editor data
     *
     * @param array $data
     *
     * @return string
     */
    private function generateEmbed(array $data): string
    {
        if (empty($data['embed'])) {
            return '';
        }

        $isFullWidth = false;

        $height = 320;
        $service = $data['service'] ?? 'youtube';
        switch ($service) {
            case 'youtube':
            case 'vimeo':
                $height = 420;
                $isFullWidth = true;
                break;
            case 'vine':
            case 'coub':
                $height = 320;
                break;
            case 'twitter':
                $height = 600;
                break;
            case 'instagram':
                $height = 505;
                break;
            case 'yandex-music-track':
                $height = 100;
                break;
            case 'gfycat':
                $height = 436;
                break;
            case 'imgur':
                $height = 500;
                break;
            case 'twitch-video':
                $height = 366;
                break;
        }

        $resultSting = '<figure' . (($isFullWidth) ? '' : ' class="layout-center-column"') . '>'
            . '<iframe class="w-full shadow-xl' . (($isFullWidth) ? '' : ' post-figure__iframe-with-border')
            . '" height="' . $height . '" src="' . $data['embed'] . '" frameborder="0" allowfullscreen></iframe>';

        if (!empty($data['caption'])) {
            $resultSting .= '<figcaption class="post-figure__caption' . (($isFullWidth) ? ' layout-center-column' : '') . '">';
            $resultSting .= $this->updateLinksInText($data['caption']);
            $resultSting .= '</figcaption>';
        }

        $resultSting .= '</figure>';

        return $resultSting;
    }

    /**
     * Generates delimiter content from editor data
     *
     * @return string
     */
    private function generateDelimiter(): string
    {
        return '<div class="layout-center-column text-center text-4xl">* * *</div>';
    }

    /**
     * Updates the passed text in the following cases:
     *  1) Remove the 'rel' attribute from a link if the link points to the current domain.
     *  2) Remove the 'rel' and 'target' attributes from a link if the link is an anchor.
     *  3) Remove the 'rel' attribute from a link
     *      if the current user has permissions to add seo links and there is a 'mark' tag inside the link.
     *
     * @param string $text
     * @param bool $userHasPermissions {@see ContentGenerationService::generate(), parameter - $userHasPermissons}.
     *
     * @return string
     */
    private function updateLinksInText(string $text, bool $userHasPermissions = false): string
    {
        $dom = new DOMDocument();

        // Disable error reporting about html5 and custom tags
        libxml_use_internal_errors(true);

        // Wrap the text in a 'div' tag to avoid adding a 'p' tag around the text
        $dom->loadHTML('<div>' . $text . '</div>', LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        libxml_clear_errors();

        $links = $dom->getElementsByTagName('a');

        $textIsChanged = false;
        /** @var DOMElement $link */
        foreach ($links as $link) {
            $href = $link->getAttribute('href');

            if (strpos($href, config('app.url')) === 0
                || strpos($href, '/') === 0
            ) {
                $link->removeAttribute('rel');

                $textIsChanged = true;
            } elseif (strpos($href, '#') === 0) {
                $link->setAttribute('href', '#anchor-' . substr($href, 1));
                $link->removeAttribute('target');
                $link->removeAttribute('rel');

                $textIsChanged = true;
            } elseif ($userHasPermissions
                && gettype($firstChild = $link->firstChild) === 'object'
                && $firstChild instanceof DOMElement
                && $firstChild->nodeName === 'mark'
            ) {
                $link->removeAttribute('rel');

                // To delete the 'mark' tag
                $link->nodeValue = $link->nodeValue;

                $textIsChanged = true;
            }
        }

        if ($textIsChanged) {
            // remove all extra characters and update text
            $text = str_replace(['<div>', '</div>', "\n"], '', utf8_decode($dom->saveHTML($dom->documentElement)));
        }

        return $text;
    }
}
