<?php

namespace App\Services\Post;

use App\Exceptions\ForbiddenException;
use App\Exceptions\NotFoundException;
use App\Models\Post;
use App\Models\User;
use App\Repositories;
use Carbon\Carbon;
use EditorJS\EditorJS;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Str;

class WritingService
{
    private Repositories\Post\WritingRepository $writingRepository;

    public function __construct(Repositories\Post\WritingRepository $writingRepository)
    {
        $this->writingRepository = $writingRepository;
    }

    /**
     * @param User $user
     * @param Post $post
     * @param string $title Article title
     * @param array $editorBlocks Blocks with the article data. {@see EditorJS::getBlocks()}
     * @param bool $publish Status of the article(draft, posted, etc.)
     *
     * @return Post
     *
     * @throws BindingResolutionException
     */
    public function updatePost(
        User $user,
        Post $post,
        string $title,
        array $editorBlocks,
        bool $publish = false
    ): Post {
        /**
         * If the $publish is TRUE and the article is new (this is a rare situation but it can be)
         * then the article does not have an ID yet.
         * This variable is needed to check this situation after saving the data, when the ID will exist.
         */
        $postId = $post->id;

        $title = $this->sanitizeTitle($title);

        $post->title = $title;
        $post->meta_title = $title . ' | MBH News';
        $post->content_editor = json_encode(['blocks' => $editorBlocks]);

        if ($post->status === Post::STATUS_DRAFT) {
            $post->slug = Str::slug($title . '-' . $postId);

            if ($publish) {
                $post->status = Post::STATUS_POSTED;
                $post->posted_at = Carbon::now();
            }
        } elseif ($post->status === Post::STATUS_POSTED || $publish) {
            $post->content = json_encode($this->getContentFromEditorBlocks($user, $editorBlocks));
        }

        if (!$post->save()) {
            throw new Exception('Error of saving the post');
        }

        if ($publish && !$postId) {
            $post->slug = $post->slug . '-' . $post->id;
            if (!$post->save()) {
                throw new Exception('Error of saving the post');
            }
        }

        return $post;
    }

    /**
     * Checks user permission to update the article.
     *
     * @param User $user
     * @param Post $post
     *
     * @return bool
     */
    public function userCanWorkWithPost(User $user, Post $post): bool
    {
        // TODO change the role to this condition
        if ($post->author_id === $user->id || $user->can('service-member all')) {
            return true;
        }

        return false;
    }

    /**
     * Returns an article data for edit by the passed ID.
     *
     * @param User $user
     * @param int $postId
     *
     * @return Post
     *
     * @throws NotFoundException If the post is not found.
     * @throws ForbiddenException If the user can't work with the post.
     */
    public function getPermittedPostForEditOrThrowException(User $user, int $postId): Post
    {
        $post = $this->writingRepository->getDataForEdit($postId);

        $this->checkPermissionOfPost($user, $post);

        $post->editorData = $post->content_editor;
        $post->updatedAt = $post->updated_at;

        unset($post->content_editor);
        unset($post->updated_at);

        return $post;
    }


    /**
     * Returns an article data for preview by the passed ID.
     *
     * @param User $user
     * @param int $postId
     *
     * @return Post
     *
     * @throws NotFoundException If the post is not found.
     * @throws ForbiddenException If the user can't work with the post.
     */
    public function getPermittedPostOrThrowException(User $user, int $postId): Post
    {
        $post = $this->writingRepository->getData($postId);

        $this->checkPermissionOfPost($user, $post);

        $post->content = json_decode($post->content, true);

        $post->isDraft = $post->is_draft;
        $post->updatedAt = $post->updated_at;
        $post->authorId = $post->author_id;

        unset($post->author_id);
        unset($post->updated_at);

        return $post;
    }

    /**
     * Returns an article with data for edit by the passed ID
     * or returns a new article with the passed user as the author.
     *
     * @param User $user
     * @param int|null $postId
     *
     * @return Post
     *
     * @throws NotFoundException If the post is not found.
     * @throws ForbiddenException If the user can't work with the post.
     */
    public function getPermittedPostOrReturnNewOneWithCurrentUserAsAuthor(User $user, ?int $postId): Post
    {
        if (empty($postId)) {
            $post = new Post();
            $post->author_id = $user->id;

            return $post;
        }

        $post = $this->writingRepository->getDataForEdit($postId);

        $this->checkPermissionOfPost($user, $post);
        return $post;
    }

    public function publishPost(Post $post): bool
    {
        $post->status = Post::STATUS_POSTED;

        return $post->save();
    }

    public function unpublishPost(Post $post): bool
    {
        $post->status = Post::STATUS_DRAFT;

        return $post->save();
    }

    public function deletePost(Post $post): bool
    {
        $post->status = Post::STATUS_DELETED;

        return $post->save();
    }

    /**
     * Returns TRUE if the post is not NULL and the user is author or has permission to work with the post.
     *
     * @param User $user
     * @param Post|null $post
     *
     * @return bool
     */
    public function checkPermissionOfPost(User $user, ?Post $post): bool
    {
        if (empty($post)) {
            throw new NotFoundException();
        }

        if (!$this->userCanWorkWithPost($user, $post)) {
            throw new ForbiddenException();
        }

        return true;
    }

    /**
     * Removes all tags and all multiple spaces from the passed title.
     *
     * @param string $title
     *
     * @return string|string[]|null
     */
    private function sanitizeTitle(string $title)
    {
        return
            preg_replace(
                '/\s{2,}/',
                ' ',
                str_replace(['&nbsp;', '&nbsp'], ' ', strip_tags($title))
            );
    }

    /**
     * Returns an array of generated content blocks from editor blocks .
     *
     * @param User $user
     * @param array $editorBlocks
     *
     * @return array
     *
     * @throws BindingResolutionException
     */
    private function getContentFromEditorBlocks(User $user, array $editorBlocks): array
    {
        /** @var ContentGenerationService $contentGenerationService */
        $contentGenerationService = app()->make(ContentGenerationService::class);

        return
            $contentGenerationService->generate(
                $editorBlocks,
                $user->can('service-member all')
            );
    }
}
