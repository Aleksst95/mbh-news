<?php

namespace App\Services\Post;

use EditorJS\EditorJS;
use EditorJS\EditorJSException;
use Exception;

class EditorService
{
    /**
     * @param string $data The data from EditorJS in JSON
     * @param array $config
     *
     * @return array
     *
     * @throws EditorJSException
     * @throws Exception
     */
    public function sanitizeData(string $data, array $config): array
    {
        $editor = new EditorJS($data, json_encode($config));
        $blocks = $editor->getBlocks();

        foreach ($blocks as $key => $block) {
            switch ($block['type']) {
                case 'header':
                case 'quote':
                case 'embed':
                case 'image':
                    $blocks[$key]['data'] = $this->{'sanitize' . ucfirst($block['type'])}(
                        $block['data'],
                        $config['tools'][$block['type']]
                    );
                    break;
                case 'paragraph':
                case 'list':
                    $blocks[$key]['data'] = $this->{'sanitize' . ucfirst($block['type'])}($block['data']);
                    break;
            }
        }

        if (empty($blocks)) {
            throw new Exception('Empty blocks');
        }

        return $blocks;
    }

    /**
     * @param array $data user's data from a header block
     * @param array $config EditorJs config for Header Tool
     *
     * @return array
     *
     * @throws Exception
     */
    private function sanitizeHeader(
        array $data,
        array $config
    ): array {
        $text = $this->trimAndRemoveMultipleSpace($data['text']);

        if (empty($text)) {
            throw new Exception('Header is empty');
        }

        if (strlen($text) > ($config['text']['length'] ?? 100)) {
            throw new Exception('Header text is too long');
        }

        $anchor = preg_replace('~[^A-Za-z0-9_-]~i', '', $data['anchor']);

        if ($data['anchor'] !== $anchor) {
            throw new Exception('Invalid characters in anchor text');
        }

        if (strlen($data['anchor']) > ($config['anchor']['length'] ?? 50)) {
            throw new Exception('Anchor text is too long');
        }

        $data['text'] = $text;

        return $data;
    }


    /**
     * @param array $data user's data from a paragraph block
     *
     * @return array
     *
     * @throws Exception
     */
    private function sanitizeParagraph(
        array $data
    ): array {
        $text = $this->trimAndRemoveMultipleSpace($data['text']);

        if (empty($text)) {
            throw new Exception('Paragraph is empty');
        }

        $data['text'] = $text;

        return $data;
    }

    /**
     * @param array $data user's data from a list block
     *
     * @return array
     * @throws Exception
     */
    private function sanitizeList(
        array $data
    ): array {
        foreach ($data['items'] as $key => $item) {
            $text = $this->removeEmptyTags(
                $this->removeRepeatedBrTagsAndSpaceNearWithBrTag(
                    $this->trimAndRemoveMultipleSpace($item)
                )
            );

            if (empty($text)) {
                unset($data['items'][$key]);
            } else {
                $data['items'][$key] = $text;
            }
        }

        if (count($data['items']) === 0) {
            throw new Exception('List is empty');
        }

        return $data;
    }

    /**
     * @param array $data user's data from a quote block
     * @param array $config EditorJs config for the Quote Tool
     *
     * @return array
     *
     * @throws Exception
     */
    private function sanitizeQuote(
        array $data,
        array $config
    ): array {
        $textQuote = $this->trimAndRemoveMultipleSpace($data['text']);
        $textCaption = $this->trimAndRemoveMultipleSpace($data['caption']);

        if (empty($textQuote)) {
            throw new Exception('Quote is empty');
        }

        if (strlen($textCaption) > ($config['caption']['length'] ?? 255)) {
            throw new Exception('Quote\'s caption is too long');
        }

        $data['text'] = $textQuote;
        $data['caption'] = $textCaption;

        return $data;
    }

    /**
     * @param array $data user's data from a quote block
     * @param array $config EditorJs config for the Quote Tool
     *
     * @return array
     *
     * @throws Exception
     */
    private function sanitizeEmbed(
        array $data,
        array $config
    ): array {
        $textCaption = $this->trimAndRemoveMultipleSpace($data['caption']);

        if (strlen($textCaption) > ($config['caption']['length'] ?? 255)) {
            throw new Exception('Embed\'s caption is too long');
        }

        $data['caption'] = $textCaption;

        return $data;
    }

    /**
     * @param array $data user's data from an image block
     * @param array $config EditorJs config for the Image Tool
     *
     * @return array
     *
     * @throws Exception
     */
    private function sanitizeImage(
        array $data,
        array $config
    ): array {
        $url = $data['file']['url'];

        $parsedUrl = parse_url($url);

        if (empty($parsedUrl['scheme']) || empty($parsedUrl['host'])) {
            throw new Exception('Invalid image url');
        }

        $textCaption = $this->trimAndRemoveMultipleSpace($data['caption']);

        if (strlen($textCaption) > ($config['caption']['length'] ?? 255)) {
            throw new Exception('Image\'s caption is too long');
        }

        $data['caption'] = $textCaption;

        return $data;
    }

    private function trimAndRemoveMultipleSpace(string $text): string
    {
        /**
         * There is a text after html_entity_decode() here. This is why we can't just trim () it.
         * We need to do str_replace() first, where chr (194). chr (160) is the space after html_entity_decode ().
         */
        return trim(
            preg_replace(
                '/\s{2,}/',
                ' ',
                str_replace(chr(194) . chr(160), ' ', $text)
            )
        );
    }

    /**
     * Replaces all repeated 'br' tags by the single 'br' tag and removes whitespaces near with 'br' tag.
     *
     * @param string $text
     *
     * @return string
     */
    private function removeRepeatedBrTagsAndSpaceNearWithBrTag(string $text): string
    {
        return preg_replace('/(\s?<br\s\/>\s?)+/', '<br />', $text);
    }

    /**
     * Removes empty tags or tags with just the 'br' tag or spaces.
     *
     * It is not a smart algorithm. This function doesn't remove tag with empty tags only.
     *
     * @param string $text
     *
     * @return string
     */
    private function removeEmptyTags(string $text): string
    {
        $text = preg_replace('/\s?<i>(\s|<br \/>)?<\/i>\s?/', '', $text);
        $text = preg_replace('/\s?<b>(\s|<br \/>)?<\/b>\s?/', '', $text);
        $text = preg_replace('/\s?<mark>(\s|<br \/>)?<\/mark>\s?/', '', $text);
        $text = preg_replace('/\s?<a(\s+[^>]*)*>(\s|<br \/>)?<\/a>\s?/', '', $text);

        if ($text === '<br />') {
            return '';
        }

        return $text;
    }
}
