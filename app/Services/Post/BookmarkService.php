<?php

namespace App\Services\Post;

use App\Models\Post\Bookmark;
use App\Repositories;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;

class BookmarkService
{
    private Repositories\Post\BookmarkRepository $bookmarkRepository;

    public function __construct(Repositories\Post\BookmarkRepository $bookmarkRepository)
    {
        $this->bookmarkRepository = $bookmarkRepository;
    }

    /**
     * @param int $userId
     * @param array $postIds
     *
     * @return Collection
     */
    public function getUserBookmarksByPostIds(int $userId, array $postIds): Collection
    {
        $items = $this->bookmarkRepository->getUserBookmarkPostsByPostIds($userId, $postIds);

        $items = $items->map(
            function ($item) {
                return $item['postId'];
            }
        );

        return $items;
    }

    /**
     * Adds the post to user bookmarks
     *
     * @param int $userId
     * @param int $postId
     *
     * @return bool
     *
     * @throws BindingResolutionException
     */
    public function addPost(int $userId, int $postId): bool
    {
        /** @var PostedService $postService */
        $postService = app()->make(PostedService::class);

        // Check that the post exists and is published
        $postService->getByIdOrThrowException($postId);

        // TODO check the article is posted
        $item = Bookmark::firstOrCreate(['user_id' => $userId, 'post_id' => $postId]);

        if ($item) {
            return true;
        }

        return false;
    }

    /**
     * Removes the post from user bookmarks
     *
     * @param int $userId
     * @param int $postId
     *
     * @return bool
     *
     * @throws Exception
     */
    public function deletePost(int $userId, int $postId): bool
    {
        return Bookmark::where([['user_id', $userId], ['post_id', $postId]])->delete() ? true : false;
    }
}
