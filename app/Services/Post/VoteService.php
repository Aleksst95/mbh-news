<?php

namespace App\Services\Post;

use App\Models\Post\Vote;
use App\Repositories\Post\VoteRepository;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;

class VoteService
{
    private VoteRepository $voteRepository;

    // TODO make a job for recounting post rating

    public function __construct(VoteRepository $voteRepository)
    {
        $this->voteRepository = $voteRepository;
    }

    /**
     * Returns an array of user's votes to articles.
     *
     *  Format: [ POST_ID => ['post_id' => POST_ID, 'vote' => VOTE], ... ]
     *
     * @param int $userId
     * @param array $postIds
     *
     * @return array
     */
    public function getUserVotesToPosts(int $userId, array $postIds): array
    {
        $items = $this->voteRepository->getUserVoteToPosts($userId, $postIds);

        if ($items->count() < count($postIds)) {
            $itemsWithoutVote = array_diff($postIds, $items->pluck('post_id')->all());

            foreach ($itemsWithoutVote as $itemId) {
                $items->push(['post_id' => intval($itemId), 'type' => 0]);
            }
        }

        return $items->all();
    }

    /**
     * Adds user vote for the post
     *
     * @param int $userId
     * @param int $postId
     * @param int $type
     *
     * @return bool
     * @throws BindingResolutionException
     */
    public function addVote(int $userId, int $postId, int $type): bool
    {
        /** @var PostedService $postService */
        $postService = app()->make(PostedService::class);

        // Check that the post exists and is published
        $post = $postService->getByIdOrThrowException($postId);

        // TODO check the article is posted
        $item = Vote::firstOrCreate(['user_id' => $userId, 'post_id' => $postId]);

        // We use the 'countPositiveVotes' instead of the 'count_positive_votes' below
        // because the post service returns the post data  'count_positive_votes as countPositiveVotes'.
        // The same situation with the 'count_negative_votes' field

        if ($item && $item->type === null) { // This is a new item
            $item->type = $type;
            if ($type === Vote::TYPE_NEGATIVE) {
                $post->count_negative_votes = $post->countNegativeVotes + 1;
            } else {
                $post->count_positive_votes = $post->countPositiveVotes + 1;
            }

            // TODO add transaction
            return $item->save() && $post->save();
        } elseif ($item && $item->type === $type) {
            return true;
        } elseif ($item && $item->type === Vote::TYPE_POSITIVE && $type === Vote::TYPE_NEGATIVE) {
            $item->type = Vote::TYPE_NEGATIVE;
            $post->count_negative_votes = $post->countNegativeVotes + 1;
            $post->count_positive_votes = $post->countPositiveVotes - 1;

            // TODO add transaction
            return $item->save() && $post->save();
        } elseif ($item && $item->type === Vote::TYPE_NEGATIVE && $type === Vote::TYPE_POSITIVE) {
            $item->type = Vote::TYPE_POSITIVE;
            $post->count_positive_votes = $post->countPositiveVotes + 1;
            $post->count_negative_votes = $post->countNegativeVotes - 1;

            // TODO add transaction
            return $item->save() && $post->save();
        }

        return false;
    }

    /**
     * Removes user vote for the post
     *
     * @param int $userId
     * @param int $postId
     *
     * @return bool
     *
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function deleteVote(int $userId, int $postId): bool
    {
        /** @var Vote $vote */
        $vote = Vote::where([['user_id', $userId], ['post_id', $postId]])->first();

        if (empty($vote)) {
            throw new Exception('User did not vote for the post');
        }

        /** @var PostedService $postService */
        $postService = app()->make(PostedService::class);

        // Check that the post exists and is published
        $post = $postService->getByIdOrThrowException($postId);

        if ($vote->type === Vote::TYPE_NEGATIVE) {
            // We use the 'countNegativeVotes' instead of the 'count_negative_votes'
            // because the post service returns the post data  'count_negative_votes as countNegativeVotes'
            $post->count_negative_votes = $post->countNegativeVotes - 1;
        } else {
            // We use the 'countPositiveVotes' instead of the 'count_positive_votes'
            // because the post service returns the post data  'count_positive_votes as countPositiveVotes'
            $post->count_positive_votes = $post->countPositiveVotes - 1;
        }

        // TODO add transaction
        return $vote->delete() && $post->save();
    }
}
