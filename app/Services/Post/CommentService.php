<?php

namespace App\Services\Post;

use App\Exceptions\NotFoundException;
use App\Models\Post;
use App\Models\Post\Comment;
use App\Repositories\Post\CommentRepository;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\DB;
use PDOException;
use Throwable;

class CommentService
{
    private CommentRepository $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    /**
     * Adds a user comment that does not have a parent
     *
     * @param int $userId
     * @param int $postId
     * @param string $message
     *
     * @return Comment
     *
     * @throws BindingResolutionException
     * @throws Exception
     */
    public function addCommentWithoutParent(int $userId, int $postId, string $message): Comment
    {
        $message = $this->sanitizeMessageAndThrowExceptionIfEmpty($message);

        $post = $this->getPostOrThrowException($postId);

        $newNodeId = $this->commentRepository->getNextNodeIdForPost($postId);

        $comment = $this->createNewCommentWithoutParent($userId, $postId, $message, $newNodeId);

        $post->increment('count_comments');

        return $comment;
    }

    /**
     * Creates a new user comment that has a parent
     *
     * @param int $userId
     * @param int $postId
     * @param string $message
     * @param int $commentParentId
     *
     * @return Comment
     *
     * @throws BindingResolutionException
     * @throws Throwable
     */
    public function addCommentWithParent(int $userId, int $postId, string $message, int $commentParentId): Comment
    {
        $message = $this->sanitizeMessageAndThrowExceptionIfEmpty($message);

        $post = $this->getPostOrThrowException($postId);

        $commentParent = $this->getByIdAndPostIdOrThrowException($commentParentId, $postId);

        try {
            DB::beginTransaction();

            $this->updateNodeKeys($postId, $commentParent->node_id, $commentParent->right_key);

            $comment = $this->createNewCommentWithParent($userId, $postId, $message, $commentParent);

            $post->increment('count_comments');

            DB::commit();
        } catch (PDOException $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }

        return $comment;
    }

    /**
     * @param int $postId
     *
     * @return array
     *
     * @throws BindingResolutionException
     */
    public function getFirstAndSecondLevelsForPostWithUserAsTree(int $postId): array
    {
        $this->checkPostOrThrowException($postId);

        $items = $this->commentRepository->getForPostWithUser($postId);

        $newItems = [];

        // To create ordered array
        $nodeIdToIndexesOfNewItems = [];
        $index = 0;

        /** @var Comment $item */
        foreach ($items as $item) {
            $item->children = [];

            if (!isset($nodeIdToIndexesOfNewItems[$item->nodeId])) {
                $nodeIdToIndexesOfNewItems[$item->nodeId] = $index++;
            }

            if (empty($newItems[$nodeIdToIndexesOfNewItems[$item->nodeId]])) {// This is the first level comment
                $newItems[$nodeIdToIndexesOfNewItems[$item->nodeId]] = $item;
            } else {// This is the second level comment
                $newItems[$nodeIdToIndexesOfNewItems[$item->nodeId]]->children = array_merge(
                    $newItems[$nodeIdToIndexesOfNewItems[$item->nodeId]]->children,
                    [$item]
                );
            }
        }

        return $newItems;
    }

    /**
     * @param int $postId
     * @param int $parentCommentId
     *
     * @return array
     *
     * @throws BindingResolutionException
     */
    public function getForPostWithUserByParentCommentId(int $postId, int $parentCommentId): array
    {
        $this->checkPostOrThrowException($postId);

        $parentComment = $this->getByIdAndPostIdOrThrowException($parentCommentId, $postId);

        $items = $this->commentRepository->getForPostWithUser($postId, $parentComment);

        return $items->all();
    }

    public function getByIdAndPostIdOrThrowException(int $commentId, int $postId): Comment
    {
        $item = $this->commentRepository->getByIdAndPostId($commentId, $postId);

        if (empty($item)) {
            throw new NotFoundException();
        }

        return $item;
    }

    private function createNewCommentWithoutParent(int $userId, int $postId, string $message, int $nodeId): Comment
    {
        return Comment::create(
            [
                'user_id' => $userId,
                'post_id' => $postId,
                'message' => $message,
                'node_id' => $nodeId,
                'level' => 1,
                'left_key' => 1,
                'right_key' => 2,
            ]
        );
    }

    private function createNewCommentWithParent(
        int $userId,
        int $postId,
        string $message,
        Comment $commentParent
    ): Comment {
        return Comment::create(
            [
                'user_id' => $userId,
                'post_id' => $postId,
                'message' => $message,
                'parent_id' => $commentParent->id,
                'node_id' => $commentParent->node_id,
                'level' => $commentParent->level + 1,
                'left_key' => $commentParent->right_key,
                'right_key' => $commentParent->right_key + 1,
            ]
        );
    }

    /**
     * @param int $postId
     * @param int $nodeId
     * @param int $key
     *
     * @return bool
     */
    private function updateNodeKeys(int $postId, int $nodeId, int $key): bool
    {
        return Comment::where('post_id', $postId)
            ->where('node_id', $nodeId)
            ->where('right_key', '>=', $key)
            ->update(
                [
                    'left_key' => DB::raw('CASE WHEN left_key > ' . $key . ' THEN left_key + 2 ELSE left_key END'),
                    'right_key' => DB::raw('right_key + 2'),
                ]
            );
    }

    /**
     * Removes double spaces, trims, and strips tags from the message.
     *
     *
     * @param string $message
     *
     * @return string
     *
     * @throws Exception
     */
    private function sanitizeMessageAndThrowExceptionIfEmpty(string $message): string
    {
        $message = trim(
            preg_replace(
                '/\s{2,}/',
                ' ',
                str_replace(['&nbsp;', '&nbsp'], ' ', strip_tags($message))
            )
        );

        if (empty($message)) {
            throw new Exception('Message is empty');
        }

        return $message;
    }

    /**
     * @param $postId
     *
     * @return Post
     *
     * @throws BindingResolutionException
     */
    private function getPostOrThrowException($postId): Post
    {
        /** @var PostedService $postService */
        $postService = app()->make(PostedService::class);

        return $postService->getByIdOrThrowException($postId);
    }

    /**
     * @param $postId
     *
     * @return bool
     *
     * @throws BindingResolutionException
     */
    private function checkPostOrThrowException($postId): bool
    {
        if ($this->getPostOrThrowException($postId)) {
            return true;
        }

        return false;
    }
}
