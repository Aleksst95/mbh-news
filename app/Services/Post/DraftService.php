<?php

namespace App\Services\Post;

use App\Models\Post;
use App\Repositories;
use Illuminate\Database\Eloquent\Collection;

class DraftService
{
    private Repositories\Post\DraftRepository $postRepository;

    public function __construct(Repositories\Post\DraftRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getAllUserPostsWithoutContent(
        int $userId,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $items = $this->postRepository->getAllUserPostsWithoutContent(
            $userId,
            $page,
            $limitPerPage,
            $largestItemId
        );

        /** @var Post $item */
        foreach ($items as &$item) {
            $item->isDraft = $item->is_draft;
            unset($item->status);
        }

        return $items;
    }
}
