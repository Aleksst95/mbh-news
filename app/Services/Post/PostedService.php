<?php

namespace App\Services\Post;

use App\Exceptions\NotFoundException;
use App\Exceptions\RedirectException;
use App\Models\Post;
use App\Repositories;
use Illuminate\Contracts\Container\BindingResolutionException;

class PostedService
{
    private Repositories\Post\PostedRepository $postRepository;

    public function __construct(Repositories\Post\PostedRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param string $slug Field `slug` from `posts` table.
     *
     * @return Post
     *
     * @throws BindingResolutionException
     */
    public function getBySlugOrThrowException(string $slug): Post
    {
        $item = $this->postRepository->getBySlug($slug);

        // Check the url changing action if the Item is empty
        if (empty($item)) {
            /** @var OldUrlService $oldPostUrlService */
            $oldPostUrlService = app()->make(OldUrlService::class);

            if ($newUrl = $oldPostUrlService->getNewLinkForOldPostUrl($slug)) {
                $oldPostUrlService->markUsageOfPostOldUrl($slug);

                throw new RedirectException(url('/' . $newUrl));
            } else {
                throw new NotFoundException();
            }
        }

        $item->content = json_decode($item->content, true);

        return $item;
    }

    /**
     * @param int $id
     *
     * @return Post
     *
     * @throws NotFoundException
     */
    public function getByIdOrThrowException(int $id): Post
    {
        $item = $this->postRepository->getById($id);

        if (empty($item)) {
            throw new NotFoundException();
        }

        return $item;
    }
}
