<?php

namespace App\Services\Post;

use App\Http\Requests\API\V1\Main\Authenticated\Post\Writing\UploadFileFromDevice;
use App\Models\Image;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Mime\MimeTypes;

class ImageService
{
    private string $path = 'images' . DIRECTORY_SEPARATOR . 'posts';

    public function uploadFileFromDeviceAndGetUrl(int $userId, UploadFileFromDevice $request): string
    {
        $uploadedFile = $request->file('file');
        $fileName = md5($userId . '_' . time()) . '.' . $uploadedFile->extension();

        Storage::disk('public')->putFileAs(
            $this->path,
            $uploadedFile,
            $fileName
        );

        $this->createNewImage($userId, Image::TYPE_POST, $this->path, $fileName);

        return asset('storage' . DIRECTORY_SEPARATOR . $this->path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * @param int $userId
     * @param string $url
     *
     * @return string
     *
     * @throws Exception
     */
    public function uploadFileByUrlAndGetUrl(int $userId, string $url): string
    {
        $extension = $this->checkFileWithoutDownLoadingAndGetExtension($url);

        if (strpos($url, env('APP_URL')) === 0) {
            return $url;
        }

        $response = Http::get($url);

        if ($response->status() !== 200) {
            throw new Exception('status');
        }

        $fileName = md5($userId . '_' . time()) . '.' . $extension;

        Storage::disk('public')->put(
            $this->path . DIRECTORY_SEPARATOR . $fileName,
            $response->body(),
        );

        $this->createNewImage($userId, Image::TYPE_POST, $this->path, $fileName);

        return asset('storage' . DIRECTORY_SEPARATOR . $this->path . DIRECTORY_SEPARATOR . $fileName);
    }

    /**
     * TODO allocate to parent class
     *
     * @param int $type
     * @param string $path
     * @param string $filename
     *
     * @return Image
     */
    private function createNewImage(int $userId, int $type, string $path, string $filename): Image
    {
        $image = new Image();

        $image->user_id = $userId;
        $image->type = $type;
        $image->path = $path;
        $image->filename = $filename;

        $image->save();

        return $image;
    }

    /**
     * @param string $url
     *
     * @return string
     *
     * @throws Exception
     */
    private function checkFileWithoutDownLoadingAndGetExtension(string $url): string
    {
        $response = Http::head($url);

        if ($response->status() !== 200) {
            throw new Exception('status');
        }

        // Check mime type
        if (
        empty(
        $extension = array_intersect(
            (new MimeTypes())->getExtensions($response->header('Content-Type')),
            Image::AVAILABLE_EXTENSIONS
        )
        )
        ) {
            throw new Exception('mimes');
        }

        // Check maximum size
        if (Image::MAX_FILE_SIZE < ($response->header('Content-Length') / 1024)) {
            throw new Exception('size');
        }

        return $extension[0];
    }
}
