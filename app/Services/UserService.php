<?php

namespace App\Services;

use App\Models\User\SocialAccount;
use App\Models\User;
use App\Repositories\EmailRepository;
use App\Repositories\SocialAccountRepository;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class UserService
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Returns common user data or throws an exception if the user is not found
     *
     * @param int $id User ID
     *
     * @return User
     */
    public function getByIdOrThrowException(int $id): User
    {
        $item = $this->userRepository->getById($id);

        if (empty($item)) {
            throw new ModelNotFoundException();
        }

        $item['permissions'] = $item->can;

        return $item;
    }

    /**
     * Returns an additional data of the user or throw an exception.
     *
     * The additional data: [
     *     'email' => EMAIL, // if email is confirmed or an empty string
     *     'linkedAccounts' => ['vkontakte', 'facebook', ...], // All linked social accounts of the user or empty array
     *     'getWeeklyNewsletter' => true/false, // if the user gets weekly newsletter - TRUE, else - FALSE
     * ]
     *
     * @param int $id
     *
     * @return array
     * @throws BindingResolutionException
     */
    public function getAdditionalDataByIdOrThrowException(int $id): array
    {
        $user = $this->userRepository->getById($id);

        if (empty($user)) {
            throw new ModelNotFoundException();
        }

        $data = [
            'email' => '',
            'getWeeklyNewsletter' => false,
            'linkedAccounts' => [],
        ];

        // Getting email and newsletter subscription status
        /** @var EmailRepository $emailRepository */
        $emailRepository = app()->make(EmailRepository::class);

        $email = $emailRepository->getConfirmedEmailByUserId($id);

        if (!empty($email)) {
            $data['email'] = $email->email;
            $data['getWeeklyNewsletter'] = $email->delivery_status;
        }

        // Getting linked social accounts
        /** @var SocialAccountRepository $socialAccountRepository */
        $socialAccountRepository = app()->make(SocialAccountRepository::class);

        $linkedAccounts = $socialAccountRepository->getProvidersByUserId($id);

        $data['linkedAccounts'] = $linkedAccounts->map(
            function (SocialAccount $item) {
                return $item->provider;
            }
        )->toArray();

        return $data;
    }


    /**
     * Creates new User.
     * Data must be valid.
     *
     * @param array $data
     *
     * @return User|null
     */
    public function createNewUser(array $data): ?User
    {
        return User::create($data);
    }
}
