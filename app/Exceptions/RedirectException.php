<?php

namespace App\Exceptions;

use RuntimeException;

class RedirectException extends RuntimeException
{
}