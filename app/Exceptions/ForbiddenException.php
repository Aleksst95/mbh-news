<?php

namespace App\Exceptions;

use RuntimeException;

class ForbiddenException extends RuntimeException
{
}
