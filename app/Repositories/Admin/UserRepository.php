<?php

namespace App\Repositories\Admin;

use App\Models\Admin\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends BaseRepository
{
    private array $commonColumns = [
        'id',
        'first_name',
        'last_name',
        'avatar',
        'created_at'
    ];

    private array $commonEmailColumns = [
        'id',
        'user_id',
        'email'
    ];

    public function model(): string
    {
        return User::class;
    }

    public function getAll(): Collection
    {
        $columns = $this->commonColumns;
        $columnsEmail = $this->commonEmailColumns;

        return $this->startCondition()
            ->select($columns)
            ->with([
                'confirmedEmail' => function ($query) use ($columnsEmail) {
                    $query->select($columnsEmail);
                }
            ])
            ->get();
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function getById(int $id): ?User
    {
        return $this->startCondition()
            ->with([
                'allEmails',
                'confirmedEmail',
                'posts',
                'socials',
                'votes',
                'comments',
                'complaints',
            ])
            ->where('id', $id)
            ->first();
    }
}
