<?php

namespace App\Repositories\Admin;

use App\Models\Post;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class PostRepository extends BaseRepository
{
    private array $commonPostColumns = [
        'id',
        'author_id',

        'slug',

        'title',
        'description',
        'cover_img',

        'meta_title',
        'meta_description',

        'count_view',
        'count_comments',
        'count_positive_votes',
        'count_negative_votes',

        'type',
        'status',

        'posted_at'
    ];

    private array $commonUserColumns = [
        'id',
        'first_name',
        'last_name',
        'avatar'
    ];

    public function model(): string
    {
        return Post::class;
    }

    public function getAll(): Collection
    {
        $columnsPost = $this->commonPostColumns;
        $columnsUser = $this->commonUserColumns;


        return $this->startCondition()
            ->select($columnsPost)
            ->with([
                'user' => function (BelongsTo $query) use ($columnsUser) {
                    $query->select($columnsUser);
                }
            ])
            ->get();
    }

    public function getById(int $id): Post
    {
        return $this->startCondition()
            ->with([
                'user'
            ])
            ->where('id', $id)
            ->first();
    }
}
