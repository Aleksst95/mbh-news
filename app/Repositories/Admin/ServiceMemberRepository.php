<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ServiceMember;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ServiceMemberRepository extends BaseRepository
{
    protected array $commonColumns = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    protected array $commonUserColumns = [
        'id',
        'first_name',
        'last_name',
        'avatar',
    ];

    public function model(): string
    {
        return ServiceMember::class;
    }

    public function getAllWithData(): Collection
    {
        $columns = $this->commonColumns;
        $columnsUser = $this->commonUserColumns;

        return $this->startCondition()
            ->select($columns)
            ->with([
                'user' => function (BelongsTo $query) use ($columnsUser) {
                    $query->select($columnsUser);
                }
            ])
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function getById(int $id): ServiceMember
    {
        $columnsUser = $this->commonUserColumns;

        return $this->startCondition()
            ->select($this->commonColumns)
            ->with([
                'user' => function (BelongsTo $query) use ($columnsUser) {
                    $query->select($columnsUser);
                }
            ])
            ->where('id', $id)
            ->first();
    }
}
