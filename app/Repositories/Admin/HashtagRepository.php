<?php

namespace App\Repositories\Admin;

use App\Models\Post\Hashtag;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

class HashtagRepository extends BaseRepository
{
    public function model(): string
    {
        return Hashtag::class;
    }

    public function getAllWithData(): Collection
    {
        return $this->startCondition()
            ->orderBy('name')
            ->get();
    }
}
