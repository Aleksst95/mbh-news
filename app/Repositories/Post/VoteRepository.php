<?php

namespace App\Repositories\Post;

use App\Models\Post\Vote;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

class VoteRepository extends BaseRepository
{
    public function model(): string
    {
        return Vote::class;
    }

    /**
     * Returns a Like object with a type of user mark for the post.
     *
     * @param int $userId
     * @param array $postIds
     *
     * @return Collection
     */
    public function getUserVoteToPosts(int $userId, array $postIds): Collection
    {
        $columns = [
            'post_id',
            'type',
        ];

        return $this->startCondition()
            ->select($columns)
            ->where('user_id', $userId)
            ->whereIn('post_id', $postIds)
            ->orderBy('post_id')
            ->get();
    }
}
