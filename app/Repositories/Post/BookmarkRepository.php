<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Models\Post\Bookmark;
use App\Repositories\BaseRepository;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class BookmarkRepository extends BaseRepository
{
    // TODO dedicate common class for repositories connected with posted articles

    private array $commonPostColumns = [
        'id',
        'author_id as authorId',

        'slug',

        'title',
        'description',
        'cover_img as coverImage',

        'meta_title as metaTitle',
        'meta_description as metaDescription',

        'count_view as countView',
        'count_comments as countComments',
        'count_positive_votes as countPositiveVotes',
        'count_negative_votes as countNegativeVotes',

        'type',

        'posted_at as postedAt',
    ];

    public function model(): string
    {
        return Bookmark::class;
    }

    /**
     * Returns all user bookmark published posts.
     *
     * @param int $userId
     * @param int $page
     * @param int $limitPerPage
     * @param string|null $largestCreatedAt
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAllWithoutContent(
        int $userId,
        int $page,
        int $limitPerPage,
        ?string $largestCreatedAt
    ): \Illuminate\Support\Collection {
        $dataBaseTablePrefix = config('database.table_prefix');
        $columns = collect($this->commonPostColumns);

        $columns = $columns->map(
            function ($item) use ($dataBaseTablePrefix) {
                return $dataBaseTablePrefix . 'posts.' . $item;
            }
        );

        $columns->push($dataBaseTablePrefix . 'bookmarks.created_at as bookmarkCreatedAt');

        $offset = $this->calculateOffset($page, $limitPerPage);

        return DB::table('posts')
            ->leftJoin('bookmarks', 'posts.id', '=', 'bookmarks.post_id')
            ->select(DB::raw($columns->implode(', ')))
            ->where('bookmarks.user_id', $userId)
            ->where('posts.status', Post::STATUS_POSTED)
            ->when(
                $largestCreatedAt,
                function (Builder $query, $largestCreatedAt) {
                    $query->where('bookmarks.created_at', '<=', $largestCreatedAt);
                }
            )
            ->orderBy('bookmarks.created_at', 'DESC')
            ->limit($limitPerPage)
            ->when(
                $offset,
                function (Builder $query, $offset) {
                    $query->offset($offset);
                }
            )
            ->get();
    }

    /**
     * @param int $userId
     * @param array $postIds
     *
     * @return Collection
     */
    public function getUserBookmarkPostsByPostIds(int $userId, array $postIds): Collection
    {
        return $this->startCondition()
            ->select(['post_id as postId'])
            ->where('user_id', $userId)
            ->whereIn('post_id', $postIds)
            ->get();
    }

    /**
     * Applies all common actions to a Query Builder to get a list of posted articles.
     *
     * @param Builder $queryBuilder
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    private function applyTemplateActionsToGetList(
        Builder $queryBuilder,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $offset = $this->calculateOffset($page, $limitPerPage);

        return $queryBuilder
            ->when(
                $largestItemId,
                function (Builder $query, $largestItemId) {
                    $query->where('id', '<', $largestItemId);
                }
            )
            ->posted()/** {@see Post::scopePosted()} */
            ->orderBy('created_at', 'DESC')
            ->limit($limitPerPage)
            ->when(
                $offset,
                function (Builder $query, $offset) {
                    $query->offset($offset);
                }
            )
            ->get();
    }

    private function calculateOffset(int $page = 1, int $limitPerPage = 1): int
    {
        $offset = 0;

        if ($page > 1) {
            $offset = ($page - 1) * $limitPerPage;
        }

        return $offset;
    }
}
