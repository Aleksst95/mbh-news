<?php

namespace App\Repositories\Post;

use App\Models\Post\Comment;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class CommentRepository extends BaseRepository
{
    public function model(): string
    {
        return Comment::class;
    }

    /**
     *
     *
     * @param int $postId
     * @param Comment|null $parentComment
     *
     * @return Collection
     */
    public function getForPostWithUser(
        int $postId,
        ?Comment $parentComment = null
    ): \Illuminate\Support\Collection { // TODO rename or rewrite this method
        $dataBaseTablePrefix = config('database.table_prefix');

        $commentColumns = collect(
            [
                'id',
                'user_id as userId',
                //                'post_id as postId',
                'parent_id as parentId',
                'node_id as nodeId',
                'level',
                'left_key as leftKey',
                'right_key as rightKey',
                'rating',
                'created_at as createdAt',
            ]
        );

        $columns = $commentColumns->map(
            function ($item) use ($dataBaseTablePrefix) {
                return $dataBaseTablePrefix . 'post_comments.' . $item;
            }
        );

        // Hide the comment message if it does not have the 'published' status
        $columns->push(
            'CASE WHEN ' . $dataBaseTablePrefix . 'post_comments.status = ' . Comment::STATUS_PUBLISHED
            . ' THEN ' . $dataBaseTablePrefix . 'post_comments.message ELSE null END AS message'
        );

        $columns->push(
            'CASE WHEN FLOOR('
            . '('
            . $dataBaseTablePrefix . 'post_comments.right_key - ' . $dataBaseTablePrefix . 'post_comments.left_key'
            . ') / 2) > 0'
            . ' THEN 1 ELSE 0 END AS hasChildren'
        );

        $columns->push(
            'CASE WHEN ' . $dataBaseTablePrefix . 'post_comments.status = ' . Comment::STATUS_PUBLISHED
            . ' THEN 0 ELSE 1 END AS isDeleted'
        );

        $columns->push($dataBaseTablePrefix . 'users.first_name as userFirstName');
        $columns->push($dataBaseTablePrefix . 'users.last_name as userLastName');
        $columns->push($dataBaseTablePrefix . 'users.avatar as userAvatar');

        return DB::table('post_comments')
            ->leftJoin('users', 'post_comments.user_id', '=', 'users.id')
            ->select(DB::raw($columns->implode(', ')))
            ->where('post_comments.post_id', $postId)
            ->orderBy('post_comments.node_id')
            ->when(
                empty($parentComment),
                function (Builder $query) {
                    $query->where('post_comments.level', '<', 3); // TODO replace '3'
                    $query->orderBy('post_comments.left_key');
                }
            )
            ->when(
                !empty($parentComment),
                function (Builder $query) use ($parentComment) {
                    $query->where('post_comments.node_id', $parentComment->node_id);
                    $query->where('post_comments.left_key', '>', $parentComment->left_key);
                    $query->where('post_comments.right_key', '<', $parentComment->right_key);
                    $query->orderBy('post_comments.created_at');
                }
            )->get();
    }


    /**
     * Returns the comment by the given comment ID and post ID.
     *
     * @param int $commentId
     * @param int $postId
     *
     * @return Comment|null
     */
    public function getByIdAndPostId(int $commentId, int $postId): ?Comment
    {
        return $this->startCondition()
            ->where('id', $commentId)
            ->where('post_id', $postId)
            ->first();
    }

    public function getNextNodeIdForPost(int $postId): int
    {
        $maxNode = $this->startCondition()
            ->max('node_id');

        if (empty($maxNode)) {
            return 1;
        }

        return intval($maxNode) + 1;
    }
}
