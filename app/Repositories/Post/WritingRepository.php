<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/*
 * This class works with articles for Writing Post actions.
 */

class WritingRepository extends BaseRepository
{
    private array $commonFields = [
        'id',
        'author_id',

        'title',
        'slug',

        'status',
        'updated_at',
    ];

    public function model(): string
    {
        return Post::class;
    }

    public function getData(int $id): ?Post
    {
        return $this->startCondition()
            ->select(
                array_merge(
                    $this->commonFields,
                    [
                        'cover_img',
                        'description',
                        'content',

                        'count_view',
                        'count_comments',
                        'count_positive_votes',
                        'count_negative_votes',

                        'type',
                    ]
                )
            )
            ->where('id', $id)
            ->first();
    }

    /**
     * Returns the article with data for edit by the given article ID.
     *
     * @param int $id
     *
     * @return Post|null
     */
    public function getDataForEdit(int $id): ?Post
    {
        return $this->startCondition()
            ->select(
                array_merge(
                    $this->commonFields,
                    [
                        'content_editor'
                    ]
                )
            )
            ->where('id', $id)
            ->first();
    }
}
