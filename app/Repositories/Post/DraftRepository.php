<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/*
 * This class works only with draft articles.
 */

class DraftRepository extends BaseRepository
{
    private array $commonPostColumns = [
        'id',
        'author_id as authorId',

        'title',
        'cover_img as coverImage',
        'status',
    ];

    public function model(): string
    {
        return Post::class;
    }

    /**
     * Returns count of all draft user articles.
     *
     * @param int|null $largestItemId ID of the article to be last.
     *      For example, this is necessary when a user has uploaded the first batch of articles and wants to upload a
     *     new one. But in between these two actions, some other user has published an article. Then, if we don't set
     *     $largeItemId, we get the wrong offset.
     *
     * @return int
     */
    public function getCount(?int $largestItemId = null): int
    {
        return $this->startCondition()
            ->when(
                $largestItemId,
                function (Builder $query, $largestItemId) {
                    $query->where('id', '<', $largestItemId);
                }
            )
            ->draft()/** {@see Post::scopeDraft()} */
            ->count();
    }

    /**
     * Returns all user article drafts without content.
     *
     * @param int $userId
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see DraftRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    public function getAllUserPostsWithoutContent(
        int $userId,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $columnsPost = $this->commonPostColumns;

        $query = $this->startCondition()
            ->select($columnsPost)
            ->where('author_id', $userId);

        return $this->applyTemplateActionsToGetList($query, $page, $limitPerPage, $largestItemId);
    }

    /**
     * Applies all common actions to a Query Builder to get a list of draft articles.
     *
     * @param Builder $queryBuilder
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see DraftRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    private function applyTemplateActionsToGetList(
        Builder $queryBuilder,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $offset = $this->calculateOffset($page, $limitPerPage);

        return $queryBuilder
            ->when(
                $largestItemId,
                function (Builder $query, $largestItemId) {
                    $query->where('id', '<', $largestItemId);
                }
            )
            ->draft()/** {@see Post::scopeDraft()} */
            ->orderByUpdate()/** {@see Post::scopeOrderByUpdate()} */
            ->limit($limitPerPage)
            ->when(
                $offset,
                function (Builder $query, $offset) {
                    $query->offset($offset);
                }
            )
            ->get();
    }

    private function calculateOffset(int $page = 1, int $limitPerPage = 1): int
    {
        $offset = 0;

        if ($page > 1) {
            $offset = ($page - 1) * $limitPerPage;
        }

        return $offset;
    }
}
