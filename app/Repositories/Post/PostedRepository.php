<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/*
 * This class works only with posted articles.
 */

class PostedRepository extends BaseRepository
{
    private array $commonPostColumns = [
        'id',
        'author_id as authorId',

        'slug',

        'title',
        'description',
        'cover_img as coverImage',

        'meta_title as metaTitle',
        'meta_description as metaDescription',

        'count_view as countView',
        'count_comments as countComments',
        'count_positive_votes as countPositiveVotes',
        'count_negative_votes as countNegativeVotes',

        'type',

        'posted_at as postedAt',
    ];

    public function model(): string
    {
        return Post::class;
    }

    /**
     * Returns count of all posted articles.
     *
     * @param int|null $largestItemId ID of the article to be last.
     *      For example, this is necessary when a user has uploaded the first batch of articles and wants to upload a
     *     new one. But in between these two actions, some other user has published an article. Then, if we don't set
     *     $largeItemId, we get the wrong offset.
     *
     * @return int
     */
    public function getCount(?int $largestItemId = null): int
    {
        return $this->startCondition()
            ->when(
                $largestItemId,
                function (Builder $query, $largestItemId) {
                    $query->where('id', '<', $largestItemId);
                }
            )
            ->posted()/** {@see Post::scopePosted()} */
            ->count();
    }

    /**
     * Returns all posted articles without content.
     *
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    public function getAllWithoutContent(
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $columnsPost = $this->commonPostColumns;

        $query = $this->startCondition()
            ->select($columnsPost)
            ->withCount('votes');

        return $this->applyTemplateActionsToGetList($query, $page, $limitPerPage, $largestItemId);
    }

    /**
     * Returns all user posted articles without content.
     *
     * @param int $userId
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    public function getAllUserWithoutContent(
        int $userId,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $columnsPost = $this->commonPostColumns;

        $query = $this->startCondition()
            ->select($columnsPost)
            ->withCount('votes')
            ->where('author_id', $userId);

        return $this->applyTemplateActionsToGetList($query, $page, $limitPerPage, $largestItemId);
    }

    /**
     * Returns all posted articles without content by the given search query.
     *
     * @param string $searchString Query string
     * @param int $page
     * @param int $limitPerPage
     *
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    public function getAllByQueryWithoutContent(
        string $searchString = '',
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $columns = [
            'id',
            'slug',
            'title',
            'description',
            'cover_img as coverImage'
        ];

        $query = $this->startCondition()
            ->select($columns)
            ->where('title', 'LIKE', '%' . $searchString . '%')
            ->orWhere('description', 'LIKE', '%' . $searchString . '%');

        return $this->applyTemplateActionsToGetList($query, $page, $limitPerPage, $largestItemId);
    }

    /**
     * Returns all posted articles without content by the given tag name.
     *
     * @param string $tagName Name of the tag
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    public function getAllByTagWithoutContent(
        string $tagName = '',
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $columns = [
            'id',
            'slug',
            'title',
            'description',
            'cover_img'
        ];

        $query = $this->startCondition()
            ->select($columns)
            ->whereHas(
                'hashtags',
                function (Builder $query) use ($tagName) {
                    $query->where('name', $tagName);
                }
            );

        return $this->applyTemplateActionsToGetList($query, $page, $limitPerPage, $largestItemId);
    }

    /**
     * Returns the posted article by the given slug.
     *
     * @param string $slug
     *
     * @return Post|null
     */
    public function getBySlug(string $slug): ?Post
    {
        $columnsPost = array_merge($this->commonPostColumns, ['content']);

        return $this->startCondition()
            ->select($columnsPost)
            ->posted()/** {@see Post::scopePosted()} */
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Returns the posted article by the given article ID.
     *
     * @param int $id
     *
     * @return Post|null
     */
    public function getById(int $id): ?Post
    {
        $columnsPost = array_merge($this->commonPostColumns, ['content']);

        return $this->startCondition()
            ->select($columnsPost)
            ->posted()/** {@see Post::scopePosted()} */
            ->where('id', $id)
            ->first();
    }

    /**
     * Applies all common actions to a Query Builder to get a list of posted articles.
     *
     * @param Builder $queryBuilder
     * @param int $page
     * @param int $limitPerPage
     * @param int|null $largestItemId {@see PostedRepository::getCount(), parameter $largestItemId}
     *
     * @return Collection
     */
    private function applyTemplateActionsToGetList(
        Builder $queryBuilder,
        int $page = 1,
        int $limitPerPage = 10,
        ?int $largestItemId = null
    ): Collection {
        $offset = $this->calculateOffset($page, $limitPerPage);

        return $queryBuilder
            ->when(
                $largestItemId,
                function (Builder $query, $largestItemId) {
                    $query->where('id', '<', $largestItemId);
                }
            )
            ->posted()/** {@see Post::scopePosted()} */
            ->orderByPosted()/** {@see Post::scopeOrderByPosted()} */
            ->limit($limitPerPage)
            ->when(
                $offset,
                function (Builder $query, $offset) {
                    $query->offset($offset);
                }
            )
            ->get();
    }

    private function calculateOffset(int $page = 1, int $limitPerPage = 1): int
    {
        $offset = 0;

        if ($page > 1) {
            $offset = ($page - 1) * $limitPerPage;
        }

        return $offset;
    }
}
