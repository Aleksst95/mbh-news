<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserRepository extends BaseRepository
{
    private array $commonUserColumns = [
        'id',
        'first_name as firstName',
        'last_name as lastName',
        'avatar',
        'created_at as createdAt'
    ];

    public function model(): string
    {
        return User::class;
    }

    /**
     * @param int $itemId
     *
     * @return User|null
     */
    public function getById(int $itemId): ?User
    {
        return $this->startCondition()
            ->select($this->commonUserColumns)
            ->where('id', $itemId)
            ->first();
    }

    /**
     * @param array $itemIds
     *
     * @return Collection
     */
    public function getItemsById(array $itemIds): Collection
    {
        return $this->startCondition()
            ->select($this->commonUserColumns)
            ->whereIn('id', $itemIds)
            ->get();
    }
}
