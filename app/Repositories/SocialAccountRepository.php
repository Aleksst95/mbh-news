<?php

namespace App\Repositories;

use App\Models\User\SocialAccount;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class SocialAccountRepository extends BaseRepository
{
    public function model(): string
    {
        return SocialAccount::class;
    }

    /**
     * Returns a user by the provider name and provider ID.
     *
     * @param string $provider
     * @param string $providerId
     *
     * @return User|null
     */
    public function getUserByProviderAndProviderId(string $provider, string $providerId): ?User
    {
        $item = $this->startCondition()
            ->where(
                [
                    'provider' => $provider,
                    'provider_id' => $providerId,
                ]
            )
            ->first();

        return $item->user ?? null;
    }

    /**
     * Returns the provider by user ID and provider name.
     *
     * @param int $userId
     * @param string $provider
     *
     * @return User|null
     */
    public function getProviderByUserIdAndProviderName(int $userId, string $provider): ?SocialAccount
    {
        return $this->startCondition()
            ->where(
                [
                    'user_id' => $userId,
                    'provider' => $provider
                ]
            )
            ->first();
    }

    /**
     * Returns all linked user providers.
     *
     * @param int $userId
     *
     * @return Collection
     */
    public function getProvidersByUserId(int $userId): Collection
    {
        return $this->startCondition()
            ->where(
                [
                    'user_id' => $userId,
                ]
            )
            ->get();
    }
}
