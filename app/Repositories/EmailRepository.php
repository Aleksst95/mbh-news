<?php

namespace App\Repositories;

use App\Models\User\Email;

class EmailRepository extends BaseRepository
{
    public function model(): string
    {
        return Email::class;
    }

    public function getByEmail(string $email): ?Email
    {
        return $this->startCondition()
            ->where(
                [
                    'email' => $email
                ]
            )
            ->first();
    }

    public function getConfirmedEmailByUserId(int $userId): ?Email
    {
        return $this->startCondition()
            ->where(
                [
                    'user_id' => $userId
                ]
            )
            ->confirmed()/** {@see Email::scopeConfirmed()} */
            ->first();
    }

}
