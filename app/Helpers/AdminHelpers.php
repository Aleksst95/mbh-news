<?php

if (!function_exists('adminUrl')) {
    /**
     * Appends the configured backpack prefix and returns
     * the URL using the standard Laravel helpers.
     *
     * @param $path
     *
     * @param  array  $parameters
     * @param  null  $secure
     *
     * @return string
     */
    function adminUrl($path = null)
    {
        $path = !$path || (substr($path, 0, 1) == '/') ? $path : '/' . $path;

        return '/' . env('ADMIN_ROUTE_PREFIX', 'admin') . $path;
    }
}