<?php

namespace App\Traits;

trait ServiceMemberTrait
{
    public function checkTwoStepAuthStatus()
    {
        if (session(config('adminlte.admin_session_key'))) {
            return true;
        }

        return false;
    }
}
