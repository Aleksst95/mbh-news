<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Traits\ServiceMemberTrait;
use Illuminate\Support\Facades\Request;

class Admin
{
    use ServiceMemberTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isServiceMember()) {
            $requestUrl = str_replace(Request::root(), '', Request::url());

            if ($this->checkTwoStepAuthStatus()) {
                if ($requestUrl == adminUrl('login')) {
                    return redirect(adminUrl());
                }

                return $next($request);
            }

            if ($requestUrl == adminUrl('login')) {
                return $next($request);
            }

            return redirect(adminUrl('login'));
        }

        return redirect('/');
    }
}
