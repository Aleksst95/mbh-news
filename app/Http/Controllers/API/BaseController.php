<?php

namespace App\Http\Controllers\API;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    use AuthorizesRequests;
    use ValidatesRequests;

    /**
     * Returns a successful response.
     *
     * @param mixed $data
     * @param int $status
     *
     * @return JsonResponse
     */
    public function sendResponse($data = null, int $status = 200)
    {
        $response = null;

        if ($data !== null) {
            $response = $data;
        }

        return response()->json($response, $status);
    }

    /**
     * Returns a response with status code "created" (201).
     *
     * @param mixed $data
     *
     * @return JsonResponse
     */
    public function sendCreatedResponse($data = null)
    {
        $response = null;

        if ($data !== null) {
            $response = $data;
        }

        return response()->json($response, 201);
    }

    /**
     * Returns a successful response without body.
     *
     * @return JsonResponse
     */
    public function sendResponseWithoutBody()
    {
        return response()->json(null, 204);
    }

    /**
     * Returns an error response.
     *
     * @param string|null $message
     * @param array|null $errorMessages
     * @param int $code
     *
     * @return JsonResponse
     */
    public function sendErrorResponse(?string $message = null, ?array $errorMessages = [], int $code = 500)
    {
        $response = [];

        if ($message !== null) {
            $response['message'] = $message;
        }

        if (!empty($errorMessages)) {
            $response['errors'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

    /**
     * Returns a not found response.
     *
     * @param null $data
     *
     * @return JsonResponse
     */
    public function sendNotFoundResponse($data = null)
    {
        $response = null;

        if ($data !== null) {
            $response = $data;
        }

        return response()->json($response, 404);
    }
}
