<?php

namespace App\Http\Controllers\API\V1\Main;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\User\GetItemsByIdsRequest;
use App\Repositories;
use App\Services;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host="mbhn.ru",
 *   basePath="/",
 *   @SWG\Info(
 *     title="Posts API",
 *     version="1.0.0"
 *   )
 * )
 */
class UserController extends BaseController
{
    private Services\UserService $userService;
    private Repositories\UserRepository $userRepository;

    public function __construct(
        Services\UserService $userService,
        Repositories\UserRepository $userRepository
    ) {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id)
    {
        try {
            $item = $this->userService->getByIdOrThrowException($id);

            return $this->sendResponse($item);
        } catch (ModelNotFoundException $e) {
            return $this->sendNotFoundResponse();
        }
    }

    public function getByIds(GetItemsByIdsRequest $request)
    {
        $itemIds = $request->post('itemIds');

        $items = $this->userRepository->getItemsById($itemIds);

        return $this->sendResponse($items);
    }
}
