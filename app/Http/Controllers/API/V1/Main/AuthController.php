<?php

namespace App\Http\Controllers\API\V1\Main;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Auth\GetLinkToProviderRequest;
use App\Http\Requests\API\V1\Main\Auth\SocialLoginRequest;
use App\Models\User\SocialAccount;
use App\Services;
use Exception;
use Illuminate\Http\JsonResponse;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends BaseController
{
    private Services\SocialAccountService $socialAccountService;

    public function __construct(
        Services\SocialAccountService $socialAccountService
    ) {
        $this->socialAccountService = $socialAccountService;
    }

    /**
     * @param GetLinkToProviderRequest $request
     *
     * @return JsonResponse
     */
    public function getLinkToProvider(GetLinkToProviderRequest $request)
    {
        $provider = $request->get('name');

        return $this->sendResponse(Socialite::driver($provider)->stateless()->redirect()->getTargetUrl());
    }

    public function socialLogin(SocialLoginRequest $request)
    {
        $provider = $request->post('provider');
        try {
            // TODO fix the problem: Stateless authentication is not available for the Twitter driver, which uses OAuth 1.0 for authentication.
            // TODO replace this code to the socialAccountService
            $user = Socialite::driver($provider)->stateless()->user();

            switch ($provider) {
                case SocialAccount::PROVIDER_NAME_VK:
                    $socialUser = $this->socialAccountService->getUserAfterVkontakteAuth($user);
                    break;
                case SocialAccount::PROVIDER_NAME_FB:
                    $socialUser = $this->socialAccountService->getUserAfterFacebookAuth($user);
                    break;
            }

            auth()->login($socialUser);
        } catch (Exception $e) {
            return $this->sendErrorResponse('Something went wrong');
        }

        return $this->sendResponseWithoutBody();
    }

    public function logout()
    {
        auth()->logout();

        return $this->sendResponseWithoutBody();
    }
}
