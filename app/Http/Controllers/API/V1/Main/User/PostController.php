<?php

namespace App\Http\Controllers\API\V1\Main\User;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\User\GetUserPostsRequest;
use App\Repositories;
use App\Services;
use Illuminate\Http\JsonResponse;

class PostController extends BaseController
{
    private Services\Post\PostedService $postService;
    private Repositories\Post\PostedRepository $postRepository;

    public function __construct(
        Services\Post\PostedService $postService,
        Repositories\Post\PostedRepository $postRepository
    ) {
        $this->postService = $postService;
        $this->postRepository = $postRepository;
    }

    /**
     * Returns posts of the user.
     *
     * @param GetUserPostsRequest $request
     *
     * @return JsonResponse
     */
    public function posts(GetUserPostsRequest $request)
    {
        $userId = $request->get('userId');
        $page = $request->get('page') ?? 1;
        $firstItemId = $request->get('firstItemId');

        $limitPerPage = config('app.limit_post_per_page_for_index');

        $items = $this->postRepository->getAllUserWithoutContent($userId, $page, $limitPerPage, $firstItemId);

        return $this->sendResponse($items);
    }
}
