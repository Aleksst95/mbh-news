<?php

namespace App\Http\Controllers\API\V1\Main\Post;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Post\Comment\IndexRequest;
use App\Models\Post\Comment;
use Exception;
use App\Repositories;
use App\Services;
use Illuminate\Http\JsonResponse;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host="mbhn.ru",
 *   basePath="/",
 *   @SWG\Info(
 *     title="Comments API",
 *     version="1.0.0"
 *   )
 * )
 */
class CommentController extends BaseController
{
    private Services\Post\PostedService $postService;
    private Services\Post\CommentService $commentService;
    private Repositories\Post\CommentRepository $commentRepository;

    public function __construct(
        Services\Post\PostedService $postService,
        Services\Post\CommentService $commentService,
        Repositories\Post\CommentRepository $commentRepository
    ) {
        $this->postService = $postService;
        $this->commentService = $commentService;
        $this->commentRepository = $commentRepository;
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/posts/{id}/comments",
     *     summary="Get list of comments for the post",
     *     tags={"Comments"},
     * )
     *
     *
     * @param int $id Post ID
     *
     * @param IndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(int $id, IndexRequest $request)
    {
        try {
            $parentCommentId = $request->get('parentId');

            if (empty($parentCommentId)) {
                $items = $this->commentService->getFirstAndSecondLevelsForPostWithUserAsTree($id);
            } else {
                $items = $this->commentService->getForPostWithUserByParentCommentId($id, $parentCommentId);
            }

            return $this->sendResponse($items);
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (\Throwable $e) {
            dd($e);
            //TODO log
        } catch (\Throwable $e) {
            dd($e);
            //TODO log
        }

        return $this->sendErrorResponse('Something went wrong');
    }
}
