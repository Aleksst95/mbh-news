<?php

namespace App\Http\Controllers\API\V1\Main\Post;

use App\Exceptions\RedirectException;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Post\Posted\IndexRequest;
use App\Http\Requests\API\V1\Main\Post\Posted\SearchRequest;
use App\Http\Requests\API\V1\Main\Post\Posted\TagRequest;
use Exception;
use App\Repositories;
use App\Services;
use Illuminate\Http\JsonResponse;

/**
 * @SWG\Swagger(
 *   schemes={"http"},
 *   host="mbhn.ru",
 *   basePath="/",
 *   @SWG\Info(
 *     title="Posts API",
 *     version="1.0.0"
 *   )
 * )
 */
class PostedController extends BaseController
{
    private Services\Post\PostedService $postService;
    private Repositories\Post\PostedRepository $postRepository;

    public function __construct(
        Services\Post\PostedService $postService,
        Repositories\Post\PostedRepository $postRepository
    ) {
        $this->postService = $postService;
        $this->postRepository = $postRepository;
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/posts",
     *     summary="Get list of posts",
     *     tags={"Posts"},
     *     @SWG\Parameter(
     *         name="page",
     *         in="path",
     *         description="Page number",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="firstItemId",
     *         in="path",
     *         description="ID of the first post on the page. It needs for correct pagination.",
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Post")
     *         ),
     *     ),
     * )
     *
     *
     * @param IndexRequest $request
     *
     * @return JsonResponse
     */
    public function index(IndexRequest $request)
    {
        $page = $request->get('page') ?? 1;
        $firstItemId = $request->get('firstItemId');

        $limitPerPage = config('app.limit_post_per_page_for_index');

        $items = $this->postRepository->getAllWithoutContent($page, $limitPerPage, $firstItemId);

        return $this->sendResponse($items);
    }

    public function show(int $id)
    {
        try {
            $item = $this->postService->getByIdOrThrowException($id);

            return $this->sendResponse($item);
        } catch (RedirectException $e) {
            return $this->sendResponse(['redirect' => $e->getMessage()]);
        } catch (Exception $e) {
            return $this->sendNotFoundResponse();
        }
    }

    public function showBySlug(string $slug = '')
    {
        try {
            $item = $this->postService->getBySlugOrThrowException($slug);

            return $this->sendResponse($item);
        } catch (RedirectException $e) {
            return $this->sendResponse(['redirect' => $e->getMessage()]);
        } catch (Exception $e) {
            return $this->sendNotFoundResponse();
        }
    }

    public function search(SearchRequest $request)
    {
        $query = $request->get('query');
        $page = $request->get('page') ?? 1;
        $firstItemId = $request->get('firstItemId');

        $limitPerPage = config('app.limit_post_per_page_for_search');

        $items = $this->postRepository->getAllByQueryWithoutContent($query, $page, $limitPerPage, $firstItemId);

        return $this->sendResponse($items);
    }

    public function tag(TagRequest $request)
    {
        $tagName = $request->get('name');
        $page = $request->get('page') ?? 1;
        $firstItemId = $request->get('firstItemId');

        $limitPerPage = config('app.limit_post_per_page_for_tag');

        $items = $this->postRepository->getAllByTagWithoutContent($tagName, $page, $limitPerPage, $firstItemId);

        return $this->sendResponse($items);
    }

    public function polzovatelskoeSoglashenie()
    {
        $this->data['title'] = 'Пользовательское соглашение | MBH News';
        $this->data['description'] = 'Пользовательское соглашение на сайте MBH News';
        return $this->render('main.polzovatelskoeSoglashenie');
    }

    public function privacyPolicy()
    {
        $this->data['title'] = 'Политика конфиденциальности | MBH News';
        $this->data['description'] = 'Политика конфиденциальности на сайте MBH News';
        return $this->render('main.privacyPolicy');
    }

    public function promo()
    {
        $this->data['title'] = 'Реклама | MBH News';
        $this->data['description'] = 'Реклама на сайте MBH News';
        return $this->render('main.promo');
    }
}
