<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Bookmark\GetByPostIdsRequest;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Bookmark\IndexRequest;
use App\Repositories;
use App\Services;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class BookmarkController extends BaseController
{
    private Services\Post\BookmarkService $bookmarkService;
    private Repositories\Post\BookmarkRepository $bookmarkRepository;

    public function __construct(
        Services\Post\BookmarkService $bookmarkService,
        Repositories\Post\BookmarkRepository $bookmarkRepository
    ) {
        $this->bookmarkService = $bookmarkService;
        $this->bookmarkRepository = $bookmarkRepository;
    }

    public function index(IndexRequest $request)
    {
        $page = $request->get('page') ?? 1;
        $firstCreatedAt = $request->get('firstPostBookmarkCreatedAt');

        $limitPerPage = config('app.limit_post_per_page_for_index');

        $items = $this->bookmarkRepository->getAllWithoutContent(
            Auth::user()->id,
            $page,
            $limitPerPage,
            $firstCreatedAt
        );

        return $this->sendResponse($items);
    }

    /**
     * Returns user's vote to articles.
     *
     * @param GetByPostIdsRequest $request
     *
     * @return JsonResponse
     */
    public function getByPostIds(GetByPostIdsRequest $request)
    {
        $itemIds = $request->get('itemIds');

        $items = $this->bookmarkService->getUserBookmarksByPostIds(Auth::user()->id, $itemIds);

        return $this->sendResponse($items);
    }

    /**
     * Adds the post to bookmarks
     *
     * @param int $id Post ID
     *
     * @return JsonResponse
     */
    public function store(int $id)
    {
        try {
            if ($this->bookmarkService->addPost(Auth::user()->id, $id)) {
                return $this->sendCreatedResponse();
            }
        } catch (\Exception $e) {
        }

        return $this->sendErrorResponse('', [], 422);
    }

    /**
     * Removes the post from bookmarks
     *
     * @param int $id Post ID
     *
     * @return JsonResponse
     */
    public function delete(int $id)
    {
        try {
            if ($this->bookmarkService->deletePost(Auth::user()->id, $id)) {
                return $this->sendResponseWithoutBody();
            }
        } catch (\Exception $e) {
            // TODO log
            return $this->sendErrorResponse('Something went wrong', [], 422);
        }

        return $this->sendErrorResponse('', [], 422);
    }
}
