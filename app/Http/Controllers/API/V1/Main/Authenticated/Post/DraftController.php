<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Draft\IndexRequest;
use App\Services;
use Illuminate\Support\Facades\Auth;

class DraftController extends BaseController
{
    private Services\Post\DraftService $draftService;

    public function __construct(
        Services\Post\DraftService $draftService
    ) {
        $this->draftService = $draftService;
    }

    public function index(IndexRequest $request)
    {
        $page = $request->get('page') ?? 1;
        $firstItemId = $request->get('firstItemId');

        $limitPerPage = config('app.limit_post_per_page_for_index');

        $items = $this->draftService->getAllUserPostsWithoutContent(
            Auth::user()->id,
            $page,
            $limitPerPage,
            $firstItemId
        );

        return $this->sendResponse($items);
    }
}
