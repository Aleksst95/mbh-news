<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Exceptions\ForbiddenException;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Writing\SaveRequest;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Writing\UploadFileByUrl;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Writing\UploadFileFromDevice;
use App\Models\Image;
use App\Repositories;
use App\Services;
use Exception;
use Illuminate\Support\Facades\Auth;

class WritingController extends BaseController
{
    private Services\Post\EditorService $editorService;
    private Services\Post\WritingService $writingService;
    private Repositories\Post\WritingRepository $writingRepository;

    /**
     * @var array EditorJS config
     */
    private array $config;

    public function __construct(
        Services\Post\WritingService $writingService,
        Services\Post\EditorService $editorService,
        Repositories\Post\WritingRepository $writingRepository
    ) {
        $this->writingService = $writingService;
        $this->editorService = $editorService;
        $this->writingRepository = $writingRepository;

        $this->config = config('editorjs');
    }

    public function showForEdit(int $id)
    {
        try {
            $post = $this->writingService->getPermittedPostForEditOrThrowException(Auth::user(), $id);
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        $post->isPublished = $post->is_posted;

        return $this->sendResponse($post);
    }

    public function show(int $id)
    {
        try {
            $post = $this->writingService->getPermittedPostOrThrowException(Auth::user(), $id);
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        return $this->sendResponse($post);
    }

    public function save(SaveRequest $request)
    {
        $postId = $request->post('postId');
        $title = $request->post('title');
        $tags = $request->post('tags');
        $publish = $request->post('publish');

        try {
            $post = $this->writingService->getPermittedPostOrReturnNewOneWithCurrentUserAsAuthor(Auth::user(), $postId);
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        try {
            $editorData = $this->editorService->sanitizeData($request->post('editorData'), $this->config);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e->getMessage(), [], 422);
        }

        try {
            $post = $this->writingService
                ->updatePost(Auth::user(), $post, $title, $editorData, $publish);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e->getMessage());
        }

        if ($post->id != $postId) {
            $response = ['postId' => $post->id];

            if ($publish) {
                $response['slug'] = $post->slug;
            }

            return $this->sendResponse($response, 201);
        }

        if ($publish) {
            return $this->sendResponse(['slug' => $post->slug]);
        }

        return $this->sendResponseWithoutBody();
    }

    public function uploadFileByUrl(Services\Post\ImageService $fileService, UploadFileByUrl $request)
    {
        try {
            $url = $fileService->uploadFileByUrlAndGetUrl(Auth::user()->id, $request->post('url'));

            return $this->sendCreatedResponse(
                [
                    'url' => $url
                ]
            );
        } catch (Exception $e) {
            if ($e->getMessage() === 'status') {
                return $this->sendErrorResponse(
                    '',
                    [
                        [
                            'type' => 'status',
                            'value' => '',
                        ]
                    ],

                    422
                );
            } elseif ($e->getMessage() === 'mimes') {
                return $this->sendErrorResponse(
                    '',
                    [
                        [
                            'type' => 'mimes',
                            'value' => implode(', ', Image::AVAILABLE_EXTENSIONS),
                        ]
                    ],
                    422
                );
            } elseif ($e->getMessage() === 'size') {
                return $this->sendErrorResponse(
                    '',
                    [
                        [
                            'type' => 'size',
                            'value' => Image::MAX_FILE_SIZE,
                        ]
                    ],
                    422
                );
            }
        }

        return $this->sendErrorResponse();
    }

    public function uploadFileFromDevice(Services\Post\ImageService $fileService, UploadFileFromDevice $request)
    {
        try {
            $url = $fileService->uploadFileFromDeviceAndGetUrl(Auth::user()->id, $request);

            return $this->sendCreatedResponse(
                [
                    'url' => $url
                ]
            );
        } catch (Exception $e) {
            dd($e);
        }

        return $this->sendErrorResponse();
    }

    public function publish(int $id)
    {
        try {
            $post = $this->writingRepository->getData($id);

            if ($this->writingService->checkPermissionOfPost(Auth::user(), $post)
                && $this->writingService->publishPost($post)
            ) {
                return $this->sendResponse(['slug' => $post->slug]);
            }
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        // TODO log
        return $this->sendErrorResponse('Something went wrong');
    }

    public function unpublish(int $id)
    {
        try {
            $post = $this->writingRepository->getData($id);

            if ($this->writingService->checkPermissionOfPost(Auth::user(), $post)
                && $this->writingService->unpublishPost($post)
            ) {
                return $this->sendResponseWithoutBody();
            }
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        // TODO log
        return $this->sendErrorResponse('Something went wrong');
    }

    public function delete(int $id)
    {
        try {
            $post = $this->writingRepository->getData($id);

            if ($this->writingService->checkPermissionOfPost(Auth::user(), $post)
                && $this->writingService->deletePost($post)
            ) {
                return $this->sendResponseWithoutBody();
            }
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (ForbiddenException $e) {
            return $this->sendErrorResponse('', [], 403);
        }

        // TODO log
        return $this->sendErrorResponse('Something went wrong');
    }
}
