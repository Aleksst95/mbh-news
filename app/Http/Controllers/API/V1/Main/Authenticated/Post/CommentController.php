<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Exceptions\NotFoundException;
use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Comment\StoreRequest;
use Auth;
use Exception;
use App\Repositories;
use App\Services;
use Illuminate\Http\JsonResponse;
use Throwable;

class CommentController extends BaseController
{
    private Services\Post\PostedService $postService;
    private Services\Post\CommentService $commentService;
    private Repositories\Post\CommentRepository $commentRepository;

    public function __construct(
        Services\Post\PostedService $postService,
        Services\Post\CommentService $commentService,
        Repositories\Post\CommentRepository $commentRepository
    ) {
        $this->postService = $postService;
        $this->commentService = $commentService;

        $this->commentRepository = $commentRepository;
    }

    /**
     * @param int $id
     * @param StoreRequest $request
     *
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(int $id, StoreRequest $request)
    {
        $message = $request->get('message');
        $commentParentId = $request->get('parentId');

        try {
            if ($commentParentId) {
                $comment = $this->commentService->addCommentWithParent(
                    Auth::user()->id,
                    $id,
                    $message,
                    $commentParentId
                );
            } else {
                $comment = $this->commentService->addCommentWithoutParent(Auth::user()->id, $id, $message);
            }

            if ($comment) {
                return $this->sendCreatedResponse(['id' => $comment->id]);
            }
        } catch (NotFoundException $e) {
            return $this->sendNotFoundResponse();
        } catch (Exception $e) {
            if ($e->getMessage() === 'Message is empty') {
                return $this->sendErrorResponse('Message is empty', [], 422);
            }
            // TODO log
        } catch (Throwable $e) {
            // TODO log
        }

        return $this->sendErrorResponse('Something went wrong');
    }
    // TODO increase count of comments in the Post
}
