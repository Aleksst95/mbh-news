<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\Post;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Vote\GetByPostIdsRequest;
use App\Http\Requests\API\V1\Main\Authenticated\Post\Vote\StoreRequest;
use App\Repositories;
use App\Services;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class VoteController extends BaseController
{
    private Services\Post\VoteService $voteService;
    private Repositories\Post\VoteRepository $voteRepository;

    public function __construct(
        Services\Post\VoteService $voteService,
        Repositories\Post\VoteRepository $voteRepository
    ) {
        $this->voteService = $voteService;
        $this->voteRepository = $voteRepository;
    }

    /**
     * Returns user's vote to articles.
     *
     * @param GetByPostIdsRequest $request
     *
     * @return JsonResponse
     */
    public function getByPostIds(GetByPostIdsRequest $request)
    {
        $itemIds = $request->get('itemIds');

        $items = $this->voteService->getUserVotesToPosts(Auth::user()->id, $itemIds);

        return $this->sendResponse($items);
    }

    /**
     * Adds the vote for bookmarks
     *
     * @param int $id Post ID
     *
     * @param StoreRequest $request
     *
     * @return JsonResponse
     */
    public function store(int $id, StoreRequest $request)
    {
        $type = $request->post('type');

        try {
            if ($this->voteService->addVote(Auth::user()->id, $id, $type)) {
                return $this->sendCreatedResponse();
            }
        } catch (Exception $e) {
            // TODO log
        }

        return $this->sendErrorResponse('', [], 422);
    }

    /**
     * Removes the vote for bookmarks
     *
     * @param int $id Post ID
     *
     * @return JsonResponse
     */
    public function delete(int $id)
    {
        try {
            if ($this->voteService->deleteVote(Auth::user()->id, $id)) {
                return $this->sendResponseWithoutBody();
            }
        } catch (Exception $e) {
            // TODO log
        }

        return $this->sendErrorResponse('', [], 422);
    }
}
