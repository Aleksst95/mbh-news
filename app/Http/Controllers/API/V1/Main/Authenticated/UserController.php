<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated;

use App\Http\Controllers\API\BaseController;
use App\Repositories;
use App\Services;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    private Services\UserService $userService;
    private Repositories\UserRepository $userRepository;

    public function __construct(
        Services\UserService $userService,
        Repositories\UserRepository $userRepository
    ) {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }

    /**
     * See description: {@see Services\UserService::getByIdOrThrowException()}
     *
     * @return JsonResponse
     */
    public function getCommonData()
    {
        try {
            $item = $this->userService->getByIdOrThrowException(Auth::user()->id);

            return $this->sendResponse($item);
        } catch (Exception $e) {
            return $this->sendNotFoundResponse();
        }
    }

    /**
     * See description: {@see Services\UserService::getAdditionalDataByIdOrThrowException()}
     *
     * @return JsonResponse
     *
     * @throws BindingResolutionException
     */
    public function getAdditionalData()
    {
        try {
            $item = $this->userService->getAdditionalDataByIdOrThrowException(Auth::user()->id);

            return $this->sendResponse($item);
        } catch (Exception $e) {
            return $this->sendNotFoundResponse();
        }
    }
}
