<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\User;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\User\Email\SetNewToUserRequest;
use App\Http\Requests\API\V1\Main\Authenticated\User\Email\SwitchWeeklyNewsletterRequest;
use App\Services;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class EmailController extends BaseController
{
    private Services\EmailService $emailService;

    public function __construct(
        Services\EmailService $emailService
    ) {
        $this->emailService = $emailService;
    }

    /**
     * @param SetNewToUserRequest $request
     *
     * @return JsonResponse
     */
    public function setNewToUser(SetNewToUserRequest $request)
    {
        $email = $request->post('value');

        try {
            if ($this->emailService->setNewEmailToUser(Auth::user()->id, $email)) {
                return $this->sendResponseWithoutBody();
            }
        } catch (Exception $e) {
            if ($e->getMessage() === 'Email is already linked') {
                return $this->sendErrorResponse('Email is already linked', [], 400);
            }
            if ($e->getMessage() === 'Email is busy') {
                return $this->sendErrorResponse('Email is busy', [], 400);
            }
            // TODO log
        }

        return $this->sendErrorResponse('Something went wrong');
    }

    public function switchGettingWeeklyNewsLetter(SwitchWeeklyNewsletterRequest $request)
    {
        $status = $request->post('status');

        try {
            if ($this->emailService->switchGettingWeeklyNewsLetter(Auth::user()->id, $status)) {
                return $this->sendResponse(['newStatus' => $status]);
            }
        } catch (Exception $e) {
            if ($e->getMessage() === 'The same status') {
                return $this->sendErrorResponse('The same status', [], 400);
            }
            if ($e->getMessage() === 'User does not have a linked email') {
                return $this->sendErrorResponse('User does not have a linked email', [], 400);
            }
            // TODO log
        }
        return $this->sendErrorResponse('Something went wrong');
    }
}
