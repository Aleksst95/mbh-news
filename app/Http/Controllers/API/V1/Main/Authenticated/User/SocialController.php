<?php

namespace App\Http\Controllers\API\V1\Main\Authenticated\User;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\V1\Main\Authenticated\User\Social\LinkRequest;
use App\Http\Requests\API\V1\Main\Authenticated\User\Social\UnlinkRequest;
use App\Services;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use RuntimeException;

class SocialController extends BaseController
{
    private Services\SocialAccountService $socialAccountService;

    public function __construct(
        Services\SocialAccountService $socialAccountService
    ) {
        $this->socialAccountService = $socialAccountService;
    }

    /**
     * @param LinkRequest $request
     *
     * @return JsonResponse
     */
    public function link(LinkRequest $request)
    {
        $provider = $request->post('provider');

        try {
            if ($this->socialAccountService->linkNewProviderToUser(Auth::user()->id, $provider)) {
                return $this->sendCreatedResponse();
            }
        } catch (RuntimeException $e) {
            return $this->sendErrorResponse('Error getting data from provider', [], 400);
            // TODO log
        } catch (Exception $e) {
            if ($e->getMessage() === 'User is already linked with this provider') {
                return $this->sendErrorResponse('User is already linked with this provider', [], 400);
            }
            // TODO log
        }

        return $this->sendErrorResponse('Something went wrong');
    }

    /**
     * @param UnlinkRequest $request
     *
     * @return JsonResponse
     */
    public function unlink(UnlinkRequest $request)
    {
        $provider = $request->get('provider');

        try {
            if ($this->socialAccountService->unlinkProviderFromUser(Auth::user()->id, $provider)) {
                return $this->sendResponseWithoutBody();
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if ($message === 'User is not linked with the provider'
                || $message === 'Can\'t unlink the last linked account') {
                return $this->sendErrorResponse($message, [], 400);
            }
        }

        return $this->sendErrorResponse('Something went wrong');
    }
}
