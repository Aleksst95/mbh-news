<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use App\Repositories\Admin\HashtagRepository;

class HashtagController extends Controller
{
    public function index(HashtagRepository $hashtagRepository)
    {
        $items = $hashtagRepository->getAllWithData();

        return view('admin.hashtag.index', compact('items'));
    }

}
