<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use App\Repositories\Admin\HashtagRepository;
use App\Repositories\Admin\PostRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {
        $items = $this->postRepository->getAll();

        return view('admin.posts.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserRepository $userRepository
     * @param HashtagRepository $hashtagRepository
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRepository $userRepository, HashtagRepository $hashtagRepository, int $id)
    {
        $item = $this->postRepository->getById($id);

        $users = $userRepository->getAll();

        $hashtags = $hashtagRepository->getAllWithData();

        return view('admin.posts.edit', compact('item', 'users', 'hashtags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
