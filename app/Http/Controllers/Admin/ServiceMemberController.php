<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use App\Http\Requests\Admin\ServiceMember\StoreRequest;
use App\Http\Requests\Admin\ServiceMember\UpdateRequest;
use App\Repositories\Admin\ServiceMemberRepository;
use App\Services\Admin\RoleAndPermissionService;
use App\Services\Admin\ServiceMemberService;
use Illuminate\Http\Response;

class ServiceMemberController extends Controller
{
    /**
     * @var ServiceMemberService
     */
    public ServiceMemberService $serviceMemberService;

    /**
     * @var RoleAndPermissionService
     */
    public RoleAndPermissionService $roleAndPermissionService;

    /**
     * @var ServiceMemberRepository
     */
    public ServiceMemberRepository $serviceMemberRepository;

    /**
     * ServiceMemberController constructor.
     *
     * @param ServiceMemberRepository $serviceMemberRepository
     * @param ServiceMemberService $serviceMemberService
     * @param RoleAndPermissionService $roleAndPermissionService
     */
    public function __construct(
        ServiceMemberRepository $serviceMemberRepository,
        ServiceMemberService $serviceMemberService,
        RoleAndPermissionService $roleAndPermissionService
    ) {
        $this->serviceMemberRepository = $serviceMemberRepository;

        $this->roleAndPermissionService = $roleAndPermissionService;
        $this->serviceMemberService = $serviceMemberService;
    }

    public function index()
    {
        $items = $this->serviceMemberRepository->getAllWithData();

        return view('admin.service-members.index', compact('items'));
    }

    public function create()
    {
        $users = $this->serviceMemberService->getAllUserNotMembers();
        $roles = $this->roleAndPermissionService->getAllRolesWithoutMainAdmin();

        return view('admin.service-members.create', compact('users', 'roles'));
    }

    public function store(StoreRequest $request)
    {
        $userId = $request->input('userId');
        $password = $request->input('pass');
        $roleIds = $request->input('roleIds');

        $this->serviceMemberService->createItem($userId, $password);

        $this->roleAndPermissionService->addRolesToUser($userId, $roleIds);

        return redirect(adminUrl('service-members'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     */
    public function show(int $id)
    {
        $item = $this->serviceMemberRepository->getById($id);

        return view('admin.service-members.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id Service Member ID
     *
     * @return Response
     * @throws \Exception
     */
    public function edit(int $id)
    {
        $item = $this->serviceMemberRepository->getById($id);
        $roles = $this->roleAndPermissionService->getAllRolesWithoutMainAdmin();

        return view('admin.service-members.edit', compact('item', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     *
     * @return Response
     * @throws \Exception
     */
    public function update(UpdateRequest $request)
    {
        $itemId = $request->input('id');
        $item = $this->serviceMemberRepository->getById($itemId);

        $password = $request->input('pass');
        $roleIds = $request->input('roleIds');

        $this->serviceMemberService->updateItem($itemId, $password);

        $this->roleAndPermissionService->deleteAllRolesToUser($item->user_id);
        $this->roleAndPermissionService->addRolesToUser($item->user_id, $roleIds);

        return redirect(adminUrl('service-members/' . $itemId));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id Service Member ID
     *
     * @return array
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $item = $this->serviceMemberRepository->getById($id);
        $this->roleAndPermissionService->deleteAllRolesToUser($item->user_id);

        if ($this->serviceMemberService->deleteItem($id)) {
            return ['status' => 'success'];
        }

        return ['status' => 'error'];
    }
}
