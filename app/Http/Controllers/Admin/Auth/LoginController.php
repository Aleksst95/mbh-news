<?php

namespace App\Http\Controllers\Admin\Auth;

use Auth;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers {
        logout as defaultLogout;
    }

    protected $data = []; // the information we send to the view
    protected $loginPath;
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->loginPath = adminUrl('login');

        // Redirect here after successful login.
        $this->redirectTo = adminUrl();
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    public function login()
    {
        $password = request()->post('password');

        if (Hash::check($password, Auth::user()->serviceMember->password)) {
            session([config('adminlte.admin_session_key') => true]);

            return redirect(adminUrl());
        }

        return back()->withInput();
    }

    public function logout()
    {
        session()->forget(config('adminlte.admin_session_key'));

        return redirect($this->loginPath);
    }
}
