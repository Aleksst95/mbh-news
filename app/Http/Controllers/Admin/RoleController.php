<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use App\Http\Requests\Admin\Role\StoreRequest;
use App\Http\Requests\Admin\Role\UpdateRequest;
use App\Services\Admin\RoleAndPermissionService;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * @var RoleAndPermissionService
     */
    protected RoleAndPermissionService $roleAndPermissionService;


    public function __construct(RoleAndPermissionService $roleAndPermissionService)
    {
        $this->roleAndPermissionService = $roleAndPermissionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $items = Role::where('name', '!=', 'Iron Man')->get();

        return view('admin.roles.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permissionGroups = $this->roleAndPermissionService->getPermissionGroups();

        return view('admin.roles.create', compact('permissionGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     *
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $name = $request->input('name');
        $permissionIds = $request->input('permissionIds');

        $roleId = $this->roleAndPermissionService->createRole($name, $permissionIds);

        return redirect(adminUrl('roles/' . $roleId));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(int $id)
    {
        $item = Role::findById($id);
        $permissions = $this->roleAndPermissionService->getRolePermissionWithGroups($item);

        return view('admin.roles.show', compact('item', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(int $id)
    {
        $item = Role::findById($id);

        $permissionGroups = $this->roleAndPermissionService->getPermissionGroups();

        return view('admin.roles.edit', compact('item', 'permissionGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     *
     * @return Response
     */
    public function update(UpdateRequest $request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $permissionIds = $request->input('permissionIds');

        $this->roleAndPermissionService->deleteAllPermissionFromRole($id);
        $this->roleAndPermissionService->addPermissionsToRole($id, $permissionIds);

        $this->roleAndPermissionService->updateRoleName($id, $name);

        return redirect(adminUrl('roles/' . $id . '/edit'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return array
     */
    public function destroy(int $id)
    {
        if ($this->roleAndPermissionService->deleteRole($id)) {
            return ['status' => 'success'];
        }

        return ['status' => 'error'];
    }
}
