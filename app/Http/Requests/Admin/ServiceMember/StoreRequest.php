<?php

namespace App\Http\Requests\Admin\ServiceMember;

use App\Models\Admin\ServiceMember;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => [
                'required',
                'numeric',
                'exists:users,id',
                // User should not be in the service_members table
                'unique:service_members,user_id'
            ],
            'pass' => [
                'required',
                'min:5',
                'max:255'
            ],
            'roleIds' => [
                'required',
                'array',
                'min:1'
            ],
            'roleIds.*' => [
                'required',
                'numeric',
                'distinct',
                // In the end, we check that the ID is not equal to the Main Admin ID
                'exists:roles,id,id,!' . ServiceMember::ROLE_ID_IRON_MAN
            ]
        ];
    }
}
