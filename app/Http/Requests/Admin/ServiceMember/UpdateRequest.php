<?php

namespace App\Http\Requests\Admin\ServiceMember;

use App\Models\Admin\ServiceMember;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                'numeric',
                'exists:service_members,id'
            ],
            'pass' => [
                'max:255'
            ],
            'roleIds' => [
                'required',
                'array',
                'min:1'
            ],
            'roleIds.*' => [
                'required',
                'numeric',
                'distinct',
                // In the end, we check that the ID is not equal to the Main Admin ID
                'exists:roles,id,id,!' . ServiceMember::ROLE_ID_IRON_MAN
            ]
        ];
    }
}
