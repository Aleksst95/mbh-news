<?php

namespace App\Http\Requests\API\V1\Main\Post\Posted;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query' => [
                'required',
                'string',
                'min:3',
                'max:100'
            ],
            'page' => [
                'numeric',
                'min:1'
            ],
            'firstItemId' => [
                'numeric',
                'min:1'
            ]
        ];
    }
}
