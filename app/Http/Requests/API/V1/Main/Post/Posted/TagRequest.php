<?php

namespace App\Http\Requests\API\V1\Main\Post\Posted;

use App\Models\Post\Hashtag;
use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'min:3',
                'max:255',
                'exists:' . Hashtag::class . ',name'
            ],
            'page' => [
                'numeric',
                'min:1'
            ],
            'firstItemId' => [
                'numeric',
                'min:1'
            ]
        ];
    }
}
