<?php

namespace App\Http\Requests\API\V1\Main\Post\Comment;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parentId' => [
                'numeric',
                'min:1',
                'nullable'
            ]
        ];
    }
}
