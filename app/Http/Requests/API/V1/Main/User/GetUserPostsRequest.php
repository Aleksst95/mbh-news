<?php

namespace App\Http\Requests\API\V1\Main\User;

use Illuminate\Foundation\Http\FormRequest;

class GetUserPostsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userId' => [
                'required',
                'numeric',
                'exists:users,id',
            ],
        ];
    }
}
