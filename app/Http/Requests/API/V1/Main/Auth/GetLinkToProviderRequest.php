<?php

namespace App\Http\Requests\API\V1\Main\Auth;

use App\Models\User\SocialAccount;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GetLinkToProviderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                Rule::in(SocialAccount::AVAILABLE_PROVIDERS),
            ],
        ];
    }
}
