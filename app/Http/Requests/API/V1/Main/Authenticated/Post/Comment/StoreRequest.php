<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Comment;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => [
                'required',
                'string',
                'min:1',
                'max:2000',
            ],
            'parentId' => [
                'numeric',
                'min:1',
                'nullable'
            ]
        ];
    }
}
