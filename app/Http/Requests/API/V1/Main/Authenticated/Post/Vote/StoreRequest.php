<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Vote;

use App\Models\Post\Vote;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                'numeric',
                Rule::in([Vote::TYPE_NEGATIVE, Vote::TYPE_POSITIVE])
            ]
        ];
    }
}
