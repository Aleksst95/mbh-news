<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Writing;

use App\Models\Image;
use Illuminate\Foundation\Http\FormRequest;

class UploadFileFromDevice extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => [
                'required',
                'file',
                'mimes:' . implode(',', Image::AVAILABLE_EXTENSIONS),
                'max:' . Image::MAX_FILE_SIZE
            ]
        ];
    }

    public function messages()
    {
        return [
            'file.mimes' => ['type' => 'mimes', 'value' => implode(', ', Image::AVAILABLE_EXTENSIONS)],
            'file.max' => 'MAX_FILE_SIZE=' . Image::MAX_FILE_SIZE,
        ];
    }
}
