<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Writing;

use Illuminate\Foundation\Http\FormRequest;

class SaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'postId' => [
                'nullable',
                'numeric',
                'min:1'
            ],
            'title' => [
                'required',
                'string',
                'min:3',
                'max:100'
            ],
            'editorData' => [
                'required',
                'JSON',
            ],
            'tags' => [
                'array',
                'nullable',
            ],
            'tags.*' => [
                'required',
                'string',
                'min:2',
                'max:50'
            ],
            'publish' => [
                'required',
                'boolean',
            ]
        ];
    }
}
