<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Vote;

use Illuminate\Foundation\Http\FormRequest;

class GetByPostIdsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'itemIds' => [
                'required',
                'array',
                'min:1'
            ],
            'itemIds.*' => [
                'required',
                'numeric',
                'min:1'
            ]
        ];
    }
}
