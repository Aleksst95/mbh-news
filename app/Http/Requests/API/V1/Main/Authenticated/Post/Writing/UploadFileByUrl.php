<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\Post\Writing;

use Illuminate\Foundation\Http\FormRequest;

class UploadFileByUrl extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => [
                'required',
                'string',
                'min:3',
                'max:255',
                'url'
            ]
        ];
    }
}
