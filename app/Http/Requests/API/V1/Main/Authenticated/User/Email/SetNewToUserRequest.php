<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\User\Email;

use Illuminate\Foundation\Http\FormRequest;

class SetNewToUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => [
                'required',
                'email'
            ],
        ];
    }
}
