<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\User\Social;

use App\Models\User\SocialAccount;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UnlinkRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => [
                'required',
                Rule::in(SocialAccount::AVAILABLE_PROVIDERS),
            ]
        ];
    }
}
