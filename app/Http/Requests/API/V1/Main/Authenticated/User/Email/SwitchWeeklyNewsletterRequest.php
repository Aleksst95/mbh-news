<?php

namespace App\Http\Requests\API\V1\Main\Authenticated\User\Email;

use App\Models\User\Email;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SwitchWeeklyNewsletterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => [
                'required',
                Rule::in([Email::STATUS_DO_NOT_GET_DELIVERY, Email::STATUS_GET_DELIVERY])
            ],
        ];
    }
}
