<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => env('ADMIN_ROUTE_PREFIX', 'admin'),
        'middleware' => ['web', 'admin'],
        'namespace' => '\App\Http\Controllers\Admin',
    ],
    function () {
        App::setLocale('en');

        Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.auth.login');
        Route::post('login', 'Auth\LoginController@login');
//        Route::get('logout', 'Auth\LoginController@logout')->name('admin.auth.logout');
        Route::post('logout', 'Auth\LoginController@logout')->name('admin.auth.logout');

        Route::get('/', 'DashboardController@index');

        Route::resource('users', 'UserController')->middleware('can:user all');
        Route::resource('service-members', 'ServiceMemberController')->middleware('can:service-member all');
        Route::resource('roles', 'RoleController')->middleware('can:role all');
        Route::resource('posts', 'PostController')->middleware('can:post all');
        Route::resource('hashtags', 'HashtagController')->middleware('can:post all');
        Route::resource('comments', 'CommentController')->middleware('can:comment all');
        Route::resource('complaints', 'ComplaintController')->middleware('can:complaint all');
    }
);

//Route::group(
//    [
//        'prefix' => config('app.admin_url_prefix', 'admin'),
//        'middleware' => ['web', 'admin'],
//        'namespace' => '\App\Http\Controllers\Admin',
//    ],
//    function () {
//        Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.auth.login');
//        Route::post('login', 'Auth\LoginController@login');
//        Route::get('logout', 'Auth\LoginController@logout')->name('admin.auth.logout');
//
//        Route::get('/', 'DashboardController@index');
//    }
//);

Route::get('/promo', 'PostController@promo');
Route::get('/polzovatelskoe-soglashenie', 'PostController@polzovatelskoeSoglashenie');
Route::get('/privacy-policy', 'PostController@privacyPolicy');


Route::view('/login/{provider}/callback', 'auth.login-callback');


Route::view('/popups/login', 'popups.login');
Route::view('/{any}', 'app')->where('any', '.*');




////////////////




Route::get('/auth/popup', 'AuthController@popup');
Route::get('/auth/logout', 'AuthController@logout');

Route::get('/user/{id}', 'UserController@index');
Route::get('/user/settings', 'UserController@settings');


Route::any('/tag/{tag}', 'PostController@tag');
Route::any('/tag/{tag}/{page}', 'PostController@tag');
Route::any('/search', 'PostController@search');

Route::get('/', 'PostController@index');
Route::get('/{page}', 'PostController@index');
Route::get('/posts/index', 'PostController@index');
Route::get('/posts/index/{page}', 'PostController@index');

Route::get('/{slug}', 'PostController@show');
