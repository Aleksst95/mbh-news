<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    ['prefix' => 'v1', 'namespace' => 'V1\Main'],
    function () {
        Route::group(['namespace' => 'Post', 'prefix' => 'posts'],
            function () {
                Route::get('/', 'PostedController@index');

                Route::get('/search', 'PostedController@search');
                Route::get('/tag', 'PostedController@tag');

                Route::get('/{id}', 'PostedController@show');
                Route::get('/{slug}', 'PostedController@showBySlug');

                Route::prefix('{id}/comments')->group(
                    function () {
                        Route::get('/', 'CommentController@index');
                    }
                );
            }
        );

        Route::prefix('users')->group(
            function () {
                Route::post('/by_ids', 'UserController@getByIds');
                Route::get('/{id}', 'UserController@show');

                Route::get('/posts', 'User\PostController@posts');
            }
        );

        Route::prefix('auth')->group(
            function () {
                Route::get('/link_to_provider', 'AuthController@getLinkToProvider');
                Route::post('/social_login', 'AuthController@socialLogin');
                Route::post('/logout', 'AuthController@logout');
            }
        );


        /*
         * Routes for an authenticated user
         */


        Route::group(
            ['middleware' => 'auth:sanctum', 'namespace' => 'Authenticated', 'prefix' => 'authenticated'],
            function () {
                Route::prefix('user')->group(
                    function () {
                        // TODO change to the base users request(users/{id}, the route above)
                        Route::get(
                            '/common_data',
                            'UserController@getCommonData'
                        );
                        Route::get('/additional_data', 'UserController@getAdditionalData');

                        Route::post('/email', 'User\EmailController@setNewToUser');
                        Route::post('/weekly_newsletter', 'User\EmailController@switchGettingWeeklyNewsLetter');

                        Route::prefix('socials')->group(
                            function () {
                                Route::post('/', 'User\SocialController@link');
                                Route::delete('/', 'User\SocialController@unlink');
                            }
                        );
                    }
                );

                Route::group(
                    ['namespace' => 'Post', 'prefix' => 'posts'],
                    function () {
                        Route::patch('/{id}/publish', 'WritingController@publish');
                        Route::patch('/{id}/unpublish', 'WritingController@unpublish');
                        Route::delete('/{id}', 'WritingController@delete');


                        Route::prefix('drafts')->group(
                            function () {
                                Route::get('/', 'DraftController@index');
                            }
                        );

                        Route::prefix('votes')->group(
                            function () {
                                Route::get('/by_post_ids', 'VoteController@getByPostIds');
                                Route::post('/{id}', 'VoteController@store');
                                Route::delete('/{id}', 'VoteController@delete');
                            }
                        );

                        Route::prefix('bookmarks')->group(
                            function () {
                                Route::get('/', 'BookmarkController@index');
                                Route::get('/by_post_ids', 'BookmarkController@getByPostIds');
                                Route::post('/{id}', 'BookmarkController@store');
                                Route::delete('/{id}', 'BookmarkController@delete');
                            }
                        );

                        Route::prefix('writing')->group(
                            function () {
                                Route::get('/{id}/data_for_edit', 'WritingController@showForEdit');
                                Route::get('/{id}', 'WritingController@show');
                                Route::post('/file_by_url', 'WritingController@uploadFileByUrl');
                                Route::post('/file_from_device', 'WritingController@uploadFileFromDevice');
                                Route::post('/save', 'WritingController@save');
                            }
                        );

                        Route::prefix('{id}/comments')->group(
                            function () {
                                Route::post('/', 'CommentController@store');
                            }
                        );
                    }
                );
            }
        );
    }
);
