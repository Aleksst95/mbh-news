module.exports = {
    theme: {
        extend: {
            screens: {
                'xs': '320px',
                'sm': '640px',
                'md': '768px',
                'lg': '1024px',
                'xl': '1280px',
                'xxl': '1520px',
            },
            minHeight: {
                '4': '1rem',
                '6': '1.5rem',
                '8': '2rem',
            },
            minWidth: {
                '32': '8rem',
                '80': '20rem',
            },
            maxWidth: {
                '160': '40rem',
            },
            width: {
              '80': '20rem',
            },
            inset: {
                '8': '2rem',
                '6': '1.5rem',
                '4': '1rem',
                '2': '0.5rem',
                '1': '0.25rem',
                '0': '0',
                '-1': '-0.25rem',
                '-2': '-0.5rem',
                '-4': '-1rem',
                '-6': '-1.5rem',
                '-8': '-2rem',
                '-16': '-4rem',
                '-24': '-6rem',
                '-28': '-7rem',
                '-32': '-8rem'
            },
            colors: {
                indigo: {
                    '100': '#e8e6fa',
                    '200': '#bbd1dc',
                    '300': '#8dbbd8',
                    '400': '#60a1d1',
                    '500': '#3f87c5',
                    '600': '#2e70b2',
                    '700': '#285a94',
                    '800': '#294066',
                    '900': '#273149',
                },
            },
            spacing: {
                '14': '3.5rem',
                '72': '18rem',
                '84': '21rem',
                '96': '24rem',
                '100': '25rem',
                '120': '30rem',
                '140': '35rem',
                '160': '40rem',
                '180': '45rem'
            },
            boxShadow: {
                'all-side-md': '0 0 4px -1px rgba(0, 0, 0, .2), 0 0 2px -1px rgba(0, 0, 0, .06)'
            },
            transitionDuration: {
                '400': '400ms'
            }
        }
    },
    corePlugins: {
        container: false
    },
    plugins: [
        function ({addComponents}) {
            addComponents({
                '.container': {
                    maxWidth: '100%',
                    '@screen xs': {
                        minWidth: '320px',
                        maxWidth: '640px'
                    },
                    '@screen sm': {
                        minWidth: '640px',
                        maxWidth: '768px'
                    },
                    '@screen md': {
                        minWidth: '768px',
                        maxWidth: '1024px'
                    },
                    '@screen lg': {
                        minWidth: '1024px',
                        maxWidth: '1280px'
                    },
                    '@screen xl': {
                        minWidth: '1280px',
                        maxWidth: '1520px'
                    },
                    '@screen xxl': {
                        minWidth: '1520px',
                    }
                }
            });
        }
    ],
    fontFamily: {
        'sans': [
            'Roboto',
            '-apple-system',
            'BlinkMacSystemFont',
            'Segoe UI',
            'Roboto',
            'Oxygen',
            'Ubuntu',
            'Cantarell',
            'Fira Sans',
            'Droid Sans',
            'Helvetica Neue',
            'sans-serif'
        ]
    },
    variants: {},
};
