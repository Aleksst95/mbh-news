export default {
    mode: 'history',

    routes: [
        {
            path: '',
            component: () => import('./layouts/Main'),
            children: [
                {
                    path: '/',
                    component: () => import('./views/Home'),
                    name: 'mainPage'
                },
                {
                    path: '/search/:query',
                    component: () => import('./views/Search'),
                    name: 'searchPage'
                },

                {
                    path: '/user/settings',
                    component: () => import('./views/user/Settings'),
                    name: 'userProfile/settings',
                    meta: {
                        requiresAuth: true
                    }
                },

                {
                    path: '/user/:id',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/posts/inRelease',
                },
                {
                    path: '/user/:id/posts',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/posts/inRelease',
                },
                {
                    path: '/user/:id/posts/drafts',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/posts/drafts',
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: '/user/:id/comments',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/comments',
                },


                {
                    path: '/user/:id/bookmarks',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/bookmarks/posts',
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: '/user/:id/bookmarks/posts',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/bookmarks/posts',
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: '/user/:id/bookmarks/comments',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/bookmarks/comments',
                    meta: {
                        requiresAuth: true
                    }
                },
                {
                    path: '/user/:id/bookmarks/vacancies',
                    component: () => import('./views/user/Profile'),
                    name: 'userProfile/bookmarks/vacancies',
                    meta: {
                        requiresAuth: true
                    }
                },


                {
                    path: '/writing',
                    component: () => import('./views/writing/MakePost'),
                    name: 'makePost',
                    meta: {
                        requiresAuth: true,
                        permission: 'post create'
                    }
                },

                {
                    path: '/writing/:id',
                    component: () => import('./views/writing/MakePost'),
                    name: 'makePost',
                    meta: {
                        requiresAuth: true
                    }
                },

                {
                    path: '/writing/preview/:id',
                    component: () => import('./views/writing/Preview'),
                    name: 'previewPost',
                    meta: {
                        requiresAuth: true
                    }
                },


                {
                    path: '/promo',
                    component: () => import('./views/Post'),
                    name: 'promo'
                },

                {
                    path: '/privacy-policy',
                    component: () => import('./views/Post'),
                    name: 'privacyPolicy'
                },
                {
                    path: '/polzovatelskoe-soglashenie',
                    component: () => import('./views/Post'),
                    name: 'userAgreement'
                },


                {
                    path: '/404',
                    name: '404',
                    component: () => import('./views/NotFound')
                },


                {
                    path: '/:slug',
                    component: () => import('./views/Post'),
                },
            ]
        }
    ],
}
