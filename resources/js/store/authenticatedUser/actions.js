import axios from 'axios';

export default {
    getUserData({commit, state, dispatch}) {
        if (state.commonData === null && localStorage.getItem('userIsLoggedIn')) {
            return dispatch('GET_USER_DATA_FROM_API')
                .then(data => {
                    if ('id' in data) {
                        commit('SET_USER_DATA', data);
                        commit('SET_USER_AS_AUTHENTICATED');

                        return true;
                    }

                    return false;
                });
        } else if (state.isAuthenticated === true) {
            return true;
        }

        return false;
    },

    getUserAdditionalData({commit, state, dispatch}) {
        if (state.additionalData !== null) {
            return state.additionalData;
        }

        return dispatch('GET_ADDITIONAL_USER_DATA_FROM_API')
            .then(data => {
                if ('email' in data) {
                    commit('SET_ADDITIONAL_USER_DATA', data);

                    return data;
                }

                return {};
            });
    },

    logout({dispatch}) {
        return dispatch('LOGOUT_USER_FROM_API')
            .then(status => {
                if (status === true) {
                    dispatch('removeAllUserTraces');

                    return true;
                }
                return false;
            });
    },

    removeAllUserTraces({commit}) {
        localStorage.removeItem('userIsLoggedIn');

        commit('SET_USER_AS_NOT_AUTHENTICATED');
        commit('UNSET_USER_DATA');
    },

    /**
     *
     * @param commit
     * @param state
     * @param dispatch
     * @param {Array.<Number>} itemIds
     * @returns {boolean|*}
     */
    loadVotesByPostIdsInState({commit, state, dispatch}, itemIds) {
        let newItemIds = [];

        for (let i = 0; i < itemIds.length; i++) {
            if (!(itemIds[i] in state.listUserVotesToPosts)) {
                newItemIds.push(itemIds[i]);
            }
        }

        if (newItemIds.length) {
            return dispatch('GET_VOTES_BY_POST_IDS_FROM_API', newItemIds)
                .then(votes => {
                    if (Array.isArray(votes)) {
                        votes.forEach(function (item) {
                            commit(
                                'ADD_VOTE_IN_STATE',
                                {postId: item['post_id'], vote: item['type']}
                            );
                        });

                        return true;
                    }

                    return false;
                });
        }

        return true;
    },

    /**
     *
     * @param commit
     * @param state
     * @param dispatch
     * @param {Array.<Number>} itemIds
     * @returns {boolean|*}
     */
    loadPostBookmarksByPostIdsInState({commit, state, dispatch}, itemIds) {
        let newItemIds = [];

        for (let i = 0; i < itemIds.length; i++) {
            if (!(itemIds[i] in state.listUserPostBookmarks)) {
                newItemIds.push(itemIds[i]);
            }
        }

        if (newItemIds.length) {
            return dispatch('GET_POST_BOOKMARKS_BY_POST_IDS_FROM_API', newItemIds)
                .then(items => {
                    if (Array.isArray(items)) {
                        commit(
                            'ADD_POST_BOOKMARKS_IN_STATE',
                            items
                        );

                        return true;
                    }

                    return false;
                })
        }

        return true;
    },

    /**
     *
     * @param commit
     * @param state
     * @param {Number} itemId
     */
    addPostToBookmarksInState({commit, state}, itemId) {
        if (state.listUserPostBookmarks.indexOf(itemId) === -1) {
            commit(
                'ADD_POST_BOOKMARKS_IN_STATE',
                [itemId]
            );
        }
    },

    /**
     *
     * @param commit
     * @param state
     * @param {Number} itemId
     */
    removePostFromBookmarksInState({commit, state}, itemId) {
        let itemIndex = state.listUserPostBookmarks.indexOf(itemId);

        if (itemIndex !== -1) {
            commit(
                'REMOVE_POST_FROM_BOOKMARKS_IN_STATE',
                itemIndex
            );
        }
    },


    /*
     * METHODS USE API REQUESTS
     */


    GET_USER_DATA_FROM_API({dispatch}) {
        return axios.get('/api/v1/authenticated/user/common_data')
            .then(response => {
                if (response.status === 200 && typeof response.data === 'object') {
                    return response.data;
                }

                return {};
            })
            .catch(error => {
                if (error.response.status === 401) {
                    dispatch('removeAllUserTraces');
                }

                return {};
            });
    },

    GET_ADDITIONAL_USER_DATA_FROM_API({dispatch}) {
        return axios.get('/api/v1/authenticated/user/additional_data')
            .then(response => {
                if (response.status === 200 && typeof response.data === 'object') {
                    return response.data;
                }

                return {};
            })
            .catch(error => {
                if (error.response.status === 401) {
                    dispatch('removeAllUserTraces');
                }

                return {};
            });
    },

    /**
     * Loads user votes by an array of post IDs from backend.
     *
     * @param dispatch
     * @param {Array.<Number>} itemIds IDs of posts
     */
    async GET_VOTES_BY_POST_IDS_FROM_API({dispatch}, itemIds) {
        return await axios.get('/api/v1/authenticated/posts/votes/by_post_ids', {params: {itemIds: itemIds}})
            .then(response => {
                if (response.status === 200 && typeof response.data === 'object') {
                    return response.data;
                }

                return {};
            })
            .catch(error => {
                if (error.response.status === 401) {
                    dispatch('removeAllUserTraces');
                }

                return {};
            });
    },

    /**
     * Loads user post bookmarks by an array of post IDs from backend.
     *
     * @param dispatch
     * @param {Array.<Number>} itemIds IDs of posts
     */
    async GET_POST_BOOKMARKS_BY_POST_IDS_FROM_API({dispatch}, itemIds) {
        return await axios.get('/api/v1/authenticated/posts/bookmarks/by_post_ids', {params: {itemIds: itemIds}})
            .then(response => {
                if (response.status === 200 && typeof response.data === 'object') {
                    return response.data;
                }

                return {};
            })
            .catch(error => {
                if (error.response.status === 401) {
                    dispatch('removeAllUserTraces');
                }

                return {};
            });
    },

    LOGOUT_USER_FROM_API() {
        return axios.post('/api/v1/auth/logout')
            .then(response => {
                if (response.status === 204) {
                    return true;
                }

                return false;
            })
            .catch(error => {
                return false;
            });
    }
};
