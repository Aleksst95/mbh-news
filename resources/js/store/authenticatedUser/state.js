export default {
    isAuthenticated: false,

    commonData: null,
    additionalData: null,

    listUserVotesToPosts: [], // like = 1, dislike = -1, not voted = 0
    listUserPostBookmarks: [],
    unknownVote: 0
};
