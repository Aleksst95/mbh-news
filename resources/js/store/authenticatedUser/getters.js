export default {
    /**
     *
     * @param state
     * @param {Number} itemId ID of the Post
     */
    getVoteByPostId: (state) => (itemId) => {
        if (itemId in state.listUserVotesToPosts) {
            return state.listUserVotesToPosts[itemId];
        }

        return state.unknownVote;
    },

    /**
     *
     * @param state
     * @param {Number} itemId ID of the Post
     */
    getPostBookmarkStatusByPostId: (state) => (itemId) => {
        return (state.listUserPostBookmarks.indexOf(String(itemId)) !== -1);
    },

    email: (state) => {
        if (state.additionalData !== null && 'email' in state.additionalData) {
            return state.additionalData.email;
        }

        return '';
    },

    linkedAccounts: (state) => {
        if (
            state.additionalData !== null
            && 'linkedAccounts' in state.additionalData
            && Array.isArray(state.additionalData.linkedAccounts)
        ) {
            return state.additionalData.linkedAccounts;
        }

        return [];
    },

    getWeeklyNewsletter: (state) => {
        if (state.additionalData !== null && 'getWeeklyNewsletter' in state.additionalData) {
            return state.additionalData.getWeeklyNewsletter;
        }

        return false;
    },
};
