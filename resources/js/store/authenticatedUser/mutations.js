export default {
    SET_USER_DATA(state, payload) {
        state.commonData = payload;
    },

    SET_ADDITIONAL_USER_DATA(state, payload) {
        state.additionalData = payload;
    },

    UNSET_USER_DATA(state) {
        state.commonData = null;
        state.listUserVotesToPosts = [];
    },

    SET_USER_AS_AUTHENTICATED(state) {
        state.isAuthenticated = true;
    },

    SET_USER_AS_NOT_AUTHENTICATED(state) {
        state.isAuthenticated = false;
    },

    /**
     *
     * @param state
     * @param {Object} payload
     */
    ADD_VOTE_IN_STATE(state, payload) {
        state.listUserVotesToPosts[payload.postId] = payload.vote;
    },

    /**
     *
     * @param state
     * @param {Array} items
     */
    ADD_POST_BOOKMARKS_IN_STATE(state, items) {
        state.listUserPostBookmarks = state.listUserPostBookmarks.concat(items);
    },

    /**
     *
     * @param state
     * @param {Number} itemIndex index of the element in the state array
     */
    REMOVE_POST_FROM_BOOKMARKS_IN_STATE(state, itemIndex) {
        state.listUserPostBookmarks.splice(itemIndex, 1);
    },
};
