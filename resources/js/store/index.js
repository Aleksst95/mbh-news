import Vue from 'vue';
import Vuex from 'vuex';

import authenticatedUser from './authenticatedUser/module';
import users from './users/module';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        authenticatedUser,
        users,
    },
});
