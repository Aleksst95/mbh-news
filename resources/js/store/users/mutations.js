export default {
    ADD_USER_IN_STATE(state, user) {
        if ('id' in user) {
            state.listUsers[user.id] = user;
        }
    }
}
