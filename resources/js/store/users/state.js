export default {
    listUsers: [],
    unknownUser: {
        id: 0,
        lastName: 'Unknown',
        firstName: '',
        avatar: '',
        createdAt: ''
    },
};
