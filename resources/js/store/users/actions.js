import axios from 'axios';

export default {
    /**
     * Returns the user by id or authenticatedUser.unknownUser
     *
     * @param commit
     * @param state
     * @param dispatch
     * @param {Number} itemId
     *
     * @return Promise<object>
     */
    getUserById({commit, state, dispatch}, itemId) {
        if (itemId in state.listUsers) {
            return state.listUsers[itemId];
        }

        return dispatch('GET_SINGLE_USER_BY_ID_FROM_API', itemId)
            .then(item => {
                if ('id' in item) {
                    commit('ADD_USER_IN_STATE', item);
                    return item;
                }
                return state.unknownUser
            });
    },

    /**
     * Checks the existence of the user by ID in State.
     *
     * @param state
     * @param {Number} itemId
     * @returns {boolean}
     */
    userIsUploadedInState({state}, itemId) {
        return itemId in state.listUsers;
    },

    /**
     * Loads users by array of IDs and puts them into State.
     *
     * @param commit
     * @param dispatch
     * @param {Array.<Number>} itemIds
     */
    loadUsersByIdsInState({commit, dispatch}, itemIds) {
        return dispatch('GET_USERS_BY_IDS_FROM_API', itemIds)
            .then(items => {
                if (Array.isArray(items)) {
                    for (let i = 0; i < items.length; i++) {
                        commit('ADD_USER_IN_STATE', items[i]);
                    }

                    return true;
                }

                return false;
            });
    },

    /*
     * METHODS USE API REQUESTS
     */

    /**
     * Load a user by ID from backend.
     *
     * @param {Number} itemId
     * @returns {Promise<{fullName: string, id: Number, avatar: string}>}
     * @constructor
     */
    async GET_SINGLE_USER_BY_ID_FROM_API({}, itemId) {
        return await axios.get('/api/v1/users/' + itemId)
            .then(response => {
                if (response.status === 200) {
                    return response.data;
                }

                return {};
            }).catch(error => {
                return {};
            });
    },

    /**
     * Loads users by an array of IDs from backend.
     *
     * @param {Array.<Number>} itemIds
     * @returns {Promise<{fullName: string, id: Number, avatar: string}>}
     * @constructor
     */
    async GET_USERS_BY_IDS_FROM_API({}, itemIds) {
        return await axios.post('/api/v1/users/by_ids', {itemIds: itemIds})
            .then(response => {
                if (response.status === 200) {
                    return response.data;
                }
                return false;
            }).catch(error => {
                return false;
            });
    },
};
