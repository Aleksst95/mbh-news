import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';

import vuexI18n from 'vuex-i18n';
import Locales from './vue-i18n-locales.generated.js';

import routes from './routes';

import store from './store';

import EventDispatcher from "./services/EventDispatcher";

Vue.use(VueRouter);

Vue.use(vuexI18n.plugin, store);

Vue.i18n.add('en', Locales.en);
Vue.i18n.add('ru', Locales.ru);

// set the start locale to use
Vue.i18n.set('ru');


const router = new VueRouter(routes);

router.beforeEach((to, from, next) => {
    const authorization = store.dispatch('authenticatedUser/getUserData');

    authorization.then((isAuthenticated) => {
        if (isAuthenticated) {
            if (
                typeof to.meta.permission !== 'undefined'
                && (
                    typeof store.state.authenticatedUser.commonData.permissions !== 'object'
                    || store.state.authenticatedUser.commonData.permissions[to.meta.permission] !== true
                )
            ) {
                next({name: 'mainPage'});
            } else {
                // Add common authenticated user data to all users data to avoid downloading them again
                store.commit('users/ADD_USER_IN_STATE', store.state.authenticatedUser.commonData)
                next();
            }
        } else {
            if (to.matched.some(record => record.meta.requiresAuth)) {
                next({name: 'mainPage'});
            } else {
                next();
            }
        }
    });
});

const app = new Vue({
    el: '#app',

    store,
    router: router,
    render: h => h(App)
});

window.EventDispatcher = new EventDispatcher;
