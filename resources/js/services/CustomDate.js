import Vue from 'vue';

class CustomDate {
    /**
     * @param {string} date Date in the format: YYYY-MM-DD HH:mm:ss
     */
    constructor(date = '')
    {
        if (date === '') {
            this._date = new Date();
        } else {
            this._date = new Date(date);

            if (this._date == 'Invalid Date') {
                this._date = new Date();
            }
        }
    }

    /**
     * Returns the time difference in a specified format between the current time and the time
     * passed to the constructor.
     *
     * @param {Boolean} short Use the short version for the showHumanReadableDate()
     *
     * @returns {string}
     */
    showPassedTime(short = false)
    {
        let differentTime = ((new Date()) - this._date) / 1000,
            date;

        if (differentTime < 60) {
            date = 'Just now';
        } else if (differentTime < 3600) {
            date = Math.floor(differentTime / 60) + ' ' + Vue.i18n.translate('min. ago');
        } else if (differentTime < 86400) {
            date = Math.floor(differentTime / 3600) + ' ' + Vue.i18n.translate('h. ago');
        } else if (differentTime < 259200) {
            date = Math.floor(differentTime / 86400) + ' ' + Vue.i18n.translate('d. ago');
        } else {
            date = this.showHumanReadableDate(short);
        }

        return date;
    }

    /**
     * Returns the human-readable date.
     *
     * - dd monthName HH:mm  -  for dates this year
     * - dd monthName YYYY HH:mm  -  for dates more than a year ago
     * - dd monthName  -  for dates this year (short version)
     * - dd.mm.YYYY  -  for dates more than a year ago (short version)
     *
     * @param {Boolean} short Use the short version of the date
     *
     * @returns {string}
     */
    showHumanReadableDate(short = false)
    {
        // If it is not the current year we need to add the year to the result
        let isCurrentYear = (new Date()).getFullYear() !== this._date.getFullYear();

        if (!short) {
            return this._getHumanReadableDate(this._date, !isCurrentYear) + ' ' + Vue.i18n.translate('at') + ' '
                + this._date.getHours() + ':' + (this._date.getMinutes() < 10 ? '0' : '') + this._date.getMinutes();
        }

        if (isCurrentYear) {
            return this._getHumanReadableDate(this._date, false);
        }

        return this._date.getDate() + '.' + this._date.getMonth() + '.' + this._date.getFullYear();
    }

    /**
     * Returns the human-readable
     *
     * @param {Date} date
     * @param {Boolean} withYear Print the year
     *
     * @returns {string}
     *
     * @private
     */
    _getHumanReadableDate(date, withYear = true)
    {
        /*
         * We pass 2 in the 'pluralization' field so that it is always plural.
         * This is necessary to print the correct date in Russian(months have different endings).
         */
        let result = Vue.i18n.translate(this._getMonthName(date.getMonth()), {day: date.getDate()}, 2);

        if (withYear) {
            result += ' ' + date.getFullYear();
        }

        return result;
    }

    /**
     * Returns the name of the month given the index of the month (0 through 11).
     *
     * @param {Number} monthIndex
     *
     * @returns {string}
     *
     * @private
     */
    _getMonthName(monthIndex)
    {
        const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];

        if (monthIndex in monthNames) {
            return monthNames[monthIndex];
        }

        return monthNames[0];
    }
}

export default CustomDate;
