import Vue from 'vue';

class EventDispatcher {
    constructor()
    {
        this.vue = new Vue();
    }

    fire(event, data = null)
    {
        this.vue.$emit(event, data);
    }

    listen(event, callback)
    {
        this.vue.$on(event, callback);
    }

    destroy(event, callback)
    {
        if (typeof callback !== 'undefined') {
            this.vue.$off(event, callback);
        } else {
            this.vue.$off(event);
        }
    }
}

export default EventDispatcher;
