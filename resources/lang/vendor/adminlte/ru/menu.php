<?php

return [
    "Dashboard" => "Dashboard",
    "Posts" => "Посты",
    "Comments" => "Комментарии",
    "Users" => "Пользователи",
    "Service Members" => "Сотрудники сервиса",
    "Roles" => "Роли",
    "Complaints" => "Жалобы",
    "Hashtags" => "Хештеги"
];
