<?php

return [
    "Dashboard" => "Dashboard",
    "Posts" => "Posts",
    "Comments" => "Comments",
    "Users" => "Users",
    "Service Members" => "Service Members",
    "Roles" => "Roles",
    "Complaints" => "Complaints",
    "Hashtags" => "Hashtags",
];
