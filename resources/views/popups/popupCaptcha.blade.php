<div class="popup__content">
    <form name="emailSubscribe" action="/emails/set_confirmation_email_delivery" method="post">
        <div style="padding-left: 8%;" class="g-recaptcha" data-sitekey="{{ $publicKey }}"></div>
        <span class="button master" style="display: inherit;margin: 20px auto 0; width: 23.5%;"
              onclick="user.subscribeEmailDelivery()">Подтвердить</span>
    </form>
</div>
<script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>