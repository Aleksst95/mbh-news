<div>
    <h3 class="text-xl -mt-2 text-gray-800 font-semibold">@{{ $t('Sign in') }}</h3>
    <div class="my-12">
        <button
            @click="$parent.fireEventDispatcher('loginClick', 'facebook')"
            class="w-full bg-transparent text-gray-700 py-3 px-4 border border-gray-300 hover:border-gray-400 rounded relative outline-none focus:outline-none overflow-hidden">
            <i class="absolute left-4 top-3 text-2xl text-blue-800 fa fa-facebook" aria-hidden="true"></i> @{{ $t('Facebook') }}
        </button>
        <button
            @click="$parent.fireEventDispatcher('loginClick', 'vkontakte')"
            class="w-full mt-4 bg-transparent text-gray-700 py-3 px-4 border border-gray-300 hover:border-gray-400 rounded relative outline-none focus:outline-none overflow-hidden">
            <i class="absolute left-4 top-3 text-2xl text-indigo-600 fa fa-vk" aria-hidden="true"></i> @{{ $t('VKontakte') }}
        </button>
    </div>
    <div class="text-gray-600">Авторизуясь, вы соглашаетесь с
        <router-link to="/polzovatelskoe-soglashenie" class="underline text-blue-400">правилами пользования сайтом
        </router-link>
        и даете
        <router-link to="/privacy-policy" class="underline text-blue-400">согласие на обработку персональных данных
        </router-link>
        .
    </div>
</div>
