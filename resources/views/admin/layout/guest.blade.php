<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('admin.inc.head')
</head>
<body class="hold-transition skin-purple fixed">

<div class="wrapper">
    <div class="content-wrapper no-margin no-padding">

    @yield('header')

        <section class="content">

            @yield('content')

        </section>
    </div>
</div>


@yield('before_scripts')
@stack('before_scripts')

@include('admin.inc.scripts')

@yield('after_scripts')
@stack('after_scripts')

</body>
</html>
