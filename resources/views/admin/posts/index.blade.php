<?php
/**
 * @var $item \App\Models\Post
 */
?>
@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Posts') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Posts') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="table">
                        <thead>
                        <tr>
                            <th>{{ __('ID') }}</th>
                            <th>{{ __('Title') }}</th>
                            <th>{{ __('Author') }}</th>
                            <th>
                                {{ __('Status') }}
                                <select class="form-control select2" name="status" id="statusFilter" multiple>
                                    @foreach(\App\Models\Post::getStatusNames() as $statusId => $statusName)
                                        <option>{{ $statusName }}</option>
                                    @endforeach
                                </select>
                            </th>
                            <th class="no-sort"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="item-{{ $item->id }}">
                                <td>{{ $item->id }}</td>
                                <td>
                                    <a href="{{ adminUrl('posts/' . $item->id) }}"
                                       data-toggle="tooltip" data-placement="bottom"
                                       title="{{ __('View post') }}">{{ $item->title }}</a>
                                </td>
                                <td>
                                    <a href="{{ adminUrl('users/' . $item->user->id) }}"
                                       data-toggle="tooltip" data-placement="bottom"
                                       title="{{ __('View user') }}">
                                        <img
                                                style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
                                                src="{{ $item->user->avatar }}"
                                        > {{ $item->user->full_name }}
                                    </a>
                                </td>
                                <td>{{ \App\Models\Post::getStatusName($item->status) }}</td>
                                <td class=" text-right">
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-primary"
                                           href="/{{ $item->slug }}" target="_blank"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('View frontend post') }}"
                                        >
                                            <i class="fas fa-eye">
                                            </i>
                                        </a>
                                        <a class="btn btn-info"
                                           href="{{ adminUrl('posts/' . $item->id . '/edit') }}"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('Update post') }}"
                                        >
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                        </a>
                                        <a onclick="deleteItem(this, {{ $item->id }})" class="btn btn-danger"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('Delete post') }}"
                                           data-href="{{ adminUrl('posts/' . $item->id)}}">
                                            <i class="fas fa-trash-alt">
                                            </i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('.select2').select2({
          width: '100%',
        });

        $('#table').DataTable({
          fixedHeader: true,
          columnDefs: [
            {targets: 'no-sort', orderable: false},

            {visible: false, targets: 0},

            {width: '15%', targets: 2},
            {width: '15%', targets: 3},
            {width: '5%', targets: 4},
          ],

          order: [[0, 'desc']],
        });

        $('#statusFilter').change(function() {
          statusFilter($(this), 3);
        });
      });

      function deleteItem(element, id) {
        let href = $(element).data('href');

        if (id != null) {
          $.ajax({
            url: href,
            type: 'DELETE',
            dataType: 'JSON',
            data: {
              'id': id,
              '_token': '{{ csrf_token() }}',
            },
            success: function(response) {
              let item = $('#item-' + id);

              item.remove();
            },
            error: function(xhr) {
              alert(xhr.responseText);
            },
          });
        }
      }

      function statusFilter(selector, columnId) {
        let filterValues = selector.val();

        if ((filterValues.length === 0)) {
          $('#table').DataTable().column(columnId).search('', true, false).draw();
        } else {
          $('#table').DataTable().column(columnId).search('(' + filterValues.join('|') + ')', true, false).draw();
        }
      }
    </script>
@endsection
