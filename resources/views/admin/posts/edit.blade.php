<?php
/**
 * @var \App\Models\Post $item
 */
?>
@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Update post') }}: <a href="/{{ $item->slug }}" target="_blank"
                                                          data-toggle="tooltip" data-placement="bottom"
                                                          title="{{ __('View frontend post') }}"
        >{{ $item->title }}</a></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('posts') }}">{{ __('Posts') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Update post') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ adminUrl('posts/' . $item->id) }}" method="post">
                @method('PATCH')
                @csrf
                <input type="hidden" name="id" value="{{ $item->id }}">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#tab-main-information"
                                                    data-toggle="tab">{{ __('Main information') }}</a></li>
                            <li class="nav-item"><a class="nav-link" href="#tab-content"
                                                    data-toggle="tab">{{ __('Content') }}</a>
                            <li class="nav-item"><a class="nav-link" href="#tab-seo"
                                                    data-toggle="tab">{{ __('SEO') }}</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#tab-hashtags"
                                                    data-toggle="tab">{{ __('Hashtags') }}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-main-information">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Title') }}</label>
                                        <input class="form-control" type="text" name="title"
                                               value="@if(!empty(old('title'))){{ old('title') }}
                                               @else{{ $item->title }}@endif">
                                    </div>
                                    <div class="col-lg-6">
                                        <label>{{ __('Author') }}</label>
                                        <select class="form-control select2" name="author_id" id="author_id">
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}"
                                                        @if(!empty(old('author_id')) && $user->id == (old('author_id')))selected
                                                        @elseif($user->id == $item->author_id) selected @endif
                                                >{{ $user->full_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Description') }}</label>
                                        <textarea class="form-control"
                                                  name="description"
                                                  id="description"
                                                  rows="3"
                                                  spellcheck="false"
                                        >@if(!empty(old('description'))){{ old('description') }}
                                            @else{{ $item->description }}@endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Status') }}</label>
                                        <select class="form-control" name="status" id="status">
                                            @foreach(\App\Models\Post::getStatusNames() as $id => $name)
                                                <option
                                                        value="{{ $id }}"
                                                        @if(!empty(old('status')) && old('status') == $id) selected
                                                        @elseif($item->status == $id)selected @endif
                                                >
                                                    {{ $name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>{{ __('Type') }}</label>
                                        <select class="form-control" name="type" id="type">
                                            @foreach(\App\Models\Post::getTypeNames() as $id => $name)
                                                <option
                                                        value="{{ $id }}"
                                                        @if(!empty(old('type')) && old('type') == $id) selected
                                                        @elseif($item->type == $id)selected @endif
                                                >
                                                    {{ $name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-content">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <label>{{ __('Content') }}</label>
                                        <textarea class="form-control"
                                                  rows="3"
                                                  name="content"
                                                  id="content">@if(!empty(old('content'))){{ old('content') }}
                                            @else{!! $item->content !!}@endif</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-seo">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Meta title') }}</label>
                                        <input class="form-control" type="text" name="meta_title" id="meta_title"
                                               value="@if(!empty(old('meta_title'))){{ old('meta_title') }}
                                               @else{{ $item->meta_title }}@endif">
                                    </div>
                                    <div class="col-lg-6">
                                        <label>{{ __('Slug') }}</label>
                                        <input class="form-control" type="text" name="slug" id="slug"
                                               value="@if(!empty(old('slug'))){{ old('slug') }}
                                               @else{{ $item->slug }}@endif">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Meta description') }}</label>
                                        <textarea class="form-control"
                                                  name="meta_description"
                                                  id="meta_description"
                                                  rows="3"
                                                  spellcheck="false"
                                        >@if(!empty(old('meta_description'))){{ old('meta_description') }}
                                            @else{{ $item->meta_description }}@endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Posted at') }}</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text">
                                                <i class="far fa-clock"></i>
                                              </span>
                                            </div>
                                            <input class="form-control float-right"
                                                   name="posted_at"
                                                   id="posted_at"
                                                   value="@if(!empty(old('posted_at'))){{ old('posted_at') }}
                                                   @else{{ $item->posted_at }}@endif"
                                                   type="text">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-hashtags">
                                <div class="form-group row">
                                    <div class="col-lg-6">
                                        <label>{{ __('Hashtags') }}</label>
                                        <select class="form-control select2" name="hashtagIds[]" id="hashtags"
                                                multiple
                                        >
                                            @foreach($hashtags as $hashtag)
                                                <option value="{{ $hashtag->id }}"
                                                        @if(is_array(old('hashtagIds')))
                                                        @if(in_array($hashtag->id, old('hashtagIds')))selected @endif
                                                        @elseif(($item->hashtags)->contains($hashtag->id)) selected
                                                        @endif
                                                >{{ $hashtag->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                        </div>
                        @if (isset($errors) && count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
      $('[data-toggle="tooltip"]').tooltip();

      $('.select2').select2({
        width: '100%'
      });

      $('#posted_at').datetimepicker({
        format: 'yyyy.mm.dd hh:ii',
        autoclose: true,
      });

      $('#content').summernote();
    </script>
@endsection