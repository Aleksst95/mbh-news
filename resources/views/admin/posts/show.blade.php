<?php

use App\Models\User\SocialAccount;
use App\Models\User\Email;

/**
 * @var $item \App\Models\Admin\User
 */

?>
@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">View User: {{ $item->full_name }} <img
            style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
            src="{{ $item->avatar }}"
        ></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 mb-2">
            {{--            <a href="{{ adminUrl('users/' . $item->id . '/edit') }}">--}}
            {{--                <button type="button" class="btn btn-primary">Update</button>--}}
            {{--            </a>--}}
            @if ($item->isServiceMember())
                <a href="{{ adminUrl('service-members/' . $item->serviceMember->id) }}"
                >
                    <button type="button" class="btn btn-primary">View service member profile</button>
                </a>
            @endif
        </div>
        <div class="col-sm-6 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('users') }}">Users</a></li>
                <li class="breadcrumb-item active">View user</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card collapsed-card">
                <div class="card-header" style="cursor: pointer;" data-card-widget="collapse">
                    <div class="card-title">Main information</div>
                </div>
                <div class="card-body">
                    <strong>First name</strong>
                    <p>{{ $item->first_name }}</p>
                    <hr>
                    <strong>Last name</strong>
                    <p>{{ $item->last_name }}</p>
                    <hr>
                    @if (!empty($item->bdate))
                        <strong>Birthday</strong>
                        <p>{{ \Carbon\Carbon::parse($item->bdate)->format('d.m.Y') }}</p>
                        <hr>
                    @endif
                    <strong>Sex</strong>
                    <p>{{ $item->sex_name }}</p>
                    <hr>
                    @if (!empty($item->confirmedEmail))
                        <strong>Email</strong>
                        <p>{{ $item->confirmedEmail->email }}</p>
                        <hr>
                        <strong>Get delivery</strong>
                        <p>@if($item->confirmedEmail->get_delivery === Email::STATUS_GET_DELIVERY) Gets @else doesn't
                            get @endif</p>
                        <hr>
                    @endif
                    <strong>Date registration</strong>
                    <p>{{ $item->created_at->format('d.m.Y') }}</p>
                    <hr>
                </div>
            </div>
        </div>
    </div>

    @if (!empty($item->socials))
        <div class="row">
            <div class="col-12">
                <div class="card collapsed-card">
                    <div class="card-header" style="cursor: pointer;" data-card-widget="collapse">
                        <div class="card-title">Social medias</div>
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach($item->socials as $social)
                                <li>
                                    @switch($social->provider)
                                        @case (SocialAccount::PROVIDER_NAME_VK)
                                        <i class="fab fa-vk" aria-hidden="true">
                                        </i> <a
                                            href="https://vk.com/id{{ $social->provider_id }}"> {{ $social->provider_id }}</a>
                                        @break
                                        @case (SocialAccount::PROVIDER_NAME_FB)
                                        <i class="fab fa-facebook">
                                        </i> {{ $social->provider_id }}
                                        @break
                                        @case (SocialAccount::PROVIDER_NAME_TW)
                                        <i class="fab fa-twitter">
                                        </i> {{ $social->provider_id }}
                                        @break
                                        @case (SocialAccount::PROVIDER_NAME_GOOGLE)
                                        <i class="fab fa-google">
                                        </i> {{ $social->provider_id }}
                                        @break
                                        @case (SocialAccount::PROVIDER_NAME_YX)
                                        <i class="fab fa-hacker-news">
                                        </i> {{ $social->provider_id }}
                                        @break
                                    @endswitch
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (!empty($item->allEmails))
        <div class="row">
            <div class="col-12">
                <div class="card collapsed-card">
                    <div class="card-header" style="cursor: pointer;" data-card-widget="collapse">
                        <div class="card-title">Emails</div>
                    </div>
                    <div class="card-body">
                        <ul>
                            @foreach($item->allEmails as $email)
                                <li>{{ $email->email }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
