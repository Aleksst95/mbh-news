@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Update role') }}: {{ $item->name }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('roles') }}">{{ __('Roles') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Update role') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ adminUrl('roles/' . $item->id) }}" method="post">
                @method('PATCH')
                @csrf
                <input type="hidden" name="id" value="{{ $item->id }}">
                <div class="card">
                    <div class="card-body">
                        <div class="col-6 form-group">
                            <label>{{ __('Name') }}</label>
                            <input class="form-control" type="text" name="name" value="@if(!empty(old('name'))){{ old('name') }}@else{{ $item->name }}@endif">
                        </div>
                        <div class="col-6 form-group">
                            <label>{{ __('Permissions') }}</label>
                            <select class="form-control select2" name="permissionIds[]" multiple>
                                @foreach($permissionGroups as $permissionGroupName => $permissionGroup)
                                    <optgroup label="{{ $permissionGroupName }}">
                                        @foreach($permissionGroup as $permission)
                                            <option
                                                    value="{{ $permission->id }}"
                                                    @if(is_array(old('permissionIds')))
                                                    @if(in_array($permission->id, old('permissionIds')))selected @endif
                                                    @elseif($item->hasPermissionTo($permission->name)) selected
                                                    @endif
                                            >{{ $permission->name }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    </div>
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
      $('.select2').select2({
        width: '100%'
      });
    </script>
@endsection