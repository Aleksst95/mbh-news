@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('View role') }}: {{ $item->name }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 mb-2">
            <a href="{{ adminUrl('roles/' . $item->id . '/edit') }}">
                <button type="button" class="btn btn-primary">{{ __('Update') }}</button>
            </a>
        </div>
        <div class="col-sm-6 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('users') }}">{{ __('Roles') }}</a></li>
                <li class="breadcrumb-item active">{{ __('View role') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">{{ __('Role permissions') }}</div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Group') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->permissions as $permission)
                            <tr>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->groupName }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
      $(document).ready(function() {
        $('#table').DataTable({
          'columnDefs': [
            {
              'targets': 'no-sort',
              'orderable': false,
            },
            {
              'visible': false,
              'targets': 1,
            },
          ],
          'drawCallback': function(settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;

            api.column(1, {page: 'current'}).data().each(function(group, i) {
              if (last !== group) {
                $(rows).eq(i).before(
                    '<tr class="group"><td colspan="5">' + group + '</td></tr>',
                );

                last = group;
              }
            });
          },
        });
      });
    </script>
@endsection
@section('css')
    <style>
        tr.group,
        tr.group:hover {
            background-color: #ddd !important;
        }
    </style>
@endsection
