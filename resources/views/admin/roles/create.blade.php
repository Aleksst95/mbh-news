@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Create role') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('roles') }}">{{ __('Roles') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Create role') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ adminUrl('roles') }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="col-6 form-group">
                            <label>{{ __('Name') }}</label>
                            <input class="form-control" type="text" name="name" required value="{{ old('name') }}">
                        </div>
                        <div class="col-6 form-group">
                            <label>{{ __('Permissions') }}</label>
                            <select class="form-control select2" name="permissionIds[]" multiple required>
                                @foreach($permissionGroups as $permissionGroupName => $permissionGroup)
                                    <optgroup label="{{ $permissionGroupName }}">
                                        @foreach($permissionGroup as $permission)
                                            <option
                                                    value="{{ $permission->id }}"
                                                    @if(in_array($permission->id, old('permissionIds') ?? []))selected @endif
                                            >{{ $permission->name }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Add') }}</button>
                    </div>
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
      $('.select2').select2();
    </script>
@endsection