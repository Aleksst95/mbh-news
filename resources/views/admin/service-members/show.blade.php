@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('View service member') }}: {{ $item->user->full_name }} <img
                style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
                src="{{ $item->user->avatar }}"
        ></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 mb-2">
            <a href="{{ adminUrl('service-members/' . $item->id . '/edit') }}">
                <button type="button" class="btn btn-primary">{{ __('Update') }}</button>
            </a>
        </div>
        <div class="col-sm-6 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('users') }}">{{ __('Service members') }}</a></li>
                <li class="breadcrumb-item active">{{ __('View service member') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">{{ __('Service member roles') }}</div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="table">
                        <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($item->user->roles as $role)
                            <tr>
                                <td>{{ $role->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
      $(document).ready(function() {
        $('#table').DataTable({
          'columnDefs': [
            {
              'targets': 'no-sort',
              'orderable': false,
            },
          ],
        });
      });
    </script>
@endsection
@section('css')
    <style>
        tr.group,
        tr.group:hover {
            background-color: #ddd !important;
        }
    </style>
@endsection
