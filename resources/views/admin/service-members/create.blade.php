@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Create service member') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('users') }}">{{ __('Service members') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Create service member') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ adminUrl('service-members') }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-body">
                        <div class="col-6 form-group">
                            <label>{{ __('User') }}</label>
                            <select class="form-control select2" name="userId">
                                @foreach($users as $user)
                                    <option
                                            value="{{ $user->id }}"
                                            @if($user->id == (old('userId') ?? 0))selected @endif
                                    >{{ $user->full_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6 form-group">
                            <label>{{ __('Password') }}</label>
                            <input class="form-control" type="text" name="pass" value="{{ old('pass') }}">
                        </div>
                        <div class="col-6 form-group">
                            <label>{{ __('Role') }}</label>
                            <select class="form-control select2" name="roleIds[]" multiple>
                                @foreach($roles as $role)
                                    <option
                                            value="{{ $role->id }}"
                                            @if(in_array($role->id, old('roleIds') ?? []))selected @endif
                                    >{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Add') }}</button>
                    </div>
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
      $('.select2').select2();
    </script>
@endsection