@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Service members') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 mb-2">
            <a href="{{ adminUrl('service-members/create') }}" class="ml-2 mb-4">
                <button type="button" class="btn btn-primary">{{ __('Add new') }}</button>
            </a>
        </div>
        <div class="col-sm-6 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Service members') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">

    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="table">
                        <thead>
                        <tr>
                            <th>{{ __('Full Name') }}</th>
                            <th class="no-sort"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <a href="{{ adminUrl('users/' . $item->user_id) }}" target="_blank">
                                        <img
                                                style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
                                                src="{{ $item->user->avatar }}"
                                        > {{ $item->user->full_name }}
                                    </a>
                                </td>
                                <td class=" text-right">
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-primary"
                                           href="{{ adminUrl('service-members/' . $item->id) }}">
                                            <i class="fas fa-eye">
                                            </i>
                                        </a>
                                        <a class="btn btn-info"
                                           href="{{ adminUrl('service-members/' . $item->id) . '/edit'}}">
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                        </a>
                                        <a onclick="deleteItem(this, {{ $item->id }})" class="btn btn-danger"
                                           data-href="{{ adminUrl('service-members/' . $item->id)}}">
                                            <i class="fas fa-trash-alt">
                                            </i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
      $(document).ready(function() {
        $('#table').DataTable({
          'columnDefs': [
            {
              'targets': 'no-sort',
              'orderable': false,
            },
            { "width": "10%", "targets": 1 }
          ],
        });
      });

      function deleteItem(element, id) {
        let href = $(element).data('href');

        if (id != null) {
          $.ajax({
            url: href,
            type: 'DELETE',
            dataType: 'JSON',
            data: {
              'id': id,
              '_token': '{{ csrf_token() }}',
            },
            success: function(response) {
              let item = $('#item-' + id);

              item.remove();
            },
            error: function(xhr) {
              alert(xhr.responseText);
            },
          });
        }
      }
    </script>
@endsection
