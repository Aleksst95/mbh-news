@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Update service member') }}: {{ $item->user->full_name }} <img
                style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
                src="{{ $item->user->avatar }}"
        ></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ adminUrl('users') }}">{{ __('Service members') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Update service member') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ adminUrl('service-members/' . $item->id) }}" method="post">
                @method('PATCH')
                @csrf
                <input type="hidden" name="id" value="{{ $item->id }}">
                <div class="card">
                    <div class="card-body">
                        <div class="col-6 form-group">
                            <label>{{ __('Password') }}</label>
                            <input class="form-control" type="text" name="pass" value="{{ old('pass') }}">
                        </div>
                        <div class="col-6 form-group">
                            <label>{{ __('Role') }}</label>
                            <select class="form-control select2" name="roleIds[]" multiple>
                                @foreach($roles as $role)
                                    <option
                                            value="{{ $role->id }}"
                                            @if(is_array(old('roleIds')))
                                            @if(in_array($role->id, old('roleIds')))selected @endif
                                            @elseif(($item->user->roles->map->id)->contains($role->id)) selected
                                            @endif
                                    >{{ $role->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Update') }}</button>
                    </div>
                    @if (isset($errors) && count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script>
      $('.select2').select2({
        width: '100%',
      });
    </script>
@endsection