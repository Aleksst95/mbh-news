<?php
/**
 * @var $item \App\Models\User
 */
?>
@extends('adminlte::page')

@section('title', 'MBHN')

@section('content_header')
    <h1 class="m-0 text-dark">{{ __('Users') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12 d-flex justify-content-md-end">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ adminUrl() }}">{{ __('Home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('Users') }}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="table">
                        <thead>
                        <tr>
                            <th>{{ __('ID') }}</th>
                            <th>{{ __('Full name') }}</th>
                            <th>{{ __('Date registration') }}</th>
                            <th>{{ __('Email') }}</th>
                            <th>{{ __('Service member') }}</th>
                            <th class="no-sort"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="item-{{ $item->id }}">
                                <td>{{ $item->id }}</td>
                                <td>
                                    <a href="{{ adminUrl('users/' . $item->id) }}"
                                       data-toggle="tooltip" data-placement="bottom"
                                       title="{{ __('View user') }}">
                                        <img
                                                style="width: 30px;height: 30px;border-radius: 15px;margin-right: 0.5rem;"
                                                src="{{ $item->avatar }}"
                                        > {{ $item->full_name }}
                                    </a>
                                </td>
                                <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                <td>@if (!empty($item->confirmedEmail)) {{ $item->confirmedEmail->email }} @else
                                        - @endif</td>
                                <td>@if ($item->isServiceMember()) {{ __('Yes') }} @else {{ __('No') }} @endif</td>
                                <td class=" text-right">
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-primary"
                                           href="/user/{{ $item->id }}" target="_blank"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('View frontend user profile') }}"
                                        >
                                            <i class="fas fa-eye">
                                            </i>
                                        </a>
                                        <a class="btn btn-info"
                                           href="{{ adminUrl('users/' . $item->id) }}"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('Update user') }}"
                                        >
                                            <i class="fas fa-pencil-alt">
                                            </i>
                                        </a>
                                        <a onclick="deleteItem(this, {{ $item->id }})" class="btn btn-danger"
                                           data-toggle="tooltip" data-placement="bottom"
                                           title="{{ __('Delete user') }}"
                                           data-href="{{ adminUrl('users/' . $item->id)}}">
                                            <i class="fas fa-trash-alt">
                                            </i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();

        $('#table').DataTable({
          'columnDefs': [
            {'targets': 'no-sort', 'orderable': false},

            {'visible': false, 'targets': 0},

            {'width': '15%', 'targets': 2},
            {'width': '25%', 'targets': 3},
            {'width': '5%', 'targets': 4},
            {'width': '5%', 'targets': 5},
          ],

          'order': [[0, 'desc']],
        });
      });

      function deleteItem(element, id) {
        let href = $(element).data('href');

        if (id != null) {
          $.ajax({
            url: href,
            type: 'DELETE',
            dataType: 'JSON',
            data: {
              'id': id,
              '_token': '{{ csrf_token() }}',
            },
            success: function(response) {
              let item = $('#item-' + id);

              item.remove();
            },
            error: function(xhr) {
              alert(xhr.responseText);
            },
          });
        }
      }
    </script>
@endsection
