<?php

/**
 * If you change the configuration, you must run tests from this directory:
 *  tests/Feature/Http/Controllers/API/V1/Main/Post/Writing/Config
 */

return [
    'tools' => [
        'header' => [
            'text' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => '',
                'length' => 100,
            ],
            'level' => [
                'required' => true,
                'type' => 'int',
                'canBeOnly' => [1, 2, 3]
            ],
            'anchor' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => '',
                'length' => 50,
            ],
        ],
        'paragraph' => [
            'text' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => 'i,b,a[href],mark'
            ]
        ],
        'list' => [
            'style' => [
                'required' => true,
                'type' => 'string',
                'canBeOnly' => ['ordered', 'unordered']
            ],
            'items' => [
                'required' => true,
                'type' => 'array',
                'data' => [
                    '-' => [
                        'type' => 'string',
                        'allowedTags' => 'i,b,br,a[href],mark'
                    ]
                ]
            ]
        ],
        'quote' => [
            'text' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => 'a[href],br'
            ],
            'caption' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => 'a[href]',
                'length' => 255
            ],
            'alignment' => [
                'required' => true,
                'type' => 'string',
                'canBeOnly' => [
                    'left',
                    'center',
                ]
            ],
        ],
        'embed' => [
            'service' => [
                'required' => true,
                'type' => 'string',
                'canBeOnly' => [
                    'youtube',
                    'vimeo',
                    'coub',
                    'vine',
                    'twitter',
                    'instagram',
                    'yandex-music-track',
                    'gfycat',
                    'imgur',
                    'twitch-video',
                ]
            ],
            'source' => 'string',
            'embed' => 'string',
            'width' => [
                'type' => 'int'
            ],
            'height' => [
                'type' => 'int'
            ],
            'caption' => [
                'required' => true,
                'type' => 'string',
                'length' => 255
            ],
        ],
        'image' => [
            'file' => [
                'required' => true,
                'type' => 'array',
                'data' => [
                    'url' => [
                        'type' => 'string'
                    ]
                ]
            ],
            'caption' => [
                'required' => true,
                'type' => 'string',
                'allowedTags' => 'a[href]',
                'length' => 255
            ],
            'withBorder' => 'boolean',
            'withBackground' => 'boolean',
            'stretched' => 'boolean',
        ],
        'delimiter' => []
    ]
];
